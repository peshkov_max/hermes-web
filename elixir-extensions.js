var gulp = require('gulp');
var shell = require('gulp-shell');
var Elixir = require('laravel-elixir');
var fs = require('fs');

var Task = Elixir.Task;

Elixir.extend('generate_i18n', function() {
        
    new Task('generate_i18n', function() {
        
       return gulp.src('').pipe(shell("php artisan vue-i18n:generate"));
        
    }).watch([
        'resources/lang/**/*.php'
    ]);
});
