<?php

namespace App\Events;

use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Wipon\Models\Alcohol\CheckStatistic;
use Wipon\Models\Cluster;

class KgdStatWasGenerated extends Event
{
    use SerializesModels;

    public $statData;

    public $aggregationTime;

    public $mappingTime;

    public $savingStatTime;

    public $timeSpent;

    public $memoryPeak;

    private $lastDay;

    /**
     * Create a new event instance.
     * @param array $mataInfo
     */
    public function __construct( array  $mataInfo = [] )
    {
        $this->lastDay = [
            Carbon::now()->subDays(1),
            Carbon::now(),
        ];

        $memoryPeak = $this->formatBytes(array_get($mataInfo, "memoryPeak", 0));

        $this->statData = [
            'clusters_number'  => Cluster::whereBetween('created_at', $this->lastDay)->count(),
            'checks_total'     => CheckStatistic::whereBetween('scanned_at', $this->lastDay)->count(),
            'checks_valid'     => $this->getStatPerUkmType('valid', $this->lastDay),
            'checks_fake'      => $this->getStatPerUkmType('fake', $this->lastDay),
            'checks_atlas'     => $this->getStatPerUkmType('atlas', $this->lastDay),
            'checks_duplicate' => $this->getStatPerUkmType('duplicate', $this->lastDay),
            'time_spent'       => array_get($mataInfo, "timeSpent", 0),
            'memory_peak'      => $memoryPeak,
        ];

        $this->aggregationTime = array_get($mataInfo, "aggregationTime", 0);
        $this->mappingTime = array_get($mataInfo, "mappingTime", 0);
        $this->savingStatTime = array_get($mataInfo, "savingStatTime", 0);
        $this->timeSpent = array_get($mataInfo, "timeSpent", 0);
        $this->memoryPeak = $memoryPeak;
    }

    /**
     * Подсчет статистики для отправки в Slack
     *
     * @param $string - Тип УКМ
     * @param $lastDay - Период (прошедшие сутки)
     * @return array
     */
    private function getStatPerUkmType( $string, $lastDay )
    {
        return [
            'total' => CheckStatistic::whereBetween('scanned_at', $lastDay)->where(function ( $q ) use ( $string ) {
                $q->where("check_{$string}_hand", '=', 1)
                    ->orWhere("check_{$string}_scan", '=', 1);
            })->count(),

            'hand' => CheckStatistic::whereBetween('scanned_at', $lastDay)->where("check_{$string}_hand", '=', 1)->count(),
            'scan' => CheckStatistic::whereBetween('scanned_at', $lastDay)->where("check_{$string}_scan", '=', 1)->count(),
        ];
    }

    /**
     * Преобразует значение возвращаемое функцией memory_get_peak_usage
     * (количество байт использованной памяти) в удобочитаемый формат - 'Kb', 'Mb', 'Gb', 'Tb'
     *
     * @param int $size - Байты
     * @param int $precision - Точность после запятой
     * @return string
     */
    private function formatBytes( $size, $precision = 2 )
    {
        $base = log($size, 1024);
        $suffixes = ['', 'Kb', 'Mb', 'Gb', 'Tb'];

        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[ floor($base) ];
    }
}
