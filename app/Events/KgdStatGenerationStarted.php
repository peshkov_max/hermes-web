<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class KgdStatGenerationStarted extends Event
{
    use SerializesModels;

    /**
     * @var int
     */
    public $checkToUpdate;

    /**
     * @var
     */
    public $time;

    /**
     * Create a new event instance.
     *
     * @param $time
     */
    public function __construct($time)
    {
        $this->time = $time;
    }
}
