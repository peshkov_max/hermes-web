<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::get('password/email', 'Auth\PasswordController@getEmail');

// Email notifications from wiponapp.com
Route::post('send-feedback', 'HomeController@postSendFeedback');
Route::post('suggestion', 'HomeController@postSuggestion');

Route::group([ 'middleware' => ['auth', 'localeSessionRedirect', 'localizationRedirect']], function () {
    
    $groups = [
        'global',
        'products',
        'checks',
        'receipts',
        'supports_devices',
        'roles_organizations',
        'geo_dictionaries',
    ];

    foreach ($groups as $group) {
        try{
            require("Routes/{$group}.php");
        }catch (\Exception $e){
            log_i(trans('app.route_group_not_found'), [$e->getMessage()]);
            dd(trans('app.route_group_not_found'));
        }
    }
});

