<?php


Route::group([ 'middleware' => ['auth']], function () {
    Route::get('users-pro/{users_pro}/api-requests', 'UserControllerPro@apiRequests');
    Route::get('regions-pro/search', 'RegionControllerPro@search');
    Route::get('store-types-pro/search', 'StoreTypeControllerPro@search');
    
    Route::group(['middleware' => ['role:support|support-pro|super-admin']], function () {
        Route::get('feedback-messages-pro', ['uses' => 'FeedbackMessagesControllerPro@index', 'as' => 'feedback-messages-pro.index']);
        Route::get('feedback-messages-pro/{feedback_messages_pro}/read/{setUnread?}', ['uses' => 'FeedbackMessagesControllerPro@read', 'as' => 'feedback-messages-pro.read']);
        Route::get('feedback-messages-pro/{feedback_messages_pro}/show/{users_pro}', ['uses' => 'FeedbackMessagesControllerPro@show', 'as' => 'feedback-messages-pro.show']);
        
        Route::resource('users-pro', 'UserControllerPro', ['only' => ['index', 'show', 'edit', 'update'] ]);
        Route::resource('stores-pro', 'StoreControllerPro', ['only' => ['edit', 'update'] ]);
        
        Route::get('notifications-pro', ['uses' => 'NotificationControllerPro@index', 'as' => 'notifications-pro.index']);
    });

    Route::group(['middleware' => ['role:support-pro|super-admin']], function () {
        Route::get('subscriptions-pro/{subscriptions_pro}/deactivate', 'SubscriptionControllerPro@deactivate');
        Route::resource('subscriptions-pro', 'SubscriptionControllerPro',['only'=>['index', 'update', 'store']]);
    });    
});