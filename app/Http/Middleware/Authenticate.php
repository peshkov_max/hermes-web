<?php

namespace App\Http\Middleware;

use Auth;
use Cache;
use Closure;
use Config;
use Wipon\Models\Organization;
use Wipon\Models\User;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle( $request, Closure $next, $guard = null )
    {

        // dd($request->user()->roles);
        if (Auth::guard($guard)->guest()) {

            // Ключ ENV_IS_TESTING должен существовать только на тестовой машине
            $isTesting = env('ENV_IS_TESTING', null);
            
            // Автоматическая авторизация для тестового IP из .env-файла
            $whiteIp = env('APP_DEV_IP', null);

            if (($whiteIp && $whiteIp == $request->getClientIp()) || $isTesting) {
                $user = User::whereName(env('APP_DEV_USERNAME', 'wipon-dev'))->first();

                if ($user) {
                    Auth::login($user);
                }
            } else {
                if ($request->ajax() || $request->wantsJson()) {
                    return response('Unauthorized.', 401);
                } else {
                    return redirect()->guest('login');
                }
            }
        }

        $this->setCurrentTimeZone($request);

        return $next($request);
    }


    /**
     * @param $request
     */
    private function setCurrentTimeZone( $request )
    {
        /**
         * @var User $user
         */
        $user = $request->user();

        /**
         * @var Organization $organization
         */
        if ($user) {
            Config::set('wipon.user_time_zone',
                Cache::remember('user_' . $user->id . '_timezone', config('wipon.minute_to_store_in_cache', 30),
                    function () use ( $user ) {
                        return ($user->organization && $user->organization->city && $user->organization->city->timezone)
                            ? $user->organization->city->timezone
                            : null;
                    }));
        }
    }
}
