<?php

Route::group(['middleware' => ['role:super-admin|product-moderator|commercial-admin']], function () {
    Route::get('products/set-status', ['uses' => 'ProductController@setStatus', 'as' => 'products.set_status']);
    Route::get('products/merge', ['uses' => 'ProductController@merge', 'as' => 'products.merge']);

    Route::patch('products/{id}/save-image', ['uses' => 'ProductController@saveImage', 'as' => 'products.save_image']);
    Route::patch('visual-features/{id}/set-order/{new_order_number}', 'VisualFeatureController@setOrder')->where('new_order_number', '[0-9]+');

    Route::resource('products', 'ProductController');
    Route::resource('visual-features', 'VisualFeatureController', ['except' => ['index', 'create', 'show']]);

    // products written on vueJS
    Route::get('vue-products/types', ['uses' => 'ProductController@types', 'as' => 'products.types']);
    Route::resource('vue-products', 'ProductVueController');

    Route::get('product-types/search', ['uses' => 'ProductTypeController@search', 'as' => 'product_types.search']);
    Route::resource('product-types', 'ProductTypeController', ['except' => ['destroy']]);
});
