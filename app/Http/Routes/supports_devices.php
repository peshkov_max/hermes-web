<?php

Route::group(['middleware' => ['can:support_user']], function () {
    Route::get('supports/{uuid}/read/{setUnread?}', ['uses' => 'SupportController@read', 'as' => 'supports.read']);
    Route::get('supports/{id}/{device_uuid?}', ['uses' => 'SupportController@show', 'as' => 'supports.show']);
    Route::get('supports/{uuid}/show-msg', ['uses' => 'SupportController@index', 'as' => 'supports.search_uuid']);
    Route::resource('supports', 'SupportController', ['only' => ['index']]);
});

Route::group(['middleware' => ['role:super-admin|support|support-pro']], function () {

    Route::get('push-notifications', ['uses' => 'PushNotificationController@index', 'as' => 'push_notifications.index']);

    Route::get('push-notifications/create', ['uses' => 'PushNotificationController@create', 'as' => 'push_notifications.create']);
    Route::get('push-notifications/send-single', ['uses' => 'PushNotificationController@sendSingle', 'as' => 'push_notifications.send_single']);
    Route::get('push-notifications/send-mass', ['uses' => 'PushNotificationController@sendMass', 'as' => 'push_notifications.send_mass']);

    Route::get('push-notifications/device-number', ['uses' => 'PushNotificationController@getDeviceNumber', 'as' => 'push_notifications.device_number']);
});

// Customers
Route::group(['middleware' => ['role:super-admin|support']], function () {

    Route::get('devices', ['uses' => 'DeviceController@index', 'as' => 'devices.index']);
    Route::get('devices/{uuid}/log', ['uses' => 'DeviceController@seeLogs', 'as' => 'devices.see_logs']);
    Route::get('devices/{uuid}/api-requests', ['uses' => 'DeviceController@getDeviceApiRequests', 'as' => 'devices.api_requests']);
    Route::get('devices/{uuid}/checks', ['uses' => 'DeviceController@getDeviceChecks', 'as' => 'devices.checks_api_requests']);
    Route::get('devices/versions', ['uses' => 'DeviceController@versions', 'as' => 'devices.versions']);

    Route::resource('customers', 'CustomerController', ['only' => ['index', 'edit', 'update']]);
});