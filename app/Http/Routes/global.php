<?php

Route::get('/', 'HomeController@index');
Route::get('/vue', 'HomeController@vue');


Route::group(['middleware' => ['role:organization-moderator|product-moderator|super-admin|organization-moderator|content-manager']], function () {
    Route::get('aliases/{aliasable_type}/{aliasable_id}', ['uses' => 'AliasController@index', 'as' => 'aliases.index']);
    Route::post('aliases', ['uses' => 'AliasController@store', 'as' => 'aliases.store']);
    Route::patch('aliases/{id}', ['uses' => 'AliasController@update', 'as' => 'aliases.update']);
    Route::delete('aliases/{id}', ['uses' => 'AliasController@destroy', 'as' => 'aliases.destroy']);
});

Route::get('users/{users}/get-password', ['uses' => 'UserController@getPassword', 'as' => 'users.get_password']);
Route::patch('users/{users}/set-password', ['uses' => 'UserController@setPassword', 'as' => 'users.set_password']);

Route::group(['middleware' => ['role:super-admin']], function () {
    Route::get('/dashboard', ['uses' => 'HomeController@dashboard', 'as' => 'home.dashboard']);
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

Route::resource('users', 'UserController', ['except' => 'show']);
