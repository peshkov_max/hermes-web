<?php

Route::get('roles/search', ['uses' => 'RoleController@search', 'as' => 'roles.search']);
Route::get('organizations/search', ['uses' => 'OrganizationController@search', 'as' => 'organizations.search']);

Route::group(['middleware' => ['role:super-admin']], function () {

    Route::get('roles/{id}/attach-user', ['uses' => 'RoleController@attachUser', 'as' => 'roles.attach_user']);
    Route::get('roles/{id}/detach-user', ['uses' => 'RoleController@detachUser', 'as' => 'roles.detach_user']);

    Route::resource('roles', 'RoleController');
});

Route::group(['middleware' => ['role:super-admin|organization-moderator']], function () {

    Route::get('organizations/{id}/attach-user', ['uses' => 'OrganizationController@attachUser', 'as' => 'organizations.attach_user']);
    Route::get('organizations/{id}/detach-user', ['uses' => 'OrganizationController@detachUser', 'as' => 'organizations.detach_user']);
    Route::get('organizations/moderate', ['uses' => 'OrganizationController@moderate', 'as' => 'organizations.moderate'])->where('status', '(moderated|not-moderated)');
    Route::get('organizations/{organizations}/manage-users', ['uses' => 'OrganizationController@manageUsers', 'as' => 'organizations.manage_users']);
    Route::get('organizations/{organizations}/merge', ['uses' => 'OrganizationController@getMerge', 'as' => 'organizations.get_merge']);
    Route::patch('organizations/{organizations}/merge', ['uses' => 'OrganizationController@patchMerge', 'as' => 'organizations.patch_merge']);
    Route::get('organizations/merging-filter', ['uses' => 'OrganizationController@mergingFilter', 'as' => 'organizations.merging_filter']);
    
    // Редактирование городов присутствия 
    Route::get('organizations/{organizations}/cities', 'OrganizationCityController@edit');
    Route::post('organizations/{organizations}/cities/{cities}/attach', 'OrganizationCityController@attach');
    Route::post('organizations/{organizations}/cities/{cities}/detach', 'OrganizationCityController@detach');

    Route::resource('organizations', 'OrganizationController', ['except' => ['show', 'destroy']]);
});

