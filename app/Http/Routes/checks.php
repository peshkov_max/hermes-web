<?php

Route::group(['middleware' => ['can:view_kgd_stat']], function () {

    Route::get('checks/map', ['uses' => 'CheckController@map', 'as' => 'checks.map']);
    Route::get('checks/map-filter', ['uses' => 'CheckController@mapFilter', 'as' => 'checks.map_filter']);
    Route::get('checks/map-export-to-excel', ['uses' => 'CheckController@mapExportToExcel', 'as' => 'checks.stat_export']);
    Route::get('checks/map-competition-export', ['uses' => 'CheckController@mapCompetitionExport', 'as' => 'checks.map_competition_export']);

    Route::get('checks/stat-refresh', ['uses' => 'CheckController@statUpdate', 'as' => 'checks.stat_refresh']);
    Route::get('checks/detail', ['uses' => 'CheckController@detail', 'as' => 'checks.detail']);
    Route::get('checks/detail-export-to-excel', ['uses' => 'CheckController@detailExport', 'as' => 'checks.detail_export']);
    
    Route::group(['middleware' => ['role:super-admin|inspector-admin']], function () {
        Route::get('checks/stats', ['uses' => 'CheckController@stats', 'as' => 'checks.stats']);
        Route::get('checks/stat-export-to-excel', ['uses' => 'CheckController@statExportToExcel', 'as' => 'checks.stat_export']);
    });
});