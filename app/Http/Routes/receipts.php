<?php

Route::group(['middleware' => ['role:receipt-moderator-admin|super-admin|receipt-moderator|inspector|inspector-admin|npp']], function () {

    Route::group(['middleware' => ['role:receipt-moderator-admin|super-admin|receipt-moderator']], function () {
        Route::get('receipts/moderate', 'ReceiptController@moderate');
    });

    Route::group(['middleware' => ['role:receipt-moderator-admin|super-admin|inspector-admin']], function () {
        Route::get('receipts/participants', 'ReceiptController@participants');
    });

    Route::get('receipts/registry', 'ReceiptController@registry');

    Route::group(['middleware' => ['role:super-admin|inspector-admin|inspector']], function () {
        Route::get('receipts/play-prize', 'ReceiptController@playPrize');
        Route::get('receipts/notify-winner', 'ReceiptController@notifyWinner');
    });

    Route::group(['middleware' => ['role:super-admin|inspector-admin|inspector|npp']], function () {
        Route::get('receipts/winners', 'ReceiptController@winners');
    });

    Route::group(['middleware' => ['role:super-admin|inspector-admin|npp']], function () {
        Route::get('receipts/statistics', 'ReceiptController@statistics');
    });

    Route::group(['middleware' => ['role:super-admin|inspector-admin|inspector|npp']], function () {
        Route::get('receipts/messages', 'ReceiptController@messages');
    });

    Route::get('receipts/export-receipts', 'ReceiptController@exportReceipts');
    Route::get('receipts/export-statistics', 'ReceiptController@exportStatistics');
    Route::get('receipts/export-messages', 'ReceiptController@exportMessages');

    Route::group(['middleware' => ['role:super-admin']], function () {
        Route::get('receipts/educate-receipt', 'ReceiptController@educateReceipt');
    });
});