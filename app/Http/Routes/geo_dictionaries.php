<?php

Route::get('cities/search', ['uses' => 'CityController@search', 'as' => 'cities.search']);
Route::resource('cities', 'CityController', ['only' => ['index']]);

Route::group(['middleware' => ['role:super-admin|content-manager']], function () {

    Route::get('cities/timezones', ['uses' => 'CityController@timezones', 'as' => 'cities.timezones']);
    Route::get('cities/{id}/merge', ['uses' => 'CityController@getMerge', 'as' => 'cities.getMerge']);
    Route::post('cities/{id}/merge', ['uses' => 'CityController@postMerge', 'as' => 'cities.postMerge']);
    Route::resource('cities', 'CityController', ['except' => ['show']]);

    Route::get('regions/search', ['uses' => 'RegionController@search', 'as' => 'regions.search']);
    Route::get('regions/{id}/cities', ['uses' => 'RegionController@cities', 'as' => 'regions.cities']);

    Route::get('regions/{id}/regional-center',
        ['uses' => 'RegionController@regional_center', 'as' => 'regions.regional_center']
    );

    Route::get('countries/search', ['uses' => 'CountryController@search', 'as' => 'countries.search']);
    Route::resource('regions', 'RegionController');

    Route::get('clusters/filter', ['uses' => 'ClusterController@filter', 'as' => 'clusters.filter']);
    Route::get('clusters/merge', ['uses' => 'ClusterController@getMerge', 'as' => 'clusters.getMerge']);
    Route::post('clusters/merge', ['uses' => 'ClusterController@postMerge', 'as' => 'clusters.postMerge']);
    Route::get('clusters/link', ['uses' => 'ClusterController@getLink', 'as' => 'clusters.getLink']);
    Route::post('clusters/link', ['uses' => 'ClusterController@postLink', 'as' => 'clusters.postLink']);
    Route::resource('clusters', 'ClusterController');
});

