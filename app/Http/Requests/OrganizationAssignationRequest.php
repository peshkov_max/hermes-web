<?php

namespace App\Http\Requests;

/**
 * @property mixed id
 * @property mixed user_id
 */
class OrganizationAssignationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'      => 'required|exists:organizations,id',
            'user_id' => 'required|exists:users,id',
        ];
    }
}
