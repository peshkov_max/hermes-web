<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Gate;

/**
 * @property mixed role
 * @property mixed description
 * @property mixed display_name
 */
class RoleFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('manage_user');
    }

    public function messages()
    {

        return [
            'name.regex'         => trans('validation.regex_latin'),
            'name.unique'         => trans('validation.role_must_be_unique'),
            'display_name.regex' => trans('validation.regex_latin'),
        ];
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'         => 
                $this->method() === 'PUT' 
                    ? 'required|regex:/^[\w_-]+$/|max:255|unique:roles,name,' . $this->id 
                    : 'required|regex:/^[\w_-]+$/|max:255|unique:roles,name',
            
            'display_name' => 'required|regex:/^[^0-9]+$/|max:255',
            'description'  => 'required|max:255',
        ];
    }
}
