<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Gate;

class OrganizationModerationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('moderate_organization');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'     => 'required|integer|exists:organizations,id',
            'status' => 'required',
        ];
    }
}
