<?php

namespace App\Http\Requests;

/**
 * @property mixed username
 * @property mixed email
 * @property mixed password
 * @property mixed organization_id
 * @property mixed roles
 * @property mixed continue_adding
 */
class UserFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'name.required' => trans('validation.required', ['attribute' => trans('users.user_name')]),
            'name.unique'   => trans('validation.unique', ['attribute' => trans('users.user_name')]),
            'name.regex'    => trans('validation.regex_latin_user', ['attribute' => trans('users.user_name')]),
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
                $passwordRule = $this->has('change_password') && $this->get('change_password') == 'true' ? 'required|min:6|max:16' : '';
                $nameRule = 'required|min:4|max:255|regex:/^[A-Za-z.-]+$/|unique:users,name,' . $this->id;
                $emailRule = 'required|email|unique:users,email,' . $this->id;
                break;
            case 'POST':
                $passwordRule = 'required|min:6|max:16';
                $nameRule = 'required|min:4|max:255|regex:/^[A-Za-z.-]+$/|unique:users,name';
                $emailRule = 'required|email|unique:users,email';
                break;
            default:
                $passwordRule = 'required|min:6|max:16';
                $nameRule = 'required|min:4|max:255|regex:/^[A-Za-z.-]+$/|unique:users,name';
                $emailRule = 'required|email|unique:users,email';
                break;
        }

        $isSuperAdmin = $this->user()->hasRole(['super-admin']);

        return [
            'name'         => $nameRule,
            'email'        => $emailRule,
            'id'           => 'integer',
            'roles'        => $isSuperAdmin ? 'required|array' : 'array',
            'organization' => $isSuperAdmin ? 'required|exists:organizations,id' : 'exists:organizations,id',
            'password'     => $passwordRule,
        ];
    }
}
