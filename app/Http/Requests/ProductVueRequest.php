<?php

namespace App\Http\Requests;

use Image;
use Wipon\Models\Product;
use Wipon\Services\ImageService;

/**
 * @property mixed change_image
 * @property mixed description_ru
 * @property mixed product_type_id
 * @property mixed organization_id
 * @property mixed description_kk
 * @property mixed description_en
 * @property mixed remove_image
 */
class ProductVueRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return auth()->user()->hasRole(['product-moderator', 'super-admin', 'commercial-admin']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'barcode'         => 'required|numeric|digits_between:13,16',
            'product_type_id' => 'required|integer',
        ];

        if ($this->isMethod('POST') && $this->user()->hasRole('super-admin')) {
            $rules['name_ru'] = 'required|max:250';
            $rules['name_en'] = 'required|max:250';
            $rules['legal_name'] = 'required|max:250';
        }

        return $rules;
    }

    public function getProductType()
    {
        return $this->product_type_id === 'null' ? null : $this->product_type_id;
    }

    public function getOrganizationId()
    {
        return $this->organization_id == 'null'
            ? null
            : (auth()->user()->hasRole(['super-admin', 'product-moderator'])
                ? $this->organization_id
                : auth()->user()->organization_id);
    }

    public function getCommonInfo()
    {
        $common_info = [];

        if ($this->description_ru)
            $common_info ['description_ru'] = $this->description_ru;

        if ($this->description_kk)
            $common_info ['description_kk'] = $this->description_kk;

        if ($this->description_en)
            $common_info ['description_en'] = $this->description_en;

        return $common_info ?? null;
    }

    /** Сохранение всех картинок продукта
     *
     * @param Product $product
     * @param string $action
     * @return mixed
     */
    public function handleImages( string $action = 'store', Product $product )
    {
        try {
            if ($this->remove_image && $this->remove_image === 'true') {
                return $this->removeImage($product);
            }

            $method = "{$action}Image";

            $product->image_id = $this->{$method}($product, 'image', 'main');
            $product->background_image_id = $this->{$method}($product, 'background_image', 'background');

            $product->save();
        } catch (\Exception $e) {
            log_e('Error while saving product images ' . ' action: ' . $action . PHP_EOL, [$e->getMessage()]);
        }

        return $product;
    }

    /** Сохранение изображении при добавлении продукта
     * @param Product $product
     * @param string $imageType
     * @param string $cropPrefix
     * @return bool|null
     * @throws \Exception
     */
    private function storeImage( Product $product, string $imageType, string $cropPrefix )
    {
        if ( ! $this->hasFile($imageType))
            return null;

        $imageFromRequest =
            Image::make($this->file($imageType))->crop(
                $this->{$cropPrefix . '_width'},
                $this->{$cropPrefix . '_height'},
                $this->{$cropPrefix . '_x'},
                $this->{$cropPrefix . '_y'});

        if ($product->{$imageType}) {

            ImageService::updateWithUploadedImage($imageFromRequest, $product->{$imageType});

            return $product->{$imageType}->id;
        }

        return ImageService::save($imageFromRequest);
    }

    /** Сохранение изображения при обновлении продукта
     *
     * @param $product
     * @param $imageType
     * @param $cropPrefix
     * @return bool
     * @throws \Exception
     */
    public function updateImage( $product, $imageType, $cropPrefix )
    {
        $existedImage = $product->{$imageType};

        // store new image
        if ( ! $existedImage && $this->hasFile($imageType)) {

            return ImageService::save(
                Image::make($this->file($imageType))->crop(
                    $this->{$cropPrefix . '_width'},
                    $this->{$cropPrefix . '_height'},
                    $this->{$cropPrefix . '_x'},
                    $this->{$cropPrefix . '_y'})
            );
        }

        // change already stored image
        if ($this->change_image === 'true' && $this->hasFile($imageType)) {

            ImageService::updateWithUploadedImage(
                Image::make($this->file($imageType))
                    ->crop(
                        $this->{$cropPrefix . '_width'},
                        $this->{$cropPrefix . '_height'},
                        $this->{$cropPrefix . '_x'},
                        $this->{$cropPrefix . '_y'}),
                $existedImage
            );
        }

        return $existedImage->id;
    }

    private function removeImage( $product )
    {
        try {

            if ($product->image) {
                $product->image->delete();
            }

            if ($product->background_image) {
                $product->background_image->delete();
            }

            $product->image_id = null;
            $product->background_image_id = null;

            $product->save();
        } catch (\Exception $e) {
            log_e('Error while removing product image', [$e->getMessage()]);
            $this->session()->push('errors', trans('app.removing_product_error'));
        }

        return $product;
    }
}
