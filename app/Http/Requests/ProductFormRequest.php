<?php

namespace App\Http\Requests;

use Auth;
use Image;
use Wipon\Models\Product;
use Wipon\Repos\ImageRepository;
use Wipon\Repos\ProductRepository;
use Wipon\Services\ImageService;

/**
 * @property mixed product_type_id
 * @property mixed barcode
 * @property mixed name_en
 * @property mixed name_ru
 * @property mixed legal_name
 * @property mixed product_id
 * @property mixed description_ru
 * @property mixed description_en
 * @property mixed name_kz
 * @property mixed add_next_product
 * @property mixed organization_id
 * @property mixed is_moderated
 * @property mixed is_enabled
 * @property mixed data_height
 * @property mixed data_x
 * @property mixed data_y
 * @property mixed data_width
 * @property mixed change_image
 * @property mixed redirectUrl
 * @property mixed change_background_image
 */
class ProductFormRequest extends Request
{

    /**
     * @var ProductRepository
     */
    private $products;

    /**
     * @var ImageRepository
     */
    private $images;


    /**
     * ProductFormRequest constructor.
     * @param ProductRepository $product
     * @param ImageRepository $image
     */
    public function __construct( ProductRepository $product, ImageRepository $image )
    {
        parent::__construct();
        $this->products = $product;
        $this->images = $image;
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name_ru.required' => trans('validation.required', ['attribute' => trans('products.product_ru')]),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return auth()->user()->can('manage_product', 'moderate_product');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        // in case we change ability to see products in requests
        if ($this->isRequestForSettingProductAbility())
            return ['is_enabled' => 'required',];

        // in case we change moderation status of product
        if ($this->isRequestForSettingProductModeration())
            return ['is_moderated' => 'required',];

        // saving only image rules
        if ($this->has('product_id') && $this->hasFile('image'))
            return [
                'product_id' => 'required',
                'image'      => 'required|image|mimes:jpeg,bmp,png',
            ];

        // saving product info rules
        return [
            'name_ru'         => 'required|max:255',
            'name_en'         => 'required|max:255',
            'legal_name'      => 'required|max:255',
            'product_type_id' => 'required|integer',
        ];
    }

    /**
     * Persist product data
     */
    public function persist() :Product
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $product = $this->products->create([
            'product_type_id' => (int) $this->product_type_id,
            'barcode'         => $this->barcode,
            'name_en'         => $this->name_en ?? null,
            'name_ru'         => $this->name_ru,
            'legal_name'      => $this->legal_name,
            'is_enabled'      => $this->isProductEnabled(),
            'is_moderated'    => $this->isProductModerated(),
            'organization_id' => $this->organization_id ?? Auth::user()->organization_id,
            'common_info'     => [],
        ]);

        $product->common_info = $this->getUpdatedCommonInfo([]);
        $product->save();

        return $product;
    }

    /** Update product
     *
     * @param $product
     * @return bool|\Illuminate\Database\Eloquent\Model|Product
     */
    public function update( $product )
    {
        $productId = $this->product_id ?? $this->id ?? null;

        if ($this->isRequestForSettingProductAbility()) {
            return $this->products->setStatus($productId, $this->only(['is_enabled',]), 'is_enabled');
        } elseif ($this->isRequestForSettingProductModeration()) {
            return $this->products->setStatus($productId, $this->only(['is_moderated',]), 'is_moderated');
        }

        return $this->products->update($productId, $this->configureInputData($product));
    }


    /** Configure input data
     *
     * @param Product $product
     * @return array
     */
    private function configureInputData( Product $product ) : array
    {
        $data = $this->only([
            'product_type_id',
            'barcode',
            'name_en',
            'name_ru',
            'legal_name',
            'is_enabled',
            'is_moderated',
            'description_en',
            'description_ru',
            'organization_id',
        ]);

        if ( ! $this->has('wizard') && $this->change_image == 'true') {
            $data['image_id'] = $this->handleImageUpdate($product);
            $data['background_image_id'] = $this->handleBackgroundImageUpdate($product);
        }

        $data['common_info'] = $this->getUpdatedCommonInfo($product->common_info);
        $data['product_type_id'] = (int) $data['product_type_id'];

        return count($data) > 0 ? $data : null;
    }

    /** Update common info from with data
     * @param array $common_info
     * @return array
     */
    private function getUpdatedCommonInfo( $common_info=[] )
    {
        if ( $this->description_ru !== '') $common_info['description_ru'] = $this->description_ru;
        else unset($common_info['description_ru']);

        if ($this->description_en !== '') $common_info['description_en'] = $this->description_en;
        else unset($common_info['description_en']);

        return $common_info;
    }


    /** Requirements:
     * @link http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5931441
     * @return bool|mixed
     */
    private function isProductModerated()
    {
        if ($this->is_moderated === 'true')
            return true;

        return false;
    }

    /**
     * @return bool|mixed
     */
    private function isProductEnabled()
    {
        if ($this->is_enabled === 'false')
            return false;

        return true;
    }

    /**
     * @return bool
     */
    private function isRequestForSettingProductAbility()
    {
        return count($this->all()) <= 5 && $this->has('is_enabled');
    }

    /**
     * @return bool
     */
    private function isRequestForSettingProductModeration()
    {
        return count($this->all()) <= 5 && $this->has('is_moderated');
    }


    public function storeWizardImages()
    {
        /*** @var Product $product */
        $product = $this->products->findById($this->product_id);
        $product->image_id = $this->wizardUpdateImage($product);
        $product->background_image_id = $this->wizardUpdateBackgroundImage($product);
        $product->save();

        return $product;
    }

    private function wizardUpdateImage( $product )
    {
        $imageFromRequest = Image::make($this->file('image'))
            ->crop($this->data_width, $this->data_height, $this->data_x, $this->data_y);

        if ($product->image) {
            ImageService::updateWithUploadedImage($imageFromRequest, $product->image);

            return $product->image->id;
        }

        return ImageService::save($imageFromRequest);
    }


    private function wizardUpdateBackgroundImage( $product )
    {
        $imageFromRequest = $this->fitBackgroundImage(Image::make($this->file('image_background')));
        $backgroundImageSize = ['width' => config("wipon.image.product_background_image.width"), 'height' => config("wipon.image.product_background_image.height")];

        if ($product->background_image) {
            ImageService::updateWithUploadedImage($imageFromRequest, $product->background_image, $backgroundImageSize);

            return $product->background_image->id;
        }

        return ImageService::save($imageFromRequest, $backgroundImageSize);
    }


    public function fitBackgroundImage( \Intervention\Image\Image $image )
    {
        $image->widen(config("wipon.image.product_background_image.width"), function ( $constraint ) {
            $constraint->upsize();
        })
            ->heighten(config("wipon.image.product_background_image.height"), function ( $constraint ) {
                $constraint->upsize();
            });

        return Image::canvas(
            config("wipon.image.product_background_image.width"),
            config("wipon.image.product_background_image.height"),
            'ffffff')
            ->insert($image, 'center');
    }


    public function handleImageUpdate( $product )
    {
        $existedImage = $product->image;

        if ( ! $existedImage && $this->hasFile('image')) {
            return ImageService::save(
                Image::make($this->file('image'))->crop($this->data_width, $this->data_height, $this->data_x, $this->data_y));
        }

        // change already stored image
        if ($this->hasFile('image') && $this->change_image === 'true') {
            ImageService::updateWithUploadedImage(
                Image::make($this->file('image'))->crop($this->data_width, $this->data_height, $this->data_x, $this->data_y), 
                $existedImage
            );
        }

        // update existed image from system file
        if ( ! $this->hasFile('image') && $this->change_image === 'true') {
            ImageService::updateWithImageFromSystem($existedImage, [$this->data_width, $this->data_height, $this->data_x, $this->data_y]);
        }

        return $existedImage->id;
    }


    public function handleBackgroundImageUpdate( $product )
    {
        $image = $product->background_image;

        if ( ! $image && $this->hasFile('image_background')) {
            return ImageService::save($this->fitBackgroundImage(Image::make($this->file('image_background'))),
                ['width' => config("wipon.image.product_background_image.width"), 'height' => config("wipon.image.product_background_image.height")]);
        }

        // change already stored image
        if ($this->hasFile('image_background') && $this->change_background_image === 'true') {
            ImageService::updateWithUploadedImage($this->fitBackgroundImage(Image::make($this->file('image_background'))), $image);
        }

        // update existed image from system file
        if ( ! $this->hasFile('image_background') && $this->change_background_image === 'true') {
            ImageService::updateWithImageFromSystem($image);
        }

        return $image->id;
    }


}
