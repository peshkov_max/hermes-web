<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Wipon\Repos\CheckRepository;
use Excel;
use Response;

class CheckStatRequest extends Request
{
    /**
     * @var CheckRepository
     */
    private $checks;

    public function __construct( CheckRepository $checks )
    {
        parent::__construct();
        $this->checks = $checks;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'period'            => 'array',
            'period.start'      => 'date_format:' . config('wipon.pss_date_format'),
            'period.end'        => 'date_format:' . config('wipon.pss_date_format'),
            'checkType'         => 'in:scan,hand,both,nothing',
            'regionId'          => 'exists:regions,id',
            'ukmType'           => 'array',
            'ukmType.all'       => 'in:true,false',
            'ukmType.valid'     => 'in:true,false',
            'ukmType.false'     => 'in:true,false',
            'ukmType.duplicate' => 'in:true,false',
            'ukmType.atlas'     => 'in:true,false',
        ];
    }

    public function filter()
    {
       return $this->checks->stats([
            'year'      => $this->year,
            'checkType' => $this->checkType,
            'ukmType'   => $this->ukmType,
            'sort'      => $this->sort,
        ]);
    }


    /** 
     * Export filtered data to Excel
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function generateStatReport(  )
    {
        $statData =  $this->filter();
            
        $fileName = 'stat_report_' . carbon_user_tz_now()->toDateTimeString();

        $filePath = storage_path('app/public/excel');

        Excel::create($fileName, function ( $excel ) use ( $statData ) {

            /*** @var  $excel LaravelExcelWriter */
            $excel->setTitle(trans('app.cluster_stat'));
            $excel->setCompany('Wipon');

            $excel->sheet('Statistics', function ( $sheet ) use ( $statData ) {

                /*** @var $sheet LaravelExcelWorksheet */
                $sheet->setOrientation('landscape');
                $sheet->loadView('reports.checks.stat.excel', ['checks' => $statData]);
            });
            
        })->store('xls', $filePath);

        $fileName .= '.xls';

        return Response::download($filePath . '/' . $fileName, $fileName, [
            'Content-Length: ' . filesize($filePath),
        ]);
    }
}
