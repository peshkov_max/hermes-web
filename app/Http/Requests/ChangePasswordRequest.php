<?php namespace App\Http\Requests;

/**
 * @property mixed password_old
 * @property mixed password
 */
class ChangePasswordRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_old'          => "required|password_old:{$this->users->id}|min:6|max:16",
            'password'              => 'required|confirmed|min:6|max:16|different:password_old',
            'password_confirmation' => 'required|min:6|max:16',
        ];
    }


    public function passwordChanged( $user )
    {
        $user->password = bcrypt($this->password . $user->created_at . config('wipon.pass_prefix'));
        return  $user->save();
    }
}
