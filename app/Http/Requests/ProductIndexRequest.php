<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Session;
use Wipon\Repos\ProductRepository;

class ProductIndexRequest extends Request
{
    /**
     * @var ProductRepository
     */
    private $products;

    public function __construct( ProductRepository $products)
    {
        parent::__construct();
        $this->products = $products;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return auth()->user()->hasRole(['product-moderator','super-admin', 'commercial-admin']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'params' => 'json'
        ];
    }

   
  
}
