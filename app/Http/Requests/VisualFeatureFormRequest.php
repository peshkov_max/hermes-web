<?php

namespace App\Http\Requests;

use Image;

use Wipon\Models\VisualFeature;
use Wipon\Repos\ImageRepository;
use Wipon\Repos\ProductRepository;
use Wipon\Repos\VisualFeatureRepository;
use Wipon\Services\ImageService;

/**
 * @property mixed feature_original_data_x
 * @property mixed feature_original_data_y
 * @property mixed feature_original_data_height
 * @property mixed feature_original_data_width
 * @property mixed feature_product_id
 * @property mixed description_ru
 * @property mixed description_en
 * @property mixed feature_fake_data_x
 * @property mixed feature_fake_data_y
 * @property mixed feature_fake_data_width
 * @property mixed feature_fake_data_height
 * @property mixed id
 * @property mixed feature_id
 * @property mixed order_number
 * @property mixed change_image_fake
 * @property mixed change_image_original
 */
class VisualFeatureFormRequest extends Request
{

    /**
     * @var ProductRepository
     */
    private $products;

    /**
     * @var VisualFeatureRepository
     */
    private $features;

    /**
     * @var ImageRepository
     */
    private $images;


    /**
     * ProductFormRequest constructor.
     * @param ProductRepository $product
     * @param VisualFeatureRepository $features
     * @param ImageRepository $images
     */
    public function __construct( ProductRepository $product, VisualFeatureRepository $features, ImageRepository $images )
    {
        parent::__construct();

        $this->products = $product;
        $this->features = $features;
        $this->images = $images;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return auth()->user()->can('manage_product');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'description_ru'         => 'required|max:200',
            'description_en'         => 'required|max:200',
            'feature_image_original' => 'required|image|mimes:jpeg,bmp,png',
            'feature_image_fake'     => 'image|mimes:jpeg,bmp,png',

            'feature_product_id' => 'required|exists:products,id',

            'feature_original_data_x'      => 'required',
            'feature_original_data_y'      => 'required',
            'feature_original_data_width'  => 'required',
            'feature_original_data_height' => 'required',
        ];

        if ($this->method() === 'PATCH') {
            $rules['feature_image_original'] = 'image|mimes:jpeg,bmp,png';
        }

        return $rules;
    }

    /**
     * @return array
     */
    public function getInputDataForCreatingFeature()
    {

        list($imageOriginalId, $imageFakeId) = $this->getImagesForCreate();

        $inputData = [
            'product_id'     => (int) $this->feature_product_id,
            'image_id'       => $imageOriginalId,
            'fake_image_id'  =>  $imageFakeId ?? null,
            'description_ru' => $this->description_ru,
            'description_en' => $this->description_en,
            'order_number'   => $this->getOrderNumber(),
        ];

        return $inputData;
    }

    /**
     * @param VisualFeature $feature
     * @return array
     * @throws \Exception
     */
    public function getInputDataForUpdatingFeature( $feature )
    {
        list($imageOriginalId, $imageFakeId) = $this->getImagesForUpdate($feature);

        $inputData = [
            'product_id'     => (int) $this->feature_product_id,
            'image_id'       => $imageOriginalId,
            'fake_image_id'  => $imageFakeId ?? null,
            'description_ru' => $this->description_ru,
            'description_en' => $this->description_en,
            'order_number'   => $this->getOrderNumber(),
        ];

        return $inputData;
    }

    /** Get order number
     * @return int|mixed
     */
    private function getOrderNumber()
    {
        // if we update feature
        if ($this->method() === 'PATCH') {
            return $this->order_number;
        }

        // if we create new feature
        $builder = VisualFeature::whereProductId($this->feature_product_id);

        if ($builder->count() > 0) {
            $order_number = $builder->orderBy('order_number', 'desc')->first()->order_number;
            $order_number++;
            return $order_number;
        } else
            return 1;
    }

    /** 
     * @return array
     */
    private function getImagesForCreate()
    {
        $imageOriginalId = ImageService::save(Image::make($this->file('feature_image_original'))->crop($this->feature_original_data_width, $this->feature_original_data_height, $this->feature_original_data_x, $this->feature_original_data_y));

        $imageFakeId = 
            $this->hasFile('feature_image_fake') 
                ? ImageService::save(Image::make($this->file('feature_image_fake'))->crop($this->feature_fake_data_width, $this->feature_fake_data_height, $this->feature_fake_data_x, $this->feature_fake_data_y)) 
                : null;

        return [$imageOriginalId, $imageFakeId];
    }

    /** 
     *  Этот метод такой нагроможденный из-за точго что пользователь может захотеть 
     *  отредактировать уже сохранё1нгное изображение, а нет только вновь загуржаемое
     * @param $feature
     * @return array 
     * @throws \Exception
     */
    private function getImagesForUpdate( $feature )
    {
        $imageOriginal = $feature->image;
        
        $this->handleOriginalImage($imageOriginal);

        $imageFake = $feature->image_fake;
        
        // no need to change
        if ( ! $this->hasFile('feature_image_fake') && ! $imageFake && $this->change_image_fake !== 'true') {
            return [$imageOriginal->id, null];
        }
        
        // save image fake
        if ($this->hasFile('feature_image_fake') && ! $imageFake) {
            $fake_image_id = ImageService::save(
                Image::make($this->file('feature_image_fake'))
                    ->crop(
                        $this->feature_fake_data_width,
                        $this->feature_fake_data_height,
                        $this->feature_fake_data_x,
                        $this->feature_fake_data_y)
            );

            return [$imageOriginal->id, $fake_image_id];
        }
 
        // need to be changed with uploaded image
        if ($this->hasFile('feature_image_fake') && $imageFake && $this->change_image_fake === 'true') {
            
            ImageService::updateWithUploadedImage(Image::make($this->file('feature_image_fake'))->crop(
                $this->feature_fake_data_width,
                $this->feature_fake_data_height,
                $this->feature_fake_data_x,
                $this->feature_fake_data_y), $imageFake);

            return [$imageOriginal->id,  $imageFake->id];
        }
        
        
        // need to be changed with existed file from system 
        if ( ! $this->hasFile('feature_image_fake') && $imageFake && $this->change_image_fake === 'true') {
            
            ImageService::updateWithImageFromSystem($imageFake, [
                $this->feature_fake_data_width,
                $this->feature_fake_data_height,
                $this->feature_fake_data_x,
                $this->feature_fake_data_y,
            ]);
        
            return [$imageOriginal->id, $imageFake->id];
        }

        return [$imageOriginal->id, null];
    }

    public function limitReached()
    {
        if ($this->hasFile('feature_image_fake') && $this->hasFile('feature_image_original') ){
            
            if (VisualFeature::whereProductId($this->feature_product_id)->whereNotNull('fake_image_id')->count() >= config('wipon.fake_image_per_feature'))
                return true;
        }

        if (!$this->hasFile('feature_image_fake') && $this->hasFile('feature_image_original') ){
            
            if (VisualFeature::whereProductId($this->feature_product_id)->whereNotNull('image_id')->whereNull('fake_image_id')->count() >= config('wipon.original_image_per_feature'))
                return true;
        }

        return false;
    }

    /**
     * @param $imageOriginal
     */
    private function handleOriginalImage( $imageOriginal )
    {
        // not fake 
        if ($this->hasFile('feature_image_original') && $this->change_image_original === 'true') {
            ImageService::updateWithUploadedImage(Image::make($this->file('feature_image_original'))->crop(
                $this->feature_original_data_width,
                $this->feature_original_data_height,
                $this->feature_original_data_x,
                $this->feature_original_data_y), $imageOriginal);
        }

        if ( ! $this->hasFile('feature_image_original') && $this->change_image_original === 'true') {
            ImageService::updateWithImageFromSystem($imageOriginal, [
                $this->feature_original_data_width,
                $this->feature_original_data_height,
                $this->feature_original_data_x,
                $this->feature_original_data_y,
            ]);
        }
    }
}