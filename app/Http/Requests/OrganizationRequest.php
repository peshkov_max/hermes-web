<?php

namespace App\Http\Requests;

use Gate;
use Image;
use Wipon\Repos\OrganizationRepository;
use Wipon\Services\ImageService;

class OrganizationRequest extends Request
{

    /**
     * @var OrganizationRepository
     */
    private $organizations;

    /**
     * OrganizationController constructor.
     * @param OrganizationRepository $organizations
     */
    public function __construct( OrganizationRepository $organizations )
    {
        parent::__construct();
        $this->organizations = $organizations;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Gate::allows('moderate_organization');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'contract_number' => 'max:20',
            'email'           => 'email',
            'contact_person'  => 'max:300',
            'legal_name'      => 'required|max:255',
            'city_id'         => 'integer|exists:cities,id',
            'is_supplier'     => 'in:true,false',
            'logo'            => 'image|mimes:jpg,jpeg,bmp,png',
        ];

        if ($this->isMethod('POST') && $this->user()->hasRole('super-admin')) {
            $rules['name_ru'] = 'required|max:250';
            $rules['legal_name'] = 'required|max:250';
            $rules['name_en'] = 'required|max:250';
        }

        return $rules;
    }

    public function getCreatingInput()
    {
        $data = $this->except(['_token', 'id', 'logo']);

        if ($this->hasFile('logo')) {
            $data = $this->storeImage($data);
        }

        return $data;
    }

    public function getUpdatingInput( $organization )
    {
        $data = $this->except(['_token', 'id', 'image']);

        if ($this->has('remove_logo') && $this->get('remove_logo') === 'true') {
            return $this->deleteImage($organization, $data);
        }

        if ($this->hasFile('logo')) {
            return $this->updateImage($organization, $data);
        }

        $data['image_id'] = $organization->logo ? $organization->logo->id : null;

        return $data;
    }

    /**
     * @param $organization
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    private function updateImage( $organization, $data )
    {

        $logo = $organization->logo;

        if ($logo) {
            $logo->delete();
        }

        $data = $this->storeImage($data);

        return $data;
    }

    private function getFileExtension( $image )
    {
        $mime = $image->mime();  //edited due to updated to 2.x
        if ($mime == 'image/jpeg')
            $extension = '.jpg';
        elseif ($mime == 'image/png')
            $extension = '.png';
        elseif ($mime == 'image/gif')
            $extension = '.gif';
        else
            $extension = null;

        return $extension;
    }

    /**
     * @param $organization
     * @param $data
     * @return mixed
     */
    private function deleteImage( $organization, $data )
    {
        ImageService::deleteImage($organization->logo);
        $organization->logo->delete();
        $data['image_id'] = null;

        return $data;
    }

    /**
     * @param $data
     * @return mixed
     * @throws \Exception
     */
    private function storeImage( $data )
    {
        $image = Image::make($this->file('logo'));

        $data['image_id'] = ImageService::save($image, [], $this->getFileExtension($image));

        return $data;
    }
}
