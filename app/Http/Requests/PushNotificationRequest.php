<?php

namespace App\Http\Requests;

use App\Jobs\PushNotifications\SendPushMass;
use App\Jobs\PushNotifications\SendPushSingle;
use Carbon\Carbon;
use Validator;
use Wipon\Models\ScheduledJob;

class PushNotificationRequest extends Request
{
    public $requestErrors;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasRole(['super-admin', 'support', 'support-pro']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->is('push-notifications/send-single')
            ? []
            : [

                'message' => $this->is('push-notifications/device-number') ? 'string|max:3500' : 'required|string|max:3500',

                'regions'   => 'array',
                'regions.*' => 'exists:regions,id',

                'versions'   => 'array',
                'versions.*' => 'exists:devices,app_version',

                'platforms' => 'exists:devices,platform',

                'receipts_require' => 'in:true,false',

                'execute_at' => 'date',

                'is_pro' => 'in:true,false',
            ];
    }

    /**
     * Проверяет параметры запроса. Если есть ошибки - формирует
     * массив с ошибками для их дальнейшей отправки
     *
     * @return bool
     */
    public function requestIsValid()
    {
        if ( ! $this->ajax()) {
            $this->requestErrors ['errors'] = 'Request type is wrong';

            return false;
        }

        $v = Validator::make($this->all(), [
            'message'    => 'required|string|max:3500',
            'uuid'       => 'exists:devices,uuid',
            'user_id'    => 'exists:pgsql_pro.users,id',
            'phone_number'    => 'exists:pgsql_pro.users,phone_number',
            'execute_at' => 'date',
        ]);

        if ($v->fails()) {
            $this->requestErrors ['errors'] = $v->messages();

            return false;
        }

        return $this->timeIsFree();
    }

    /**
     * Проверяет есть ли в расписании отправка уведомлений на заданное время
     *
     * @return bool
     */
    public function timeIsFree()
    {
        // Если запрос не содержит параметр execute_at - значит пуш
        // уведомление не должно отправляться по расписанию, а
        // значит проверять расписание нет необходимости
        if ( ! $this->has('execute_at')) {
            return true;
        }

        $startToSend = $this->wrapDateToCarbon($this->input('execute_at'));

        // Если дата отправки менее текущей даты - говорим пользователю
        // что уведомление не может быть отправлено в прошлое
        if ($startToSend->lt(Carbon::now()->subSeconds(300))) {
            $this->requestErrors ['errors'] = ['execute_at' => [trans('app.time_is_wrong')]];

            return false;
        }

        $endToSend = $startToSend->copy();

        // Берем время необходимое на отправку текущего уведомления,
        // и смотрим, запланирована ли в указанный период отправка уведомлений
        $jobs = ScheduledJob::where('is_executed', false)
            ->whereBetween('execute_at', [$startToSend, $endToSend->addSeconds(SendPushMass::getDeviceNumber($this->all()))])
            ->where('name', "App\\Jobs\\SendPushMass")
            ->whereRaw('(params->\'device_number\') is not null')
            ->get();

        if ($jobs->count()) {

            $this->requestErrors ['errors'] = [
                'execute_at' => [
                    trans('app.try_after', [
                        'datetime' => $endToSend->timezone(config('wipon.user_time_zone'))
                            ->format(config('wipon.dt_format_d_m_y_h_i'))
                    ])],
            ];

            return false;
        }

        return true;
    }

    /**
     * Парсит дату и отдает Carbon объект даты
     *
     * @param null $time
     * @return Carbon
     */
    private function wrapDateToCarbon( $time = null ) :Carbon
    {
        if (is_null($time)) {
            return carbon_user_tz_now();
        }

        try {

            $timeToSend = Carbon::createFromFormat(config('wipon.dt_format_d_m_y_h_i'), $time, config('wipon.user_time_zone')
            )->timezone('UTC');
        } catch (\Exception $e) {

            log_i('Error date', [$e->getMessage()]);
            $timeToSend = carbon_user_tz_now();
        }

        return $timeToSend;
    }

    /**
     * Отправить массовое уведомление или запланировать отправку на будущее
     */
    public function sendPushMass()
    {
        $params = $this->except('execute_at');

        // Если пуш не отложенный - отправляем его сейчас
        if ( ! $this->has('execute_at')) {
            dispatch((new SendPushMass($params))->onQueue('cabinet_push'));

            return;
        }

        // Отправить отложенный Push
        /**
         * device_number - количество устройств, на которое необходимо отправить пуш
         * уведомление 1 устройство  = 1 секунда на отправку, т. е. - в принципе можно считать что
         * это общее количество секунд на отправку
         */
        $params['device_number'] = SendPushMass::getDeviceNumber($params);

        ScheduledJob::create([
            'name'       => SendPushMass::class,
            'params'     => $params,
            'execute_at' => $this->wrapDateToCarbon($this->input('execute_at')),
            'user_id'    => $this->user()->id,
        ]);
    }

    /**
     * Отправить одиночное уведомление или запланировать отправку на будущее
     */
    public function sendPushSingle()
    {
        // Если пуш не отложенный - отправляем его сейчас
        if ( ! $this->has('execute_at')) {

            dispatch((new SendPushSingle($this->only('uuid', 'user_id', 'phone_number', 'message')))->onQueue('cabinet_push'));

            return;
        }

        $params = $this->only('uuid', 'user_id', 'phone_number','message');
        $params['device_number'] = 1;

        ScheduledJob::create([
            'name'       => SendPushSingle::class,
            'params'     => $params,
            'execute_at' => $this->wrapDateToCarbon($this->input('execute_at')),
            'user_id'    => $this->user()->id,
        ]);
    }
}
