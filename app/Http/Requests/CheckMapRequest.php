<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Excel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use Pheanstalk\Exception;
use Response;
use Wipon\Models\Check;
use Wipon\Models\Region;
use Wipon\Repos\CheckRepository;
use Wipon\Repos\CheckStatisticRepository;
use Wipon\Repos\RegionRepository;
use Wipon\Services\StatisticService;
use Wipon\Support\ClusterDetailExportHelper;

class CheckMapRequest extends Request
{
    /**
     * @var CheckStatisticRepository
     */
    private $check_statistics;

    /**
     * @var RegionRepository
     */
    private $regions;

    public function __construct( CheckStatisticRepository $check_statistics, RegionRepository $regions )
    {
        parent::__construct();

        $this->check_statistics = $check_statistics;
        $this->regions = $regions;
    }


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'period'            => 'array',
            'period.start'      => 'date_format:' . config('wipon.pss_date_format'),
            'period.end'        => 'date_format:' . config('wipon.pss_date_format'),
            'checkType'         => 'in:scan,hand,both,nothing',
            'regionId'          => 'exists:regions,id',
            'ukmType'           => 'array',
            'ukmType.all'       => 'in:true,false',
            'ukmType.valid'     => 'in:true,false',
            'ukmType.false'     => 'in:true,false',
            'ukmType.duplicate' => 'in:true,false',
            'ukmType.atlas'     => 'in:true,false',
        ];
    }

    /** Filter map stat
     * @return array
     */
    public function doFilter()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $clusters = $this->check_statistics->filter($this->getFilterParams());

        return [$clusters, $this->getScanSum($clusters, $this->all())];
    }

    /** Get default kazakhstan region
     * @return mixed
     */
    public function getKazakhstanRegion()
    {
        return $this->regions->getKazakhstanRegion()->toArray();
    }

    /** Count scan sum to render on the scam index page
     *
     * @param $clusters
     * @param $filterParam
     * @return int|mixed
     */
    public function getScanSum( $clusters, array $filterParam )
    {
  
        switch (true) {
            case $clusters->count() == 0:
                return 0;
                break;

            case $this->allExcluded($filterParam):
                return 0;
                break;

            case $this->allIncluded($filterParam):

                return $this->countAllUkm($clusters);
                break;

            case $this->allEnterTypeByUkm($filterParam):

                return $this->countAllEnterTypeByUkm($clusters, $filterParam);
                break;

            case $this->notAllEnterTypeAllUkm($filterParam):

                return $this->countAllEnterTypeAllUkm($clusters, $filterParam);
                break;

            case $this->notAllEnterTypeNotAllUkm($filterParam):

                return $this->countAllEnterTypeNotAllUkm($clusters, $filterParam);
                break;

            default;
                return $clusters->sum('check_total');
                break;
        }
    }

    /** Count All Enter Types and Not All Ukm
     *
     * @param Builder $clusters
     * @param $filterPrams
     * @return int
     */
    protected function countAllEnterTypeNotAllUkm( $clusters, array $filterPrams )
    {
        $checkType = $filterPrams['checkType'];
        $ukmType = $filterPrams['ukmType'];

        $sum = 0;

        if (filter_var($ukmType["fake"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_fake_$checkType");
        if (filter_var($ukmType["atlas"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_atlas_$checkType");
        if (filter_var($ukmType["valid"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_valid_$checkType");
        if (filter_var($ukmType["duplicate"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_duplicate_$checkType");

        return $sum;
    }

    /** If not All EnterType and  Not All Ukm
     *
     * @param $filterParam
     * @return bool
     */
    protected function notAllEnterTypeNotAllUkm( array $filterParam = [] )
    {
        
        if (
            array_has($filterParam, 'checkType')  &&
            array_get($filterParam, 'checkType') != 'both' &&
            array_has($filterParam, 'ukmType') &&
            ! $this->ukmTypeIsAll($filterParam)
        )
            return true;
        else
            return false;
    }


    /** If not All Enter Types and  All Ukm
     *
     * @param $filterParam
     * @return bool
     */
    protected function notAllEnterTypeAllUkm( array $filterParam = [] )
    {
        if (
            array_has($filterParam, 'checkType') &&
            array_get($filterParam, 'checkType') != 'both' &&
            array_has($filterParam, 'ukmType') &&
            $this->ukmTypeIsAll($filterParam)
        )
            return true;
        else
            return false;
    }


    /** Count all enter types and all ukm
     *
     * @param Collection $clusters
     * @param array $filter
     * @return mixed
     */
    protected function countAllEnterTypeAllUkm( Collection $clusters, array $filter = [] )
    {
        switch (array_get($filter, 'checkType')) {
            case 'hand':
                return $clusters->sum('check_by_hand_sum');
                break;
            case 'scan':
                return $clusters->sum('check_by_scan_sum');
                break;
            default;
                return $clusters->sum('check_total');
                break;
        }
    }


    /** If there is all enter type and ukm ! = all
     *
     * @param array $filterParam
     * @return bool
     */
    protected function allEnterTypeByUkm( array $filterParam = [] )
    {
        if (
            array_has($filterParam, 'checkType') &&
            array_get($filterParam, 'checkType') == 'both' &&
            array_has($filterParam, 'ukmType') &&
            ! $this->ukmTypeIsAll($filterParam)
        )
            return true;
        else
            return false;
    }

    /** Count All Enter Types by given ukm types
     *
     * @param $clusters
     * @param $filterParams
     * @return int
     */
    protected function countAllEnterTypeByUkm( $clusters, array $filterParams = [] )
    {
        if ( ! $ukmType = $filterParams['ukmType'])
            return 0;

        if ($this->ukmTypeIsAll($filterParams)) 
            return $this->countAllUkm($clusters);

        $sum = 0;

        if (filter_var($ukmType["fake"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_fake_sum");
        if (filter_var($ukmType["atlas"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_atlas_sum");
        if (filter_var($ukmType["valid"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_valid_sum");
        if (filter_var($ukmType["duplicate"], FILTER_VALIDATE_BOOLEAN)) $sum += $clusters->sum("check_duplicate_sum");

        return $sum;
    }

    /**  If checkType = nothing
     *
     * @param $filterParam
     * @return bool
     */
    protected function allExcluded( array $filterParam = [] )
    {
        return isset($filterParam['checkType']) && $filterParam['checkType'] == 'nothing' ? true : false;
    }

    /**  If there is an checkType = both and ukmType = all
     *
     * @param $filterParam
     * @return bool
     */
    protected function allIncluded( $filterParam = [] )
    {
        return isset($filterParam['checkType']) &&
        $filterParam['checkType'] == 'both' &&
        isset($filterParam['ukmType']) &&
        isset($filterParam['ukmType']) &&
        $this->ukmTypeIsAll($filterParam) ? true : false;
    }

    private function ukmTypeIsAll( array $filterParam = [] )
    {
        return
            filter_var(array_get($filterParam, 'ukmType.all'), FILTER_VALIDATE_BOOLEAN)
            || (
                filter_var(array_get($filterParam, 'ukmType.valid'), FILTER_VALIDATE_BOOLEAN) &&
                filter_var(array_get($filterParam, 'ukmType.fake'), FILTER_VALIDATE_BOOLEAN) &&
                filter_var(array_get($filterParam, 'ukmType.duplicate'), FILTER_VALIDATE_BOOLEAN) &&
                filter_var(array_get($filterParam, 'ukmType.atlas'), FILTER_VALIDATE_BOOLEAN)
            );
    }


    public function getAllRegionReport()
    {
        list($aggregationPerCluster, $region, $period, $checkType) = $this->getParamsForMapExcelReport();

        $fileName = trans('app.map_report_file_name') . carbon_user_tz_now()->toDateTimeString();
        $filePath = storage_path('app/public/excel');

        Excel::create($fileName, function ( $excel ) use ( $aggregationPerCluster, $region, $period, $checkType ) {

            /*** @var  $excel LaravelExcelWriter */
            $excel->setTitle('Scan in region');
            $excel->setCompany('Wipon');
            $excel = $this->regionExcelSheet($excel, $region, $aggregationPerCluster, $period, $checkType);
            $excel->setActiveSheetIndex(0);
        })->store('xls', $filePath);

        $fileName .= '.xls';

        return Response::download($filePath . '/' . $fileName, $fileName, [
            'Content-Length: ' . filesize($filePath),
        ]);
    }


    /**
     *  Get Excel sheet for report ( used in scanRegion, scanRegions methods)
     *
     * @param $excel
     * @param $region
     * @param $aggregationPerCluster
     * @param $period
     * @param $checkType
     * @return mixed
     */
    protected function regionExcelSheet( $excel, $region, $aggregationPerCluster, $period, $checkType )
    {
        // Create sheet with clusters
        $excel = $this->createSheetClusterGeneral($excel, $region, $aggregationPerCluster, $period);

        // Create sheet with clusters scans
        $excel = $this->createSheetClustersScans($excel, $region, $checkType, $period);

        return $excel;
    }

    private function createSheetClusterGeneral( $excel, $region, $clusters, $period )
    {
        $excel->sheet($region->name, function ( $sheet ) use ( $region, $clusters, $period ) {

            $sheet->loadView('reports.checks.map.region', [
                'regionName' => $region->name,
                'clusters'   => $clusters,
                'userName'   => auth()->user()->name,
                'period'     => $period,
            ]);
            $sheet->setAutoFilter('E12:L12');

            wipon_image_to_excel($sheet);
        });

        return $excel;
    }


    private function createSheetClustersScans( $excel, $region, $checkType, $period )
    {
        $checks = $this->check_statistics->getChecksForReport($this->getFilterParams());

        $excel->sheet(trans('app.details'), function ( $sheet ) use ( $region, $checks, $checkType, $period ) {

            $sheet->loadView('reports.checks.map.region_scan', [
                'regionName' => $region->name,
                'checks'     => $checks,
                'userName'   => auth()->user()->name,
                'period'     => $period,
            ]);
            $sheet->setAutoFilter('A10:L10');

            wipon_image_to_excel($sheet);
        });

        return $excel;
    }

    /**
     * @return array
     */
    public function getCurrentRegionAndStatUpdate()
    {
        /** * @var Region $currentRegion */
        $currentRegion = $this->regionId
            ? $this->regions->findById($this->regionId)
            : $this->regions->getDefaultRegion();

        $lastStatUpdateDateTime = StatisticService::getLastKgdStatUpdateDate();

        return [$currentRegion, $lastStatUpdateDateTime];
    }

    /** Get report date period
     *
     * @return string
     */
    private function getReportPeriod()
    {
        return ($this->period && isset($this->period['start']) && isset($this->period['end']))
            ? $this->period['start'] . ' - ' . $this->period['end'] : $this->getFirstAndLastScanDatesInString();
    }

    /** Get first and last scanned_at dates from alcohol_scans table
     *
     * @return string
     */
    private function getFirstAndLastScanDatesInString()
    {
        try {

            $dates = Check::selectRaw('max(created_at) as max_date, min(created_at) as min_date')->first();

            return
                Carbon::createFromFormat(config('wipon.date_time_format'), $dates->max_date)->format('Y-m-d')
                . ' - '
                . Carbon::createFromFormat(config('wipon.date_time_format'), $dates->min_date)->format('Y-m-d');
        } catch (\ErrorException $e) {

            log_e(__METHOD__ . ' Check table is empty!!! ');

            return trans('app.not_set');
        }
    }

    public function getKgdCompetitionExcel()
    {
        $period = $this->getCompetitionPeriod();

        $competitionData = $this->check_statistics->getKgdCompetitionParticipants($period['start'], $period['end'], $this->regionId);

        list($filePath, $fileName) = $this->saveKgdCompetitionParticipantsExcel($competitionData, $period);

        $fileName .= '.xls';

        return [$filePath, $fileName];
    }

    /**
     *
     * @param $competitionData
     * @param $period
     * @return array
     */
    private function saveKgdCompetitionParticipantsExcel( $competitionData, $period )
    {
        $filePath = storage_path('app/public/excel');
        $fileName = trans('app.competition_data_file_name');

        $region = $this->regions->findById($this->regionId);

        Excel::create($fileName, function ( $excel ) use ( $competitionData, $region, $period ) {

            /*** @var  $excel LaravelExcelWriter */
            $excel->setTitle('Scan in region');
            $excel->setCompany('Wipon');
            $excel->sheet($region->name, function ( $sheet ) use ( $competitionData, $region, $period ) {
                $sheet->loadView('reports.checks.map.competition_participants', [
                    'regionName' => $region->name,
                    'items'      => $competitionData,
                    'userName'   => auth()->user()->name,
                    'period'     => $period,
                ]);
                $sheet->setAutoFilter('A12:L12');
                wipon_image_to_excel($sheet);
            });
        })->store('xls', $filePath);

        return [$filePath, $fileName];
    }

    /** Выгрузка детализаци сканирований по кластеру в Excel
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \Exception
     */
    public function generateDetailReport()
    {
        $helper = new ClusterDetailExportHelper(new CheckStatisticRepository(), new CheckRepository(), $this->cluster_id, $this->all());

        list($filePath, $fileName) = $helper->createExcelReport();

        $fileName .= '.xls';

        return Response::download($filePath . '/' . $fileName, $fileName, [
            'Content-Length: ' . filesize($filePath),
        ]);
    }

    /**
     * Get competition period
     *
     * @return array
     */
    private function getCompetitionPeriod()
    {
        /** @var Carbon $date */
        $date = carbon_user_tz_now();

        $date->year = $this->year;
        $date->month = $this->month;

        $period['start'] = $date->firstOfMonth()->format(config('wipon.date_time_format'));
        $period['end'] = $date->lastOfMonth()->format(config('wipon.date_time_format'));

        return $period;
    }

    /**
     * @return array
     */
    private function getParamsForMapExcelReport()
    {
        list($aggregationPerCluster) = $this->doFilter();

        return [
            $aggregationPerCluster
                ->load('checks.item.product.type', 'checks.item.product.organization')
                ->sortByDesc(function ( $cluster ) {
                    return $cluster->checks()->first()->created_at;
                }),
            Region::findOrFail($this->regionId),
            $this->getReportPeriod(),
            $this->checkType,
        ];
    }

    /**
     * @return array
     */
    private function getFilterParams()
    {
        $statisticsFilter = $this->reset && $this->reset === 'true'
            ? [
                'period'    => null,
                'checkType' => null,
                'regionId'  => null,
                'ukmType'   => null,
            ]
            : [
                'period'    => $this->period,
                'checkType' => $this->checkType ?? null,
                'regionId'  => $this->regionId,
                'ukmType'   => $this->ukmType,
            ];

        return $statisticsFilter;
    }

    /**
     * @param $clusters
     * @return int
     */
    protected function countAllUkm( $clusters )
    {
        $sum = 0;
        $sum += $clusters->sum("check_fake_sum");
        $sum += $clusters->sum("check_atlas_sum");
        $sum += $clusters->sum("check_valid_sum");
        $sum += $clusters->sum("check_duplicate_sum");


        return $sum;
    }

}
