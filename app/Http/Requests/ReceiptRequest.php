<?php

namespace App\Http\Requests;

use App\Jobs\PushNotifications\SendPushSingle;
use Carbon\Carbon;
use Excel;
use Wipon\Models\Receipt;
use Wipon\Models\Region;
use Wipon\Repos\ReceiptReportRepository;
use Wipon\Repos\ReceiptRepository;
use Wipon\Repos\RegionRepository;

class ReceiptRequest extends Request
{
    /**
     * @var ReceiptRepository
     */
    private $receipts;

    /**
     * ReceiptRequest constructor.
     */
    public function __construct( ReceiptRepository $receipts )
    {
        /**
         * Для формирования отчётов может потребоваться много памяти
         * Это решение проблемы - Allowed memory size of 134217728 bytes exhausted (tried to allocate 21319120 bytes */
        ini_set('memory_limit', '1024M');

        /**
         * Это решение проблемы - Maximum execution time of 30 seconds exceeded
         * Устанавливаем время выполнения скрипта  - 30 минут */
        set_time_limit(1800);
        parent::__construct();
        $this->receipts = $receipts;
    }

    function __destruct()
    {
        log_i('Memory used for ReceiptRequest handling: ' . $this->format_bytes(memory_get_peak_usage(1)));
    }

    private function format_bytes( $size, $precision = 2 )
    {
        $base = log($size, 1024);
        $suffixes = ['', 'Kb', 'Mb', 'Gb', 'Tb'];

        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[ floor($base) ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->hasRole(['super-admin', 'receipt-moderator', 'inspector', 'inspector-admin', 'npp', 'receipt-moderator-admin']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->isSettingStatus()) {
            $rules['status'] = 'required|in:member,blurred,not-fiscal,invalid-date,empty-bin,empty-date-time,empty-sum,processing,winner,duplicate,doesnt-match-terms';
            $rules['receipt_id'] = 'required|exists:receipts,id';
        }

        return $rules;
    }

    /** Проверяет, осуществляется ли текущий запрос для установки статуса чека
     * @return bool
     */
    public function isSettingStatus()
    {
        return $this->has('status') && $this->get('status') != '';
    }

    public function isExportingParticipants()
    {
        return $this->has('export') && $this->get('export') == true;
    }

    /**
     * Установка статуса чека
     */
    public function setStatus()
    {
        try {

            $this->receipts->setStatus($this->receipt_id, $this->status);

            if ( ! in_array($this->status, ['member', 'winner', 'not_fiscal', 'not-fiscal'])) {
                $this->sendPushNotification($this->receipts->findById($this->receipt_id));
            } else {
                log_i("Notification hasn't been sent because receipt status is {$this->status}. 
                We do not sent push notifications with statuses: member, winner, not_fiscal, not-fiscal.");
            }
        } catch (\Exception $e) {

            log_e('Error while setting receipt status', [$e->getMessage()]);
        }
    }

    /** Отправка пуш уведомления,е если статус не равно: member
     * @param $receipt
     */
    private function sendPushNotification( $receipt )
    {
        $status = implode('_', preg_split('/[-,]+/', $this->status));

        $message = trans("app.notification_{$status}", ['id' => $receipt->id]);

        dispatch((
        new SendPushSingle([
            'uuid'    => $receipt->device_uuid,
            'message' => $message,
        ]))->onQueue('cabinet_push'));

        log_i("PUSH notification was pushed to the queue. " . PHP_EOL
            . "#User with uuid: {$receipt->device_uuid}" . PHP_EOL .
            "#Message: {$message}");
    }

    /** Сохраняет отчет по чекамм  в каталоге  storage/app/public/excel
     * @return array
     */
    public function storeReceiptsReport()
    {
        $filePath = config('wipon.excel_files_path');
        $fileName = $this->onlyWinners ? trans('app.receipt_report_winners') : trans('app.receipt_report_file_name');
        $params = $this->all();
        $regionName = $this->getRegionName($params);

        Excel::create($fileName, function ( $excel ) use ( $params, $fileName, $regionName ) {
            $excel->setTitle($fileName);
            $excel->setCompany('Wipon');
            $excel->sheet('Лист 1', function ( $sheet ) use ( $params, $fileName, $regionName ) {
                $receipts = (new ReceiptRepository())->filter($params)->get();
                $sheet->loadView('reports.receipts.receipts', [
                    'regionName' => $regionName,
                    'items'      => $receipts,
                    'userName'   => auth()->user()->name,
                    'period'     => ReceiptRepository::getPeriods($params, config('wipon.dt_format_d_m_y_h_i')),
                    'title'      => $fileName,
                ]);
                $sheet->setAutoFilter('A9:H9');
                $sheet->setColumnFormat([
                    'E10:E' . (10 + $receipts->count()) => '#,##0.00_-',
                ]);
                $sheet->setColumnFormat([
                    'C10:C' . (10 + $receipts->count()) => '0',
                ]);
                wipon_image_to_excel($sheet);
            });
        })->store('xls', storage_path($filePath));

        return ['filePath' => $filePath, 'fileName' => $fileName . '.xls'];
    }

    /** Сохраняет отчет по статистики  в каталоге storage/app/public/excel
     * @return array
     */
    public function storeStatisticReport()
    {
        $filePath = config('wipon.excel_files_path');
        $fileName = trans('app.require_receipt_stat');
        $params = $this->all();

        Excel::create($fileName, function ( $excel ) use ( $params, $fileName ) {
            $excel->setTitle($fileName);
            $excel->setCompany('Wipon');
            $receiptRepo = new ReceiptRepository();
            // Лист количества 
            $excel->sheet(trans('app.quantity'), function ( $sheet ) use ( $params, $receiptRepo ) {
                $params['dataTypeSwitch'] = 'switch_quantity';
                $stat = $receiptRepo->getStatistics($params);
                $sheet->loadView('reports.receipts.stat', [
                    'items'         => $stat,
                    'receiptNumber' => trans('app.receipts_quantity', ['number' => $stat[ count($stat) - 1 ]['total']]),
                    'userName'      => auth()->user()->name,
                    'year'          => array_get($params, 'year'),
                ]);
                $sheet->setAutoFilter('A8:N8');
                wipon_image_to_excel($sheet);
            });

            // Замороженный функционал может быть добавлен в будущем

            // // Лист суммы  
            // $excel->sheet(trans('reports.sum'), function ( $sheet ) use ( $params, $receiptRepo ) {
            //     $params['dataTypeSwitch'] = 'switch_sum';
            //     $stat = $receiptRepo->getStatistics($params);
            //     $sheet->loadView('reports.receipts.stat', [
            //         'items'         => $stat,
            //         'year'          => array_get($params, 'year'),
            //         'receiptNumber' => trans('reports.receipts_sum', ['number' => number_format($stat[ count($stat) - 1 ]['total'], 2, ',', ' ')]),
            //         'userName'      => auth()->user()->name,
            //     ]);
            //     $sheet->setAutoFilter('A8:N8');
            //     $sheet->setColumnFormat([
            //         'B10:N26' => '#,##0.00_-',
            //     ]);
            //     wipon_image_to_excel($sheet);
            // });
        })->store('xls', storage_path($filePath));

        return ['filePath' => $filePath, 'fileName' => $fileName . '.xls'];
    }

    /** Сохраняет отчет сообщений в каталоге  storage/app/public/excel
     * @return array
     */
    public function storeMessageReport()
    {
        $filePath = config('wipon.excel_files_path');
        $fileName = trans('app.receipt_report_messages');
        $params = $this->all();
        $regionName = $this->getRegionName($params);

        Excel::create($fileName, function ( $excel ) use ( $params, $fileName, $regionName ) {
            $excel->setTitle($fileName);
            $excel->setCompany('Wipon');
            $excel->sheet('Лист 1', function ( $sheet ) use ( $params, $regionName ) {
                $receiptNumber = (new ReceiptReportRepository)->filter($params, false)->count();
                $sheet->loadView('reports.receipts.messages', [
                    'regionName'    => $regionName,
                    'items'         => (new ReceiptReportRepository)->filter($params)->get(),
                    'receiptNumber' => trans('app.message_sum', ['number' => $receiptNumber]),
                    'userName'      => auth()->user()->name,
                    'period'        => ReceiptRepository::getPeriods($params, config('wipon.dt_format_d_m_y_h_i')),
                ]);
                $sheet->setAutoFilter('A9:L9');
                wipon_image_to_excel($sheet);
            });
        })->store('xls', storage_path($filePath));

        return ['filePath' => $filePath, 'fileName' => $fileName . '.xls'];
    }


    private function getRegionName( $params )
    {
        if (array_has($params, 'regionId') && array_get($params, 'regionId') === 'all')
            return trans('app.all_region');

        return array_has($params, 'regionId') && array_get($params, 'regionId') !== 'all'
            ? Region::findOrFail(array_get($params, 'regionId'))->name
            : (new RegionRepository)->getDefaultRegion()->name;
    }


    /**
     * Store receipt after education
     */
    public function storeReceipt()
    {
        try {
            $receipt = Receipt::findOrFail($this->id);

            $receipt->bin = $this->bin;
            $receipt->sum = $this->sum;
            $receipt->given_datetime = Carbon::createFromFormat(config('wipon.date_time_format'), $this->given_datetime);

            $receipt->save();

            log_i("Receipt with ID {$this->receipt_id} was educated");
        } catch (\Exception $e) {

            log_e('Error while educating receipt ', [$e->getMessage()]);
        }
    }

    /**
     * Skip receipt while education
     */
    public function skipReceipt()
    {
        try {
            $receipt = Receipt::findOrFail($this->id);
            $receipt->bin = '000000000001';
            $receipt->save();

            log_i("Receipt with ID {$this->receipt_id} was skipped");
        } catch (\Exception $e) {

            log_e('Error while skipping receipt ', [$e->getMessage()]);
        }
        
    }

    public function educateReceipt()
    {
        if ($this->hasEducationParams()) {
            $this->storeReceipt();
        }

        if ($this->has('skip')) {
            $this->skipReceipt();
        }

        $builder =
            Receipt::where(function ( $q ) {
                // не обученные
                $q->where('bin', '=', '000000000000')
                    // пропущенные
                    ->orWhere('bin', '=', '000000000001');
            })
                ->whereSum(0)
                ->whereGivenDatetime('2016-09-30 00:00:00');

        try {
            $inQueue = $builder->count();
            $receipt = $inQueue > 0 ? $builder ->inRandomOrder()->take(1)->get()->first()->toArray() : [];
        } catch (\Exception $e) {
            log_e($e->getMessage());
        }

        return [$inQueue, $receipt];
    }

    /**
     * @return bool
     */
    public function hasEducationParams()
    {
        return $this->has('id') &&
        $this->has('bin') &&
        $this->has('sum') &&
        $this->has('given_datetime');
    }
}
