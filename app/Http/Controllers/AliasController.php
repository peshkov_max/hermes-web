<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Wipon\Models\Alias;
use Wipon\Repos\AliasRepository;

class AliasController extends Controller
{

    /**
     * @var AliasRepository
     */
    private $aliases;

    /**
     * AliasController constructor.
     * @param AliasRepository $aliases
     */
    public function __construct( AliasRepository $aliases )
    {
        $this->aliases = $aliases;
        
        $this->middleware('ajax', ['except' => ['index']]);
    }

    public function index( Request $request, $aliasable_type, $aliasable_id )
    {
        $this->validateArray(
            [
                'aliasable_type' => $aliasable_type,
                'aliasable_id'   => $aliasable_id,
            ], [
                'aliasable_type' => 'required|alpha',
                'aliasable_id'   => 'required|integer',
            ]
        );
        
        $this->authorize('index', [new Alias(), $aliasable_type]);

        /** @noinspection PhpUndefinedFieldInspection */
        $aliasFilter = [
            'search_query' => $request->search_query,
        ];

        $model = $this->aliases->findMorthModel($aliasable_type, $aliasable_id);

        $aliases = $this->aliases->filter($model, $aliasFilter)->get();

        if ($request->ajax()) {
            return $aliases->toJson();
        }

        return view('vue', [
            'params'    => [
                'aliasable_type' => $aliasable_type,
                'item'           => $model,
            ],
            'viewModel' => 'vm-aliases']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $this->validate($request, [
            'aliasable_type' => 'required|alpha',
            'aliasable_id'   => 'required|integer',
            'name'           => 'required',
        ]);

        $this->authorize('store', [new Alias(), $request->aliasable_type]);

        $model = $this->aliases->createMorth($request->aliasable_type, $request->aliasable_id, $request->name);

        return $this->aliases->filter($model)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request )
    {
        $this->validate($request, [
            'id'             => 'required|exists:aliases,id',
            'aliasable_type' => 'required|alpha',
            'aliasable_id'   => 'required|integer',
            'name'           => 'required',
        ]);

        $this->authorize('update', [new Alias(), $request->aliasable_type]);

        if ($this->aliases->update($request->id, ['name' => $request->name])) {
            return $this->aliases->filter($this->aliases->findMorthModel($request->aliasable_type, $request->aliasable_id))->get()->toJson();
        } else {
            return $this->aliases->getModel()->get();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy( Request $request)
    {
        $this->validate($request, [
            'aliasable_type' => 'required|alpha',
            'aliasable_id'   => 'required|integer',
            'id'             => 'required|exists:aliases,id',
        ]);

        $this->authorize('destroy', [new Alias(), $request->aliasable_type]);

        $this->aliases->destroy($request->id);

        return $this->aliases->filter($this->aliases->findMorthModel($request->aliasable_type, $request->aliasable_id))
            ->get()->toJson();
    }
}
