<?php

namespace App\Http\Controllers\Pro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Wipon\Models\Pro\Region as RegionPro;

class RegionControllerPro extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax', ['only' => ['search']]);
    }
    
    /**
     * Поиск по регионам
     *
     * @param Request $request
     * @return mixed
     */
    public function search( Request $request )
    {
        $this->validate($request, [
            'search' => 'required|string|max:255',
        ]);

        $str = str_for_search($request->input('search'));

        return response(RegionPro::where(function ( $query ) use ( $str ) {
            $query
                ->where('name_ru', 'ilike', $str)
                ->orWhere('name_kk', 'ilike', $str)
                ->orWhere('name_en', 'ilike', $str);
        })->paginate(10), 200);
    }
}