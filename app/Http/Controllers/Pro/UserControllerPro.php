<?php

namespace App\Http\Controllers\Pro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Wipon\Models\Pro\User as UserPro;
use Wipon\Repos\Pro\UserRepositoryPro;

class UserControllerPro extends Controller
{
    private $usersPro;

    public function __construct( UserRepositoryPro $customerPro )
    {
        $this->usersPro = $customerPro;

        $this->middleware('ajax', ['only' => ['apiRequests', 'update']]);
    }

    /**
     * Отобразить список пользователей
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {
            return $this
                ->usersPro
                ->filter($request->all())
                ->paginate($this->usersPro->perPage());
        }

        return view('vue', [
            'params'    => [

                // НЕ ОЧЕВИДНАЯ ЛОГИКА
                // Если в параметрах передан deviceUUID, то необходимо 
                // установить значение фильтра чтобы при первом ajax запросе 
                // осуществился поиск устройства deviceUUID.
                // Для осуществления этой логики прокидывается параметр deviceUUID
                'phone_number' => $request->phone_number,
                'user_id'      => $request->user_id,
            ],
            'viewModel' => 'vm-users-pro',
        ]);
    }

    /**
     * Открыть пользователя
     *
     * @param UserPro $user - Пользователь
     * @return mixed
     */
    public function show( UserPro $user )
    {
        return view('vue', [
            'params'    => [
                'item' => $user->load('api_requests', 'feedback_messages', 'notifications')->toArray(),
            ],
            'viewModel' => 'vm-users-pro-show',
        ]);
    }

    /**
     * Открыть форму редактирования пользователей
     *
     * @param UserPro $user - Пользователь
     * @return mixed
     */
    public function edit( UserPro $user )
    {
        return view('vue', [
            'params'    => [
                'item' => $user->load('store.region', 'feedback_messages')->toArray(),
            ],
            'viewModel' => 'vm-users-pro-edit',
        ]);
    }

    /**
     * Обновить пользователя
     *
     * @param Request $request
     * @param UserPro $user - Пользователь
     * @return
     */
    public function update( Request $request, UserPro $user )
    {
        $v = Validator::make($request->input(), [
            'id'                => "required|exists:customers,id",
            'name'              => "required|string|max:255",
            'phone_number'      => "required|max:255|unique:pgsql_pro.users,phone_number," . $request->input('id'),
            'work_phone_number' => "required|max:255",
            'email'             => "email|max:255",
            'platform'          => "string|max:255",
            'device_model'      => "string|max:255",
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->phone_number = $request->input('phone_number');
        $user->work_phone_number = $request->input('work_phone_number');
        $user->platform = $request->input('platform');
        $user->model = $request->input('device_model');
        
        return $user->save()
            ? response('saved', 200)
            : response('Error while saving', 500);
    }

    /**
     * Api Requests пользователя
     *
     * @param UserPro $user - Пользователь
     * @return mixed
     */
    public function apiRequests( UserPro $user )
    {
        return response($user->api_requests()->paginate(10), 200);
    }
}