<?php

namespace App\Http\Controllers\Pro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Wipon\Models\Pro\StoreType as StoreTypePro;


class StoreTypeControllerPro extends Controller
{
    public function __construct()
    {
        $this->middleware('ajax', ['only' => ['search']]);
    }

    /**
     * Поиск типа магазина
     *
     * @param Request $request
     * @return mixed
     */
    public function search( Request $request )
    {
        $this->validate($request, [
            'search' => 'required|string|max:255',
        ]);

        $str = str_for_search($request->input('search'));

        return response(StoreTypePro::where(function ( $query ) use ( $str ) {
            $query
                ->where('name_ru', 'ilike', $str)
                ->orWhere('name_kk','ilike', $str)
                ->orWhere('name_en', 'ilike', $str);
        })->paginate(10)->toArray(), 200);
    }
}