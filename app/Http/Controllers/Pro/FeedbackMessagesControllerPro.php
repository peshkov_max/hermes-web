<?php

namespace App\Http\Controllers\Pro;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Wipon\Models\Pro\FeedbackMessage as FeedbackMessagePro;
use Wipon\Models\Pro\User;
use Wipon\Repos\Pro\FeedbackMessagesRepositoryPro;

class FeedbackMessagesControllerPro extends Controller
{
    /**
     * @var FeedbackMessagesRepositoryPro
     */
    private $supportPro;

    public function __construct( FeedbackMessagesRepositoryPro $feedback )
    {
        $this->supportPro = $feedback;

        $this->middleware('ajax', ['only' => ['read']]);
    }

    /**
     * Отображение списка сообщений
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {
            return $this
                ->supportPro
                ->filter($request->all())
                ->paginate($this->supportPro->perPage());
        }

        return view('vue', [
            'viewModel' => 'vm-support-pro-index',
            'params'    => [
                'user_id' => $request->user_id,
            ],
        ]);
    }

    /**
     * Установка статуса в "Прочитано/Не прочитано"
     *
     * @param FeedbackMessagePro $message - Сообщение обратной связи
     * @param bool $isUnread - статус сообщения
     * @return mixed
     */
    public function read( FeedbackMessagePro $message, $isUnread = true )
    {
        $this->validateArray(['set_unread' => (bool) $isUnread,], ['set_unread' => 'boolean',]);

        return $message->update(['is_unread' => filter_var($isUnread, FILTER_VALIDATE_BOOLEAN)])
            ? ['status' => 'success']
            : ['status' => 'error'];
    }

    /**
     * Просмотр сообщения обратной связи
     *
     * @param FeedbackMessagePro $message - Сообщение обратной связи
     * @param User $user - пользователь Wipon Pro
     * @return \Illuminate\Http\Response
     */
    public function show( FeedbackMessagePro $message, User $user )
    {
        return view('vue', [
            'viewModel' => 'vm-support-pro-show',
            'params'    => [
                'item' => $message,
                'user' => $user->load('feedback_messages', 'notifications'),
            ],
        ]);
    }
}
