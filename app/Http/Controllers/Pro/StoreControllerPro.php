<?php

namespace App\Http\Controllers\pro;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;
use Wipon\Models\Pro\Store as StorePro;

class StoreControllerPro extends Controller
{

    /**
     * Редактировать точку продаж
     *
     * @param StorePro $store
     * @return \Illuminate\Http\Response
     */
    public function edit( StorePro $store )
    {
        return view('vue', [
            'params'    => [
                'item' => $store->load('region', 'type')->toArray(),
            ],
            'viewModel' => 'vm-stores-pro-edit',
        ]);
    }

    /**
     * Сохранить изменения
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, StorePro $store )
    {
        $v = Validator::make($request->input(), [
            'id'             => 'required|exists:pgsql_pro.stores,id',
            'name'           => 'required|string|max:255',
            'legal_name'     => 'required|string|max:255',
            'bin'            => 'required|numeric',
            'license_number' => 'required|string|max:255',
            'city'           => 'required|string|max:255',
            'street'         => 'required|string|max:255',
            'house'          => 'required|string|max:10',
            'region'         => 'required|exists:pgsql_pro.regions,id',
            'type'           => 'required|exists:pgsql_pro.store_types,id',
            'latitude'       => 'required|numeric',
            'longitude'      => 'required|numeric',
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        $store->name = $request->input('name');
        $store->legal_name = $request->input('legal_name');
        $store->bin = $request->input('bin');
        $store->license_number = $request->input('license_number');
        $store->street = $request->input('street');
        $store->house = $request->input('house');
        $store->region_id = $request->input('region');
        $store->store_type_id = $request->input('type');
        $store->latitude = $request->input('latitude');
        $store->longitude = $request->input('longitude');

        return $store->save()
            ? response('saved', 200)
            : response('Error while saving store', 500);
    }
}
