<?php

namespace App\Http\Controllers\Pro;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Wipon\Models\Pro\Subscription;
use Wipon\Repos\Pro\SubscriptionRepositoryPro;

class SubscriptionControllerPro extends Controller
{
    /**
     * @var SubscriptionRepositoryPro
     */
    private $repositoryPro;

    public function __construct( SubscriptionRepositoryPro $repositoryPro )
    {
        $this->repositoryPro = $repositoryPro;

        $this->middleware('ajax', ['except' => ['index']]);
    }

    /**
     * Список подписок
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {
            return $this
                ->repositoryPro
                ->filter($request->all())
                ->paginate($this->repositoryPro->perPage());
        }

        return view('vue', [
            'viewModel' => 'vm-subscriptions-pro-index',
        ]);
    }

    /**
     * Сохранить новую подписку
     *
     * @param Request $request
     * @return
     * @throws \Exception
     */
    public function store( Request $request )
    {
        $v = Validator::make($request->input(), [
            'user_id' => "required|numeric|exists:pgsql_pro.users,id",
            'type'    => "required|string|in:subscribe_per_month,subscribe_per_year",
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        $data = [
            'user_id'   => $request->input("user_id"),
            'is_active' => true,
        ];

        /***
         * @var Carbon $now
         */
        $now = carbon_user_tz_now();

        switch ($request->input('type', null)) {
            case 'subscribe_per_month':
                $data['expires_at'] = $now->addDays(30);
                break;
            case 'subscribe_per_year':
                $data['expires_at'] = $now->addDays(365);
                break;
            default:
                throw new \Exception('Subscription type was excepted, but null given!!!');
                break;
        }

        return Subscription::create($data)
            ? response('saved', 200)
            : response('error', 200);
    }

    /**
     * Сохранить отредактированную подписку
     *
     * @param  \Illuminate\Http\Request $request
     * @param Subscription $subscription
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update( Request $request, Subscription $subscription )
    {
        $v = Validator::make($request->all(), [
            'expires_at' => "required:date_format:d.m.Y H:i",
            'status'  => "required|in:active,not_active",
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        $subscription->is_active = $request->input('status') === 'active';
        $subscription->expires_at = Carbon::createFromFormat('d.m.Y H:i', $request->input('expires_at') );

        return $subscription->save()
            ? response('saved', 200)
            : response('error', 200);
    }

    /**
     * Деактивация подписки
     *
     * @param Subscription $subscription - Подписка
     * @return mixed
     */
    public function deactivate( Subscription $subscription )
    {
        $subscription->is_active = false;

        return $subscription->save()
            ? response(['status' => 'success'], 200)
            : response(['status' => 'error'], 500);
    }
}
