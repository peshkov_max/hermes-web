<?php

namespace App\Http\Controllers\Pro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Wipon\Repos\Pro\NotificationRepositoryPro;

class NotificationControllerPro extends Controller
{
    /**
     * @var NotificationRepositoryPro
     */
    private $notificationPro;

    public function __construct( NotificationRepositoryPro $notificationPro )
    {
        $this->notificationPro = $notificationPro;
    }

    /**
     * Список отправленных уведомлений
     *
     * @param Request $request
     * @return mixed
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {
            return $this->notificationPro->filter($request->all())->paginate($this->notificationPro->perPage());
        }

        return view('vue', [
                'viewModel' => 'vm-notifications-pro',
                'params'    => [
                    'user_id' => $request->user_id,
                ],
            ]
        );
    }
}