<?php namespace App\Http\Controllers;

use App\Http\Requests\RoleAssignationRequest;
use App\Http\Requests\RoleFormRequest;
use Illuminate\Http\Request;
use Wipon\Models\Role;
use Wipon\Repos\RoleRepository;
use Wipon\Repos\UserRepository;

class RoleController extends Controller
{

    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    private $roles;

    public function __construct( UserRepository $users, RoleRepository $roles )
    {
        $this->users = $users;
        $this->roles = $roles;

        $this->middleware('ajax', [
            'only' => [
                'search', 'attachUser', 'detachUser'
            ],
        ]);
    }

    /**
     * Показать список ролей
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index( Request $request )
    {
        if ( ! $request->ajax()) {
            return view('vue', ['viewModel' => 'vm-role-index']);
        }

        if (has_i('search')) {
            $q = str_for_search(get_i('search'));

            return $this->roles->getModel()->where(function ( $query ) use ( $q ) {

                $query->where('name', 'ilike', $q)
                    ->orWhere('display_name', 'ilike', $q)
                    ->orWhere('description', 'ilike', $q);
            })->with('users')->paginate($this->roles->perPage());
        }

        return $this->roles->getModel()->with('users')->paginate($this->roles->perPage());
    }


    /**
     * Поиск роли
     *
     * @param Request $request
     * @return mixed
     */
    public function search( Request $request )
    {
        if ( ! $request->search) {
            abort(409);
        }

        return $this->roles->withName(str_for_search($request->search))->paginate($this->roles->perPage())->toJson();
    }

    /**
     * Отобразить роль и всех пользователей, которым назначена данная роль
     *
     * @param  Role $role
     * @return
     */
    public function show( Role $role )
    {
        return view('vue', [
            'params'    => [
                'item'  => $role->load('users')->toArray(),
                'users' => $this->users->getModel()->where('is_testing', false)->get()->toArray(),
            ],
            'viewModel' => 'vm-role-show',
        ]);
    }

    /**
     * Создать роль
     *
     * @return
     */
    public function create()
    {
        return view('vue', ['viewModel' => 'vm-role-create']);
    }

    /**
     * Сохранить роль
     *
     * @param RoleFormRequest $roleRequest
     * @return
     */
    public function store( RoleFormRequest $roleRequest )
    {
        return $this->roles->create($roleRequest->except(['_token', 'id']))
            ? response('stored', 200)
            : response('error', 500);
    }


    /**
     * Редактировать роль
     *
     * @param Role $role ,
     */
    public function edit( Role $role )
    {
        return view('vue', [
            'params'    => [
                'item' => $role->toArray(),
            ],
            'viewModel' => 'vm-role-edit',
        ]);
    }


    /**
     * Изменить роль
     *
     * @param RoleFormRequest $roleRequest
     * @return
     */
    public function update( RoleFormRequest $roleRequest )
    {
        $this->roles->update($roleRequest->id, $roleRequest->only(['name', 'display_name', 'description']));

        return response('stored', 200);
    }

    /**
     * Удалить роль
     *
     * @param Role $role ,
     * @return View
     */
    public function destroy( Role $role )
    {
        if ($role->users()->count() > 0) {
            return response('hasUsers', 200);
        }

        try {

            $role->forceDelete();

            return response('success', 200);
        } catch (\Exception $e) {
            log_e('Error while deleting role', [$e->getMessage()]);

            return response('error', 500);
        }
    }

    /**
     * Назначить роль пользователю
     *
     * @param RoleAssignationRequest $request
     * @return string
     */
    public function attachUser( RoleAssignationRequest $request )
    {
        $role = $this->roles->findById($request->id);

        $user = $this->users->findById($request->user_id);

        if ($user->hasRole($role->name))
            return 'user_already_has_role';

        $user->attachRole($role);

        return response($user, 200);
    }

    /**
     * Открепить роль от пользователя
     *
     * @param RoleAssignationRequest $request
     * @return string
     */
    public function detachUser( RoleAssignationRequest $request )
    {
        $role = $this->roles->findById($request->id);
        $user = $this->users->findById($request->user_id);

        if ($user->id === auth()->user()->id) {
            log_i('You are authorized as user ' . $user->name . ', so you can not delete yourself!');

            return response('user_cant_be_detached', 200);
        }

        if ($user->name === 'wipon' || $user->name === 'wipon-dev')
            return response('user_cant_be_detached', 200);

        if ( ! $user->hasRole($role->name))
            return response('user_already_detached', 200);

        $user->detachRole($role);

        return response($user, 200);
    }

}
