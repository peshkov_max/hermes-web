<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Wipon\Models\User;
use Wipon\Repos\OrganizationRepository;
use Wipon\Repos\RoleRepository;
use Wipon\Repos\UserRepository;

class UserController extends Controller
{


    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @var OrganizationRepository
     */
    protected $organizations;

    /**
     * UserController constructor.
     * @param UserRepository $users
     * @param RoleRepository $roles
     * @param OrganizationRepository $organizations
     */
    public function __construct(
        UserRepository $users,
        RoleRepository $roles,
        OrganizationRepository $organizations
    )
    {
        $this->users = $users;
        $this->roles = $roles;
        $this->organizations = $organizations;
    }

    /**
     * Отобразить список пользователей
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $this->authorize('index');

        if ($request->ajax()) {
            return $this->users->filter($request->all())->paginate($this->users->perPage());
        }

        return view('vue', [
            'params'    => [
                'role' => $request->has('roleId') ? $this->roles->findById($request->roleId)->toArray() : null,
                'roles'  => $this->roles->getModel()->select('id', 'display_name')->get()->toArray(),
            ],
            'viewModel' => 'vm-user-index',
        ]);
    }


    /**
     * Открыть форму добавления пользователя
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create');

        return view('vue', ['viewModel' => 'vm-user-create',]);
    }

    /**
     * Сохранить пользователя
     *
     * @param Requests\UserFormRequest $userFormRequest
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function store( Requests\UserFormRequest $userFormRequest )
    {
        $this->authorize('store');

        $this->users->create($userFormRequest->only(["name", "email", "password", "organization", "roles"]));

        return response('stored', 200);
    }


    /**
     * Открыть форму редактирования пользователя
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function edit( User $user )
    {
        $this->authorize('edit', $user);

        return view('vue', [
            'params'    => [
                'item' => $user->load('organization', 'roles')->toArray(),
            ],
            'viewModel' => 'vm-user-edit',
        ]);
    }

    /**
     * Сохранить редактируемого пользователя
     *
     * @param Requests\UserFormRequest $userFormRequest
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update( Requests\UserFormRequest $userFormRequest, User $user )
    {
        $this->authorize('update', $user);

        $this->users->update((int) $user->id, $userFormRequest->only(["name", "email", "password", "organization", "roles", "change_password"]));

        return response('stored', 200);
    }

    /**
     * Удалить пользователя
     *
     * @param  User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy( User $user )
    {
        $this->authorize('destroy');

        if ($user->id === auth()->user()->id) {
            return response('You are authorized as user ' . $user->name . ', so you can not delete yourself!', 409);
        }

        return $this->users->delete((int) $user->id) ? response('success', 200) : response('error', 200);
    }

    /**
     * Открыть форму изменения пароля
     *
     * @param User $user
     * @return mixed
     */
    public function getPassword( User $user )
    {
        $this->authorize('update_password', $user);

        return view('vue', [
            'params'    => [
                'item' => $user->toArray(),
            ],
            'viewModel' => 'vm-password',
        ]);
    }

    /**
     * Установить новый пароль
     *
     * @param Requests\ChangePasswordRequest $request
     * @param User $user
     * @return mixed
     */
    public function setPassword( Requests\ChangePasswordRequest $request, User $user )
    {
        $this->authorize('update_password', $user);

        return $request->passwordChanged($user)
            ? response('stored', 200)
            : response('error', 500);
    }

}
