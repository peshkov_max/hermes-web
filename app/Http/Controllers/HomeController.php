<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Validator;
use Wipon\Repos\SupportRepository;
use Wipon\Services\SlackSupport;
use Wipon\Services\StatisticService;

class HomeController extends Controller
{
    /**
     * @var SupportRepository
     */
    private $feedback;

    /**
     * Create a new controller instance.
     *
     * @param SupportRepository $feedback
     */
    public function __construct( SupportRepository $feedback )
    {
        $this->feedback = $feedback;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->user()->hasRole('super-admin'))
            return redirect(route('home.dashboard'));

        if ($request->user()->hasRole(['product-moderator', 'commercial-admin']))
            return redirect(route('products.index'));

        if ($request->user()->hasRole(['inspector-admin', 'inspector']))
            return redirect(route('checks.map'));

        if ($request->user()->hasRole('support'))
            return redirect(route('supports.index'));

        if ($request->user()->hasRole(['receipt-moderator','receipt-moderator-admin']))
            return redirect(url('receipts/moderate'));

        if ($request->user()->hasRole('npp'))
            return redirect(url('receipts/registry'));

        if ($request->user()->hasRole('organization-moderator'))
            return redirect(route('organizations.index'));

        return view('home');
    }

    /** Page for developer purposes
     *
     * @return mixed
     */
    public function dashboard( )
    {
        $statService = new StatisticService();
        
        return view('vue', [
            'viewModel' => 'vm-dashboard',
            'params'    => [
                'deviceStat'  => $statService->getDeviceStat(),
                'clusterStat' => $statService->getClusterStat(),
                'productStat' => $statService->getProductItemStat(),
            ],
        ]);
    }

    /**
     * @return mixed
     */
    public function postSendFeedback()
    {
        list($name, $phone_number, $email, $message, $v) = $this->validateFeedBackInput();

        if ($v->fails())
            return response([
                'messages' => $v->messages(),
            ], 403);

        $this->storeFeedbackMessageInDB($name, $phone_number, $email, $message);

        list($email, $support_email, $topic, $data)  = $this->configureFeedbackMailData($email, $message, $name, $phone_number);
        
        Mail::queueOn('cabinet_email', 'emails.feedback', $data, function ( $message ) use ( $topic, $email, $support_email ) {
            $message->from('partnership@wiponapp.com', config('mail.from.name'));
            $message->to( in_array(config('app.env'), ['local', 'testing', 'staging']) ? config('wipon.dev_email') :  $support_email)
                ->subject($topic);
        });

        if (! in_array(config('app.env'), ['local', 'testing', 'staging']) ) {
            app('support_slack')->sendFeedback($data);
        }

        return response('success', 200);
    }


    /**
     * Send feedback
     *
     * @throws \Exception
     * @throws \Throwable
     */
    public function postSuggestion()
    {
        $suggestion = get_i('suggestion');

        $v = Validator::make(['mess' => $suggestion,], ['mess' => 'required|max:300',]);

        if ($v->fails()) {
            return response('Input is not wrong!', 400);
        }

        list($support_email, $topic, $data) = $this->configureSuggestionData($suggestion);

        Mail::queueOn('cabinet_email', 'emails.suggestion', $data, function ( $message ) use ( $support_email, $topic ) {
            $message->from('partnership@wiponapp.com', config('mail.from.name'));
            $message->to(in_array(config('app.env'), ['local', 'testing', 'staging']) ? config('wipon.dev_email') :  $support_email)
                ->subject($topic);
        });

        if (! in_array(config('app.env'), ['local', 'testing', 'staging']) ) {
            app('support_slack')->sendSuggestion($data);
        }
        
        return response('success', 200);
    }

    /**
     * @param $email
     * @param $message
     * @param $name
     * @param $phone_number
     * @return array
     */
    private function configureFeedbackMailData( $email, $message, $name, $phone_number )
    {
        $email = trim($email);
        $support_email = config('wipon.support_email');
        $topic = trans('app.feedback_email');
        
        /** @var Carbon $dateTime */
        $dateTime = carbon_user_tz_now();

        $data = [
            'text'     => $message,
            'time'     => $dateTime->format('h:m'),
            'date'     => $dateTime->format('d.m.Y'),
            'userName' => $name,
            'email'    => $email,
            'phone'    => $phone_number,
        ];

        return [$email, $support_email, $topic, $data];
    }

    /**
     * @param $suggestion
     * @return array
     */
    private function configureSuggestionData( $suggestion )
    {
        $support_email = config('wipon.support_email');
        $topic = trans('app.suggestion');
        
        /** @var Carbon $dateTime */
        $dateTime = carbon_user_tz_now();

        $data = [
            'text' => $suggestion,
            'time' => $dateTime->format('h:m'),
            'date' => $dateTime->format('d.m.Y'),
        ];

        return [$support_email, $topic, $data];
    }

    /**
     * @return array
     */
    private function validateFeedBackInput()
    {
        $name = get_i('name');
        $phone_number = get_i('phone_number');
        $email = get_i('email');
        $message = get_i('message');

        $v = Validator::make([
            'mess'  => $message,
            'email' => $email,
        ], [
            'mess'  => 'required|max:300',
            'email' => 'required|email',
        ]);

        return [$name, $phone_number, $email, $message, $v];
    }

    private function storeFeedbackMessageInDB( $name, $phone_number, $email, $message )
    {
        $this->feedback->create(['name' => $name, 'phone_number' => $phone_number, 'email' => $email, 'text' => $message]);
    }
}
