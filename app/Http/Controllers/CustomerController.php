<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Wipon\Repos\CustomerRepository;

class CustomerController extends Controller
{
    /**
     * @var CustomerRepository
     */
    private $customer;

    public function __construct( CustomerRepository $customer)
    {
        $this->customer = $customer;

        $this->middleware('ajax', ['only' => ['update',]]);
    }

    /**
     * Отобразить список пользователей
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ( ! $request->ajax()) {

            return view('vue', [
                'params'    => [

                    // НЕ ОЧЕВИДНАЯ ЛОГИКА
                    // Если в параметрах передан deviceUUID, то необходимо 
                    // установить значение фильтра чтобы при первом ajax запросе 
                    // осуществился поиск устройства deviceUUID.
                    // Для осуществления этой логики прокидывается параметр deviceUUID
                    'phone_number' => $request->phone_number,
                ],
                'viewModel' => 'vm-support-customers',
            ]);
        }

        return $this->customer->filter($request->all())->paginate($this->customer->perPage());
    }


    /**
     * Открыть форму редактирования пользователей
     *
     * @param $id
     * @return mixed
     */
    public function edit( $id )
    {
        return view('vue', [
            'params'    => ['item' => $this->customer->findById($id)->toArray(),],
            'viewModel' => 'vm-support-customer',
        ]);
    }

    /**
     * Обновить пользователя
     *
     * @param Request $request
     */
    public function update( Request $request)
    {
        $v = Validator::make($request->input(), [
            'id'           => "required|exists:customers,id",
            'name'         => "required|string|max:255",
            'phone_number' => "required|max:255|unique:customers,phone_number," . $request->input('id'),
            'email'        => "email|max:255",
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        return $this->customer->update($request->input('id'), $request->except('id','_method'))
            ? response('saved', 200)
            : response('Error while saving', 500);
    }
}