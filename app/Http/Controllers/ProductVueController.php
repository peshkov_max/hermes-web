<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Wipon\Models\Product;
use Wipon\Repos\OrganizationRepository;
use Wipon\Repos\ProductRepository;
use Wipon\Repos\ProductTypeRepository;

class ProductVueController extends Controller
{

    /**
     * @var ProductTypeRepository
     */
    protected $product_types;

    /**
     * @var ProductRepository
     */
    protected $products;

    /**
     * @var OrganizationRepository
     */
    protected $organizations;

    public function __construct(
        ProductRepository $productRepository,
        ProductTypeRepository $product_types,
        OrganizationRepository $organizations )
    {
        $this->products = $productRepository;
        $this->product_types = $product_types;
        $this->organizations = $organizations;

        $this->middleware('ajax', [
            'only' => [
                'types', 'store', 'update'
            ],
        ]);

        $this->middleware('role:super-admin', [
            'only' => [
                'store', 'create', 
            ],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ($request->ajax())
        {
            return $this->products->filter($request->all())->paginate($this->products->perPage());
        }

        return view('vue', [
            'viewModel' => 'vm-product-index',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vue', ['viewModel' => 'vm-product-create']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\ProductVueRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store( Requests\ProductVueRequest $request )
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $product = $this->products->create([
            'product_type_id' => $request->getProductType(),
            'barcode'         => $request->barcode,
            'name_en'         => $request->name_en,
            'name_ru'         => $request->name_ru,
            'name_kk'         => $request->name_kk,
            'legal_name'      => $request->legal_name,
            'is_enabled'      => $request->is_enabled,
            'is_moderated'    => $request->is_moderated,
            'organization_id' => $request->getOrganizationId(),
            'common_info'     => $request->getCommonInfo(),
        ]);

        if ( ! is_null($product)) {
            $request->handleImages('store', $product);

            return response($product, 200);
        }

        return response('Product was not saved', 500);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show( $id )
    {
        $product = $this->products->findById($id);
        
        return Input::has('visual_features')
            ? view('products.show', [
                'product'         => $product->load('organization', 'type'),
                'visual_features' => Input::get('visual_features')])
            : view('products.show', ['product' => $product->load('organization', 'type')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     * @internal param null $previous_route
     */
    public function edit( $id )
    {
        return view('vue',  [
            'params'    => [
                'item' => $this->products->findById($id)->load('organization', 'image', 'background_image')->toArray(),
            ],
            'viewModel' => 'vm-product-edit']);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Requests\ProductVueRequest $formRequest
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update( Requests\ProductVueRequest $formRequest)
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $product = $this->products->update( $formRequest->product_id, [
            'barcode'         => $formRequest->barcode,
            'name_en'         => $formRequest->name_en,
            'name_ru'         => $formRequest->name_ru,
            'name_kk'         => $formRequest->name_kk,
            'legal_name'      => $formRequest->legal_name,
            'is_enabled'      => $formRequest->is_enabled,
            'is_moderated'    => $formRequest->is_moderated,
            'product_type_id' => $formRequest->getProductType(),
            'organization_id' => $formRequest->getOrganizationId(),
            'common_info'     => $formRequest->getCommonInfo(),
        ]);
       
        if ( ! is_null($product)) {
            
            $formRequest->handleImages('update', $product);

            return response($product, 200);
        }

        return response('success', 200);
    }

    /** Get all product types
     * @return string
     */
    public function types( )
    {
        return $this->product_types->getModel()->with('children')->get()->toArray();
    }
   
}
