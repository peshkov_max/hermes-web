<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Wipon\Models\ApiRequest;
use Wipon\Models\Check;
use Wipon\Models\Device;
use Wipon\Repos\DeviceRepository;
use Log;

class DeviceController extends Controller
{
    /**
     * @var DeviceRepository
     */
    private $devices;

    /**
     * DeviceController constructor.
     * @param DeviceRepository $devices
     */
    public function __construct( DeviceRepository $devices )
    {
        $this->devices = $devices;
        
        $this->middleware('ajax', ['only' => ['versions',]]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {

            $this->validate($request, [
                'period'             => 'array',
                'period.start.value' => 'date_format:' . config('wipon.date_time_format'),
                'period.end.value'   => 'date_format:' . config('wipon.date_time_format'),
                'search'             => 'array',
                'sort'               => 'array',
            ]);

            return $this->devices->filter($request->all())->paginate($this->devices->perPage());
        }

        return view('vue', ['viewModel' => 'vm-device-index']);
    }


    /**
     * Display the specified resource.
     * @param $uuid
     * @return \Illuminate\Http\Response
     */
    public function seeLogs( $uuid )
    {
        /** @var $item Device */
        $item = $this->devices->findById($uuid)->load('latest_api_request', 'customer');

        return view('vue', [
            'viewModel' => 'vm-device-logs',
            'params'    => [
                'messages_count' => $this->devices->getCountDeviceMessages($uuid),
                'item'           => $item->toArray(),
            ],
        ]);
    }

    /**
     * Get Device requests.
     *
     * @param Request $request
     */
    public function getDeviceApiRequests( Request $request )
    {
        if ( ! $request->ajax())
        {
            return response('Requests type not allowed. Ajax request type was excepted!', 400);
        }

        $this->validate($request, ['device_uuid' => 'required|exists:devices,uuid']);

        return ApiRequest::whereDeviceUuid($request->device_uuid)
            ->orderBy('created_at', 'desc')
            ->paginate($this->devices->perPage());
    }

    /**
     * Get Device requests which contains only checks.
     * 
     * @param Request $request
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getDeviceChecks( Request $request )
    {
        if ( ! $request->ajax()) {
            return response('Requests type not allowed. Ajax request type was excepted!', 400);
        }

        $this->validate($request, ['device_uuid' => 'required|exists:devices,uuid']);

        return Check::whereDeviceUuid($request->device_uuid)
            ->with('item','cluster.city', 'cluster.type')
            ->get();
    }

    /** Get devices versions
     *
     * @return string
     */
    public function versions( )
    {
        return $this->devices->getModel()->select('app_version')->groupBy('app_version')->orderBy('app_version')->get();
    }
}

