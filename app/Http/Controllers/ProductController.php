<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use Validator;
use Wipon\Models\Product;
use Wipon\Repos\OrganizationRepository;
use Wipon\Repos\ProductRepository;
use Wipon\Repos\ProductTypeRepository;

class ProductController extends Controller
{

    /**
     * @var ProductTypeRepository
     */
    protected $product_types;

    /**
     * @var OrganizationRepository
     */
    protected $organizations;

    /**
     * @var ProductRepository
     */
    protected $products;

    public function __construct(
        ProductRepository $productRepository,
        ProductTypeRepository $product_types,
        OrganizationRepository $organizations )
    {
        $this->products = $productRepository;
        $this->product_types = $product_types;
        $this->organizations = $organizations;

        $this->middleware('role:super-admin', [
            'only' => [
                'store', 'create', 'merge'
            ],
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Requests\ProductIndexRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index( Requests\ProductIndexRequest $request )
    {
        if ($request->ajax()) {
            return $this->products->filter($request->all())->paginate($this->products->perPage());
        }

        return view('vue', [
            'viewModel' => 'vm-product-index',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product_types = $this->product_types->getModel()->all();

        return view('products.wizard', compact('product_types'));
    }
    
    /** Get all product types
     *
     * @param Request $request
     */
    public function types( Request $request )
    {
        if ($request->ajax()) {
            return $this->product_types->getModel()->with('children')->get()->toArray();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\ProductFormRequest $formRequest
     * @return \Illuminate\Http\Response
     */
    public function store( Requests\ProductFormRequest $formRequest )
    {
        $product = $formRequest->persist();
        
        return response($product, 200);
   }

    /**
     * Display the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show( Product $product )
    {
        return Input::has('visual_features')

            ? view('products.show', [
                'product'         => $product->load('organization', 'type'),
                'visual_features' => Input::get('visual_features')])

            : view('products.show', ['product' => $product->load('organization', 'type')]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @internal param null $previous_route
     */
    public function edit( Product $product )
    {
        $previous_route = get_i('previous_route');

        $product_types = $this->product_types->getModel()->all();

        return view('products.update', compact('product', 'product_types', 'previous_route'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\ProductFormRequest $formRequest
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update( Requests\ProductFormRequest $formRequest, Product $product )
    {
        $product = $formRequest->update($product);

        if ($formRequest->ajax())
            return response($product, 200);

        if ($this->needRedirection($formRequest, 'products.index'))
            return Session::has('productIndexPageUrl') ? redirect(Session::get('productIndexPageUrl')) : redirect(route('products.index'));

        if ($this->needRedirection($formRequest, 'products.show'))
            return redirect(route('products.show', [$product->id]));

        if ($this->needRedirection($formRequest, 'app.edit')) {
            return redirect($formRequest->go_to);
        }

        return Session::has('productIndexPageUrl') ? redirect(Session::get('productIndexPageUrl')) : redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Product $product
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy( Product $product )
    {
        $product->delete();

        /** @noinspection PhpUndefinedMethodInspection */
        return redirect()->route('products.index');
    }

    /** Save products image
     * @param Requests\ProductFormRequest $formRequest
     * @return bool
     */
    public function saveImage( Requests\ProductFormRequest $formRequest )
    {
        if ($formRequest->ajax()) {
            return $formRequest->storeWizardImages();
        }

        abort(405);
    }


    /** Save products image
     * @param Request $request
     * @return bool
     */
    public function setStatus( Request $request )
    {
        /** @noinspection PhpUndefinedFieldInspection */
        $product_ids = json_decode($request->product_ids);

        /** @noinspection PhpUndefinedFieldInspection */
        $v = Validator::make(
            [
                'field'       => $request->field,
                'status'      => $request->status,
                'product_ids' => $product_ids,
            ],
            [
                'field'       => 'required',
                'status'      => 'required',
                'product_ids' => 'required|array',
            ]);

        if ($v->fails()) {
            /** @noinspection PhpUndefinedMethodInspection */
            return redirect()->back()->withInput(Input::all())->withErrors($v);
        }

        /** @noinspection PhpUndefinedFieldInspection */
        $field = $request->field;
        /** @noinspection PhpUndefinedFieldInspection */
        $status = $request->status;

        if ($field === 'is_moderated') {
            if ($status === 'true') Product::whereIn('id', $product_ids)->update(['is_moderated' => true]);
            if ($status === 'false') Product::whereIn('id', $product_ids)->update(['is_moderated' => false]);
        }

        if ($field === 'is_enabled') {
            if ($status === 'true') Product::whereIn('id', $product_ids)->update(['is_enabled' => true]);
            if ($status === 'false') Product::whereIn('id', $product_ids)->update(['is_enabled' => false]);
        }

        /** @noinspection PhpUndefinedMethodInspection */
        return Session::has('productIndexPageUrl') ? redirect(Session::get('productIndexPageUrl')) : redirect()->back();
    }

    public function merge( Request $request )
    {
        if ($request->ajax()) {

            /** @noinspection PhpUndefinedFieldInspection */
            $v = Validator::make(
                [
                    'masterId'  => $request->masterId,
                    'slavesIds' => $request->slavesIds,
                ],
                [
                    'masterId'    => 'required|exists:products,id',
                    'slavesIds.*' => 'required|exists:products,id',
                ]);

            if ($v->fails()) {
                return response($v->messages(), 400);
            }

            return response(
                $this->products->merge($request->masterId, $request->slavesIds)
                    ? 'successfully_merged'
                    : 'error_while_merging', 200);
        }

        /** @noinspection PhpUndefinedFieldInspection */
        $v = Validator::make(['id' => $request->id], ['id' => 'required|exists:products,id']);

        if ($v->fails()) {
            return response($v->messages(), 400);
        }

        return view('vue', [
            'viewModel' => 'vm-product-merge',
            'params'    => [
                'product' => $this->products->findById($request->id)->load('visual_features')->toArray(),
            ],
        ]);
    }
}
