<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Response;
use URL;
use Wipon\Repos\CheckRepository;
use Wipon\Repos\CheckStatisticRepository;
use Wipon\Services\StatisticService;

class CheckController extends Controller
{
    /**
     * @var CheckStatisticRepository
     */
    private $check_aggregation;

    /**
     * @var CheckRepository
     */
    private $checks;

    public function __construct( CheckStatisticRepository $aggregation, CheckRepository $checks )
    {
        $this->check_aggregation = $aggregation;
        $this->checks = $checks;
    }


    /** Opens stat index page and filter data via ajax
     *
     * @param Requests\CheckStatRequest $statRequest
     * @return mixed
     */
    public function stats( Requests\CheckStatRequest $statRequest )
    {
        if ($statRequest->ajax()) {
            return $statRequest->filter();
        }

        return view('vue', ['viewModel' => 'vm-check-table']);
    }

    /**
     * Export stat filtered data to Excel
     *
     * @param Requests\CheckStatRequest $statRequest
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function statExportToExcel( Requests\CheckStatRequest $statRequest )
    {
        return $statRequest->generateStatReport();
    }

    /** Update stat data
     *
     * @param Request $request
     * @return Response
     */
    public function statUpdate( Request $request )
    {
        if ($request->has('checkStatus')) {
            return response(StatisticService::kgdStatIsUpdatingNow() ? 'updating' : 'finished', 200);
        }

        if (StatisticService::kgdStatIsUpdatingNow()) {
            return response('alreadyGenerating', 200);
        }

        StatisticService::regenerateKGDStat();

        return response('started', 200);
    }

    /** Open map view
     *
     * @param Requests\CheckMapRequest $mapRequest
     * @return mixed
     */
    public function map( Requests\CheckMapRequest $mapRequest )
    {
        list($currentRegion, $lastStatUpdateDateTime) = $mapRequest->getCurrentRegionAndStatUpdate();

        return view('vue', ['viewModel' => 'vm-check-maps', 'params' => [
            'currentRegionId'        => $currentRegion->id,

            // статус обновления статистики
            'updateInProgress'       => StatisticService::kgdStatIsUpdatingNow(),
            'regions'                => $mapRequest->getKazakhstanRegion(),
            'lastStatUpdateDateTime' =>
                gettype($lastStatUpdateDateTime) == "string"
                    ? $lastStatUpdateDateTime
                    : $lastStatUpdateDateTime->format(config('wipon.dt_format_d_m_y_h_i')),
            'mapCenter'              => [
                'latitude'  => $currentRegion->regional_center->latitude ?? config('wipon.region.default.latitude'),
                'longitude' => $currentRegion->regional_center->longitude ?? config('wipon.region.default.longitude'),
            ],
        ]]);
    }

    /**  Handle map filter
     *
     * @param Requests\CheckMapRequest $mapRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function mapFilter( Requests\CheckMapRequest $mapRequest )
    {
        list($currentRegion, $lastStatUpdateDateTime) = $mapRequest->getCurrentRegionAndStatUpdate();

        list($clusters, $checkSum) = $mapRequest->doFilter();

        return Response::json([
            'checkSum'               => $checkSum,
            'points'                 => $clusters->toArray(),
            'currentRegionId'        => $currentRegion->id,
            'lastStatUpdateDateTime' =>
                gettype($lastStatUpdateDateTime) == "string"
                    ? $lastStatUpdateDateTime
                    : $lastStatUpdateDateTime->format(config('wipon.dt_format_d_m_y_h_i')),
            'mapCanter'              => [
                'latitude'  => $currentRegion->regional_center->latitude ?? config('wipon.region.default.latitude'),
                'longitude' => $currentRegion->regional_center->longitude ?? config('wipon.region.default.longitude'),
            ],
        ]);
    }

    /** Export already filtered data to excel
     *
     * @param Requests\CheckMapRequest $mapRequest
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function mapExportToExcel( Requests\CheckMapRequest $mapRequest )
    {
        return $mapRequest->getAllRegionReport();
    }

    /** Export KGD competition data to excel
     *
     * @param Requests\CheckMapRequest $mapRequest
     * @return array|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function mapCompetitionExport( Requests\CheckMapRequest $mapRequest )
    {
        if ($mapRequest->ajax()) {

            // нам нужен список годов от 2015 до текущего
            $years = [];
            for ($start = 2015;$start <= Carbon::now()->year;$start++) {
                $years [] = $start;
            }

            $this->validate($mapRequest, [
                'year'     => 'required|in:' . implode(',', $years),
                'month'    => 'required|numeric:min:1|max:12',
                'regionId' => 'required|exists:regions,id',
            ]);

            list($filePath, $fileName) = $mapRequest->getKgdCompetitionExcel();

            return [
                'status' => 'dataFormed',
                'url'    => url('checks/map-competition-export') . '?filePath=' . $filePath . '&fileName=' . $fileName,
            ];
        }

        $this->validate($mapRequest, ['filePath' => 'required', 'fileName' => 'required']);

        return Response::download($mapRequest->filePath . '/' . $mapRequest->fileName, $mapRequest->fileName, [
            'Content-Length: ' . filesize($mapRequest->filePath),
        ]);
    }

    /** Open stat detail per cluster
     *
     * @param Request $request
     * @return mixed
     */
    public function detail( Request $request )
    {
        $this->validate($request, ['cluster_id' => 'exists:clusters,id']);

        $check = $this->check_aggregation->findByClusterId($request->cluster_id);

        $checks = $this->checks->getChecksByClusterId($request->cluster_id, $request->all());

        if ($request->ajax()) {
            return $checks->paginate(50);
        }

        // сделано на случай, если ссылка на текущую страницу передается
        // в чате(или через другой канал коммуникации) и переход назад должен вести на карту сканировании
        $previousUrl = URL::previous();
        $parsedUrl = parse_url($previousUrl);

        return view('vue', [
            'viewModel' => 'vm-check-detail',
            'params'    => [
                'map_url'           => (isset($parsedUrl['path']) && $parsedUrl['path'] === "/checks/map") ? $previousUrl : route('checks.map'),
                'check_aggregation' => $check->toArray(),
                'checks'            => $checks->paginate(50),
            ]]);
    }

    /** Export cluster detail
     *
     * @param Requests\CheckMapRequest $mapRequest
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function detailExport( Requests\CheckMapRequest $mapRequest )
    {
        $this->validate($mapRequest, ['cluster_id' => 'exists:clusters,id']);

        return $mapRequest->generateDetailReport();
    }


}
