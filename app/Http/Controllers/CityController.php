<?php

namespace App\Http\Controllers;

use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Validator;
use Wipon\Models\Alias;
use Wipon\Models\City;
use Wipon\Models\Country;
use Wipon\Models\Region;
use Wipon\Repos\CityRepository;

class CityController extends Controller
{
    protected $cities;


    public function __construct( CityRepository $cities )
    {
        $this->cities = $cities;
    }

    public function validationRules()
    {
        return [
            'region_id'          => 'required|integer',
            'name_ru'            => 'required|string',
            'name_en'            => 'required|string',
            'name_kk'            => 'required|string',
            'timezone'           => 'string',
            'longitude'          => 'numeric',
            'latitude'           => 'numeric',
            'is_regional_center' => 'boolean',
        ];
    }

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $filter = $request->reset ? [
                'search'     => null,
                'sort'       => null,
                'checkboxes' => null,
            ] : [
                'search'     => $request->search,
                'sort'       => $request->sort,
                'checkboxes' => $request->checkboxes,
            ];

            $cities = $this->cities->filter($filter);

            return $cities ? $cities->paginate(20)->appends($filter) : $cities;
        }

        return view('vue', ['viewModel' => 'vm-city-index']);
    }

    public function edit( City $city )
    {
        return view('vue', [
            'viewModel' => 'vm-city-edit',
            'params'    => [
                'item' => $city->load('region.country'),
            ],
        ]);
    }

    public function create( Request $request )
    {
        $v = Validator::make($request->input(), [
            'countryId' => "integer|exists:countries,id",
            'regionId'  => "integer|exists:regions,id,country_id,{$request->input('countryId')}",
            'noReturn'  => "boolean",
        ]);

        $region = null;
        if ( ! $v->fails()) {
            if ($request->input('regionId')) {
                $region = Region::whereId($request->input('regionId'))->with('country')->first();
            } elseif ($request->input('countryId')) {
                $region = [
                    'country' => Country::find($request->input('countryId')),
                ];
            }
        }

        return view('vue', [
            'viewModel' => 'vm-city-create',

            // Лучше немного быдлокода здесь, чем куча быдлокода во вьюхе
            'params'    => [
                'item'     => [
                    'region'             => $region,
                    'region_id'          => $region ? $region->id : null,
                    'name_ru'            => null,
                    'name_en'            => null,
                    'name_kk'            => null,
                    'timezone'           => null,
                    'longitude'          => null,
                    'latitude'           => null,
                    'is_regional_center' => false,
                ],
                'noReturn' => $request->input('noReturn'),
            ],
        ]);
    }

    /**
     * @param Request $request
     * @param City $city
     * @return array|void
     */
    public function update( Request $request, City $city )
    {
        $v = Validator::make($request->input(), $this->validationRules());

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        if ($request->input('is_regional_center')) {

            /** @var Region $region */
            $region = Region::findOrFail($request->input('region_id'));
            $region->cities()->update([
                'is_regional_center' => false,
            ]);

            $region->update([
                'regional_center_city_id' => $city->id,
            ]);
        }

        $city->update([
            'region_id'          => $request->input('region_id'),
            'name_ru'            => $request->input('name_ru'),
            'name_en'            => $request->input('name_en'),
            'name_kk'            => $request->input('name_kk'),
            'timezone'           => $request->input('timezone'),
            'longitude'          => $request->input('longitude'),
            'latitude'           => $request->input('latitude'),
            'is_regional_center' => $request->input('is_regional_center'),
        ]);

        return ['status' => 'success'];
    }

    public function store( Request $request )
    {
        $v = Validator::make($request->input(), $this->validationRules());

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        /** @var City $city */
        $city = City::create([
            'region_id'          => $request->input('region_id'),
            'name_ru'            => $request->input('name_ru'),
            'name_en'            => $request->input('name_en'),
            'name_kk'            => $request->input('name_kk'),
            'timezone'           => $request->input('timezone'),
            'longitude'          => $request->input('longitude'),
            'latitude'           => $request->input('latitude'),
            'is_regional_center' => $request->input('is_regional_center'),
        ]);

        if ($city->is_regional_center) {

            /** @var Region $region */
            $region = Region::findOrFail($city->region_id);
            $region->cities()->where('id', '<>', $city->id)->update([
                'is_regional_center' => false,
            ]);

            $region->update([
                'regional_center_city_id' => $city->id,
            ]);
        }
    }

    /**
     * @param Request $request
     * @param City $city
     * @return View
     */
    public function getMerge( City $city  )
    {
        return view('vue', [
            'viewModel' => 'vm-city-merge',
            'params'    => [
                'item' => $city,
            ],
        ]);
    }

    /**
     * @param Request $request
     * @param City $item
     * @return array|\Illuminate\Http\JsonResponse|string
     * @throws \Exception
     */
    public function postMerge( Request $request,  City $item)
    {
        $v = Validator::make($request->input(), ['city_id' => "required|integer|not_in:{$item->id}}"]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        /** @var City $city */
        $city = City::whereId($request->input('city_id'))->with('aliases')->firstOrFail();

        // Удаляем дублированные алиасы между городами
        $aliases = [];
        foreach ($city->aliases as $alias) {
            $aliases[] = $alias->name;
        }

        $item->aliases()->whereIn('name', $aliases)->delete();

        // Присваиваем алиасы текущего города к новому
        $item->aliases()->update(['aliasable_id' => $city->id]);

        // Добавляем имя текущего города как алиасы к новому
        $aliases = [];

        if ($item->name_ru) {
            $aliases[] = new Alias([
                'name' => $item->name_ru,
            ]);
        }

        if ($item->name_en) {
            $aliases[] = new Alias([
                'name' => $item->name_en,
            ]);
        }

        if ($item->name_kk) {
            $aliases[] = new Alias([
                'name' => $item->name_kk,
            ]);
        }

        if (count($aliases)) {
            $city->aliases()->saveMany($aliases);
        }

        // Привязываем кластеры текущего города к новому
        $item->clusters()->update(['city_id' => $city->id]);

        // Отвязываем текущий город как региональный центр
        Region::where('regional_center_city_id', '=', $item->id)->update([
            'regional_center_city_id' => null,
        ]);

        // Удаляем текущий город
        $item->delete();

        return ['status' => 'success'];
    }

    /**
     * Поиск города в селектах форм
     *
     * @param Request $request
     * @return mixed
     */
    public function search( Request $request )
    {

        $search = $request->input('search');
        $params = $request->input('params');
        $withoutPagination = $request->input('withoutPagination');

        $q = new City();

        if ($search) {
            $q = $q->withName($search);
        }

        if ($params) {
            $regionId = array_get(json_decode($params, true), 'regionId');

            if ($regionId) {
                $q = $q->whereRegionId($regionId);
            }
        }

        if ($withoutPagination == "true") {
            return $q->with('region')->get();
        } else {
            return $q->with('region')->paginate(20);
        }
    }

    /**
     * Получить список часовых поясов
     *
     * @return array|null
     */
    public function timezones()
    {
        $timezones = DateTimeZone::listIdentifiers(DateTimeZone::ALL);
        $timezone_offsets = [];

        foreach ($timezones as $timezone) {
            $tz = new DateTimeZone($timezone);
            $timezone_offsets[ $timezone ] = $tz->getOffset(new DateTime);
        }

        asort($timezone_offsets);

        $timezone_list = [];
        foreach ($timezone_offsets as $timezone => $offset) {
            $timezone_list[] = $timezone;
        }

        return $timezone_list;
    }

}
