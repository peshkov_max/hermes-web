<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Jobs\PushNotifications\SendPushMass;
use Illuminate\Http\Request;
use Wipon\Repos\PushNotificationRepository;
use Wipon\Repos\RegionRepository;

class PushNotificationController extends Controller
{
    /**
     * @var PushNotificationRepository
     */
    private $pushNotifications;


    public function __construct( PushNotificationRepository $push )
    {
        $this->pushNotifications = $push;
    }

    /**
     * Отправленные уведомлений
     *
     * @param Request $request
     * @return mixed
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {
            return $this->pushNotifications->filter($request->all())->paginate($this->pushNotifications->perPage());
        }

        return view('vue', ['viewModel' => 'vm-support-notifications']);
    }

    /**
     * Открыть форму отправки пуш уведомлений
     *
     * @param Request $request
     * @return mixed
     */
    public function create( Request $request )
    {
        if ($request->user()->hasRole(['support-pro']) || ($request->has('user_id') || $request->has('phone_number'))) {
            $show_pro = true;
        } else {
            $show_pro = false;
        }

        return view('vue', [
            'params'    => [
                'show_pro'     => $show_pro,
                'user_id'      => $request->user_id,
                'phone_number' => $request->phone_number,

                'uuid'    => $request->uuid,
                'message' => null,
                'regions' => (new RegionRepository)->getKazakhstanRegion()->toArray(),
            ],
            'viewModel' => 'vm-push',
        ]);
    }

    /**
     * Открыть форму отправки пуш уведомлений
     *
     * @param Requests\PushNotificationRequest $request
     * @return mixed
     */
    public function sendSingle( Requests\PushNotificationRequest $request )
    {
        if ( ! $request->requestIsValid()) {
            return response($request->requestErrors, 409);
        }

        $request->sendPushSingle();

        return response('success', 200);
    }

    /**
     * Открыть форму отправки пуш уведомлений
     *
     * @param Requests\PushNotificationRequest $request
     * @return mixed
     */
    public function sendMass( Requests\PushNotificationRequest $request )
    {
        if ( ! $request->requestIsValid()) {
            return response($request->requestErrors, 409);
        }

        $request->sendPushMass();

        return response('success', 200);
    }

    /**
     * Возвращает возможное количество времени, необходимо на отправку уведомления
     *
     * @param Requests\PushNotificationRequest $request
     * @return mixed
     */
    public function getDeviceNumber( Requests\PushNotificationRequest $request )
    {
        if ( ! $request->timeIsFree()) {
            return response($request->requestErrors, 409);
        }

        return response([
            'deviceNumber' => SendPushMass::getDeviceNumber($request->all()),
        ], 200);
    }

}
