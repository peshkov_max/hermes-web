<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Wipon\Traits\Auth\AuthenticatesUsersTrait;


/**
 * Class AuthController implements Wipon auth requirements
 *
 * @see http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5406808
 * @package App\Http\Controllers\Auth
 */
class AuthController extends Controller
{

    use AuthenticatesUsersTrait, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    
    /**
     * Redirect after logout 
     */
    protected $redirectAfterLogout = '/login';

    /**
     * Create a new authentication controller instance.
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }
    
}
