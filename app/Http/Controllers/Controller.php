<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\UrlGenerator;
use Input;
use Validator;

;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;


    /**
     * Validate the given request with the given rules.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  array $rules
     * @param  array $messages
     * @param  array $customAttributes
     * @return void
     */
    public function validate( Request $request, array $rules, array $messages = [], array $customAttributes = [] )
    {
        $validator = $this->getValidationFactory()->make($request->all(), $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            $this->throwValidationException($request->all(), $validator);
        }
    }


    /**
     * Validate the given array with the given rules.
     *
     * @param  array $array
     * @param  array $rules
     * @param  array $messages
     * @param  array $customAttributes
     * @return void
     */
    public function validateArray( array $array, array $rules, array $messages = [], array $customAttributes = [] )
    {
        $validator = $this->getValidationFactory()->make($array, $rules, $messages, $customAttributes);

        if ($validator->fails()) {
            $this->throwValidationException($array, $validator);
        }
    }


    /**
     * Throw the failed validation exception.
     *
     * @param array $array
     * @param  \Illuminate\Contracts\Validation\Validator $validator
     * @throws ValidationException
     * @internal param Request $request
     */
    protected function throwValidationException( array $array, $validator )
    {
        throw new ValidationException($validator, $this->buildFailedValidationResponse(
            $array, $this->formatValidationErrors($validator)
        ));
    }

    /**
     * Create the response for when a request fails validation.
     *
     * @param array $array
     * @param  array $errors
     * @return \Illuminate\Http\Response
     */
    protected function buildFailedValidationResponse( array $array, array $errors )
    {
        return redirect()->to(app(UrlGenerator::class)->previous())
            ->withInput($array)
            ->withErrors($errors, $this->errorBag());
    }

    /**
     * Validate Request id
     * Usually used before you want to execute searches
     * @param $id
     */
    protected function validateId( $id )
    {

        $v = Validator::make(["id" => $id], ['id' => 'required|integer']);

        if ($v->fails()) abort(400);
    }


    /**
     * @param Request $request
     * @return bool
     */
    protected function needRedirection( $request, $routeName )
    {
        return $request->has('previous_route') && $request->get('previous_route') === $routeName;
    }

}
