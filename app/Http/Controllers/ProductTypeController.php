<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Wipon\Models\ProductType;
use Wipon\Repos\ProductTypeRepository;

class ProductTypeController extends Controller
{
    /**
     * @var ProductTypeRepository
     */
    private $types;

    public function __construct( ProductTypeRepository $types )
    {
        $this->types = $types;

        $this->middleware('ajax', ['except' => ['index', 'create', 'edit', 'show']]);
    }


    /**
     * Просмотр списка типов продукта
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {

            return $this->types->filter($request->all())->with('parent')->paginate($this->types->perPage());
        }

        return view('vue', ['viewModel' => 'vm-product-type-index',]);
    }

    /**
     * Отобразить страницу добавления типа
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('vue', ['viewModel' => 'vm-product-type-create',]);
    }

    /**
     * Сохранить новый тип
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store( Request $request )
    {
        $v = Validator::make($request->all(), [
            'name_ru'   => 'max:255',
            'name_en'   => 'required|max:255',
            'name_kk'   => 'max:255',
            'parent_id' => 'exists:product_types,id',
        ]);

        if ($v->fails()) {
            return response($v->messages(), 422);
        }

        if ($type = ProductType::create($request->all())) {

            return response(['item' => $type, 'status' => 'success'], 200);
        } else {

            return response(['item' => null, 'status' => 'error'], 200);
        }
    }

    /**
     * Открыть страницу редактирования типа
     *
     * @param ProductType $type
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit( ProductType $type )
    {
        return view('vue', [
            'params'    => [
                'item' => $type->load('parent'),
            ],
            'viewModel' => 'vm-product-type-edit',
        ]);
    }

    /**
     * Сохранить изменения типа
     *
     * @param  \Illuminate\Http\Request $request
     * @param ProductType $productType
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, ProductType $productType )
    {

        $v = Validator::make($request->all(), [
                'name_ru'   => 'max:255',
                'name_en'   => 'required|max:255',
                'name_kk'   => 'max:255',
                'parent_id' => 'exists:product_types,id|not_in:' . $productType->id,
            ]);

        if ($v->fails()) {
            return response($v->messages(), 422);
        }

        if ($productType->update($request->all())) {

            return response(['item' => $productType, 'status' => 'success'], 200);
        } else {

            return response(['item' => null, 'status' => 'error'], 200);
        }
    }

    // Just uncomment to unable the delete functionality
    //
    //  
    // /**
    //  * удалить тип продукта
    //  *
    //  * @param  ProductType $productType
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy( ProductType $productType )
    // {
    //     if ($productType->children()->count() > 0) {
    //         return response('hasChildren', 200);
    //     }
    //
    //     $productType->delete();
    //
    //     return response('remover', 200);
    // }

    /**
     * Просмотр типа продукта
     *
     * @param ProductType $productType
     * @return mixed
     */
    public function show( ProductType $productType )
    {
        return view('vue', [
            'params'    => [
                'item' => $productType->load('parent'),
            ],
            'viewModel' => 'vm-product-type-show',
        ]);
    }

    /**
     * Поиск типа продукта
     *
     * @param Request $request
     * @return mixed
     */
    public function search( Request $request )
    {
        $params = [];

        $params ['searchByName'] = $request->input('search');

        return $this->types->filter($params)->take($this->types->perPage())->get();
    }
}

