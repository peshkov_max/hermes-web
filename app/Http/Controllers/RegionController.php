<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Wipon\Models\Region;
use Wipon\Repos\RegionRepository;
use Validator;

class RegionController extends Controller
{
    protected $regions;

    public function __construct(RegionRepository $regions)
    {
        $this->regions = $regions;
    }

    public function validationRules()
    {
        return [
            'regional_center_city_id'   => 'required|integer',
            'name_ru'                   => 'required|string',
            'name_en'                   => 'required|string',
            'name_kk'                   => 'required|string',
        ];
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $filter = $request->reset ? [
                'search'     => null,
                'sort'       => null,
                'checkboxes' => null,
                'radio'      => null,
            ] : [
                'search'     => $request->search,
                'sort'       => $request->sort,
                'checkboxes' => $request->checkboxes,
                'radios'      => $request->radios,
            ];

            $regions = $this->regions->filter($filter);

            return $regions ? $regions->paginate(20)->appends($filter) : $regions;
        }

        return view('vue', [
            'viewModel' => 'vm-region-index']);
    }

    public function edit($id)
    {
        if (! filter_var($id, FILTER_VALIDATE_INT)) {
            abort(400);
        }

        $item = Region::whereId($id)->with('regional_center')->firstOrFail();

        return view('vue', [
            'viewModel' => 'vm-region-edit',
            'params' => [
                'item' => $item,
            ]
        ]);
    }

    public function search(Request $request)
    {
        if ($request->ajax()) {
            $countryId = null;

            if (get_i('params')) {
                $countryId = array_get(json_decode(get_i('params'), true), 'countryId');
            };

            return ['data' => $this->regions
                ->countrySearch(get_i('search'), $countryId)
                ->get()];
        }

        return '';
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function update(Request $request, $id)
    {
        if (! filter_var($id, FILTER_VALIDATE_INT)) {
            abort(400);
        }

        if (! $request->ajax()) {
            return '';
        }

        $region = Region::findOrFail($id);

        $v = Validator::make($request->input(), $this->validationRules());

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        $region->update([
            'regional_center_city_id'   => $request->input('regional_center_city_id'),
            'name_ru'                   => $request->input('name_ru'),
            'name_en'                   => $request->input('name_en'),
            'name_kk'                   => $request->input('name_kk'),
        ]);

        return ['status' => 'success'];
    }

    /**
     * Получение регионального центра для региона
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Database\Query\Builder|City
     */
    public function regional_center(Request $request, $id)
    {
        $region = Region::find($id);

        if ($region) {
            return $region->regional_center;
        }

        return null;
    }

    public function cities(Request $request, $id)
    {
        if (! filter_var($id, FILTER_VALIDATE_INT)) {
            abort(400);
        }

        $item = Region::whereId($id)->with(['cities' => function($q) {
            $q->orderBy('name_ru', 'ASC');
        }])->firstOrFail();

        return $item->cities;
    }

}
