<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Wipon\Models\VisualFeature;
use Wipon\Repos\VisualFeatureRepository;

use Illuminate\Http\Request;

class VisualFeatureController extends Controller
{
    /**
     * @var VisualFeatureRepository
     */
    private $features;

    public function __construct( VisualFeatureRepository $features )
    {
        $this->features = $features;
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param Requests\VisualFeatureFormRequest $formRequest
     * @return \Illuminate\Http\Response
     */
    public function store( Requests\VisualFeatureFormRequest $formRequest )
    {
       if ($formRequest->limitReached()) return  response('limitReached', 200);
        
        $feature = $this->features->create( $formRequest->getInputDataForCreatingFeature());

        if ($formRequest->ajax()){
            return response( $feature->load('image', 'image_fake'), 200);
        }

        return abort(400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param VisualFeature $feature
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit( VisualFeature $feature, Request $request)
    {
        if ($request->ajax()){
            return response($feature->load('image', 'image_fake'), 200);
        }

        return abort(400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Requests\VisualFeatureFormRequest $formRequest
     * @param VisualFeature $feature
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update( Requests\VisualFeatureFormRequest $formRequest, VisualFeature $feature)
    {
        $feature = $this->features->update($formRequest->feature_id, $formRequest->getInputDataForUpdatingFeature($feature));

        if ($formRequest->ajax()){
            /** @noinspection PhpUndefinedMethodInspection */
            return response($feature->load(['image', 'image_fake']), 200);
        }

        return abort(400);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param VisualFeature $feature
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy( VisualFeature $feature , Request $request)
    {
        $feature->delete();
        
        if ($request->ajax()){
            return response('success', 200);
        }

        return abort(400);
    }

    /** Set order to visual feature
     * (reorder items)
     * @param Request $request
     * @return mixed
     */
    public function setOrder(Request $request )
    {
        if ($request->ajax())

            /** @noinspection PhpUndefinedFieldInspection */
            return $this->features->setOrder($request->id, $request->new_order_number)
                ? response('success', 200)
                : response('error', 200);

        else

            return response('Method must be ajax', 500);
    }
}
