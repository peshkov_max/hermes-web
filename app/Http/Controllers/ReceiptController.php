<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReceiptRequest;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Response;
use Validator;
use Wipon\Models\Receipt;
use Wipon\Models\ReceiptReportType;
use Wipon\Repos\ReceiptReportRepository;
use Wipon\Repos\ReceiptRepository;
use Wipon\Repos\RegionRepository;

class ReceiptController extends Controller
{
    /**
     * @var ReceiptRepository
     */
    private $receipts;

    /**
     * @var RegionRepository
     */
    private $regions;

    /**
     * @var ReceiptReportRepository
     */
    private $receiptReports;

    public function __construct( ReceiptRepository $receipts, RegionRepository $regions, ReceiptReportRepository $receiptReports )
    {
        $this->receipts = $receipts;
        $this->regions = $regions;
        $this->receiptReports = $receiptReports;

        $this->middleware('ajax', ['only' => ['notifyWinner',],]);
    }

    /**
     * Реестр чеков
     * @param ReceiptRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function registry( ReceiptRequest $request )
    {
        if ($request->ajax()) {

            if ($request->isSettingStatus()) {
                $request->setStatus();

                /** @noinspection PhpUndefinedFieldInspection */
                return response("Status {$request->status} was successfully assign to receipt with id {$request->receipt_id}", 200);
            }

            return $this->receipts->filter($request->all())->paginate(50);
        }

        /** @noinspection PhpUndefinedFieldInspection */
        return view('vue', [
            'params'    => [

                // НЕ ОЧЕВИДНАЯ ЛОГИКА
                // Если в параметрах передан deviceUUID, то необходимо 
                // установить значение фильтра чтобы при первом ajax запросе 
                // осуществился поиск устройства deviceUUID.
                // Для осуществления этой логики прокидывается параметр deviceUUID
                'receipt_id'      => $request->receipt_id ?? null,
                'deviceUUID'      => $request->deviceUUID,
                'regions'         => $this->getRegion($request),
                'currentRegionId' => $this->regions->getDefaultRegion()->id,
            ],
            'viewModel' => 'vm-registry']);
    }

    /**
     * Выгрузка отчета чеков
     * Первый запрос (ajax) - формирование отчёта - возвращает имя файла и путь к файлу
     * Второй запрос - загружает отчет по пришедшему имени файла и пути к файлу
     *
     * @param ReceiptRequest $request
     * @return array
     */
    public function exportReceipts( ReceiptRequest $request )
    {
        if ($request->ajax()) {
            return [
                'status' => 'dataFormed',
                'url'    => url('receipts/export-receipts') . '?' . http_build_query($request->storeReceiptsReport()),
            ];
        }

        $this->validate($request, ['filePath' => 'required', 'fileName' => 'required']);

        /** @noinspection PhpUndefinedFieldInspection */
        $filePath = storage_path($request->filePath);

        /** @noinspection PhpUndefinedFieldInspection */
        return Response::download($filePath . '/' . $request->fileName, $request->fileName, [
            'Content-Length: ' . filesize($filePath),
        ]);
    }

    /**
     * Модерация чеков
     *
     * 1-ый запрос - открытие страницы
     * 2-ой и последующие запросы - получение Receipt b
     *
     * @param ReceiptRequest $request
     * @return Response
     */
    public function moderate( ReceiptRequest $request )
    {
        if ($request->ajax()) {

            if ($request->isSettingStatus()) {
                $request->setStatus();
            }

            /** @noinspection PhpUndefinedFieldInspection */
            $this->validate($request, ['regionId' => $request->region_id == 'all' ? 'in:all' : 'exists:regions,id']);

            $itemBuilder = Receipt::selectRaw(
                DB::raw('
                        receipts.id, 
                        receipts.region_id, 
                        receipts.device_uuid, 
                        receipts.bin,
                        receipts.sum,
                        receipts.status,
                        receipts.photo, 
                        receipts.given_datetime, 
                        receipts.created_at')
            )
                ->where('receipts.status', '=', 'processing');

            $inQueueBuilder = clone $itemBuilder;

            /** @noinspection PhpUndefinedFieldInspection */
            if ($request->region_id != 'all') {

                $region_id = $request->user()->hasRole(['receipt-moderator-admin', 'super-admin'])
                    ? $request->region_id
                    : $this->regions->getDefaultRegion()->id;

                /** @noinspection PhpUndefinedFieldInspection */
                $itemBuilder->where('receipts.region_id', (int) $region_id);

                /** @noinspection PhpUndefinedFieldInspection */
                $inQueueBuilder->where('receipts.region_id', (int) $region_id);
            }

            /** @var $item  Receipt */
            /** @noinspection PhpUndefinedMethodInspection */

            $item = $itemBuilder
                ->orderBy('receipts.created_at')
                ->take(1)->get()->first();

            return response([
                'inQueue' => $inQueueBuilder->count(),
                'receipt' => $item ? $item->toArray() : [],
            ], 200);
        }

        return view('vue', [
            'viewModel' => 'vm-moderation',
            'params'    => [
                'regions' => $this->getRegion($request),
            ],
        ]);
    }

    /**
     *
     * Статистика по чекам
     * @param ReceiptRequest $request
     * @return array
     */
    public function statistics( ReceiptRequest $request )
    {
        if ($request->ajax())
        {
            return $this->receipts->getStatistics($request->all());
        }

        return view('vue', ['viewModel' => 'vm-statistics']);
    }

    /**
     * Выгрузка отчета по статистике
     * Первый запрос (ajax) - формирование отчёта - возвращает имя файла и путь к файлу
     * Второй запрос - загружает отчет по пришедшему имени файла и пути к файлу
     *
     * @param ReceiptRequest $request
     * @return array|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportStatistics( ReceiptRequest $request )
    {
        if ($request->ajax())
        {
            return [
                'status' => 'dataFormed',
                'url'    => url('receipts/export-receipts') . '?' . http_build_query($request->storeStatisticReport()),
            ];
        }

        $this->validate($request, ['filePath' => 'required', 'fileName' => 'required']);

        /** @noinspection PhpUndefinedFieldInspection */
        $filePath = storage_path($request->filePath);

        /** @noinspection PhpUndefinedFieldInspection */
        return Response::download($filePath . '/' . $request->fileName, $request->fileName, [
            'Content-Length: ' . filesize($filePath),
        ]);
    }

    /**
     * Розыгрыш чеков
     *
     * @param ReceiptRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function playPrize( ReceiptRequest $request )
    {
        if ( ! $request->ajax()) {

            return view('vue', [
                'params'    => [
                    'regions'         => $this->getRegion($request, ['super-admin', 'inspector-admin']),
                    'currentRegionId' => $this->regions->getDefaultRegion()->id,
                ],
                'viewModel' => 'vm-play-prize']);
        }

        if ($this->searchingWinner($request)) {
            return new Paginator($this->receipts->playPrizeFilter($request->all())->get(), 1);
        }

        return $this->receipts->playPrizeFilter($request->all())->paginate(50);
    }

    /**
     * Отправить пуш уведомление победителю
     *
     * @param Request $request
     * @return Response
     */
    public function notifyWinner( Request $request )
    {
        $validator = Validator::make($request->all(), [
            'uuid'       => 'exists:devices,uuid',
            'msg'        => 'required|string|max:3500',
            'receipt_id' => 'required|exists:receipts,id',
        ], ['exists' => trans('validation.invalid_uuid')]);

        if ($validator->fails()) return response(['errors' => $validator->messages()], 401);

        // validate only push token
        $validator = Validator::make($request->all(),
            ['uuid' => 'exists:devices,uuid,push_token,NOT_NULL'],
            ['exists' => trans('validation.push_token_absent')]);

        if ($validator->fails()) return response(['errors' => $validator->messages()], 401);

        // отправляем всем устройствам пользователя уведомления о выигрыше
        $this->receipts->notifyWinner($request->input('uuid'), $request->input('msg'));

        // устанавливаем статус победителя
        if ($this->receipts->setStatus($request->receipt_id, 'winner')) {
            return response('Successfully send push notification!', 200);
        } else {
            return response('error', 500);
        }
    }

    /**
     * Список победителей
     *
     * @param ReceiptRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function winners( ReceiptRequest $request )
    {
        if ($request->ajax()) {
            return $this->receipts->filter($request->all())->paginate(50);
        }

        return view('vue', [
            'params'    => [
                'regions'         => $this->regions->getKazakhstanRegion()->toArray(),
                'currentRegionId' => $this->regions->getDefaultRegion()->id,
            ],
            'viewModel' => 'vm-winners']);
    }

    /**
     * Сообщения
     *
     * @param ReceiptRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function messages( ReceiptRequest $request )
    {
        if ($request->ajax()) {
            return $this->receiptReports->filter($request->all())->paginate(50);
        }

        return view('vue', [
            'params'    => [

                // заполнение фильтров
                'types'           => ReceiptReportType::all()->toArray(),
                'regions'         => $this->regions->getKazakhstanRegion()->toArray(),
                'currentRegionId' => $this->regions->getDefaultRegion()->id,
            ],
            'viewModel' => 'vm-messages']);
    }


    /**
     * Сообщения
     * @param ReceiptRequest $request
     * @return array|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportMessages( ReceiptRequest $request )
    {
        if ($request->ajax())
            return [
                'status' => 'dataFormed',
                'url'    => url('receipts/export-messages') . '?' . http_build_query($request->storeMessageReport()),
            ];

        $this->validate($request, ['filePath' => 'required', 'fileName' => 'required']);

        /** @noinspection PhpUndefinedFieldInspection */
        $filePath = storage_path($request->filePath);

        /** @noinspection PhpUndefinedFieldInspection */
        return Response::download($filePath . '/' . $request->fileName, $request->fileName, [
            'Content-Length: ' . filesize($filePath),
        ]);
    }


    /**
     * Сомнительные пользователи
     *
     * @param ReceiptRequest $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function participants( ReceiptRequest $request )
    {
        if ($request->ajax()) {
            return $this->receipts->getParticipants($request->region_id)->paginate(50);
        }

        return view('vue', [
            'params'    => [
                'regions'         => $this->regions->getKazakhstanRegion()->toArray(),
                'currentRegionId' => $this->regions->getDefaultRegion()->id,
            ],
            'viewModel' => 'vm-participants']);
    }

    /** Обучение чеков
     *
     * @param ReceiptRequest $request
     * @return \Illuminate\Http\JsonResponse|Response
     */
    public function educateReceipt( ReceiptRequest $request )
    {
        if ( ! $request->ajax()) {

            return view('vue', [
                'params'    => [
                    'regions' => $this->regions->getKazakhstanRegion()->toArray(),
                ],
                'viewModel' => 'vm-education']);
        }

        if ($request->hasEducationParams()) {
            $v = Validator::make($request->input(), [
                'id'             => 'required|exists:receipts,id',
                'bin'            => 'required|digits:12',
                'sum'            => 'required|numeric',
                'given_datetime' => 'required|date_format:' . config('wipon.date_time_format'),]);

            if ($v->fails()) {
                return response()->json($v->errors()->toArray(), 422);
            }
        }

        list ($inQueue, $receipt) = $request->educateReceipt();

        return response(compact('inQueue', 'receipt'), 200);
    }

    private function getRegion( $request, $roles = ['receipt-moderator-admin', 'super-admin', 'inspector', 'npp', 'inspector-admin'] )
    {
        return $request->user()->hasRole($roles)
            ? $this->regions->getKazakhstanRegion()->toArray()
            : null;
    }

    /**
     * @param ReceiptRequest $request
     * @return bool
     */
    private function searchingWinner( ReceiptRequest $request )
    {
        return $request->has('search') && $request->get('search') != '';
    }
}