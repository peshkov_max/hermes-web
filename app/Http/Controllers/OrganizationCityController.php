<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Wipon\Models\City;
use Wipon\Models\Organization;

/**
 * Редактирование городов присутствия в организации
 *
 * Class OrganizationCityController
 * @package App\Http\Controllers
 */
class OrganizationCityController extends Controller
{

    /**
     * Instantiate a new UserController instance.
     *
     */
    public function __construct()
    {
        $this->middleware('ajax', ['except' => ['edit',]]);
    }

    /**
     * Открыть форму редактирование городов присутствия организации
     *
     * @param Organization $organization - Организация, от которой открепляется город
     * 
     * @return \Illuminate\Http\Response
     */
    public function edit( Organization $organization )
    {
        return view('vue', [
            'params'    => [
                'item' => $organization->load('city_presents')->toArray(),
            ],
            'viewModel' => 'vm-organization-city-editor',
        ]);
    }


    /**
     * Назначить город присутствия для организации
     *
     * @param Organization $organization - Организация, от которой открепляется город
     * @param City $city - Открепляемый город
     * 
     * @return \Illuminate\Http\Response
     */
    public function attach( Organization $organization, City $city )
    {
        if (  $organization->city_presents()->where('id', $city->id)->count() ===0 )
        {
            $organization->city_presents()->attach($city);

            return response($city, 200);
        }

        return response('alreadyAttached', 200);
    }


    /**
     * Удалить город присутствия в организации
     *
     * @param Organization $organization - Организация, от которой открепляется город
     * @param City $city - Открепляемый город
     * 
     * @return \Illuminate\Http\Response
     */
    public function detach( Organization $organization, City $city )
    {
        try {

            $organization->city_presents()->detach($city);
            $status = 'success';
        } catch (\Exception $e) {

            $status = 'error: ' . $e->getMessage();
            log_e($e->getMessage());
        }

        return response($status, 200);
    }
}
