<?php

namespace App\Http\Controllers;

use DB;
use Validator;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Wipon\Models\Cluster;
use Wipon\Models\ClusterType;
use Wipon\Models\Region;
use Wipon\Repos\ClusterRepository;
use Wipon\Repos\RegionRepository;

class ClusterController extends Controller
{
    protected $clusters;
    protected $regions;


    public function __construct(ClusterRepository $clusters, RegionRepository $regions)
    {
        $this->clusters = $clusters;
        $this->regions = $regions;

        $this->middleware('ajax', [
            'only' => [
                'postMerge', 'postLink', 'store', 'update'
            ]
        ]);
    }

    public function validationRules()
    {
        return [
            'city_id'               => 'required|integer|exists:cities,id',
            'cluster_type_id'       => 'required|integer|exists:cluster_types,id',
            'name_ru'               => 'required|string',
            'name_en'               => 'required|string',
            'name_kk'               => 'required|string',
            'longitude'             => 'required|numeric',
            'latitude'              => 'required|numeric',
            'accuracy'              => 'required|numeric',
            'house'                 => 'string',
            'street'                => 'string',
            'is_licensed'           => 'boolean',
            'is_fixed'              => 'boolean',
            'is_moderated'          => 'boolean',
        ];
    }

    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        $regions = $this->regions->getKazakhstanRegion();
        $cluster_types = ClusterType::all();

        $region_id = $request->input('select.region');

        if (empty($region_id)) {
            $region = Region::whereNameEn('Astana');
        } else {
            $region = Region::whereId($region_id);
        }

        $region = $region->with('cities')->first();

        return view('vue', [
            'viewModel' => 'vm-cluster-index',
            'params' => [
                'regions'       => $this->modelsToSelectItems($regions),
                'cities'        => $this->modelsToSelectItems($region->cities),
                'cluster_types' => $this->modelsToSelectItems($cluster_types)
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function getMerge(Request $request)
    {
        $clusters = Cluster::whereIn('id', $request->input('clusters'))->with('cluster_type')->get();

        return view('vue', [
            'viewModel' => 'vm-cluster-merge',
            'params' => [
                'clusters' => $clusters
            ]
        ]);
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse|string
     */
    public function postMerge(Request $request)
    {

        $v = Validator::make($request->input(), [
            'parent' => "required|integer|exists:clusters,id",
            'clusters' => "array",
            'clusters.*' => "integer|exists:clusters,id",
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        $parent = Cluster::findOrFail($request->input('parent'));
        $children = Cluster::whereIn('id', $request->input('clusters'))->get();

        // Дублирование проверки по БТ с фронтэнда
        foreach ($children as $child) {
            if ($child->is_licensed) {
                return response()->json(['licensed' => true], 400);
            }

            if (! $parent->is_fixed && $child->is_fixed) {
                return response()->json(['fixed' => true], 400);
            }
        }

        foreach ($children as $child) {
            // Перемещаем все проверки из дочернего кластера в родительский
            $child->checks()->update(['cluster_id' => $parent->id]);

            // Удаляем записи статистики, связанные с дочерним кластером
            DB::table("kgd_alcohol.check_statistics")->where("cluster_id", $child->id)->delete();

            // Удаляем дочерний кластер
            $child->delete();
        }

        return ['status' => 'success'];
    }

    /**
     * @param Request $request
     * @return View
     */
    public function getLink(Request $request)
    {
        $clusters = Cluster::whereIn('id', $request->input('clusters'))->with('cluster_type', 'city')->get();

        return view('vue', [
            'viewModel' => 'vm-cluster-link',
            'params' => [
                'clusters' => $clusters
            ]
        ]);
    }

    public function postLink(Request $request)
    {
        $v = Validator::make($request->input(), [
            'cityId' => "required|integer|exists:cities,id",
            'clusters' => "array",
            'clusters.*' => "integer|exists:clusters,id",
        ]);

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        Cluster::whereIn('id', $request->input('clusters'))->update([
            'city_id' => $request->input('cityId'),
        ]);

        return ['status' => 'success'];
    }

    public function create(Request $request)
    {
        $cluster_types = ClusterType::all();

        return view('vue', [
            'viewModel' => 'vm-cluster-create',

            // Лучше немного быдлокода здесь, чем куча быдлокода во вьюхе
            'params' => [
                'item' => [
                    'city'            => null,
                    'city_id'         => null,
                    'cluster_type'    => null,
                    'cluster_type_id' => null,
                    'name_ru'         => null,
                    'name_en'         => null,
                    'name_kk'         => null,
                    'longitude'       => null,
                    'latitude'        => null,
                    'accuracy'        => null,
                    'house'           => null,
                    'street'          => null,
                    'is_licensed'     => false,
                    'is_fixed'        => false,
                    'is_moderated'    => false,
                ],
                'cluster_types' => $cluster_types,
                'names' => null
            ]
        ]);
    }

    public function edit(Request $request, $id)
    {
        if (! filter_var($id, FILTER_VALIDATE_INT)) {
            abort(400);
        }

        $item = Cluster::whereId($id)->with('city.region.country', 'cluster_type')->firstOrFail();
        $cluster_types = ClusterType::all();

        $checks = $item->checks()->select(DB::raw('DISTINCT retailer'))->get();
        $names = [];

        foreach ($checks as $check) {
            if (empty($check->retailer)) {
                continue;
            }

            $names[] = $check->retailer;
        }

        return view('vue', [
            'viewModel' => 'vm-cluster-edit',
            'params' => [
                'item' => $item,
                'cluster_types' => $cluster_types,
                'names' => $names
            ]
        ]);
    }

    public function store(Request $request)
    {
        $v = Validator::make($request->input(), $this->validationRules());

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        Cluster::create([
            'city_id'               => $request->input('city_id'),
            'cluster_type_id'       => $request->input('cluster_type_id'),
            'name_ru'               => $request->input('name_ru'),
            'name_en'               => $request->input('name_en'),
            'name_kk'               => $request->input('name_kk'),
            'longitude'             => $request->input('longitude'),
            'latitude'              => $request->input('latitude'),
            'accuracy'              => $request->input('accuracy'),
            'house'                 => $request->input('house'),
            'street'                => $request->input('street'),
            'is_licensed'           => $request->input('is_licensed', false),
            'is_fixed'              => $request->input('is_fixed', false),
            'is_moderated'          => $request->input('is_moderated', false),
        ]);

        return ['status' => 'success'];
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|void
     */
    public function update(Request $request, $id)
    {
        if (! filter_var($id, FILTER_VALIDATE_INT)) {
            abort(400);
        }
        
        $cluster = Cluster::findOrFail($id);

        $v = Validator::make($request->input(), $this->validationRules());

        if ($v->fails()) {
            return response()->json($v->errors()->toArray(), 422);
        }

        $cluster->update([
            'city_id'               => $request->input('city_id'),
            'cluster_type_id'       => $request->input('cluster_type_id'),
            'name_ru'               => $request->input('name_ru'),
            'name_en'               => $request->input('name_en'),
            'name_kk'               => $request->input('name_kk'),
            'longitude'             => $request->input('longitude'),
            'latitude'              => $request->input('latitude'),
            'accuracy'              => $request->input('accuracy'),
            'house'                 => $request->input('house'),
            'street'                => $request->input('street'),
            'is_licensed'           => $request->input('is_licensed'),
            'is_fixed'              => $request->input('is_fixed'),
            'is_moderated'          => $request->input('is_moderated'),
        ]);

        return ['status' => 'success'];
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function filter(Request $request)
    {
        $items = $this->clusters->filter($request->input())->get();

        return $items;
    }

    /**
     * Преобразует коллекцию моделей в массив для селекта
     *
     * @param $collection
     * @return array
     */
    private function modelsToSelectItems($collection)
    {
        if (! $collection) {
            return [];
        }

        $items = [];

        foreach ($collection as $model) {
            $items[] = [
                'id' => $model->id,
                'text' => $model->name,
            ];
        }

        return $items;
    }
}
