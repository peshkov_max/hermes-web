<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Wipon\Models\Device;
use Wipon\Repos\CountryRepository;

class CountryController extends Controller
{
    protected $countries;

    public function __construct( CountryRepository $countries )
    {
        $this->countries = $countries;

        $this->middleware('ajax', ['only' => ['search',]]);
    }

    public function index( Request $request )
    {
        if ($request->ajax()) {
            $devices = Device::all();

            return $devices;
        }

        return view('vue', ['viewModel' => 'vm-region-index']);
    }

    public function search( Request $request )
    {
        if ($request->search === null) {

            $model = $this->countries->getModelClass();

            return $model::all();
        }

        return ['data' => $this->countries->searchByName($request->search)->take(10)->get()];
    }

}
