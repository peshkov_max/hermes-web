<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Wipon\Repos\SupportRepository;

class SupportController extends Controller
{
    /**
     * @var SupportRepository
     */
    private $support;

    public function __construct( SupportRepository $feedback )
    {
        $this->support = $feedback;

        $this->middleware('ajax', ['only' => ['read']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param null $uuid
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request, $uuid = null )
    {
        if ($request->ajax()) {
            return $this->support->filter($request->all())->paginate($this->support->perPage())->appends($request->all());
        }

        return view('vue', ['viewModel' => 'vm-support-index', 'params' => ['uuid' => $uuid]]);
    }

    /**
    
     * @param int $id
     * @param null $setUnread
     * @return mixed
     */
    public function read( $id, $setUnread = null )
    {
        $this->validateArray([
            'id'         => $id,
            'set_unread' => (bool) $setUnread,
        ], [
            'id'         => 'required|exists:feedback_messages,id',
            'set_unread' => 'boolean',
        ]);

        return $this->support->read($id, (bool) $setUnread)
            ? ['status' => 'success']
            : ['status' => 'error'];
    }


    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @param null $device_uuid
     * @return \Illuminate\Http\Response
     */
    public function show( Request $request, $id, $device_uuid = null )
    {

        if ($request->ajax()) {

            $this->validateArray([
                'id'          => $id,
                'device_uuid' => $device_uuid,
            ], [
                'id'          => 'required|exists:feedback_messages,id',
                'device_uuid' => 'required|exists:devices,uuid',
            ]);

            return $this->support->getOtherDeviceMessages($id, $device_uuid)->with('device')->get();
        }

        return view('vue', [
            'viewModel' => 'vm-support-show',
            'params'    => [
                'item' => $this->support->findById($id)->load('device.customer'),
            ],
        ]);
    }

}
