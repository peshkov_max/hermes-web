<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrganizationAssignationRequest;
use App\Http\Requests\OrganizationRequest;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Wipon\Models\Organization;
use Wipon\Models\User;
use Wipon\Repos\OrganizationRepository;
use Wipon\Repos\UserRepository;

class OrganizationController extends Controller
{
    /**
     * @var OrganizationRepository
     */
    protected $organizations;

    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * OrganizationController constructor.
     *
     * @param OrganizationRepository $organizations
     * @param UserRepository $users
     */
    public function __construct( OrganizationRepository $organizations, UserRepository $users )
    {
        $this->organizations = $organizations;
        $this->users = $users;

        $this->middleware('ajax', [
            'only' => [
                'store', 'update',  'search', 'mergingFilter', 'attachUser', 'detachUser'
            ],
        ]);
        
        $this->middleware('role:super-admin', [
            'only' => [
                'store', 'create', 'getMerge', 'postMarge', 'mergingFilter'
            ],
        ]);
    }

    /**
     * Список организаций
     *
     * @param Request $request
     * @return View
     */
    public function index( Request $request )
    {
        if ($request->ajax()) {
            return $this->organizations->filter($request->all())->paginate($this->organizations->perPage());
        }

        return view('vue', [
            'viewModel' => 'vm-organization-index',
        ]);
    }

    /**
     * Создание организации
     *
     * @return View
     */
    public function create()
    {
        return view('vue', [
            'viewModel' => 'vm-organization-create',
        ]);
    }

    /**
     * Сохранение новой организации
     *
     * @param OrganizationRequest $request
     * @return View
     */
    public function store( OrganizationRequest $request )
    {
        $this->organizations->create($request->getCreatingInput());

        return response('stored', 200);
    }

    /**
     * Открыть форму редактирования организации
     *
     * @param Organization $organization
     * @return View
     */
    public function edit( Organization $organization )
    {
        return view('vue', [
            'params'    => [
                'item' => $organization->load('city', 'logo')->toArray(),
            ],
            'viewModel' => 'vm-organization-update',
        ]);
    }


    /**
     * Сохранение отредактированной организации
     *
     * @param Organization $organization
     * @param OrganizationRequest|Request $request
     * @return View
     */
    public function update( Organization $organization, OrganizationRequest $request )
    {
        $this->organizations->update($request->id, $request->getUpdatingInput($organization));

        return response('stored', 200);
    }

    /**
     * Поиск организации
     *
     * @param Request $request
     * @return mixed
     */
    public function search( Request $request )
    {
        return $this->organizations->filter($request->all())->paginate($this->organizations->perPage());
    }

    /**
     * Установить статус модерации организации
     * @param Request $request
     * @return mixed
     */
    public function moderate( Request $request )
    {
        $ids = $request->ids;
        $status = $request->status;

        if ( ! $ids) {
            return response(['errors' => ['Organizations IDs are required']], 422);
        }

        if ($this->organizations->getModel()->whereIn('id', $ids)->count() != count($ids)) {
            return response(['errors' => ['Some of organization ids are not exists']], 422);
        }

        if ( ! $status || ! in_array($status, ['true', 'false'])) {
            return response(['errors' => ['Status is required and it must be is_moderated or is_not_moderated']], 422);
        }

        $this->organizations->getModel()->whereIn('id', $ids)->update(['is_moderated' => $status === 'true' ? true : false]);

        return response('status-set', 200);
    }

    /**
     * Просмотр списка пользователей организации
     *
     * @param Organization $organization
     * @return View
     */
    public function manageUsers( Organization $organization )
    {
        return view('vue', [
            'params'    => [
                'users' => $this->users->getModel()->where('is_testing', false)->get()->toArray(),
                'item'  => $organization->load('city', 'users', 'logo')->toArray(),
            ],
            'viewModel' => 'vm-organization-users',
        ]);
    }

    /**
     * Открыть странице объединения организаций
     *
     * @param Organization $organization
     * @return mixed
     */
    public function getMerge( Organization $organization )
    {
        return view('vue', [
            'params'    => [
                'has_users' => $organization->users->count() > 0,
                'item'      => $organization->toArray(),
            ],
            'viewModel' => 'vm-organization-merge',
        ]);
    }

    /**
     * Объединить организации
     *
     * @param Request $request
     * @return mixed
     */
    public function patchMerge( Request $request )
    {
        $this->validate($request, [
            'master_id' => 'required|exists:organizations,id',
            'slave_ids' => 'required|exists:organizations,id',
        ]);

        $this->organizations->merge($request->master_id, [$request->slave_ids]);

        return response('success', 200);
    }

    /**
     * Поиск для объединения
     * @param Request $request
     * @return mixed
     */
    public function mergingFilter( Request $request )
    {
        $this->validate($request, ['params.id' => 'required', 'search' => 'required']);

        /** @noinspection PhpUndefinedFieldInspection */
        return Organization::where('id', '!=', $request->id)
            ->withName($request->search)
            ->orderBy('name_' . current_locale())->take(10)->get()->toJson();
    }

    /**
     * Прикрепить пользователя
     *
     * @param OrganizationAssignationRequest $request
     * @return string
     */
    public function attachUser( OrganizationAssignationRequest $request )
    {
        /** @var User $user */
        $user = $this->users->findById($request->user_id);

        if ($user->organization_id == $request->id) {
            return 'userAlreadyAttached';
        }

        $user->organization_id = $request->id;

        $user->save();

        return response($user->toArray(), 200);
    }

    /**
     * Открепить пользователя
     *
     * @param OrganizationAssignationRequest $request
     * @return string
     */
    public function detachUser( OrganizationAssignationRequest $request )
    {
        /** @var User $user */
        $user = $this->users->findById($request->user_id);

        if ($user->id == $request->user()->id) {
            return response('current_user_attached_to_this_organization', 200);
        }

        if ($user->organization_id != $request->id) {
            return 'user_already_detached';
        }

        $user->organization_id = null;
        
        $user->save();

        return response($user->toArray(), 200);
    }
}