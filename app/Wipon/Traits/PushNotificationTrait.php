<?php
namespace Wipon\Traits;

use PushNotification;
use Wipon\Models\Device;
use Wipon\Models\DeviceNotification;
use Wipon\Models\Pro\Notification;
use Wipon\Models\Pro\User;

/**
 * Class PushNotificationTrait
 *
 * Методы используется в
 * \App\Jobs\SendPushToDeviceBunch
 * \App\Jobs\SendPush
 *
 * @package Wipon\Traits
 */
trait PushNotificationTrait
{
    /**
     * Отправить уведомление пользователю Wipon (через устройство)
     *
     * @param Device $device - Устройство пользователя
     */
    private function sendToDevice( Device $device )
    {
        if ($this->pushCantBeSendToWiponDevice($device->uuid)) {
            $this->delete();

            return;
        }

        if ( ! $device || empty($device->push_token)) {
            $this->delete();

            return;
        }

        try {
            $this->sendNotification(
                $device->platform,
                $device->push_token,
                $device->notifications_count ? $device->notifications_count + 1 : 0
            );
        } catch (\Exception $e) {
            log_e("PUSH notification error: sending to ($device->platform) $device->push_token", [$e->getMessage()]);
            $this->delete();
        }

        $device->notifications()->save(new DeviceNotification([
            'text' => $this->message,
        ]));
    }

    /**
     * Отправить уведомление пользователю Wipon Pro
     *
     * @param User $user - Пользователь Wipon Pro
     */
    protected function sendToUser( User $user )
    {
        if ($this->pushCantBeSendToWiponProUser($user->phone_number)) {
            return;
        }

        if ( ! $user) {
             log_w("Cant send push notification. Cant find user.");

            return;
        }

        if ( ! isset($user->device['push_token'])) {
             log_w("Cant send push notification. Empty push token. User id: " . $user->id);

            return;
        }

        if ( ! isset($user->device['platform'])) {
             log_w("Cant send push notification. Empty platform. User id: " . $user->id);

            return;
        }

        try {
            $this->sendNotification(
                $user->device['platform'],
                $user->device['push_token'],
                $user->notifications_count ? $user->notifications_count + 1 : 0
            );
        } catch (\Exception $e) {
            log_e("PUSH notification error: sending to ({$user->device['platform']}) $user->device['push_token']", [$e->getMessage()]);
            $this->delete();

            return;
        }

        $user->notifications()->save(new Notification([
            'text' => $this->message,
        ]));
    }

    /**
     * Отправить PUSH уведомление на устройство по PUSH токену
     *
     * @param string $platform - Платформа
     * @param string $pushToken - Push token
     * @param int $unreadCount - Количество не прочитанных уведомлений
     */
    private function sendNotification( $platform, $pushToken, $unreadCount )
    {
        if (mb_strpos($platform, 'Android') === 0) {
            PushNotification::app('wiponAndroid')
                ->to($pushToken)
                ->send($this->message, ['title'             => 'Wipon', "badge" => $unreadCount, "timestamp" => time(),
                                        "content-available" => 1]);
        } elseif (mb_strpos($platform, 'iOS') === 0) {
            PushNotification::app('wiponIOS')
                ->to($pushToken)
                ->send($this->message, ["badge" => $unreadCount, "timestamp" => time(), "content-available" => 1]);
        }

        log_i("PUSH notification sent to ($platform) $pushToken");
    }

    /**
     * Проверяет может ли пуш уведомление быть отправлено
     *
     * На всех серверах КРОМЕ PRODUCTION пуш уведомления должны
     * отправляться ТОЛЬКО на устройства из whitelist-a (config/wipon.php:91)
     *
     * @param string $uuid -  UUID устройства
     * @return bool
     */
    public function pushCantBeSendToWiponDevice( string $uuid )
    {
        $result = config('app.env') !== 'production' && ! in_array($uuid, config("wipon.whitelist"));

        if ($result) {
            log_w('Can not send push notification. App env is not production'
                .' and given device UUID is out of Wipon whitelist.' . PHP_EOL .
                "# Given UUID: $uuid" . PHP_EOL .
                '# See whitelist here: config/wipon.php');
        }

        return $result;
    }

    /**
     * Проверяет может ли пуш уведомление быть отправлено
     *
     * На всех серверах КРОМЕ PRODUCTION пуш уведомления должны
     * отправляться ТОЛЬКО на устройства из whitelist_pro (config/wipon.php:112)
     *
     * @param string $phone_number - Номер телефона
     * @return bool
     */
    public function pushCantBeSendToWiponProUser( string $phone_number )
    {
        $result = config('app.env') !== 'production' && ! in_array($phone_number, config("wipon.whitelist_pro"));

        if ($result) {
            log_w('Can not send push notification. App env is not production'
                .' and given device phone_number is out of Wipon whitelist_pro.' . PHP_EOL .
                "# Given Phone Number: $phone_number" . PHP_EOL .
                '# See whitelist_pro here: config/wipon.php');
        }

        return $result;
    }
}