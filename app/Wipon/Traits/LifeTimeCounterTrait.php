<?php

namespace Wipon\Traits;

/**
 * Class ClassLiveTimeCounter
 * 
 * Логирует время существования класса, а также количество задействованной памяти 
 *   
 * Использование: 
 * 
 *      use Wipon\Traits\LifeTimeCounterTrait;
 * 
 *      use LifeTimeCounterTrait {
 *           LifeTimeCounterTrait::__construct as startClassLifeTimeCounting;
 *           LifeTimeCounterTrait::__destruct as endClassLifeTimeCounting;
 *      }
 * 
 *      public function __construct( array $uuids, string $message ){
 *          $this->startClassLifeTimeCounting();
 *      }
 *
 *      function __destruct(){
 *          $this->endClassLifeTimeCounting();
 *      }
 * 
 * @author Peshkov Maxim <peshkov-maximum@yandex.ru>
 * @package Wipon\Traits
 */
trait LifeTimeCounterTrait
{
    private $secStartAll;

    private $useStartAll;

    public function __construct()
    {
        list($useStartAll, $secStartAll) = explode(' ', microtime());

        $this->secStartAll = $useStartAll;
        $this->useStartAll = $secStartAll;
    }

    public function __destruct()
    {
        log_i( 'Life time estimation'. PHP_EOL .
            '# Instance of ' . get_called_class() . ' was estimated.' . PHP_EOL .
            '# Memory used: ' . '' . $this->format_bytes(memory_get_peak_usage(1)) . PHP_EOL .
            '# Time spend: ' . $this->getClassExecutionTime() . PHP_EOL);
    }

    /** Возвращает время выполнения метода в секундах
     *
     * @return integer
     */
    private function getClassExecutionTime()
    {
        list($useEndAll, $secEndAll) = explode(' ', microtime());
        $totalTime = $secEndAll - $this->secStartAll + $useEndAll - $this->useStartAll;

        return $totalTime;
    }

    /**
     * Форматирование вывода используемой памяти
     *
     * @param $size
     * @param int $precision
     * @return string
     */
    private function format_bytes( $size, $precision = 2 )
    {
        $base = log($size, 1024);
        $suffixes = ['', 'Kb', 'Mb', 'Gb', 'Tb'];

        return round(pow(1024, $base - floor($base)), $precision) . ' ' . $suffixes[ floor($base) ];
    }
}