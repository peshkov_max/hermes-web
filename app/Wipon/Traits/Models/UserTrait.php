<?php
namespace Wipon\Traits\Models;

use Wipon\Models\Role;

/**
 * Class UserTrait
 * @package Wipon\Traits\Models
 */
trait UserTrait
{

    /**
     * @return mixed
     */
    public function getRolesInString()
    {
        return implode(', ', $this->roles->pluck('display_name')->toArray());
    }
    
    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        if ($this->roles->contains('name', 'super-admin') && $this->organization->name_en === config('wipon.organization_name_en') ) {
            return true;
        }

        return false;
    }
  
    /**
     * Alias to eloquent many-to-many relation's attach() method.
     *
     * @param mixed $role
     */
    public function attachRole( $role )
    {
        if (is_object($role)) {
            $role = $role->getKey();
        }

        if (is_array($role)) {
            $role = $role['id'];
        }

        $this->roles()->attach($role);
    }

    /**
     * Alias to eloquent many-to-many relation's detach() method.
     *
     * @param mixed $role
     */
    public function detachRole( $role )
    {
        if (is_object($role)) {
            $role = $role->getKey();
        }

        if (is_array($role)) {
            $role = $role['id'];
        }

        $this->roles()->detach($role);
    }

    /**
     * Attach multiple roles to a user
     *
     * @param mixed $roles
     */
    public function attachRoles( $roles )
    {
        foreach ($roles as $role) {
            $this->attachRole($role);
        }
    }

    /**
     * Detach multiple roles from a user
     *
     * @param mixed $roles
     */
    public function detachRoles( $roles = null )
    {
        if ( ! $roles) $roles = $this->roles()->get();

        foreach ($roles as $role) {
            $this->detachRole($role);
        }
    }


    /**
     * @param $role
     * @return bool
     */
    public function hasRole( $role )
    {
        if (is_string($role)) {

            if ($this->roles->count()>0)
            {
                foreach ($this->roles as $userRole) {
                    return $this->checkRole($userRole, $role);
                }
            }else{
                return false;
            }
        }

        if (is_array($role)) {
            foreach ($role as $r) {
                if ($this->hasRole($r) === true)
                    return true;
            }

            return false;
        }

        /** @noinspection PhpUndefinedMethodInspection */
        return ! ! $role->intersect($this->roles)->count();
    }


    /** Checks User Role's name equals given name
     *
     * @param Role $userRole - Role model
     * @param string $roleName - Role to check
     * @return bool
     */
    private function checkRole( $userRole, $roleName )
    {
        if (str_is($userRole->name, $roleName))
            return true;

        return false;
    }

    /**
     * Checks role(s) and permission(s).
     *
     * @param string|array $roles Array of roles or comma separated string
     * @param string|array $permissions Array of permissions or comma separated string.
     * @param array $options validate_all (true|false) or return_type (boolean|array|both)
     *
     * @throws \InvalidArgumentException
     *
     * @return array|bool
     */
    public function ability( $roles, $permissions, $options = [] )
    {
        // Convert string to array if that's what is passed in.
        if ( ! is_array($roles)) {
            $roles = explode(',', $roles);
        }
        if ( ! is_array($permissions)) {
            $permissions = explode(',', $permissions);
        }

        // Set up default values and validate options.
        if ( ! isset($options['validate_all'])) {
            $options['validate_all'] = false;
        } else {
            if ($options['validate_all'] !== true && $options['validate_all'] !== false) {
                throw new \InvalidArgumentException();
            }
        }
        if ( ! isset($options['return_type'])) {
            $options['return_type'] = 'boolean';
        } else {
            if ($options['return_type'] != 'boolean' &&
                $options['return_type'] != 'array' &&
                $options['return_type'] != 'both'
            ) {
                throw new \InvalidArgumentException();
            }
        }

        // Loop through roles and permissions and check each.
        $checkedRoles = [];
        $checkedPermissions = [];
        foreach ($roles as $role) {
            $checkedRoles[ $role ] = $this->hasRole($role);
        }
        foreach ($permissions as $permission) {
            $checkedPermissions[ $permission ] = $this->can($permission);
        }

        // If validate all and there is a false in either
        // Check that if validate all, then there should not be any false.
        // Check that if not validate all, there must be at least one true.
        if (($options['validate_all'] && ! (in_array(false, $checkedRoles) || in_array(false, $checkedPermissions))) ||
            ( ! $options['validate_all'] && (in_array(true, $checkedRoles) || in_array(true, $checkedPermissions)))
        ) {
            $validateAll = true;
        } else {
            $validateAll = false;
        }

        // Return based on option
        if ($options['return_type'] == 'boolean') {
            return $validateAll;
        } elseif ($options['return_type'] == 'array') {
            return ['roles' => $checkedRoles, 'permissions' => $checkedPermissions];
        } else {
            return [$validateAll, ['roles' => $checkedRoles, 'permissions' => $checkedPermissions]];
        }
    }

}