<?php
namespace Wipon\Traits\Models;

trait ModerationTrait
{
    public function scopeApplyModerationFilter($query, $field)
    {
        if ($field && $field === 'is_moderated') {
            $query->whereRaw('is_moderated is true');
        } elseif ($field && $field === 'is_not_moderated') {
            $query->whereRaw('is_moderated is false');
        }
        
        return $query;
    }

}