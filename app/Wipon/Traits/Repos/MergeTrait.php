<?php
namespace Wipon\Traits\Repos;

use Wipon\Models\Alias;

trait MergeTrait
{
    private $nameFields = [
        'name_ru', 'name_en', 'name_kk',
    ];

    /**
     * @param $slave
     * @return array
     */
    private function getEntityAliases( $slave )
    {
        // берём все имена name_ru name_kz name_en
        // делаем их них массив (позже в этот массив будем добавлять альясы)
        $names = [];

        foreach ($this->nameFields as $field) {
            if ($slave->{$field} !== null && $slave->{$field} !== '')
                $names[] = $slave->{$field};
        }

        // берём все псевдонима организации
        $aliasesNames = $slave->aliases->pluck('name')->toArray();

        // добавляем выбранные псевдонисы в массив созданный ранее
        return array_merge($names, $aliasesNames);
    }


    /**
     * @param $aliases
     * @param $slave
     * @param $master
     * @return mixed
     */
    private function updateMasterAliases( $aliases, $slave, $master )
    {
        if (count($aliases) == 0)
            return;

        // Обновим псевдонимы slave  
        $slave->aliases()->update(['aliasable_id'=>$master->id]);

        // для каждого имени производим поиск в таблице альясов
        foreach ($aliases as $alias) {
            $this->addAliasToMaster($alias, $master);
        }
    }


    /**
     * @param $aliasName
     * @param $master
     */
    private function addAliasToMaster( $aliasName, $master )
    {
        // если алъяс не найден - создаем новый алияс привязанный к master сущности
        if (Alias::where('name', $aliasName)->count() == 0) {
            $master->aliases()->create(['name' => $aliasName]);
        }
        // если аляс найден - ничего не делаем
    }

}