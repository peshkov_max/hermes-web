<?php 
namespace Wipon\Traits\Auth;

use Auth;
use Lang;
use Validator;
use \Illuminate\Http\Request;
use Illuminate\Foundation\Auth\{
    AuthenticatesUsers,
    RegistersUsers
};

/**
 * Wipon AuthenticatesUsers
 *
 * @see requirements http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5406808
 * @package App\Http\Controllers\Auth
 */
 
trait AuthenticatesUsersTrait{
    
    use AuthenticatesUsers, RegistersUsers {
        AuthenticatesUsers::redirectPath insteadof RegistersUsers;
        AuthenticatesUsers::getGuard insteadof RegistersUsers;
    }

    /** Whether user login with email or with username
     * @var
     */
    private $loginUserByField;

    /** Form login field name
     * @var
     */
    private $formLoginFieldName = 'login';

    /** Form password field name
     * @var
     */
    private $formPasswordFieldName = 'password';

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->loginUserByField = filter_var($request->{$this->formLoginFieldName}, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';

        /** @noinspection PhpUndefinedFieldInspection */
        $v = Validator::make(
            [
                $this->formLoginFieldName       => trim($request->login),
                $this->formPasswordFieldName      =>  trim($request->password),
            ],
            [
                $this->formLoginFieldName=> "required|exists:users,$this->loginUserByField",
                $this->formPasswordFieldName => 'required',
            ], [$this->formLoginFieldName.'.exists'=>trans('auth.failed_no_user')]);

        if ($v->fails()) {
            /** @noinspection PhpUndefinedMethodInspection */
            return redirect()->back()->withInput($request->all())->withErrors($v);
        }
      
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the name and
        // the IP address of the client making these requests into this application.
        $throttles = $this->isUsingThrottlesLoginsTrait();

        if ($throttles && $lockedOut = $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        $credentials = $this->getUserCredentials($request);

        if (Auth::guard($this->getGuard())->attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        /** @var bool $lockedOut */
        if ($throttles && !$lockedOut) {
            $this->incrementLoginAttempts($request);
        }
   
        return $this->sendFailedLoginResponse($request);
    }

    /** 
     * Get user creditionals 
     * 
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    private function getUserCredentials($request) : array
    { 
       return [
           $this->loginUserByField => trim($request->{$this->formLoginFieldName}),
           $this->formPasswordFieldName => trim($request->{$this->formPasswordFieldName}),
       ];
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function loginUsername() :string
    {
        return property_exists($this, 'loginUserByField') ? $this->loginUserByField : 'email';
    }

    /**
     * Get the failed login message.
     *
     * @return string
     */
    protected function getFailedLoginMessage()
    {
        return Lang::has('auth.failed')
            ? Lang::get('auth.failed')
            : 'These credentials do not match our records.';
    }

    /*
    |--------------------------------------------------------------------------
    | Disable registration section
    |--------------------------------------------------------------------------
    |
    | Wipon require registration disabled
    */
    /** Show registration form
     *
     * @return mixed
     */
    public function showRegistrationForm()
    {
        return redirect('login');
    }

    /** Redirect to login page
     *
     * @return mixed
     */
    public function register()
    {
        return redirect('login');
    }
}