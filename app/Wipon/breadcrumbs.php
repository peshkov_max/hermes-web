<?php

// Wipon
Breadcrumbs::register('home', function ( $breadcrumbs ) {
    /** @var \DaveJamesMiller\Breadcrumbs\Generator $breadcrumbs */
    $breadcrumbs->push(trans('app.wipon'), '/');
});

// Wipon > DashBoard
Breadcrumbs::register('home.dashboard', function ( $breadcrumbs ) {
    /** @var \DaveJamesMiller\Breadcrumbs\Generator $breadcrumbs */
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.general_information'), url('/'));
});

// User manage
Breadcrumbs::register('users.manage', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.settings_users'));
});

// Products > All products
Breadcrumbs::register('products.index', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.product_list'),route('products.index'));
});

// Products > Add
Breadcrumbs::register('products.create', function($breadcrumbs)
{
    $breadcrumbs->parent('products.index');
    $breadcrumbs->push(trans('app.create_product'), route('products.create'));
});

// Products > Show
Breadcrumbs::register('products.show', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('products.index');
    $breadcrumbs->push($product->name , route('products.show', $product->id));
});

// Products > Update
Breadcrumbs::register('products.update', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('products.show', $product);
    $breadcrumbs->push(trans('app.edit_general'), route('products.update', $product->id));
});

Breadcrumbs::register('vue_js', function ( $breadcrumbs, $viewModel ) {

    defineVieJsBreadcrumbs($viewModel, $breadcrumbs);
});

Breadcrumbs::register('supports.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.support'));
});

Breadcrumbs::register('support_pro.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.support_pro'));
});

Breadcrumbs::register('support_pro_subscriptions.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('support_pro.index');
    $breadcrumbs->push(trans('app.subscriptions_pro'));
});


Breadcrumbs::register('logs.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.menu_toggle_label'));
});

Breadcrumbs::register('devices.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('supports.index');
    $breadcrumbs->push(trans('app.devices'), route('devices.index'));
});

Breadcrumbs::register('checks.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.ukm_menu_main_title'));
});

Breadcrumbs::register('cities.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.cities'));
});

Breadcrumbs::register('regions.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.regions'));
});

Breadcrumbs::register('clusters.index', function ( $breadcrumbs ) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push(trans('app.cluster_title'));
});

// for testing purposes
if (! function_exists('defineVieJsBreadcrumbs')){
    function defineVieJsBreadcrumbs( $viewModel, $breadcrumbs )
    {

        switch ($viewModel) {
            case 'vm-dashboard':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.dashboard_title'));
                break;
            case 'vm-city-index':
                $breadcrumbs->parent('cities.index');
                $breadcrumbs->push(trans('app.city_list'));
                break;
            case 'vm-city-edit':
                $breadcrumbs->parent('cities.index');
                $breadcrumbs->push(trans('app.city_edit'));
                break;
            case 'vm-city-create':
                $breadcrumbs->parent('cities.index');
                $breadcrumbs->push(trans('app.create'));
                break;
            case 'vm-city-merge':
                $breadcrumbs->parent('cities.index');
                $breadcrumbs->push(trans('app.merge'));
                break;
            case 'vm-region-index':
                $breadcrumbs->parent('cities.index');
                $breadcrumbs->push(trans('app.regions'));
                break;
            case 'vm-region-edit':
                $breadcrumbs->parent('regions.index');
                $breadcrumbs->push(trans('app.region_edit'));
                break;
            case 'vm-cluster-index':
                $breadcrumbs->parent('clusters.index');
                $breadcrumbs->push(trans('app.map'));
                break;
            case 'vm-cluster-merge':
                $breadcrumbs->parent('clusters.index');
                $breadcrumbs->push(trans('app.merging'));
                break;
            case 'vm-cluster-link':
                $breadcrumbs->parent('clusters.index');
                $breadcrumbs->push(trans('app.linking'));
                break;
            case 'vm-cluster-edit':
                $breadcrumbs->parent('clusters.index');
                $breadcrumbs->push(trans('app.edit'));
                break;
            case 'vm-cluster-create':
                $breadcrumbs->parent('clusters.index');
                $breadcrumbs->push(trans('app.cluster_create'));
                break;


            case 'vm-support-index':
                $breadcrumbs->parent('supports.index');
                $breadcrumbs->push(trans('app.feedback'));
                break;
            case 'vm-support-show':
                $breadcrumbs->parent('supports.index');
                $breadcrumbs->push(trans('app.show_msg'));
                break;
            case 'vm-support-customers':
                $breadcrumbs->parent('supports.index');
                $breadcrumbs->push(trans('app.customer_list'));
                break;
            case 'vm-support-customer':
                $breadcrumbs->parent('supports.index');
                $breadcrumbs->push(trans('app.customer_edit'));
                break;
            case 'vm-support-notifications':
                $breadcrumbs->parent('supports.index');
                $breadcrumbs->push(trans('app.delivered_notification_list'));
                break;

            // Support pro
            case 'vm-support-pro-index':
                $breadcrumbs->parent('support_pro.index');
                $breadcrumbs->push(trans('app.feedback'));
                break;
            case 'vm-support-pro-show':
                $breadcrumbs->parent('support_pro.index');
                $breadcrumbs->push(trans('app.show_msg'));
                break;

            case 'vm-users-pro':
                $breadcrumbs->parent('support_pro.index');
                $breadcrumbs->push(trans('app.customer_list'));
                break;
            case 'vm-users-pro-edit':
                $breadcrumbs->parent('support_pro.index');
                $breadcrumbs->push(trans('app.customer_edit'));
                break;
            case 'vm-users-pro-show':
                $breadcrumbs->parent('support_pro.index');
                $breadcrumbs->push(trans('app.user_log_show'));
                break;
            case 'vm-stores-pro-edit':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.store_edit'));
                break;

            case 'vm-notifications-pro':
                $breadcrumbs->parent('support_pro.index');
                $breadcrumbs->push(trans('app.delivered_notification_list'));
                break;

            // Subscriptions 
            case 'vm-subscriptions-pro-index':
                $breadcrumbs->parent('support_pro_subscriptions.index');
                $breadcrumbs->push(trans('app.subscriptions_index'));
                break;

            case 'vm-push':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.push'));
                break;

            case 'vm-device-logs':
                $breadcrumbs->parent('devices.index');
                $breadcrumbs->push(trans('app.show_log'));
                break;
            case 'vm-device-show':
                $breadcrumbs->parent('vue_js');
                $breadcrumbs->push(trans('app.show_log'));
                break;
            case 'vm-device-index':
                $breadcrumbs->parent('supports.index');
                $breadcrumbs->push(trans('app.devices'));
                break;
            case 'vm-log-api-requests':
                $breadcrumbs->parent('logs.index');
                $breadcrumbs->push(trans('app.index_page_header'));
                break;

            // Products
            case 'vm-product-index':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.product_list'));
                break;
            case 'vm-product-merge':
                $breadcrumbs->parent('products.index');
                $breadcrumbs->push(trans('app.merge_page_title'));
                break;
            case 'vm-product-create':
                $breadcrumbs->parent('products.index');
                $breadcrumbs->push(trans('app.product_add'));
                break;
            case 'vm-product-edit':
                $breadcrumbs->parent('products.index');
                $breadcrumbs->push(trans('app.product_edit'));
                break;

            // Product Types
            case 'vm-product-type-index':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.product_types'));
                break;
            case 'vm-product-type-edit':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.product_type_edit'));
                break;
            case 'vm-product-type-create':
                $breadcrumbs->parent('products.index');
                $breadcrumbs->push(trans('app.product_type_add'));
                break;
            case 'vm-product-type-show':
                $breadcrumbs->parent('products.index');
                $breadcrumbs->push(trans('app.product_type_show'));
                break;

            // UKM
            case 'vm-check-table':
                $breadcrumbs->parent('checks.index');
                $breadcrumbs->push(trans('app.menu_stats'));
                break;
            case 'vm-check-maps':
                $breadcrumbs->parent('checks.index');
                $breadcrumbs->push(trans('app.ukm_menu_checks'));
                break;
            case 'vm-check-detail':
                $breadcrumbs->parent('checks.index');
                $breadcrumbs->push(trans('app.detail_title'));
                break;

            // Receipts
            case 'vm-moderation':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.receipt_moderation'));
                break;
            case 'vm-registry':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.registry'));
                break;
            case 'vm-play-prize':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.play_prize'));
                break;
            case 'vm-winners':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.winners'));
                break;
            case 'vm-statistics':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.statistics'));
                break;
            case 'vm-messages':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.messages'));
                break;
            case 'vm-participants':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.participant_list'));
                break;
            case 'vm-education':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.education'));
                break;

            // Users
            case 'vm-user-index':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.users'));
                break;
            case 'vm-user-create':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.user_add'));
                break;
            case 'vm-user-edit':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.user_edit'));
                break;
            case 'vm-password':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.set_password'));
                break;

            // Organizations
            case 'vm-organization-index':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.organizations'));
                break;
            case 'vm-organization-create':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.organization_add'));
                break;
            case 'vm-organization-update':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.organization_edit'));
                break;
            case 'vm-organization-users':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.organization_users'));
                break;
            case 'vm-organization-merge':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.merge_organizations'));
                break;
            case 'vm-organization-city-editor':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.organization_cities'));
                break;

            // Aliases
            case 'vm-aliases':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.alias_edit'));
                break;

            // Roles
            case 'vm-role-index':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.roles'));
                break;
            case 'vm-role-create':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.create_role'));
                break;
            case 'vm-role-edit':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.update_role'));
                break;
            case 'vm-role-show':
                $breadcrumbs->parent('home');
                $breadcrumbs->push(trans('app.roles_users'));
                break;

            default:
                $breadcrumbs->push(trans('vue_js'));
                break;
        }
    }
}


