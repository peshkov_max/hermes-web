<?php

namespace Wipon\Services;

use Lavary\Menu\Builder;
use Lavary\Menu\Item;
use Menu;

/** Сервис, генерирующий меню
 *
 * Class MenuGeneratorService
 * @package Wipon\Services
 */
class MenuGeneratorService
{
    /** Генерирует меню
     *
     * @return \Lavary\Menu\Menu
     */
    public static function generate()
    {
        return (new self())->createMenu();
    }

    /** Создание меню
     *
     * @return \Lavary\Menu\Menu
     */
    private function createMenu()
    {
        if (auth()->check()) {

            return Menu::make('leftMenu',

                function ( $menu ) {

                    $user = auth()->user();

                    // Продукты

                    $this->pushMenuItem($menu,
                        'app.product_title',
                        route('vue-products.index'),
                        'fa fa-shopping-cart fa-1',
                        ['vue-products/*', 'aliases/products/*'],
                        ['super-admin','product-moderator', 'commercial-admin', 'content-manager'], 'menu-products');

                    // УКМ
                    if ($user->hasRole('inspector')) {
                        $this->pushMenuItem($menu,
                            'app.ukm_menu_checks', route('checks.map'), 'fa fa-area-chart fa-1', 'checks/map', [ 'inspector', ], 'menu-checks-checks');
                    }

                    if ($user->hasRole('inspector-admin')) {
                        $this->pushMenuItem($menu, 'app.ukm_menu_checks', route('checks.map'), 'fa fa-area-chart fa-1', ['checks/map', 'checks/detail',], ['inspector-admin'], 'menu-checks-checks');
                        $this->pushMenuItem($menu, 'app.menu_stats', route('checks.stats'), 'fa fa-area-chart fa-1', 'checks/stats', ['inspector-admin'], 'menu-checks-table');
                    }
                    

                    if ($user->isSuperAdmin() || $user->hasRole('content-manager')){
                        $this->pushMenuItem($menu,
                            'app.product_types',
                            route('product-types.index'), 'fa  fa-list fa-1', ['product-types/*'], ['super-admin', 'content-manager', 'product-moderator',], 'menu-product-types');
                    }

                    if ($user->isSuperAdmin()) {
                        
                        $kgd = $this->pushMenuItem($menu,
                            'app.ukm_menu_main_title', '#', 'fa  fa-area-chart fa-1', ['checks/*'], ['super-admin', 'inspector', 'inspector-admin',], 'menu-checks-toggle');

                        $this->pushSubMenuItem($menu, $kgd->id, 'app.ukm_menu_checks', route('checks.map'), 'checks/map', ['super-admin', 'inspector', 'inspector-admin',], 'menu-checks-checks');
                        $this->pushSubMenuItem($menu, $kgd->id, 'app.menu_stats', route('checks.stats'), 'checks/stats', ['super-admin', 'inspector', 'inspector-admin',], 'menu-checks-table');
                    }

                    // Модерация чеков

                    $this->setInvoiceMenuItems($menu, $user, $this);

                    // Поддержка B2C

                    $support = $this->pushMenuItem($menu,
                        'app.support',
                        '#',
                        'fa fa-tty fa-1',
                        ['customers/*', 'support/notifications'],
                        ['support', 'super-admin'], 'menu-support-toggle');

                    $this->pushSubMenuItem($menu, $support->id, 'app.feedback', route('supports.index'), 'supports/*', ['support', 'super-admin'], 'menu-support-index');
                    $this->pushSubMenuItem($menu, $support->id, 'app.devices', route('devices.index'), 'devices/*', ['support', 'super-admin'], 'menu-mobile-app-devices');

                    $this->pushSubMenuItem($menu, $support->id, 'app.customer_list', route('customers.index'), 'customers/*', ['support', 'super-admin'], 'menu-customer-list');
                    $this->pushSubMenuItem($menu, $support->id, 'app.delivered_notification_list', route('push_notifications.index'), 'push-notifications', ['support', 'super-admin'], 'menu-push-notifications-list');

                    // Поддержка B2C Pro
                    
                    $support_pro = $this->pushMenuItem($menu,
                        'app.support_pro',
                        '#',
                        'fa fa-volume-control-phone fa-1',
                        ['feedback-messages-pro/*', 'feedback-messages-pro', 'users-pro/*', 'notifications-pro/*','subscriptions-pro/*'],
                        ['support', 'support-pro', 'super-admin'  ], 'menu-support-pro-toggle');

                    $this->pushSubMenuItem($menu, $support_pro->id, 'app.feedback', route('feedback-messages-pro.index'), 'feedback-messages-pro/*', ['support', 'support-pro','super-admin'], 'menu-support-pro-index');
                    $this->pushSubMenuItem($menu, $support_pro->id, 'app.customer_list', route('users-pro.index'), 'users-pro/*', ['support','support-pro', 'super-admin'], 'menu-customer-pro-list');
                    $this->pushSubMenuItem($menu, $support_pro->id, 'app.delivered_notification_list', route('notifications-pro.index'), 'notifications-pro/*', ['support-pro', 'super-admin'], 'menu-notifications-pro-list');
                    $this->pushSubMenuItem($menu, $support_pro->id, 'app.subscriptions_pro', route('subscriptions-pro.index'), 'subscriptions-pro/*', ['super-admin', 'support-pro'], 'menu-subscriptions-index');


                    // Пуш уведомления
                    
                    $this->pushMenuItem($menu,
                        'app.push_notifications',
                        route('push_notifications.create'),
                        'fa fa-bell-o fa-1',
                        'push-notifications/create',
                        ['support', 'support-pro',  'super-admin'], 'menu-push-create');

                    // Управление пользователями ЛК

                    $user_manager = $this->pushMenuItem($menu,
                        'app.settings_users',
                        '#',
                        'fa fa-male fa-1',
                        ['roles/*', 'organizations/*', 'aliases/organizations/*', 'users/*'],
                        ['super-admin'], 'menu-users');

                    $this->pushSubMenuItem($menu, $user_manager->id, 'app.users', route('users.index'), 'users/*', ['super-admin', 'organization-moderator'], 'menu-settings-users');
                    $this->pushSubMenuItem($menu, $user_manager->id, 'app.organizations', route('organizations.index'), 'organizations/*', ['super-admin', 'organization-moderator'], 'menu-organizations');
                    $this->pushSubMenuItem($menu, $user_manager->id, 'app.roles', route('roles.index'), 'roles/*', ['super-admin'], 'menu-roles');

                    if ( ! $user->isSuperAdmin() && $user->hasRole('organization-moderator')) {
                        
                        $this->pushMenuItem($menu,
                            'app.organizations',
                            route('organizations.index'),
                            'fa fa-university fa-1',
                            ['organizations/*'],
                            ['super-admin','organization-moderator'],
                            'menu-organizations'
                        );
                    }

                    
                    // Кластеры
                    
                    $this->pushMenuItem($menu,
                        'app.cluster_title',
                        route('clusters.index'),
                        'fa fa-map-marker fa-1',
                        ['clusters/*'],
                        ['super-admin', 'content-manager'], 
                        'menu-clusters'
                    );

                    // Гео. справочники
                    
                    $geo = $this->pushMenuItem($menu,
                        'app.geo_title',
                        '#',
                        'fa fa-compass fa-1',
                        ['cities/*', 'regions/*'],
                        ['super-admin', 'content-manager'], 'menu-geo-toggle');

                    $this->pushSubMenuItem($menu,
                        $geo->id,
                        'app.cities',
                        route('cities.index'),
                        'cities/*',
                        ['super-admin', 'content-manager'],
                        'menu-geo-cities'
                    );

                    $this->pushSubMenuItem(
                        $menu,
                        $geo->id,
                        'app.regions',
                        route('regions.index'),
                        'regions/*',
                        ['super-admin', 'content-manager'],
                        'menu-geo-regions'
                    );


                    // Логи
                    
                    $this->pushMenuItem($menu,
                        'app.menu_laravel_logs',
                        'logs',
                        'fa fa-archive fa-1',
                        ['log/*'],
                        ['super-admin'],  'menu-logs-laravel');
                    

                })->filter(function ( $item ) {
                    /** @var \Lavary\Menu\Item $item */
                    foreach ($item->data('roles') as $role) {
                        if (auth()->user()->hasRole($role)) return true;
                    }

                return false;
            });
        } else {

            // For visual testing reason we have auto login for local ip address (Visual regression testing with Wraith).
            // So we call Auth::login($user) for those ip in Authenticate middleware.
            // But for some reason (I don`t know why), This middleware called before Authenticate middleware.
            // And as you see, it has auth()->check() condition, that obtain «false» value every time after «auto login»
            // But we must initialize $leftMenu variable to prevent errors in the future.
            // Therefore we wrote this dirty special workaround

            Menu::make('leftMenu', function () {});
        }

        return Menu::make('leftMenu', function ( $menu ) {});
    }

    /** Добавляет пункт меню
     *
     * @param Builder $menu
     * @param string $titleLocalization
     * @param string $route
     * @param string $icon
     * @param string $activate
     * @param string | array $role
     * @param $link_id
     * @return Item
     */
    private function pushMenuItem( Builder $menu, string $titleLocalization, string $route, string $icon, $activate, $role, $link_id ) :  Item
    {
        if (is_string($activate)) {
            $item = $menu->add($this->span($titleLocalization), $route)
                ->prepend('<div class="menu-item-icon-wipon-left"><i  class="' . $icon . '"></i></div>')
                ->data('roles', $role);

            $item->link->attr('id', $link_id);
            $this->activateLi($activate, $item);

            return $item;
        }

        $item = $menu->add($this->span($titleLocalization), $route)
            ->prepend('<div class="menu-item-icon-wipon-left"><i class="' . $icon . '"></i></div> ')
            ->data('roles', $role);

        $item->link->attr('id', $link_id);

        if (is_array($activate)) {

            $li = $menu->whereId($item->id)->first();

            foreach ($activate as $activate_pattern) {
                $this->activateLi($activate_pattern, $item);
            }

            return $li;
        }
    }

    /**  Добаляет подменю в меню первого уровня
     *
     * @param Builder $menu
     * @param $parent_id
     * @param $titleLocalization
     * @param $route
     * @param $activate
     * @param $ROLES
     * @return Item
     * @internal param $remission
     */
    private function pushSubMenuItem( Builder $menu, $parent_id, $titleLocalization, $route, $activate, $ROLES, $lick_id )
    {

        $parentItem = $menu->whereId($parent_id)->first();

        if (is_string($activate)) {
            $item = $parentItem->add($this->span($titleLocalization), $route)
                ->active($activate)->data('roles', $ROLES);

            $item->link->attr('id', $lick_id);
            $this->activateLi($activate, $parentItem);

            return $menu;
        }

        $parentItem->add($this->span($titleLocalization), $route)->data('roles', $ROLES);

        if (is_array($activate)) {
            foreach ($activate as $activate_pattern) {
                $parentItem->active($activate_pattern);
                $this->activateLi($activate_pattern, $parentItem);
            }

            return $parentItem->builder;
        }
    }

    /** Добавляет локализованнное название пункта меню, и оборачивает текст в нужные HTML элементы
     *
     * @param string $srt
     * @return string
     */
    private function span( string $srt ) : string
    {
        return '<div class="menu-item-span-wipon"><span>' . trans($srt) . '</span></div>';
    }


    /** Добавляет класс open к пункту меню если url соответствует заданному шаблону
     *
     * @param $activate_pattern
     * @param $parentItem
     */
    private function activateLi( $activate_pattern, $parentItem )
    {
        if ($this->itemIsActive($activate_pattern)) {
            $parentItem->attr('class', 'open');
        }
    }

    /**  Провериет активен ли элемент
     *
     * @param $pattern
     * @return bool
     */
    private function itemIsActive( $pattern )
    {
        $pattern = ltrim(preg_replace('/\/\*/', '(/.*)?', $pattern), '/');
        if (preg_match("@^{$pattern}\z@", \Request::path())) {
            return true;
        }

        return false;
    }

    private function setInvoiceMenuItems( $menu, $user)
    {
   
        if ($user->isSuperAdmin() || $user->hasRole('inspector-admin')) {

            $invoice = $this->pushMenuItem($menu,
                'app.project_name',
                '#',
                'fa fa-print fa-1',
                ['receipts/moderate'],
                ['super-admin', 'receipt-moderator', 'inspector-admin'], 'menu-receipt-moderation');

            $this->pushSubMenuItem($menu, $invoice->id, 'app.receipt_moderation', url('receipts/moderate'), 'receipts/moderate', ['super-admin'], 'menu-logs-moderation');
            $this->pushSubMenuItem($menu, $invoice->id, 'app.participant_list', url('receipts/participants'), 'receipts/participants/*', ['super-admin', 'inspector-admin',  'receipt-moderator-admin',], 'menu-receipt-participants');
            $this->pushSubMenuItem($menu, $invoice->id, 'app.registry', url('receipts/registry'), 'receipts/registry*', ['super-admin', 'inspector-admin'], 'menu-receipt-registry');
            $this->pushSubMenuItem($menu, $invoice->id, 'app.play_prize', url('receipts/play-prize'), 'receipts/play-prize/*', ['super-admin', 'inspector', 'inspector-admin'], 'menu-receipt-prize');
            $this->pushSubMenuItem($menu, $invoice->id, 'app.winners', url('receipts/winners'), 'receipts/winners/*', ['super-admin', 'inspector-admin'], 'menu-receipt-winners');
            $this->pushSubMenuItem($menu, $invoice->id, 'app.statistics', url('receipts/statistics'), 'receipts/statistics/*', ['super-admin','inspector-admin'], 'menu-receipt-statistics');
            $this->pushSubMenuItem($menu, $invoice->id, 'app.messages', url('receipts/messages'), 'receipts/messages/*', ['super-admin','inspector-admin'], 'menu-receipt-messages');
            $this->pushSubMenuItem($menu, $invoice->id, 'app.education', url('receipts/educate-receipt'), 'receipts/educate-receipt/*', ['super-admin','receipt-moderator'], 'menu-receipt-education');

        } else {
    
            $this->pushMenuItem($menu,
                'app.receipt_moderation',
                url('receipts/moderate'),
                'fa fa-wpforms  fa-1',
                ['receipts/moderate'],
                ['super-admin', 'receipt-moderator', 'receipt-moderator-admin', ], 'menu-receipt-moderation');

            $this->pushMenuItem($menu,
                'app.participant_list',
                url('receipts/participants'),
                'fa fa-user-secret  fa-1',
                'receipts/participants/*',
                ['super-admin', 'inspector-admin', 'receipt-moderator-admin' ], 'menu-receipt-participants');
            
            $this->pushMenuItem($menu,
                'app.registry',
                url('receipts/registry'),
                'fa fa-files-o fa-1',
                ['receipts/registry*'],
                ['super-admin', 'inspector', 'receipt-moderator-admin', 'inspector-admin', 'npp'], 'menu-receipt-registry');

            $this->pushMenuItem($menu,
                'app.play_prize',
                url('receipts/play-prize'),
                'fa fa-gift fa-1',
                ['receipts/play-prize/*'],
                ['super-admin', 'inspector', 'inspector-admin'], 'menu-receipt-prize');

            $this->pushMenuItem($menu,
                'app.winners',
                url('receipts/winners'),
                'fa fa-trophy fa-1',
                ['receipts/winners/*'],
                ['super-admin', 'inspector', 'inspector-admin', 'npp'], 'menu-receipt-winners');

            $this->pushMenuItem($menu,
                'app.statistics',
                url('receipts/statistics'),
                'fa fa-bar-chart fa-1',
                ['receipts/statistics/*'],
                ['super-admin', 'inspector-admin', 'npp'], 'menu-receipt-statistics');

            $this->pushMenuItem($menu,
                'app.messages',
                url('receipts/messages'),
                'fa fa-envelope-o fa-1',
                ['receipts/messages/*'],
                ['super-admin', 'inspector-admin', 'inspector', 'npp'], 'menu-receipt-messages');

            $this->pushMenuItem($menu,
                'app.education',
                url('receipts/educate-receipt'),
                'fa fa-graduation-cap fa-1',
                ['receipts/educate-receipt/*'],
                ['super-admin', ], 'menu-receipt-education');
        }

        return $menu;
    }

}