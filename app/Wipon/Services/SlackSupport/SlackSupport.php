<?php

namespace Wipon\Services\SlackSupport;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Log;

/**
 * Class SlackSupport
 * SlackSupport позволяет отправлять сообщения напрямую в Slack команды Wipon
 * @package Wipon\Services\SlackSupport
 */
class SlackSupport
{
    private $_client;

    private $_hookUrl;

    private $_devChanel = "@m.peshkov";

    private $_analyticsChanel = "#analytics";

    private $envIsTesting;

    public function __construct( $hookUrl = "https://hooks.slack.com/services/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" )
    {
        $this->_client = new Client([
            "timeout"         => 5.0,
            "connect_timeout" => 5.0,
        ]);

        $this->_hookUrl = $hookUrl;

        $this->envIsTesting = in_array(config('app.env'), ['local', 'testing', 'staging']);
    }

    /**
     * Пересылка сообщения обратной связи в комнату #feedback
     *
     * @param array $messageFields
     * @return bool
     */
    public function sendFeedback( array $messageFields ): bool
    {
        return $this->executeCurl([
            "channel"    => $this->envIsTesting ? $this->_devChanel : "#feedback",
            "username"   => "WiponApp.com Support",
            "text"       => "*Посетитель Wiponapp.com отправил вам следующее сообщение: *\n" .
                "*Текст:*\n" .
                "```" . array_get($messageFields, "text") . "```\n" .
                "*Имя:* " . array_get($messageFields, "userName") . "\n" .
                "*Электронная почта:* " . array_get($messageFields, "email") . "\n" .
                "*Телефон пользователя:* " . array_get($messageFields, "phone") . "\n" .
                "*Отправлено:* " . array_get($messageFields, "date") . " " . array_get($messageFields, "time") . "\n",
            "icon_emoji" => ":support:",
        ]);
    }

    /**
     * Пересылка сообщения о предложении в комнату #feedback
     *
     * @param  $messageFields
     * @return bool
     */
    public function sendSuggestion( array $messageFields ): bool
    {
        return $this->executeCurl([
            "channel"    => $this->envIsTesting ? $this->_devChanel : "#feedback",
            "username"   => "WiponApp.com Support",
            "text"       => "*Посетитель Wiponapp.com отправил вам следующее предложение: *\n" .
                "*Текст:*\n" .
                "```" . array_get($messageFields, "text") . "```\n" .
                "*Отправлено:* " . array_get($messageFields, "date") . " " . array_get($messageFields, "time") . "\n",
            "icon_emoji" => ":support:",
        ]);
    }

    /**
     * Уведомление о начале обновления статистики
     *
     * @return bool
     */
    public function startSgdStatisticsAggregation( )
    {
        return $this->executeCurl([
            "channel"    => $this->envIsTesting ? $this->_devChanel : $this->_analyticsChanel,
            "username"   => "Cabinet Statistics Service",
            "text"       => "Обновление статистики началось\n",
            "icon_emoji" => ":stat:",
        ]);
    }

    /**
     * Отправка уведомления в канал #analytics ежедневно после собра статистики
     *
     * @param array $messageFields
     * @return bool
     */
    public function sendStatistics( array $messageFields ): bool
    {
        return $this->executeCurl([
            "channel"  => $this->envIsTesting ? $this->_devChanel : $this->_analyticsChanel,
            "username" => "Cabinet Statistics Service",
            "text"     => "Статистика была обновлена. За последние 24 часа: \n" .
                ">*Было проверено " . array_get($messageFields, "checks_total", 0) . " акцизов. Из них:*\n" .

                "> - Поддельных: " . array_get($messageFields, "checks_fake.total", 0) .
                " (сканированием - " . array_get($messageFields, "checks_fake.scan", 0) .
                ", вводом вручную - " . array_get($messageFields, "checks_fake.hand", 0) . ") \n" .

                "> - Подлинных: " . array_get($messageFields, "checks_valid.total", 0) .
                " (сканированием - " . array_get($messageFields, "checks_valid.scan", 0) .
                ", вводом вручную - " . array_get($messageFields, "checks_valid.hand", 0) . ") \n" .

                "> - Атлас: " . array_get($messageFields, "checks_atlas.total", 0) .
                " (сканированием - " . array_get($messageFields, "checks_atlas.scan", 0) .
                ", вводом вручную - " . array_get($messageFields, "checks_atlas.hand", 0) . ") \n" .

                "> - Дубликатов: " . array_get($messageFields, "checks_duplicate.total", 0) .
                " (сканированием - " . array_get($messageFields, "checks_duplicate.scan", 0) .
                ", вводом вручную - " . array_get($messageFields, "checks_duplicate.hand", 0) . ")\n\n".
                "На обновление статистики затрачено: \n" .
                "> - времени: *" . round(array_get($messageFields, "time_spent", 0)) . " секунд *\n".
                "> - памяти: *" . round(array_get($messageFields, "memory_peak", 0)) . " MB *\n",

            "icon_emoji" => ":stat:",
        ]);
    }

    /**
     * Отправка уведомления в канал #alert если за последние 24 часа было проведено > 2200 проверок акцизов
     *
     * @return bool
     */
    public function sendClusterAlert(): bool
    {
        return $this->executeCurl([
            "channel"    => $this->envIsTesting ? $this->_devChanel : "#alert",
            "username"   => "Cabinet Statistics Service",
            "text"       => "*За последние 24 часа было создано более "
                . config('wipon.cluster_creating_limit')
                . " кластеров.* \n"
                . " *МЫ ПРИБЛИЗИЛИСЬ К ГЕОЛОКАЦИОННОМУ ЛИМИТУ* \n"
                . " *Следует подумать о покупке Google или Yandex подписки* \n"
                . " *В противном случае рискуем перестать отдавать пользователям данные о геолокации и привязывать проверки акцизов к регионам (привет статистика кабинета для ДГД)*",
            "icon_emoji" => ":support:",
        ]);
    }

    /**
     * Отправка сообщения при ошибках обновления статистики
     *
     * @param array $errorData
     * @return bool
     */
    public function sendStatisticErrorAlert( array $errorData )
    {
        return $this->executeCurl([
            "channel"    => $this->envIsTesting ? $this->_devChanel : $this->_analyticsChanel,
            "username"   => "Cabinet Statistics Service",
            "text"       => "Ошибка при обновлении статистики: \n" .
                ">*Ошибка из-за: " . array_get($errorData, "message", 'Message was not provided') . "*\n" .
                ">*Код ошибки: " . array_get($errorData, "errorCode", 0) . "*\n" .
                ">*Сообщение ошибки: " . array_get($errorData, "errorMessage", 'Message was not provided') . "*\n" .
                ">*Метод, в котором произошла ошибка: " . array_get($errorData, "method", 'Not set') . "*\n" .
                ">*Ссылка на лог файлы: " . env('APP_URL', 'localhost') . "/logs" . " *\n",
            "icon_emoji" => ":stat:",
        ]);
    }

    /**
     * @param array $postFields
     * @return bool
     */
    private function executeCurl( array $postFields ): bool
    {
        try {
            $response = $this->_client->post($this->_hookUrl, [
                "json" => $postFields,
            ]);
        } catch (RequestException $e) {
            $response = $e->getResponse();

            Log::error("WiponApp.com can't send Slack message!", [
                "message"       => $postFields,
                "slackResponse" => [
                    "statusCode" => $response->getStatusCode(),
                    "body"       => $response->getBody()->getContents(),
                ],
            ]);
        }

        return $response->getStatusCode() == 200;
    }


}