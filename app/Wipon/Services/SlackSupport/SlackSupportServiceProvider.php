<?php

namespace Wipon\Services\SlackSupport;

use Illuminate\Support\ServiceProvider;

class SlackSupportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('support_slack', function () {
            return new SlackSupport(config('wipon.slack_webhook'));
        });
    }

    public function provides()
    {
        return ['support_slack'];
    }
}