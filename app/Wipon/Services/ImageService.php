<?php

namespace Wipon\Services;

use File;
use Image;
use Intervention\Image\Image as ImageInstance;
use Wipon\Models\Image as WiponImage;
use Wipon\Repos\ImageRepository;

class ImageService
{

    /** Path to original image  stored in db
     * @var
     */
    protected $pathDB;

    protected $pathSystem;


    /** Extension of saving images
     * @var string
     */
    protected $extension = '.jpg';

    /**
     * @var ImageRepository
     */
    protected $image;

    /** Image size
     * @var int
     */
    private $size;

    /**
     * @throw Exception
     * @param ImageRepository $image
     * @param string $extension
     */
    protected function __construct( ImageRepository $image, $extension = '.jpg' )
    {
        $this->setPaths();

        $this->image = $image;
        $this->extension = $extension;
    }

    /** Set public path's
     *
     * @return  void
     */
    protected function setPaths()
    {
        $this->pathSystem = public_path("images/cabinet/");
        $this->pathDB = "cabinet/";

        $this->checkDirOrNew($this->pathSystem);
    }

    /**
     * Save pic to storage
     *
     * @param ImageInstance $file
     * @param array $size
     * @param string $extension
     * @return bool
     * @throws \Exception
     */
    public static function save( ImageInstance $file, array $size = null, $extension = '.jpg' )
    {
        $service = new self(new ImageRepository(), $extension);

        $service->size = $size;
        $imageModelInstance = $service->createImageModel();

        $paths = $service->storeImages($file, $imageModelInstance);

        $service->updateImageModel($imageModelInstance->id, $paths->original->db, $paths->thumb->db);

        if ($service->imageOnDisk($paths)) {

            $service->makeImageProgressive($paths->original->system);
            $service->makeImageProgressive($paths->thumb->system);

            return $imageModelInstance->id;
        }

        throw new \Exception('Visual Feature Image is not saved.');
    }

    public static function updateWithImageFromSystem( WiponImage $image, $data = [] )
    {
        //original
        $original_path = public_path('images/' . $image->getRawAttribute('original_uri'));
        $thumb_uri = public_path('images/' . $image->getRawAttribute('thumb_uri'));

        if (count($data) > 0) {
            list($data_width, $data_height, $data_x, $data_y) = $data;
            // update existing image original
            $image = Image::make($original_path)->crop($data_width, $data_height, $data_x, $data_y);
        } else {
            $image = Image::make($original_path);
        }

        File::delete($original_path);
        File::delete($thumb_uri);

        // save original 
        $image->save($original_path);

        // save thumb
        $image->resize(
            null,
            config("wipon.image.thumb.height"),
            function ( $constraint ) {
                $constraint->aspectRatio();
            })->save($thumb_uri);
    }

    /** Crop and update image on disk
     *
     * @param ImageInstance $image_resource
     * @param WiponImage $image_old
     * @param array $size
     * @param string $extension
     */
    public static function updateWithUploadedImage( ImageInstance $image_resource, WiponImage $image_old, array $size = null, $extension = '.jpg' )
    {
        $service = new self(new ImageRepository(), $extension);

        list($path_original, $path_thumb) = $service->deleteImage($image_old);

        if ($service->canBeSized($size)) {
            $image_resource->resize($size['width'], $size['height']);
        }

        // in case path is broken
        if (File::isWritable($path_original) && File::isWritable($path_thumb)) {
            $service->saveUploadedImage($image_resource, $path_original, $path_thumb);
        } else {
            $originalPath = $service->recoverPathFromUri('images/' . $image_old->getRawAttribute('original_uri'));
            $thumbPath = $service->recoverPathFromUri('images/' . $image_old->getRawAttribute('thumb_uri'));

            $service->saveUploadedImage($image_resource, $originalPath, $thumbPath);
        }
    }

    private function saveUploadedImage( $image_resource, $pathOriginal, $pathThumb )
    {
        $image_resource = $this->resizeImage($image_resource, config('wipon.image.original_height'));
        $image_resource->save($pathOriginal);

        $image_resource = $this->resizeImage($image_resource, config('wipon.image.thumb_height'));
        $image_resource->save($pathThumb);

        $this->makeImageProgressive($pathOriginal);
        $this->makeImageProgressive($pathThumb);
    }

    private function recoverPathFromUri( $uri )
    {
        $fragments = preg_split("/\/+/", $uri);
        $publicPath = public_path();

        foreach ($fragments as $fragment) {

            $publicPath .= '/' . $fragment;

            if ($this->isFileName($fragment)) {
                break;
            }

            $this->checkDirOrNew($publicPath);
        }

        return $publicPath;
    }


    /**
     * @param WiponImage $image_old
     * @return array
     */
    public static function deleteImage( WiponImage $image_old )
    {
        $original_uri = 'images/' . $image_old->getRawAttribute('original_uri');
        $thumb_uri = 'images/' . $image_old->getRawAttribute('thumb_uri');

        $path_original = public_path($original_uri);
        $path_thumb = public_path($thumb_uri);

        File::delete($path_original);
        File::delete($path_thumb);

        return [$path_original, $path_thumb];
    }

    /**
     * Check if the the given path exists
     *
     * @param  string $path
     * @return string   mixed
     */
    protected function checkDirOrNew( string $path ) :string
    {
        if ( ! File::exists($path))

            // create dir
            File::makeDirectory($path);

        return $path;
    }

    /** Create new image
     *
     * @return WiponImage
     */
    protected function createImageModel() : WiponImage
    {
        return $this->image->create(['original_uri' => '', 'thumb_uri' => '']);
    }

    /** Update image instance with new links
     *
     * @param $id
     * @param $original_uri
     * @param $thumb_uri
     * @return WiponImage
     */
    protected function updateImageModel( $id, $original_uri, $thumb_uri )  : WiponImage
    {
        return $this->image->update($id, ['original_uri' => $original_uri, 'thumb_uri' => $thumb_uri]);
    }

    /**  Converts JPEG to progressive JPEG
     *
     * @param string $path_to_file
     * @throws \Exception
     * @return void
     */
    protected function makeImageProgressive( string $path_to_file )
    {
        if ($this->extension === '.png') return;

        getimagesize($path_to_file);

        /* Attempt to open */
        $im = @imagecreatefromjpeg($path_to_file);

        /* See if it failed */
        if ($im) {
            /* Tri to save image in progressive JPEG */
            if (imageinterlace($im, 1)) {
                imagejpeg($im, $path_to_file);
                imagedestroy($im);
            } else {
                dd('Error saving image in progressive JPEG');
            }
        }
    }

    /** Store original and thumbs
     *
     * @param ImageInstance $file
     * @param $imageModelInstance
     * @return \stdClass paths
     */
    protected function storeImages( ImageInstance $file, $imageModelInstance )
    {
        $paths = $this->getImagePaths($imageModelInstance->id);

        // store original 
        $this->storeImageOnDisk($this->resizeImage($file, config('wipon.image.original_height')), $paths->original->system);
        $this->storeImageOnDisk($this->resizeImage($file, config('wipon.image.thumb_height')), $paths->thumb->system);

        return $paths;
    }

    /** Get images paths in system and in DB
     *
     * @param int $image_id
     * @return object
     */
    protected function getImagePaths( int $image_id )
    {
        $hash = crc32($image_id);

        list($dirToStore, $pathInDB) = $this->getDirToStore($hash);
        list($original, $thumb) = $this->getImageNames($hash);

        return array_to_std_object([
            'original' => [
                'system' => $dirToStore . '/' . $original,
                'db'     => $pathInDB . '/' . $original,
            ],
            'thumb'    => [
                'system' => $dirToStore . '/' . $thumb,
                'db'     => $pathInDB . '/' . $thumb,
            ],
        ]);
    }

    /**
     * Store image on disk
     *
     * @param ImageInstance $image
     * @param string $systemImagePath
     * @return string
     */
    private function storeImageOnDisk( ImageInstance $image, string $systemImagePath )
    {
        if (File::exists($systemImagePath)) {
            File::delete((string) $systemImagePath);
        }
        
        $imageWithWhiteBackground = Image::canvas($image->width(), $image->height(), 'ffffff')->insert($image, 'center');

        $imageWithWhiteBackground->save($systemImagePath);

        $this->makeImageProgressive($systemImagePath);
    }

    /** Get concatenated with  image names
     * @param $hash
     * @return array
     */
    protected function getImageNames( $hash )
    {
        $original = $hash . '_original' . $this->extension;
        $thumb = $hash . '_thumb' . $this->extension;

        return [$original, $thumb];
    }

    /** Get image storing dir name
     * @param $hash
     * @return string
     */
    protected function getDirToStore( $hash )
    {
        $dirToStore = $this->pathSystem . substr($hash, 0, config('wipon.image_dirs_length'));
        $pathInDB = $this->pathDB . substr($hash, 0, config('wipon.image_dirs_length'));

        $this->checkDirOrNew($dirToStore);

        return [$dirToStore, $pathInDB];
    }

    /** Check if image exists on disk
     * @param $paths
     * @return bool
     */
    protected function imageOnDisk( $paths )
    {
        return File::exists($paths->original->system) && File::exists($paths->thumb->system);
    }

    /**
     * @param ImageInstance $image
     * @return ImageInstance
     */
    public static function fitPhoneScreen( ImageInstance $image )
    {
        $image->widen(config("wipon.image.original.width"), function ( $constraint ) {
            $constraint->upsize();
        })
            ->heighten(config("wipon.image.original.height"), function ( $constraint ) {
                $constraint->upsize();
            });

        return Image::canvas(
            config("wipon.image.original.width"),
            config("wipon.image.original.height"),
            'ffffff')
            ->insert($image, 'center');
    }

    /** If size elements were given
     * @param $size
     * @return bool
     */
    private function canBeSized( $size )
    {
        return $size && $size['width'] && $size['height'];
    }

    /** Resize images to default values
     * @param $image
     * @param $size
     * @return mixed
     */
    public function resizeImage( $image, $size )
    {
        $image->resize(null, $size, function ( $constraint ) {
            $constraint->aspectRatio();
        });

        return $image;
    }

    private function isFileName( $name )
    {
        $fragments = preg_split("/\.+/", $name);

        return (count($fragments) > 0) && isset($fragments[1]) && $this->isImageExtension($fragments[1]);
    }

    private function isImageExtension( $ext )
    {
        $supportedExtensions = ['jpg', 'png', 'jpeg'];

        return in_array($ext, $supportedExtensions);
    }
}