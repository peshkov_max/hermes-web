<?php

namespace Wipon\Services;

use App\Jobs\RefreshKgdStatJob;
use Cache;
use Carbon\Carbon;
use DB;
use Storage;
use Wipon\Models\{
    Alcohol\StatisticLog, Check, Cluster, Device, Item
};

/** Statistics creator
 *
 * Class StatisticService
 * @package Wipon\Services
 */
class StatisticService
{
    /**
     * The number of devices in system
     * @var int
     */
    private $deviceSum = 0;

    /**
     * The number of clusters in system
     * @var int
     */
    private $clusterSum = 0;


    /**
     * The number of product items in system
     * @var int
     */
    private $itemSum = 0;


    /*** The number of checks
     * @var int
     */
    private $checkSum = 0;

    /**
     * StatisticService constructor.
     */
    public function __construct()
    {
        $this->storeCacheMinute = config('wipon.cache_store_minute', 10);

        // Only devices with platform filled field
        $this->deviceSum = Device::where('platform', '!=', '')->count();
        $this->clusterSum = Cluster::count();
        $this->itemSum = Item::count();
        $this->checkSum = Check::count();
    }


    /** Get number of device
     * @return int
     */
    public function getDevicesSum()
    {
        return $this->deviceSum;
    }

    /** Get number of clusters
     * @return int
     */
    public function getClusterSum()
    {
        return $this->clusterSum;
    }

    /** Get number of product items
     * @return int
     */
    public function getProductItemSum()
    {
        return $this->itemSum;
    }

    /** Get device statistics
     * @return mixed
     */
    public function getDeviceStat()
    {
        $service = $this;

        return Cache::remember('deviceStat', $this->storeCacheMinute, function () use ( $service ) {

            if ($service->deviceSum == 0) return [0, 0, 0];

            return [
                'total'             => $service->deviceSum,
                'unknownPercentage' => Device::where('platform', '=', '')->count(),
                'iosPercentage'     => $service->getPlatformPercentageByName('ios'),
                'androidPercentage' => $service->getPlatformPercentageByName('android'),
            ];
        });
    }

    /** Get cluster statistics
     * @return mixed
     */
    public function getClusterStat()
    {
        $service = $this;

        return Cache::remember('clusterStat', $this->storeCacheMinute, function () use ( $service ) {

            if ($service->clusterSum == 0) return [0, 0, 0, 0, 0];

            return [
                'total' => $service->clusterSum,

                'moderated' => $service->getModeratedClusterSum(),
                'fixed'     => $service->getFixedClusterSum(),
                'licensed'  => $service->getLicencedClusterSum(),

                'moderatedPercentage' => $service->getPercentage($service->getModeratedClusterSum(), $service->clusterSum),
                'fixedPercentage'     => $service->getPercentage($service->getFixedClusterSum(), $service->clusterSum),
                'licensedPercentage'  => $service->getPercentage($service->getLicencedClusterSum(), $service->clusterSum),

                'clusterInChecks'           => $service->getClusterInChecksSum(),
                'clusterInChecksPercentage' => $service->getPercentage($service->getClusterInChecksSum(), $service->clusterSum),
            ];
        });
    }

    /** Get product item statistics
     * @return mixed
     */
    public function getProductItemStat()
    {
        $service = $this;

        return Cache::remember('productStat', $this->storeCacheMinute, function () use ( $service ) {

            if ($service->itemSum == 0) return [0, 0, 0, 0, 0, 0, 0];

            return [
                'total' => $service->itemSum,

                'scansTotal'      => $service->checkSum,
                'scansWithGeo'    => $service->getPercentage($service->getScansWithGeo(), $service->checkSum),
                'scansWithoutGeo' => $service->getPercentage($service->getScansWithoutGeo(), $service->checkSum),

                'fake'      => $service->getFakeItemSum(),
                'valid'     => $service->getValidItemSum(),
                'atlas'     => $service->getAtlasItemSum(),
                'duplicate' => $service->getDuplicatedItemSum(),

                'fakePercentage'       => $service->getPercentage($service->getFakeItemSum(), $service->itemSum),
                'validPercentage'      => $service->getPercentage($service->getValidItemSum(), $service->itemSum),
                'atlasPercentage'      => $service->getPercentage($service->getAtlasItemSum(), $service->itemSum),
                'duplicatedPercentage' => $service->getPercentage($service->getDuplicatedItemSum(), $service->itemSum),

                'handSum' => $service->getItemByHandSum(),
                'scanSum' => $service->getItemByScanSum(),

                'handPercentage' => $service->getPercentage($service->getItemByHandSum(), $service->itemSum),
                'scanPercentage' => $service->getPercentage($service->getItemByScanSum(), $service->itemSum),

                'checksWithoutGeolocation' => $service->getChecksWithoutGeolocation(),

            ];
        });
    }

    /** Get sum of moderated clusters
     * @return mixed
     */
    public static function getModeratedClusterSum()
    {
        return Cache::remember('moderatedClusterSum', config('wipon.cache_store_minute', 10), function () {
            return Cluster::where('is_moderated', true)->count();
        });
    }

    /** Get sum of fixed clusters
     * @return mixed
     */
    public static function getFixedClusterSum()
    {
        return Cache::remember('fixedClusterSum', config('wipon.cache_store_minute', 10), function () {
            return Cluster::where('is_fixed', true)->count();
        });
    }

    /** Get sum of licenced clusters
     * @return mixed
     */
    public static function getLicencedClusterSum()
    {
        return Cache::remember('licencedClusterSum', config('wipon.cache_store_minute', 10), function () {
            return Cluster::where('is_licensed', true)->count();
        });
    }

    /** Get number if clusters in which scans were performed
     * @return mixed
     */
    public function getClusterInChecksSum()
    {
        return Cache::remember('clusterInChecksSum', $this->storeCacheMinute, function () {

            $queryResult = DB::select(DB::raw(
                "select count(*) from (SELECT *, row_number() over (
                        PARTITION BY kgd_alcohol.check_statistics.cluster_id
                        ORDER BY kgd_alcohol.check_statistics.cluster_id ) as r
                        FROM kgd_alcohol.check_statistics
                )
                as q
            where q.r = 1"));

            return ! isset($queryResult[0]) ? 0 : $queryResult[0]->count;
        });
    }

    /** Get number of checks without geolocation
     * @return mixed
     */
    public static function getChecksWithoutGeolocation()
    {
        return Cache::remember('checksWithoutGeolocation', config('wipon.cache_store_minute', 10), function () {
            return Check::whereNull('cluster_id')->count();
        });
    }

    /** Get the number of fake items
     * @return mixed
     */
    public static function getFakeItemSum()
    {
        return Cache::remember('fakeItemSum', config('wipon.cache_store_minute', 10), function () {
            return Item::where('status', '=', 'fake')->count();
        });
    }

    /** Get the number of valid items
     * @return mixed
     */
    public static function getValidItemSum()
    {
        return Cache::remember('validItemSum', config('wipon.cache_store_minute', 10), function () {
            return Item::where('status', '=', 'valid')->count();
        });
    }

    /** Get the number of atlas items
     * @return mixed
     */
    public static function getAtlasItemSum()
    {
        return Cache::remember('atlasItemSum', config('wipon.cache_store_minute', 10), function () {
            return Item::where('status', '=', 'atlas')->count();
        });
    }

    /** Get the number of duplicate items
     * @return mixed
     */
    public static function getDuplicatedItemSum()
    {
        return Cache::remember('duplicatedItemSum', config('wipon.cache_store_minute', 10), function () {
            return Item::where('is_duplicated', true)->count();
        });
    }

    /** Get the number of items checked by hand
     * @return mixed
     */
    public function getItemByHandSum()
    {
        return Cache::remember('itemByHand', $this->storeCacheMinute, function () {
            return Item::where("items.sticker->serial_number", '!=', '')->count();
        });
    }

    /** Get the number of items checked by scan
     * @return mixed
     */
    public function getItemByScanSum()
    {
        return Cache::remember('itemByScan', $this->storeCacheMinute, function () {
            return Item::where("items.sticker->excise_code", '!=', '')->count();
        });
    }

    /**
     * Calculates the percentage of devices with a specific platform in the total number of devices
     *
     * @param string $platform
     * @return float
     */
    private function getPlatformPercentageByName( string $platform = 'android' ): float
    {
        try {

            $devices = Device::where('platform', 'ilike', "%$platform%")->count();

            return $devices == 0 ? 0 : $this->getPercentage($devices, $this->deviceSum);
        } catch (\Exception $e) {

            log_e($e);

            return 0.0;
        }
    }

    /**
     * Count the percentage
     *
     * @param int $total - Total items
     * @param int $items - The number of items
     * @return float
     */
    private function getPercentage( int $items, int $total ) : float
    {
        try {

            return round($items * 100 / $total, 0);
        } catch (\Exception $e) {

            log_e('Error while counting percentage');
            log_e($e);

            return 0;
        }
    }


    /** Возвращает время последнего обновления статистики
     *
     * @return Carbon date | string
     */
    public static function getLastKgdStatUpdateDate()
    {
        /**
         * Кешируем результат на две минуты (учитываем время обновления статистики)
         */
        return Cache::remember('lastKgdStatUpdate', 2, function () {

            $lastGenerate = StatisticLog::where('action', 'generate')->orderBy('created_at', 'desc')->first();

            return $lastGenerate ? $lastGenerate->created_at->setTimezone(config('wipon.user_time_zone')) : trans('app.stat_was_not_formed');
        });
    }

    /**
     * Запуск регенерации статистики КГД
     */
    public static function regenerateKGDStat()
    {
        if ( ! self::kgdStatIsUpdatingNow()) {

            dispatch((new RefreshKgdStatJob())->onQueue('cabinet_stat'));
        } else
            log_w("Trying to start stat regenerating, but it's now regeneration(in progress)");
    }

    /** Проверяет обновляется сейчас статистика или нет
     * @return bool
     */
    public static function kgdStatIsUpdatingNow()
    {
        return Storage::exists('stat_updating_pid');
    }

    private function getScansWithGeo()
    {
        return Cache::remember('getScansWithGeo', config('wipon.cache_store_minute', 10), function () {

            return Check::whereNotNull('geo_longitude')->whereNotNull('geo_latitude')->count();
        });
    }

    private function getScansWithoutGeo()
    {
        return Cache::remember('getScansWithoutGeo', config('wipon.cache_store_minute', 10), function () {

            return Check::whereNull('geo_longitude')->whereNull('geo_latitude')->count();
        });
    }

    private function getScansTotal()
    {
        return Cache::remember('getScansWithoutGeo', config('wipon.cache_store_minute', 10), function () {

            return Check::count();
        });
    }
}