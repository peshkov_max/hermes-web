<?php 

namespace  Wipon\Guard;

use Wipon\Models\User;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;

/**
 * Class AccessUserProvider implements Wipon's specific requirements
 * 
 * @see http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5407003
 * @package Wipon\Services
 */
class UserProvider extends EloquentUserProvider{

    /**
     * Validate a user against the given credentials.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param  array $credentials
     * @return bool
     */

}