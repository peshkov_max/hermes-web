<?php

namespace Wipon\Models;

/**
 * Wipon\Models\VisualFeature
 *
 * @property mixed id
 * @property mixed product_id
 * @property mixed image_id
 * @property null fake_image_id
 * @property string description_ru
 * @property string description_en
 * @property string description_kk
 * @property mixed image_fake
 * @property mixed image
 * @property int order_number
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereProductId($value)

 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereFakeImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereDescriptionKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereDescriptionRu($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereDescriptionEn($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\VisualFeature whereOrderNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel withName($name)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 * @mixin \Eloquent
 */
class VisualFeature extends I18nModel
{
    protected $visibleI18n = ['description'];

    public $timestamps = false;
    
    protected $fillable = [
        'description_en', 'description_kk', 'description_ru', 'fake_image_id', 'image_id', 'product_id', 'order_number'
    ];
    
    protected $casts = [
        'image_id' => 'integer' ,
        'order_number' =>'integer' ,
        'product_id' => 'integer' ,
    ];

    protected $hidden = [
        'description_kk'
    ];

    /** Get product
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /** get Image
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    /** Get fake image
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image_fake()
    {
        return $this->belongsTo(Image::class,  'fake_image_id', 'id');
    }
    
 



}
