<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * Wipon\Models\Permission
 *
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static Builder|Permission whereId($value)
 * @method static Builder|Permission whereName($value)
 * @method static Builder|Permission whereDisplayName($value)
 * @method static Builder|Permission whereDescription($value)
 * @method static Builder|Permission whereCreatedAt($value)
 * @method static Builder|Permission whereUpdatedAt($value)
 * @method static Builder|Permission whereDeletedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Role[] $roles
 */
class Permission extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'display_name', 'description',
    ];

    protected $visible = [
        'name',
    ];

    protected $dates = [
        'deleted_at', 'created_at', 'updated_at'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
