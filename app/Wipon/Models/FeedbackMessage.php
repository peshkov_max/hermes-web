<?php

namespace Wipon\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;


class FeedbackMessage extends BaseModel
{
    
    protected $fillable = [
        'name', 'phone_number', 'email', 'text', 'app_log', 'created_at', 'updated_at',
    ];
    
    protected $visible = [
        'device_label',  
        'sent_at',
        'api_requests', 
        'id', 
        'read_at', 
        'device', 
        'name', 
        'phone_number', 
        'email', 
        'text',  
        'created_at',
        'device_uuid',
        //'device_message_count'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'read_at'
    ];

    protected $casts = [
        'app_log' => 'array',
    ];

    protected $appends = [
        'device_label', 'sent_at'
    ];

    public function getDeviceLabelAttribute()
    {
        return $this->device ? $this->device->model . ' (' . $this->device->platform.')' : '-';
    }

    public function getSentAtAttribute()
    {
        $time = trans('app.time');
        $date = trans('app.date');
        
        $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])
            ->setTimezone(new \DateTimeZone(config('wipon.user_time_zone') ?? config('app.timezone')));
        
        return "$time: {$created_at->format('H:i')}. $date: {$created_at->format('d.m.Y')}";
    }

    /** Get feedback messages device
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    /** Get device api requests
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function api_requests()
    {
        return $this->hasMany(ApiRequest::class, 'device_uuid', 'device_uuid');
    }

    /**
     * Эта функция преобразует все HTML-сущности  содержимого поля text в соответствующие символы
     *
     * Из-за того, что на некоторых устройствах при конвертации в JSON ковычки преобразуются в &quot;
     * а иногда просто экранируются.
     *
     * @param $value
     * @return mixed
     */
    public function getTextAttribute( $value )
    {
        return html_entity_decode($value);
    }
}
