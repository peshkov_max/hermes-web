<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Role
 *
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\User[] $users
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Role whereDeletedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Permission[] $permissions
 */
class Role extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name','display_name','description'
    ];
    
    protected $visible = [
        'id','display_name','description' , 'users', 'name'
    ];
    
    const PER_PAGE = 20;
    
    protected $dates = ['deleted_at', 'crated_at', 'updated_at'];
    
    /** get users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'role_user');
    }

    /** Get permissions
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    
}
