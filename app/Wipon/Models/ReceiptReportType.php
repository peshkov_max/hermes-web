<?php

namespace Wipon\Models;


/**
 * Wipon\Models\ReceiptReportType
 *
 * @property integer $id
 * @property string $dev_name
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kk
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReportType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReportType whereDevName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReportType whereNameRu($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReportType whereNameEn($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReportType whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel withName($name)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 * @mixin \Eloquent
 */
class ReceiptReportType extends I18nModel
{
    public $timestamps = false;
    
    protected $visibleI18n = [
        'name'
    ];

}
