<?php
namespace Wipon\Models;

use Carbon\Carbon;

/**
 * Wipon\Models\Receipt
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $device_uuid
 * @property integer $bin
 * @property float $summ
 * @property string $status
 * @property string $photo
 * @property string $given_datetime
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereRegionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereDeviceUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereBin($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereSumm($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt wherePhoto($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereGivenDatetime($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property float $sum
 * @property-read \Wipon\Models\Device $device
 * @property-read \Wipon\Models\Region $region
 * @property-read mixed $date
 * @property-read mixed $time
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Receipt whereSum($value)
 */
class Receipt extends BaseModel
{
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['date', 'time'];

    protected $dates = [
        'created_at', 'given_datetime', 'updated_at',
    ];

    protected $fillable = [
        'bin', 'sum', 'given_datetime'
    ];

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function getDateAttribute()
    {
        return $this->given_datetime 
            ? $this->given_datetime->format('d.m.Y')
            : null;
    }

    public function getTimeAttribute()
    {
        return $this->given_datetime
            ? $this->given_datetime->format('H:i')
            : null;
    }

    public function getCreatedAtAttribute( $date )
    {
        return Carbon::createFromFormat(config('wipon.date_time_format'), $date, 'UTC')
            ->setTimezone(config('wipon.user_time_zone'))->format(config('wipon.dt_format_d_m_y_h_i'));
    }
    
}