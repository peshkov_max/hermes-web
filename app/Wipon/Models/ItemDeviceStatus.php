<?php

namespace Wipon\Models;

use Illuminate\Database\Query\Builder;
/**
 * Wipon\Models\ItemDeviceStatus
 *
 * @property string $device_uuid
 * @property integer $item_id
 * @property string $status
 * @property-read \Wipon\Models\Item $item
 * @property-read \Wipon\Models\Device $device
 * @method static Builder|ItemDeviceStatus whereDeviceUuid($value)
 * @method static Builder|ItemDeviceStatus whereItemId($value)
 * @method static Builder|ItemDeviceStatus whereStatus($value)
 * @mixin \Eloquent
 */
class ItemDeviceStatus extends BaseModel
{
    public $timestamps = false;
    
    protected $visible = ['status'];

    /** Get status item 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /** Get status devise
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }
}
