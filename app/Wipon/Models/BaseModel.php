<?php
namespace Wipon\Models;

use Cache;
use File;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 *
 * Реализует кэширование зависимость
 *
 * Если вы хотите чтобы relation текущей модели была закэширована
 * Определите в модели следующее свойство:
 * public $cacheable = [ 'roles', 'organization' ]; - Укажите, список кэшируемых зависимостей
 *
 * @package Wipon\Models
 */
class BaseModel extends Model
{
    /**
     * Настройки кэширования зависимостей
     *
     * @var array
     */
    public $cacheable = [];
    
    /**
     * Dynamically retrieve attributes on the model.
     *
     * @parent
     * @param string $method
     * @return mixed|null
     */
    public function __get( $method )
    {
        // log_i();
        if ($this->needParentCall($method)) {
            return parent::__get($method);
        }
     

        if (method_exists($this, $method)) {

            $model = $this;

            return Cache::remember($this->getCachedKey($method), config('cache.minute_to_store_in_cache', 30),
                function () use ( $model, $method ) {

                    try {

                        return $model->getRelationValue($method);
                    } catch (\Exception $e) {
                        log_i('Error while getting model relationship', [$e->getMessage()]);

                        return null;
                    }
                });
        }

        return null;
    }

    /**
     * Получить ключ для хэша
     *
     * @param $method
     * @return string
     */
    protected function getCachedKey( $method )
    {
        $idToPaste = isset($this->attributes['id']) ? $this->attributes['id'] : (auth()->user()->id ?? str_limit(md5('$method'), 3, ''));
        
        log_on_dev(' Relation loaded from cache: ' . str_singular($this->getTable()) . '_' . $idToPaste . '_' . $method);
        
        return  str_singular($this->getTable()) . '_' . $idToPaste . '_' . $method;
    }

    /**
     * Проверяет не превышает ли количество используемой памяти текущего лимита.
     *
     * Лимит определен здесь: config/cache.php:53
     */
    private function cantStoreToMemcached()
    {
        $m = new \Memcached();

        $host = config('cache.stores.memcached.servers')[0]['host'];
        $port = config('cache.stores.memcached.servers')[0]['port'];

        $m->addServer($host, $port);

        $stats = $m->getStats();

        if (isset($stats["$host:$port"]) && isset($stats["$host:$port"]['bytes'])) {

            return $stats["$host:$port"]['bytes'] > config('cache.stores.memcached.cache_size_limit');
        }

        log_e('Cant get memcached driver stat info. Not save relation in cache!');

        return true;
    }

    /**
     * Проверяет не превышает ли размер каталога кэша текущего лимита.
     *
     * Лимит определен здесь: config/cache.php:53
     */
    private function cantStoreToFile()
    {
        try {

            $fileSize = 0;

            foreach (File::allFiles(storage_path('framework/cache')) as $file) {
                $fileSize += $file->getSize();
            }

            return $fileSize > config('cache.stores.file.cache_size_limit');
        } catch (\Exception $e) {

            log_e('Error while calculating cache folder size', [$e->getMessage()]);

            return false;
        }
    }

    /**
     * Проверяет нужно ли сохранять relationship в кэше
     *
     * @param $method
     * @return bool
     */
    private function needParentCall( $method )
    {
        
        
        // Не сохраняем в кэш если в свойстве модели cacheable нет массива с именем relations     
        if ( ! is_array($this->cacheable)) {

            return true;
        }

        // Не сохраняем в кэш если вызываемое имя свойства $method
        // не включено в массив relations свойства cacheable
        if ( ! in_array($method, $this->cacheable)) {

            return true;
        }

        // Если используется cache driver memcached - проверяем возможность сохранения в кэше 
        if (config('cache.default') === 'memcached' && $this->cantStoreToMemcached()) {

            return true;
        }

        // Если используется cache driver file - проверяем возможность сохранения в кэше 
        if (config('cache.default') === 'file' && $this->cantStoreToFile()) {

            return true;
        }

        return false;
    }

  
    /**
     * Remove relations from cache 
     */
    public function removeModelFromCache()
    {
        foreach ( $this->cacheable as $relation) {
            Cache::forget($this->getCachedKey($relation));
        }
    }
}