<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Wipon\Traits\Models\ModerationTrait;

/**
 * Wipon\Models\Company
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Product[] $products
 * @property-read \Wipon\Models\City $city
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $city_id
 * @property string $logo_url
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kz
 * @property string $deleted_at
 * @property-read \Wipon\Models\User $user
 * @property-read mixed $Checkned_at
 * @property-read mixed $created_at
 * @property mixed name_kk
 * @property null image_id
 * @property null is_supplier
 * @property null email
 * @property null contract_number
 * @property null contact_person
 * @property mixed is_moderated
 * @property mixed aliases
 * @method static Builder|Organization whereId($value)
 * @method static Builder|Organization whereCityId($value)
 * @method static Builder|Organization whereLogoUrl($value)
 * @method static Builder|Organization whereNameRu($value)
 * @method static Builder|Organization whereNameEn($value)
 * @method static Builder|Organization whereNameKz($value)
 * @method static Builder|Organization whereDeletedAt($value)
 * @method static Builder|I18nModel withName($cityName)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\User[] $users
 * @property-read \Wipon\Models\Image $logo
 * @property mixed legal_name 
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization whereImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization whereLegalName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization whereContractNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization whereContactPerson($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization whereIsModerated($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Organization applyModerationFilter($field)
 */
class Organization extends I18nModel
{
    use SoftDeletes, ModerationTrait;

    public $timestamps = false;

    protected $visibleI18n = [
        'name', 'city', "is_supplier"
    ];

    protected $fillable = [
        'name_kk', 'name_ru', 'name_en', 'image_id', 'city_id', 'contract_number', 'email', 'contact_person', 'is_moderated', 'legal_name' , "is_supplier"
    ];

    public $cacheable = [
        'logo', 'city'
    ];

    /**
     * Get all of the Organization's aliases.
     */
    public function aliases()
    {
        return $this->morphMany(Alias::class, 'aliasable');
    }

    /** Get company products
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /** Get company city
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /** Get company city_presents
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city_presents()
    {
        return $this->belongsToMany(City::class);
    }

    /** Get company user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /** Get company user
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function logo()
    { 
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function getLogoUrlAttribute()
    {
        return $this->attributes['logo_url'] ?? config('wipon.image_path');
    }

    public function getModerationLabel()
    {
        return $this->is_moderated 
            ? '<label class="label label-success">'.trans('app.moderated').'</label>' 
            : '<label class="label label-danger">'.trans('app.not_moderated').'</label>';
    }
}