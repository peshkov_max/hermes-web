<?php

namespace Wipon\Models;

/**
 * Wipon\Models\Suggestion
 *
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $suggestion
 * @property-read \Wipon\Models\Product $product
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $suggestable_id
 * @property string $suggestable_type
 * @property integer $product_id
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Suggestion whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Suggestion whereSuggestableId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Suggestion whereSuggestableType($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Suggestion whereProductId($value)
 */
class Suggestion extends BaseModel
{
    /** Get suggestion
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function suggestion()
    {
        return $this->morphTo();
    }
 
    /** Get product
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /**
     * Get all of the products that are assigned this tag.
     */
    public function products()
    {
        return $this->morphedByMany(Product::class, 'suggestion');
    }

    /**
     * Get all of the product_types that are assigned this tag.
     */
    public function product_types()
    {
        return $this->morphedByMany(ProductType::class, 'suggestion');
    }
}
