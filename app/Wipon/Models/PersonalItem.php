<?php

namespace Wipon\Models;

use Eloquent;

class PersonalItem extends Eloquent
{

    protected $visible = [
        'id', 'incorrect_sticker','device_uuid', 'item_id'
    ];
    protected $casts = [
        'incorrect_sticker' => 'array',
    ];
}