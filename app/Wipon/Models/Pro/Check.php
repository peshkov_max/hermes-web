<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\Check
 *
 * @property integer $id
 * @property integer $item_id
 * @property integer $user_id
 * @property integer $store_id
 * @property float $latitude
 * @property float $longitude
 * @property integer $accuracy
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Wipon\Models\Pro\User $user
 * @property-read \Wipon\Models\Pro\Item $item
 * @property-read \Wipon\Models\Pro\Store $store
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereAccuracy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Check whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Check extends ModelPro
{
    protected $guarded = [
        "id",
    ];

    protected $visible = [
        "item",
        "created_at",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function store()
    {
        return $this->belongsTo(Store::class);
    }
}