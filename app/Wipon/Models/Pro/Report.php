<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;


class Report extends ModelPro
{
    protected $fillable = [
        "item_id",
        "user_id",
        "sticker_uri",
        "label_uri",
        "causer",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }
}