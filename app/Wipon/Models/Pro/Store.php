<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Pro\Store
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $region_id
 * @property string $city
 * @property string $name
 * @property string $bin
 * @property string $legal_name
 * @property string $license_number
 * @property float $latitude
 * @property float $longitude
 * @property integer $radius
 * @property string $street
 * @property string $house
 * @property integer $store_type_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Wipon\Models\Pro\Region $region
 * @property-read \Wipon\Models\Pro\StoreType $type
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereRegionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereBin($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereLegalName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereLicenseNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereRadius($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereStreet($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereHouse($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Store whereStoreTypeId($value)
 * @mixin \Eloquent
 */
class Store extends ModelPro
{
    use SoftDeletes;

    protected $dates = ["deleted_at"];

    protected $guarded = ["id", "user_id", "updated_at", "created_at", "deleted_at"];

    protected $hidden = [
        "region_id", "radius", "updated_at", "created_at", "deleted_at"
    ];
    
    public function getRouteKeyName()
    {
        return 'user_id';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function type()
    {
        return $this->belongsTo(StoreType::class, "store_type_id");
    }
}
