<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

class ModelPro extends Model
{
    protected $connection = 'pgsql_pro';
}