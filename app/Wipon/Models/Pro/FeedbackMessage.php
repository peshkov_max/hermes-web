<?php

namespace Wipon\Models\Pro;

use Carbon\Carbon;

/**
 * Wipon\Models\Pro\FeedbackMessage
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $phone_number
 * @property string $email
 * @property string $text
 * @property mixed $app_log
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Wipon\Models\Pro\User $user
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage wherePhoneNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereAppLog($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\FeedbackMessage whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FeedbackMessage extends ModelPro
{
    protected $visible = [
        'id', 'name', 'phone_number', 'email', 'text', 'app_log', 'user_id', 'user', 'is_unread', 'created_at', 'sent_at'
    ];

    protected $appends = [
        'sent_at',
    ];

    protected $fillable = [
        'user_id', 'name', 'phone_number', 'email', 'text', 'app_log', 'is_unread',
    ];

    protected $casts = [
        'app_log' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /** 
     * Получаем дату в формате: Время: 23:29. Дата: 02.02.2005
     *
     * @return string
     */
    public function getSentAtAttribute()
    {
        $time = trans('app.time');
        $date = trans('app.date');

        $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])
            ->setTimezone(new \DateTimeZone(config('wipon.user_time_zone') ?? config('app.timezone')));

        return "$time: {$created_at->format('H:i')}. $date: {$created_at->format('d.m.Y')}";
    }
}