<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\AccountTransaction
 *
 * @property integer $id
 * @property integer $account_id
 * @property float $sum
 * @property mixed $raw_info
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Wipon\Models\Pro\Account $account
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AccountTransaction whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AccountTransaction whereAccountId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AccountTransaction whereSum($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AccountTransaction whereRawInfo($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AccountTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AccountTransaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AccountTransaction extends ModelPro
{
    protected $hidden = ["user_id", "raw_info", "updated_at"];

    protected $fillable = ["sum", "raw_info"];

    protected $casts = [
        "raw_info" => "array",
        "sum" => "float"
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }
}
