<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\StoreType
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kk
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\StoreType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\StoreType whereNameRu($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\StoreType whereNameEn($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\StoreType whereNameKk($value)
 * @mixin \Eloquent
 */
class StoreType extends ModelPro
{
    public $timestamps = false;

    protected $guarded = ["id"];

    


}
