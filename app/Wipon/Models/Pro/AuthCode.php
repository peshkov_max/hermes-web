<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\AuthCode
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $code
 * @property \Carbon\Carbon $expires_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Wipon\Models\Pro\User $user
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AuthCode whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AuthCode whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AuthCode whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AuthCode whereExpiresAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AuthCode whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\AuthCode whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AuthCode extends ModelPro
{
    protected $dates = ["expires_at"];

    protected $fillable = [
        "user_id", "code", "expires_at"
    ];

    protected $hidden = [
        "id", "user_id", "created_at",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}