<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Pro\Account
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $number
 * @property string $provider
 * @property integer $balance
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Wipon\Models\Pro\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\AccountTransaction[] $transactions
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereProvider($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereBalance($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Account whereDeletedAt($value)
 * @mixin \Eloquent
 */
class Account extends ModelPro
{
    use SoftDeletes;

    protected $dates = ["deleted_at"];

    protected $fillable = [
        "user_id", "number", "provider", "balance"
    ];

    protected $visible = [
        "id", "number", "provider", "balance"
    ];

    protected $casts = [
        "sum" => "float"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->hasMany(AccountTransaction::class);
    }
}
