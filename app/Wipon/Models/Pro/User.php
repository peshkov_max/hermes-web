<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Pro\User
 *
 * @property integer $id
 * @property string $api_token
 * @property string $phone_number
 * @property mixed $device
 * @property string $name
 * @property string $email
 * @property string $platform
 * @property string $model
 * @property string $app_version
 * @property string $app_language
 * @property string $work_phone_number
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\AuthCode[] $auth_codes
 * @property-read \Wipon\Models\Pro\Store $store
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\ApiRequest[] $api_requests
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\Account[] $accounts
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\AccountTransaction[] $transactions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\Subscription[] $subscriptions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\Notification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\Check[] $checks
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereApiToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User wherePhoneNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereDevice($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereAppVersion($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereAppLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\User whereWorkPhoneNumber($value)
 * @mixin \Eloquent
 */
class User extends ModelPro
{
    protected $connection = 'pgsql_pro';

    use SoftDeletes;

    protected $dates = ["deleted_at"];

    protected $fillable = [
        "work_phone_number", "name", "email", "app_language", "app_version", "device",
    ];

    protected $visible = [
        "id", "feedback_messages", "store", "name", "phone_number", "work_phone_number",
        "email", "app_language", "app_version", "device", "device_info", "updated_at", "feedback_messages_count",
        "notifications", "push_token"
    ];

    protected $casts = [
        "device" => "array",
    ];

    protected $appends = [
        'device_info', 'push_token'
    ];

    public function auth_codes()
    {
        return $this->hasMany(AuthCode::class);
    }

    public function store()
    {
        return $this->hasOne(Store::class);
    }

    public function api_requests()
    {
        return $this->hasMany(ApiRequest::class);
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function transactions()
    {
        return $this->hasManyThrough(AccountTransaction::class, Account::class);
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function checks()
    {
        return $this->hasMany(Check::class);
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }

    public function feedback_messages()
    {
        return $this->hasMany(FeedbackMessage::class);
    }

    
    
    public function setPlatformAttribute( $value )
    {
        $this->setDevice('platform', $value);
    }

    public function setModelAttribute( $value )
    {
        $this->setDevice('model', $value);
    }

    private function setDevice( $attribute, $value )
    {
        $device = $this->device;
        $device[ $attribute ] = $value;
        $this->device = $device;
    }

    public function getPlatformAttribute()
    {
        return $this->device['platform'] ?? null;
    }

    public function getModelAttribute()
    {
        return $this->device['model']  ?? null;
    }
    
    public function getPushTokenAttribute()
    {
        return $this->device['push_token']  ?? null;
    }

    /**
     * Получить поле device info в формате: Устройство ( Платформа )
     *
     * @return string
     */
    public function getDeviceInfoAttribute()
    {
        $str = '-';

        if ( ! $this->device) {
            return $str;
        }

        if (isset($this->device['model'])) {
            $str = $this->device['model'];
        }

        if ( ! isset($this->device['platform'])) {
            return $str;
        }

        if ($str === '-') {
            $str = $this->device['platform'];
        }

        if ($str !== '-') {
            $str .= ' (' . $this->device['platform'] . ')';
        }

        return $str;
    }
}
