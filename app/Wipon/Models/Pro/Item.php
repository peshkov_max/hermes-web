<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\Item
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $excise_code
 * @property string $serial_number
 * @property string $status
 * @property \Carbon\Carbon $bottled_at
 * @property string $created_at
 * @property string $updated_at
 * @property-read \Wipon\Models\Pro\Product $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Pro\Check[] $checks
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereExciseCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereSerialNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereBottledAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Item whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Item extends ModelPro
{
    protected $dates = ["bottled_at"];

    protected $fillable = [
        "product_id",
        "status",
        "excise_code",
        "serial_number",
    ];

    protected $visible = [
        "id",
        "product",
        "status",
        "bottled_at",
    ];

    public function getStatusAttribute($value)
    {
        if ($value === ITEM_STATUS_VALID) {
            return "valid";
        }

        if ($value === ITEM_STATUS_FAKE) {
            return "fake";
        }

        if ($value === ITEM_STATUS_ATLAS) {
            return "atlas";
        }

        return null;
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function checks()
    {
        return $this->hasMany(Check::class);
    }

    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}