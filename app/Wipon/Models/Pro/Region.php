<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\Region
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kk
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Region whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Region whereNameRu($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Region whereNameEn($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Region whereNameKk($value)
 * @mixin \Eloquent
 */
class Region extends ModelPro
{
    public $timestamps = false;

    protected $guarded = ["id"];
}
