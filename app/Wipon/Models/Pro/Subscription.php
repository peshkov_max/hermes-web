<?php

namespace Wipon\Models\Pro;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Pro\Subscription
 *
 * @property-read \Wipon\Models\Pro\User $user
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user_id
 * @property boolean $is_active
 * @property \Carbon\Carbon $expires_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Subscription whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Subscription whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Subscription whereIsActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Subscription whereExpiresAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Subscription whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Subscription whereDeletedAt($value)
 */
class Subscription extends ModelPro
{
    use SoftDeletes;

    protected $dates = ["deleted_at", "expires_at"];

    protected $fillable = [
         "is_active", "expires_at", "user_id"
    ];

    protected $visible = [
        "is_active", "expires_at", "created_at", "user", "id"
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getExpiresAtAttribute($value)
    {
      return $this->format($value);
    }

    public function getCreatedAtAttribute($value)
    {
        return $this->format($value);
    }

    private function format(  $value)
    {
        $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $value)
            ->setTimezone(new \DateTimeZone(config('wipon.user_time_zone') ?? config('app.timezone')));
        
        return "{$created_at->format('d.m.Y')} {$created_at->format('H:i')}";
    }
}
