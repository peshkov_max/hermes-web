<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\ApiRequest
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $client_ip
 * @property string $method
 * @property string $url
 * @property string $path
 * @property mixed $header
 * @property mixed $content
 * @property mixed $response
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Wipon\Models\Pro\User $user
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereClientIp($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereHeader($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereResponse($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\ApiRequest whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ApiRequest extends ModelPro
{
    protected $guarded = ['id'];
    protected $hidden = [ 'user_id', 'client_ip'];

    protected $casts = [
        'header' => 'array',
        'content' => 'array',
        'response' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
