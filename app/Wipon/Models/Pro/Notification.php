<?php

namespace Wipon\Models\Pro;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Pro\Notification
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $text
 * @property boolean $is_unread
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Notification whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Notification whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Notification whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Notification whereIsUnread($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Notification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Notification whereDeletedAt($value)
 * @mixin \Eloquent
 */
class Notification extends ModelPro
{
    use SoftDeletes;

    protected $visible = [
        "id", "text", "is_unread", "created_at", "user_id", "user", "feedback_messages_count", "sent_at"
    ];

    protected $fillable = [
        "text", "is_unread"
    ];

    protected $dates = [
        "deleted_at"
    ];

    protected $appends = [
        'sent_at',
    ];

    public function feedback_messages()
    {
        return $this->hasMany(FeedbackMessage::class, 'user_id', 'user_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Получаем дату в формате: Время: 23:29. Дата: 02.02.2005
     *
     * @return string
     */
    public function getSentAtAttribute()
    {
        $time = trans('app.time');
        $date = trans('app.date');

        $created_at = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])
            ->setTimezone(new \DateTimeZone(config('wipon.user_time_zone') ?? config('app.timezone')));

        return "$time: {$created_at->format('H:i')}. $date: {$created_at->format('d.m.Y')}";
    }
}