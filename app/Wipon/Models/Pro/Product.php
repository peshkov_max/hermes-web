<?php

namespace Wipon\Models\Pro;

use Illuminate\Database\Eloquent\Model;

/**
 * Wipon\Models\Pro\Product
 *
 * @property integer $id
 * @property string $type
 * @property string $organization
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Product whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Product whereOrganization($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Pro\Product whereName($value)
 * @mixin \Eloquent
 */
class Product extends ModelPro
{
    public $timestamps = false;

    protected $guarded = [
        "id"
    ];

    protected $visible = [
        "name",
        "type",
        "organization",
    ];
}