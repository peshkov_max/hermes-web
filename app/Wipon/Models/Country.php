<?php

namespace Wipon\Models;


/** @noinspection PhpUnnecessaryFullyQualifiedNameInspection */

/**
 * Country
 *
 * @property-read Continent $continent
 * @property-read \Illuminate\Database\Eloquent\Collection|Region[] $regions
 * @property integer $id
 * @property integer $continent_id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_kz
 * @property string $code
 * @property string $deleted_at
 * @method static \Illuminate\Database\Query\Builder|Country whereId($value)
 * @method static \Illuminate\Database\Query\Builder|Country whereContinentId($value)
 * @method static \Illuminate\Database\Query\Builder|Country whereNameEn($value)
 * @method static \Illuminate\Database\Query\Builder|Country whereNameRu($value)
 * @method static \Illuminate\Database\Query\Builder|Country whereNameKz($value)
 * @method static \Illuminate\Database\Query\Builder|Country whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|Country whereDeletedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\GeoAlias[] $aliases
 * @property-read mixed $name
 * @property-read mixed $scanned_at
 * @property-read mixed $created_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel withName($cityName)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Country filter($countryFilter)
 * @mixin \Eloquent
 * @property string $name_kk
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Region[] $cities
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Country whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 */
class Country extends I18nModel
{
    
    public $timestamps = false;
    
    protected $visible = [
        'continent', 'code', 'id', 'name_kk', 'name_ru','name_en',
    ];
    
    protected $visibleI18n = [
        'name'
    ];
    
    protected $fillable = [
        'name_kk', 'name_ru','name_en', 'continent_id'
    ];

    /*** Get country continent
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function continent()
    {
        return $this->belongsTo(Continent::class);
    }

    /** Get country regions
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function regions()
    {
        return $this->hasMany(Region::class);
    }

    /** Get country cities
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function cities()
    {
        return $this->hasManyThrough(Region::class, City::class);
    }

    /** Get country aliases
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function aliases()
    {
        return $this->morphMany(Alias::class, 'alias');
    }
}
