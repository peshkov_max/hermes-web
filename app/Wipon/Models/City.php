<?php

namespace Wipon\Models;

use Illuminate\Database\Query\Builder;

/**
 * City
 *
 * @property-read Region $region
 * @property-read Country $country
 * @property integer $id
 * @property integer $region_id
 * @property integer $country_id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_kz
 * @property string $deleted_at
 * @method static Builder|City whereId($value)
 * @method static Builder|City whereRegionId($value)
 * @method static Builder|City whereCountryId($value)
 * @method static Builder|City whereNameEn($value)
 * @method static Builder|City whereNameRu($value)
 * @method static Builder|City whereNameKz($value)
 * @method static Builder |City whereDeletedAt($value)
 * @property-read mixed $name_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Cluster[] $clusters
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\GeoAlias[] $aliases
 * @property-read mixed $name
 * @property-read mixed $scanned_at
 * @property-read mixed $created_at
 * @method static Builder|I18nModel withName($cityName)
 * @method static Builder|City getRegionCities()
 * @method static Builder|City defaultCity($cityName)
 * @method static Builder|City filter($cityFilter = null)
 * @mixin \Eloquent
 * @property float $longitude
 * @property float $latitude
 * @property string $timezone
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\City whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\City whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\City whereTimezone($value)
 * @property string $name_kk
 * @property boolean $is_regional_center
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\City whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\City whereIsRegionalCenter($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 */
class City extends I18nModel
{
    
    public $timestamps = false;
    
    protected $visible = [
        'id', 'region', 'region_id', 'country', 'id', 'latitude',
        'longitude', 'name', 'name_ru', 'name_kk', 'name_en',
        'timezone', 'is_regional_center',
    ];
    
    protected $fillable = [
        'name_ru', 'name_kk', 'name_en', 'latitude', 'longitude', 'timezone', 'region_id', 'is_regional_center'
    ];
    
    protected $visibleI18n = [
        'name', 'latitude', 'longitude', 'id'

    ];
    
    /** Get string in format  "city_name(city_id)" 
     * @return string
     */
    public function getNameIdAttribute()
    {
        return $this->name . ' (' . $this->id . ')';
    }

    /** Get city's region
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    /** Get city's clusters
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function clusters()
    {
        return $this->hasMany(Cluster::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function aliases()
    {
        return $this->morphMany(Alias::class, 'aliasable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function organizations()
    {
        return $this->belongsToMany(Organization::class);
    }


}
