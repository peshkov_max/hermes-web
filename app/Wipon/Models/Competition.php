<?php

namespace Wipon\Models;


/**
 * App\Competition
 *
 * @property integer $id
 * @property \Carbon\Carbon $ending_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Competition whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Competition whereEndingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Competition whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Competition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Competition extends BaseModel
{

    protected $dates =[
        'ending_at',
        'created_at',
        'updated_at',
    ];
}
