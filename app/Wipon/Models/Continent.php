<?php

namespace Wipon\Models;

use \Illuminate\Database\Query\Builder;


/**
 * Continent
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Country[] $countries
 * @property integer $id
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_kz
 * @method static Builder| Continent whereId($value)
 * @method static Builder| Continent whereNameEn($value)
 * @method static Builder| Continent whereNameRu($value)
 * @method static Builder| Continent whereNameKz($value)
 * @property-read mixed $name
 * @property-read mixed $scanned_at
 * @property-read mixed $created_at
 * @property-read mixed $deleted_at
 * @method static Builder|I18nModel withName($cityName)
 * @mixin \Eloquent
 * @property string $name_kk
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Continent whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 */
class Continent extends I18nModel
{
    public $timestamps = false;
    
    protected $visibleI18n = [
        'name'
    ];
    
    protected $fillable = [
        'name_kk', 'name_ru','name_en'
    ];

    /** Get continent countries
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function countries()
    {
        return $this->hasMany(Country::class);
    }
}
