<?php

namespace Wipon\Models\Alcohol;

use Carbon\Carbon;
use Wipon\Models\BaseModel;

/**
 * Wipon\Models\Alcohol\StatisticLog
 *
 * @property string total
 * @property Carbon created_at
 * @property int memory_peak
 * @property double aggregation
 * @property double mapping
 * @property double saving_stat
 * @property string action
 * @mixin \Eloquent
 */
class StatisticLog extends BaseModel
{
    public $timestamps = false;

    protected $table = 'kgd_alcohol.statistic_logs';

    protected $casts = [
        'aggregation' => 'double',
        'mapping' => 'double',
        'saving_stat' => 'double',
        'total'  => 'double',
    ];

    protected $fillable = ['created_at', 'total', 'aggregation', 'action', 'memory_peak', 'mapping', 'saving_stat'];

    protected $dates = ['created_at'];
}
