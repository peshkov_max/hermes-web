<?php

namespace Wipon\Models\Alcohol;

use Wipon\Models\BaseModel;
use Wipon\Models\Check;
use Wipon\Models\City;

/**
 * Wipon\Models\ClusterStatistic
 *
 * @mixin \Eloquent
 * @property mixed latitude
 * @property mixed longitude
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Check[] $checks
 * @property-read \Wipon\Models\City $city
 * @property-read mixed $address
 */
class CheckStatistic extends BaseModel
{
    public $timestamps = false;

    protected $table = 'kgd_alcohol.check_statistics';

    protected $dates = [
        'scanned_at',
    ];

    protected $guarded = [
        'fake_field',
    ];

    /**
     * Used while generation stat
     *
     * const GENERATION_MAP
     */
    const GENERATION_MAP =
        [
            'item_valid_hand' => ['valid', 'hand', 'distinct item_id'],
            'item_valid_scan' => ['valid', 'scan', 'distinct item_id'],

            'item_fake_hand' => ['fake', 'hand', 'distinct item_id'],
            'item_fake_scan' => ['fake', 'scan', 'distinct item_id'],

            'item_duplicate_hand' => ['duplicate', 'hand', 'distinct item_id'],
            'item_duplicate_scan' => ['duplicate', 'scan', 'distinct item_id'],

            'item_atlas_hand' => ['atlas', 'hand', 'distinct item_id'],
            'item_atlas_scan' => ['atlas', 'scan', 'distinct item_id'],

            'check_valid_hand' => ['valid', 'hand', '*'],
            'check_valid_scan' => ['valid', 'scan', '*'],

            'check_fake_hand' => ['fake', 'hand', '*'],
            'check_fake_scan' => ['fake', 'scan', '*'],

            'check_duplicate_hand' => ['duplicate', 'hand', '*'],
            'check_duplicate_scan' => ['duplicate', 'scan', '*'],

            'check_atlas_hand' => ['atlas', 'hand', '*'],
            'check_atlas_scan' => ['atlas', 'scan', '*'],

            'unique_devices' => ['unique_devices', 'unique_devices', 'distinct checks.device_uuid'],
        ];

    /**
     * Поля выгружаемые для отображения, используются в CheckStatisticRepository
     */
    const SELECT_FIELDS = "
    sub_query.id,
    sub_query.check_id,
    sub_query.city_id,
    sub_query.name,
    sub_query.latitude,
    sub_query.longitude,
    sub_query.street,
    sub_query.house,
    
    sub_query.check_valid_hand,
    sub_query.check_valid_scan,
    sub_query.check_fake_hand,
    sub_query.check_fake_scan,
    sub_query.check_atlas_hand,
    sub_query.check_atlas_scan,
    sub_query.check_duplicate_hand,
    sub_query.check_duplicate_scan,
    sub_query.item_valid_hand,
    sub_query.item_valid_scan,
    sub_query.item_fake_hand,
    sub_query.item_fake_scan,
    sub_query.item_atlas_hand,
    sub_query.item_atlas_scan,
    sub_query.item_duplicate_hand,
    sub_query.item_duplicate_scan,
    
    sub_query.check_valid_sum,
    sub_query.check_fake_sum,
    sub_query.check_atlas_sum,
    sub_query.check_duplicate_sum,
    
    sub_query.item_valid_sum,
    sub_query.item_fake_sum,
    sub_query.item_atlas_sum,
    sub_query.item_duplicate_sum,
    sub_query.item_total,
    sub_query.check_total,
    sub_query.check_by_hand_sum,
    sub_query.check_by_scan_sum,
    sub_query.item_by_hand_sum,
    sub_query.item_by_scan_sum,
    sub_query.unique_devices,
    core.cities.name_ru as city_name,
    core.regions.name_ru as region_name ";

    /**
     *
     * Поля выгружаемые из подзапроса, используются в CheckStatisticRepository
     */
    const SUB_QUERY_FIELDS =
        "core.clusters.id as id,
    core.clusters.cluster_type_id as cluster_type_id,
    kgd_alcohol.check_statistics.check_id as check_id, 
    core.clusters.city_id,
   
    core.clusters.name_ru as name,  
    core.clusters.latitude,
    core.clusters.longitude,
    core.clusters.accuracy,
    core.clusters.street,
    core.clusters.house,
    core.clusters.is_moderated,
    core.clusters.is_fixed,
    core.clusters.is_licensed,
    row_number()
    OVER w AS row_number,
    sum(check_valid_hand)
    OVER w AS check_valid_hand,
    sum(check_valid_scan)
    OVER w AS check_valid_scan,
    sum(check_fake_hand)
    OVER w AS check_fake_hand,
    sum(check_fake_scan)
    OVER w AS check_fake_scan,
    sum(check_atlas_hand)
    OVER w AS check_atlas_hand,
    sum(check_atlas_scan)
    OVER w AS check_atlas_scan,
    sum(check_duplicate_hand)
    OVER w AS check_duplicate_hand,
    sum(check_duplicate_scan)
    OVER w AS check_duplicate_scan,
    sum(item_valid_hand)
    OVER w AS item_valid_hand,
    sum(item_valid_scan)
    OVER w AS item_valid_scan,
    sum(item_fake_hand)
    OVER w AS item_fake_hand,
    sum(item_fake_scan)
    OVER w AS item_fake_scan,
    sum(item_atlas_hand)
    OVER w AS item_atlas_hand,
    sum(item_atlas_scan)
    OVER w AS item_atlas_scan,
    sum(item_duplicate_hand)
    OVER w AS item_duplicate_hand,
    sum(item_duplicate_scan)
    OVER w AS item_duplicate_scan,

    sum(check_valid_hand + check_valid_scan)
    OVER w AS check_valid_sum,
    sum(check_fake_hand + check_fake_scan)
    OVER w AS check_fake_sum,
    sum(check_atlas_hand + check_atlas_scan)
    OVER w AS check_atlas_sum,
    sum(check_duplicate_hand + check_duplicate_scan)
    OVER w AS check_duplicate_sum,

    sum(item_valid_hand + item_valid_scan)
    OVER w AS item_valid_sum,
    sum(item_fake_hand + item_fake_scan)
    OVER w AS item_fake_sum,
    sum(item_atlas_hand + item_atlas_scan)
    OVER w AS item_atlas_sum,
    sum(item_duplicate_hand + item_duplicate_scan)
    OVER w AS item_duplicate_sum,
    sum(
        item_valid_hand + item_valid_scan +
        item_fake_hand + item_fake_scan +
        item_duplicate_hand + item_duplicate_scan +
        item_atlas_hand + item_atlas_scan)
    OVER w AS item_total,
    sum(
        check_valid_hand + check_valid_scan +
        check_fake_hand + check_fake_scan +
        check_duplicate_hand + check_duplicate_scan +
        check_atlas_hand + check_atlas_scan)
    OVER w AS check_total,
    sum(
        check_valid_hand +
        check_fake_hand +
        check_duplicate_hand +
        check_atlas_hand)
    OVER w AS check_by_hand_sum,
    sum(
        check_valid_scan +
        check_fake_scan +
        check_duplicate_scan +
        check_atlas_scan)
    OVER w AS check_by_scan_sum,
    sum(
        item_valid_hand +
        item_fake_hand +
        item_duplicate_hand +
        item_atlas_hand)
    OVER w AS item_by_hand_sum,
    sum(
        item_valid_scan +
        item_fake_scan +
        item_duplicate_scan +
        item_atlas_scan)
    OVER w AS item_by_scan_sum,
    unique_devices";

    public static function getQueryRequiredFields()
    {
        return [self::SELECT_FIELDS, self::SUB_QUERY_FIELDS];
    }

    public function toArray()
    {
        $array = parent::toArray();

        $array['address'] = $this->address;
        $array['name'] = $this->name ?? trans('app.no_info');

        return $array;
    }


    /** Get cluster's scan
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checks(){
        return $this->hasMany(Check::class, 'id', 'check_id');
    }


    /** Get cluster's city
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city(){
        return $this->belongsTo(City::class);
    }

    public function getAddressAttribute()
    {
        $var [] = isset($this->attributes["city_name"]) ? trans('app.city_prefixed', ['city_name' => $this->attributes["city_name"]]) : trans('app.no_city');
        $var [] = isset($this->attributes['street']) ? trans('app.street_prefixed', ['street_name' => $this->attributes["street"] ?? ' - ']) : trans('app.no_street');
        $var [] = isset($this->attributes['house']) && $this->attributes['house'] ? $this->attributes['house'] : trans('app.no_house');

        return implode(', ', $var);
    }
}