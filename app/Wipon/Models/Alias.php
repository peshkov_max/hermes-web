<?php

namespace Wipon\Models;

/**
 * Wipon\Models\Alias
 *
 * @property integer $id
 * @property integer $aliasable_id
 * @property string $aliasable_type
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $aliasable
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Alias whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Alias whereAliasableId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Alias whereAliasableType($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Alias whereName($value)
 * @mixin \Eloquent
 */
class Alias extends BaseModel
{

    public $timestamps = false;
    
    public $fillable = ['name'];
    
    public function aliasable()
    {
        return $this->morphTo();
    }
}

