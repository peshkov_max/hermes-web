<?php

namespace Wipon\Models;

use Cache;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Wipon\Models\Scopes\TestScope;
use Wipon\Traits\Models\CacheableTrait;
use Wipon\Traits\Models\UserTrait;

/**
 * Wipon\Models\User
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property Relation | Organization $organization
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereUpdatedAt($value)
 * @property string $api_token
 * @property string $deleted_at
 * @property mixed $organization_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Role[] $roles
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereApiToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereCompanyId($value)
 * @property boolean $is_testing
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereIsTesting($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\User whereOrganizationId($value)
 */
class User extends Authenticatable
{
    use SoftDeletes, UserTrait;

    public $cacheable = [
        'roles', 'organization', 
    ];
    
    protected $fillable = [
        'name', 'email', 'password', 'organization_id', 'api_token', 'created_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        /** According to project docs
         * @link http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5932740
         * @link http://docs.wiponapp.com:8090/pages/diffpagesbyversion.action?pageId=5932740&selectedPageVersions=8&selectedPageVersions=7
         */
        static::addGlobalScope(new TestScope());
    }

    /** Get user company
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /** Get roles
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }
}
