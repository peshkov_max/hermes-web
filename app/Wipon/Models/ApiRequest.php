<?php

namespace Wipon\Models;

class ApiRequest extends BaseModel
{
    
    protected $connection = 'pgsql_logs';
    
    protected $guarded = ['id'];

    protected $hidden = [
        'client_ip',
    ];

    protected $visible = [
        'id', 'created_at', 'device', 'method', 'url', 'path', 'header', 'content', 'response',

        // Следующие поля динамические, используются в  app/Wipon/Repos/DeviceRepository.php:38
        'last_request_timestamp', 'device_uuid', 'model', 'platform', 'feedback_messages_count', 'push_token', 'app_version',

        // Используется здесь - app/Wipon/Repos/DeviceRepository.php:120
        'customer'
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    protected $casts = [
        'header'   => 'array',
        'content'  => 'array',
        'response' => 'array',
    ];

    public function toArray()
    {
        $array = parent::toArray();

        if (isset($array['last_request_timestamp'])) {
            $array['last_request_timestamp'] = $this->getLastRequestTimestamp($array['last_request_timestamp']);
        }

        return $array;
    }

    /**
     * Device from with we did curent request
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    /**
     * Date need to be the following format  ДД.ММ,ГГГГ ЧЧ:ММ
     * @see http://jira.wiponapp.com:8090/pages/viewpage.action?pageId=5932945
     * @param $date
     * @return string
     */
    public function getCreatedAtAttribute( $date )
    {
        return set_user_timezone($date);
    }

    /** Get device feedback messages
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedback_messages()
    {
        return $this->hasMany(FeedbackMessage::class, 'device_uuid', 'device_uuid');
    }

    public function getLastRequestTimestamp( $value )
    {
        return set_user_timezone($value);
    }

    /**
     * Does not exists in model
     * Used only in app/Wipon/Repos/DeviceRepository.php:120
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
