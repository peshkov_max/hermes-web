<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\Debug\Exception\FatalThrowableError;
use Wipon\Traits\Models\ModerationTrait;

/**
 * Wipon\Models\Product
 * @property-read \Illuminate\Database\Eloquent\Relations\HasMany $items
 * @property-read \Wipon\Models\ProductType $product_type
 * @property-read \Wipon\Models\Organization $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Suggestion[] $suggestions
 * @property array common_info
 * @property mixed description
 * @property mixed video
 * @property mixed image
 * @property int product_type_id
 * @property int is_enabled
 * @property int is_verified
 * @property string name_ru
 * @property string name_en
 * @property string name_kk
 * @property string barcode
 * @property int $id
 * @property int $image_id
 * @property Image $background_image
 * @property string organization_id
 * @property mixed is_moderated
 * @property mixed background_image_id
 * @property mixed legal_name
 * @property Organization organization
 * @property ProductType type
 * @method whereOrganizationId
 * @mixin \Eloquent
 */
class Product extends I18nModel
{
    use  ModerationTrait;

    public $timestamps = false;

    protected $visibleI18n = ['name'];

    protected $fillable = [
        'background_image_id', 
        'image_id', 
        'product_type_id', 
        'organization_id', 
        'name_ru', 
        'name_en', 
        'name_kk', 
        'is_enabled', 
        'common_info', 
        'barcode', 
        'is_moderated',
    ];
    
    protected $visible = [
        'image',
        'background_image',
        'product_type_id', 
        'organization', 
        'id',
        'barcode', 
        'common_info', 
        'is_moderated', 
        'is_enabled', 
        'legal_name',  
        'name_ru', 
        'name_en', 
        'name_kk',
        'visual_features'
    ];


    /**
     * @property array common_info
     * {
     *     video_url: string
     *     image_url: string?
     *     description_ru: string?
     *     description_en: string?
     *     description_kk: string?
     *     ]
     * }
     *
     * @var array
     */
    protected $casts = [
        'debug_info'   => 'array',
        'common_info'  => 'array',
        'is_enabled'   => 'boolean',
        'is_moderated' => 'boolean',
    ];


    public function toArray()
    {
        $array = parent::toArray();

        $array{'thumb_uri'} = asset($this->getImageUrl());

        try {
            $array{'type'} = $this->type ? $this->type->toArray() : [];
            $array{'organization'} = $this->organization ? $this->organization->toArray() : [];
        } catch (FatalThrowableError  $e) {
            $array{'type'} = [];
            $array{'organization'} = [];
        }

        return $array;
    }

    /**
     * Get all of the Product's aliases.
     */
    public function aliases()
    {
        return $this->morphMany(Alias::class, 'aliasable');
    }

    /**
     * Get product type
     */
    public function type()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id', 'id');
    }

    /** Get product company
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /** Get product items
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /** Get product visual_features
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visual_features()
    {
        return $this->hasMany(VisualFeature::class);
    }

    /** Get product image
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    /** Get product background image
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function background_image()
    {
        return $this->belongsTo(Image::class, 'background_image_id', 'id');
    }

    /** Get product suggestions
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function suggestions()
    {
        return $this->morphMany(Suggestion::class, 'suggestion');
    }

    /** Get description
     *
     * @return null
     */
    public function getDescriptionAttribute()
    {
        return $this->common_info[ 'description_' . current_locale() ] ?? null;
    }

    public function getImageUrl()
    {
        return $this->image->original_uri ?? null;
    }

    public function getBackgroundImageUrl()
    {
        return $this->background_image->original_uri ?? null;
    }

    public function isEnabled()
    {
        return $this->is_enabled ? '<label class="label label-success">' . trans('app.shown') . '</label>' : '<label class="label label-danger">' . trans('app.hidden') . '</label>';
    }

    public function isModerated()
    {
        return $this->is_moderated ? '<label class="label label-success">' . trans('app._is_moderated') . '</label>' : '<label class="label label-danger">' . trans('app._is_not_moderated') . '</label>';
    }

    /**
     * @param $q Builder
     * @return Builder
     */
    public function scopeFilterAccess( $q )
    {
        /** @noinspection PhpUndefinedMethodInspection
         *
         * @var $user User
         */
        $user = auth()->user();

        if ($user->hasRole(['super-admin', 'product-moderator']))
            return $q;

        elseif ($user->hasRole(['commercial', 'commercial-admin']))
            return $q->where('organization_id', $user->organization_id);

        abort(400);

        return $q;
    }

    /**
     * Get model table name
     *
     * @return string
     */
    public static function getTableName()
    {
        return ((new self)->getTable());
    }
}
