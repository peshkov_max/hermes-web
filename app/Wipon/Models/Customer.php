<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Customer
 *
 * @property integer $id
 * @property string $phone_number
 * @property string $name
 * @property string $email
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property integer $region_id
 * @property-read mixed $name_phone_email_region
 * @property-read \Wipon\Models\Region $region
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Device[] $devices
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer wherePhoneNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Customer whereRegionId($value)
 * @mixin \Eloquent
 */
class Customer extends BaseModel
{
    use SoftDeletes;

    /**
     * Used in resources/assets/js/vue/views/devices/vm-logs.vue:77
     * to display user general information
     **/
    protected $appends = ['name_phone_email_region'];

    /**
     * Used in resources/assets/js/vue/views/devices/vm-logs.vue:77
     * to display user general information
     **/
    public function getNamePhoneEmailRegionAttribute()
    {
        try {
            $name = $this->attributes['name'] ?? trans('app.name_not_specified');
            $phone_number = $this->attributes['phone_number'] ?? trans('app.phone_number_not_specified');
            $email = $this->attributes['email'] ??  trans('app.email_not_specified');
        } catch (\Exception $e) {
            log_e('Error while forming name_phone_email_region',[$e->getMessage()]);
            $name = trans('app.name_not_specified');
            $phone_number = trans('app.phone_number_not_specified');
            $email = trans('app.email_not_specified');
        }

        return $name . ', ' . $phone_number . ', ' . $email ;
    }

    /** Get Customer devices
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices()
    {
        return $this->hasMany(Device::class);
    }
}
