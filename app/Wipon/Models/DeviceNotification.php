<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Wipon\Models\Continent
 *
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kz
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Country[] $countries
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Continent whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Continent whereNameRu($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Continent whereNameEn($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Continent whereNameKz($value)
 * @mixin \Eloquent
 * @property string $device_uuid
 * @property string $text
 * @property boolean $is_unread
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\DeviceNotification whereDeviceUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\DeviceNotification whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\DeviceNotification whereIsUnread($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\DeviceNotification whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\DeviceNotification whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\DeviceNotification whereDeletedAt($value)
 */
class DeviceNotification extends BaseModel
{
    use SoftDeletes;

    protected $visible = [
        'id', 'text', 'is_unread', 'created_at', 'device_uuid',

        /*
         |  See app/Wipon/Repos/PushNotificationRepository.php:23 
         */
        'app_version', 'phone_number', 'push_token',

        /*
         |  See app/Wipon/Repos/PushNotificationRepository.php:36 
         */
        'feedback_messages_count',
    ];

    protected $fillable = ['text', 'is_unread'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /** 
     * Get device 
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    /** Get device feedback messages
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedback_messages()
    {
        return $this->hasMany(FeedbackMessage::class, 'device_uuid', 'device_uuid');
    }

    /**
     * Date need to be the following format  ДД.ММ,ГГГГ ЧЧ:ММ
     * 
     * @param $date
     * @return string
     */
    public function getCreatedAtAttribute( $date )
    {
        return set_user_timezone($date);
    }

}