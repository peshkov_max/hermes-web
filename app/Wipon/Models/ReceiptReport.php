<?php
namespace Wipon\Models;

use Carbon\Carbon;

/**
 * Wipon\Models\ReceiptReport
 *
 * @property integer $id
 * @property integer $region_id
 * @property string $device_uuid
 * @property string $organization_name
 * @property string $organization_address
 * @property string $message
 * @property string $photo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereRegionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereDeviceUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereOrganizationName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereOrganizationAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport wherePhoto($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property integer $receipt_report_type_id
 * @property-read \Wipon\Models\Device $device
 * @property-read \Wipon\Models\Region $region
 * @property-read \Wipon\Models\ReceiptReportType $type
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ReceiptReport whereReceiptReportTypeId($value)
 */
class ReceiptReport extends BaseModel{

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function type()
    {
        return $this->belongsTo(ReceiptReportType::class, 'receipt_report_type_id', 'id');
    }

    public function getCreatedAtAttribute( $date )
    {
        return Carbon::createFromFormat(config('wipon.date_time_format'), $date, 'UTC')
            ->setTimezone(config('wipon.user_time_zone'))->format(config('wipon.dt_format_d_m_y_h_i'));
    }
}