<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Query\Builder;

/**
 * Wipon\Models\Cluster
 *
 * @property-read ClusterType $cluster_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Check[] $Checks
 * @property-read City $city
 * @property-read mixed $updated_at
 * @property-read mixed $created_at
 * @property-read mixed $deleted_at
 * @method static Builder|I18nModel withName($cityName)
 * @method static Builder|Cluster getByClientCity($city_name = null)
 * @method static Builder|Cluster getByClusterList($cluster_list)
 * @method static Builder|Cluster manageFilter($param, $city_name = null, $search_query = null)
 * @property integer $id
 * @property integer $city_id
 * @property integer $cluster_type_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kz
 * @property string $geo_address
 * @property float $longitude
 * @property float $latitude
 * @property float $accuracy
 * @property float $virtual_accuracy
 * @property string $street
 * @property string $house
 * @property boolean $is_moderated
 * @property boolean $is_fixed
 * @property boolean $is_licensed
 * @property-read mixed $scanned_at
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereClusterTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereNameRu($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereNameEn($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereNameKz($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereGeoAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereAccuracy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereVirtualAccuracy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereStreet($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereHouse($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereIsVerified($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereIsFixed($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereIsLicensed($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $name_kk
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Check[] $checks
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Check[] $product_items
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Cluster whereIsModerated($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 */
class Cluster extends I18nModel
{
    use SoftDeletes;

    protected $casts = [
        'is_licensed'       =>'boolean',
        'is_fixed'          =>'boolean',
        'is_verified'       =>'boolean',
        'accuracy'          =>'float',
        'virtual_accuracy'  =>'float',
        'latitude'          =>'float',
        'longitude'         =>'float'
    ];
    
    protected $dates =[
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    
    protected $fillable = [
        'id',
        'name_ru',
        'name_kk',
        'name_en',
        "accuracy",
        "virtual_accuracy",
        'city_id',
        'latitude',
        'longitude',
        'geo_address',
        'cluster_type_id',

        'is_licensed',
        'is_moderated',
        'is_fixed',

        'street',
        'house',
    ];
    
    protected $visible = [
        'id',
        'city',
        'city_id',

        'name_ru',
        'name_kk',
        'name_en',
        "accuracy",
        "virtual_accuracy",
        'latitude',
        'longitude',
        'geo_address',
        'cluster_type',
        'cluster_type_id',

        'is_licensed',
        'is_moderated',
        'is_fixed',

        'street',
        'house',
        'scans',


        'item_valid_hand',
        'item_valid_scan' ,

        'item_fake_hand',
        'item_fake_scan',

        'item_duplicate_hand',
        'item_duplicate_scan',

        'item_atlas_hand',
        'item_atlas_scan' ,

        'check_valid_hand' ,
        'check_valid_scan' ,

        'check_fake_hand' ,
        'check_fake_scan' ,

        'check_duplicate_hand' ,
        'check_duplicate_scan',

        'check_atlas_hand' ,
        'check_atlas_scan' ,

        'unique_devices',
    ];

    protected $visibleI18n = ['name'];

    /** Get cluster's type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cluster_type()
    {
        return $this->belongsTo(ClusterType::class);
    }

    /** Get cluster's scan 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checks()
    {
        return $this->hasMany(Check::class);
    }

    /** Get cluster's city
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**Get cluster's product items
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function product_items()
    {
        return $this->hasManyThrough(Check::class, Item::class);
    }

    /**Get cluster's type
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function type()
    {
        return $this->belongsTo(ClusterType::class);
    }
}
