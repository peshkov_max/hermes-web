<?php

namespace Wipon\Models;


/**
 * Wipon\Models\Check
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $item_id
 * @property string $device_uuid
 * @property integer $cluster_id
 * @property float $geo_longitude
 * @property float $geo_latitude
 * @property Item $item
 * @property float $geo_accuracy
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Wipon\Models\Item $product_item
 * @property-read \Wipon\Models\Device $device
 * @property-read \Wipon\Models\Cluster $cluster
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereItemId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereDeviceUuid($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereClusterId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereGeoLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereGeoLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereGeoAccuracy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereUpdatedAt($value)
 * @property string $retailer
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Check whereRetailer($value)
 */
class Check extends BaseModel
{
    protected $table = 'core.checks';

    protected $casts = [
        'geo_longitude' => 'float',
        'geo_latitude'  => 'float',
    ];

    protected $visible = [
        'id',
        'item',
        'cluster_id',
        'device',
        'device_uuid',
        'retailer',
        'cluster',
        'geo_longitude',
        'geo_latitude',
        'geo_accuracy',
        'created_at',
    ];

    protected $dates = [
        'created_at', 'updated_at',
    ];

    /** Get product items
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /** Get Check 's device
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function device()
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    /** Get Check cluster
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cluster()
    {
        return $this->belongsTo(Cluster::class);
    }


    /**
     * Date need to be the following format  ДД.ММ,ГГГГ ЧЧ:ММ
     * @see http://jira.wiponapp.com:8090/pages/viewpage.action?pageId=5932945
     * @param $date
     * @return string
     */
    public function getCreatedAtAttribute( $date )
    {
        return set_user_timezone($date);
    }
}
