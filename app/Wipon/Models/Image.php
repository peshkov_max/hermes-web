<?php

namespace Wipon\Models;

/**
 * Class Image
 *
 * @property mixed id
 * @property mixed original_uri
 * @package Wipon\Models
 * @property string $thumb_uri
 * @property-read \Wipon\Models\Organization $organization
 * @property-read mixed $raw
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Image whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Image whereOriginalUri($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Image whereThumbUri($value)
 * @mixin \Eloquent

 */
class Image extends BaseModel
{
    public $timestamps = false;

    public $cacheable = [
        'organization',
    ];
    
    protected $fillable = [
        'original_uri', 'thumb_uri',
    ];

    protected $visible = [
        'id', 'original_uri', 'thumb_uri',
    ];

    /** Get company
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }


    /** Get raw attribute value
     * @param string $key
     * @return string
     */
    public function getRawAttribute(string $key) : string
    {
        return $this->attributes[$key];
    }

}
