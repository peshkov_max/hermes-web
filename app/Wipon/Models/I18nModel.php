<?php

namespace Wipon\Models;

/**
 * App\I18nModel
 *
 * @property-read mixed $scanned_at
 * @property-read mixed $created_at
 * @property-read mixed $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\I18nModel withName($cityName)
 * @mixin \Eloquent
 * @property-read string $name
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 */
class I18nModel extends BaseModel
{

    const PAGINATE_NUMBER = 20;

    protected $visibleI18n = [];

    public function toArray()
    {
        $array = parent::toArray();

        foreach ($this->visibleI18n as $field) {
            $array[ $field ] = $this->{$field};
        }

        return $array;
    }

    /**
     * Get an attribute from the model.
     *
     * @param string $key
     * @return mixed
     */
    public function getAttribute( $key )
    {
        $inAttributes = array_key_exists($key, $this->attributes);

        // If the key references an attribute, we can just go ahead and return the
        // plain attribute value from the model. This allows every attribute to
        // be dynamically accessed through the _get method without accessors.
        if ($inAttributes || $this->hasGetMutator($key)) {
            return $this->getAttributeValue($key);
        } else {
            $i18nKey = $key . '_' . current_locale();

            $enKey = $key . '_en';

            $inAttributes = array_key_exists($i18nKey, $this->attributes);

            if ($inAttributes || $this->hasGetMutator($i18nKey)) {
                $value = $this->getAttributeValue($i18nKey);

                if (empty($value)) $value = trans('app.has_no_name');

                return $value ? $value : $this->getAttributeValue($enKey);
            }
        }

        // If the key already exists in the relationships array, it just means the
        // relationship has already been loaded, so we'll just return it out of
        // here because there is no need to query within the relations twice.
        if (array_key_exists($key, $this->relations)) {
            return $this->relations[ $key ];
        }

        // If the "attribute" exists as a method on the model, we will just assume
        // it is a relationship and will load and return results from the query
        // and hydrate the relationship's value on the "relationships" array.
        if (method_exists($this, $key)) {
            return $this->getRelationshipFromMethod($key);
        }
    }


    /**
     * @param \Illuminate\Database\Query\Builder| I18nModel $query
     * @param $name
     * @return mixed
     */
    public function scopeWithName( $query, $name )
    {

        if ( ! isset($name))
            return $query;

        if ($name === '')
            return $query;

        $text = '%' . trim($name) . '%';

        $query->where('name_en', 'ilike', $text);
        $query->orWhere('name_kk', 'ilike', $text);
        $query->orWhere('name_ru', 'ilike', $text);

        return $query;
    }

    public function scopeOrderedWithDirection( $query, $direction )
    {
        if ($direction && $direction == 'desc') {
            $query->orderBy('name_' . current_locale(), 'desc');
        } elseif ($direction && $direction == 'asc') {
            $query->orderBy('name_' . current_locale(), 'asc');
        } else {
            $query->orderBy('name_' . current_locale(), 'asc');
        }

        return $query;
    }

}