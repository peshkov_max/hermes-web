<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;

/**
 * Wipon\Models\Device
 *
 * @property string $uuid
 * @property string $model
 * @property string $platform
 * @property string $push_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Check[] $scans
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\ApiRequest[] $api_requests
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\FeedbackMessage[] $feedback_messages
 * @method static Builder|Device whereUuid($value)
 * @method static Builder|Device whereModel($value)
 * @method static Builder|Device wherePlatform($value)
 * @method static Builder|Device wherePushToken($value)
 * @method static Builder|Device whereCreatedAt($value)
 * @method static Builder|Device whereUpdatedAt($value)
 * @method static Builder|Device whereDeletedAt($value)
 * @mixin \Eloquent
 * @property string $app_version
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\DeviceNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Check[] $checks
 * @property-read \Wipon\Models\ApiRequest $latest_api_request
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Device whereAppVersion($value)
 * @property integer $customer_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Receipt[] $receipts
 * @property-read \Wipon\Models\Receipt $first_receipt
 * @property-read \Wipon\Models\Customer $customer
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Device whereCustomerId($value)
 */
class Device extends BaseModel
{
    use SoftDeletes;

    public $incrementing = false;

    protected $primaryKey = 'uuid';

    protected $fillable= ['uuid', 'platform'];
    
    protected $visible = [
        'customer_id', 'app_version', 'first_receipt', 'receipts', 'customer', 'id', 'model', 'platform', 'push_token', 'uuid', 'api_requests', 'updated_at', 'feedback_messages', 'feedback_messages_count', 'receipts_count',
        
        /*
        | Смотрите app/Wipon/Repos/DeviceRepository.php:43  
        */
        'phone_number',
        
        /* 
        | Используется для вывода сомнительных пользователей
        |
        | Wipon\Repos\ReceiptRepository::getSuspiciousUsers */
       'region_name', 'receipts_count', 'member_name', 'member_phone_number'
    ];

    protected $casts = [
        'uuid' => 'string',
    ];

    protected $dates =[
        'created_at',
        'updated_at',
        'deleted_at'
    ];
   
    public function notifications()
    {
        return $this->hasMany(DeviceNotification::class, 'device_uuid', 'uuid');
    }
    
    /** Get Device Checks
     * @return \Illuminate\Database\Eloquent\Relations\HasMany  | Check
     */
    public function checks()
    {
        return $this->hasMany(Check::class, 'device_uuid', 'uuid');
    }

    /** Get Device Receipts
     * @return \Illuminate\Database\Eloquent\Relations\HasMany  | Check
     */
    public function receipts()
    {
        return $this->hasMany(Receipt::class, 'device_uuid', 'uuid');
    }

    public function first_receipt()
    {
        return  $this->hasOne(Receipt::class, 'device_uuid', 'uuid');
    }


    /** Get device api requests
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function api_requests()
    {
        return $this->hasMany(ApiRequest::class, 'device_uuid', 'uuid');
    }

    public function latest_api_request()
    {
        return $this->hasOne(ApiRequest::class, 'device_uuid', 'uuid')->latest();
    }

    /** Get device feedback messages
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feedback_messages()
    {
        return $this->hasMany(FeedbackMessage::class, 'device_uuid', 'uuid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
    
    public function getUpdatedAtAttribute( $date )
    {
        return set_user_timezone($date);
    }
  

}

