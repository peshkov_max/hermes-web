<?php

namespace Wipon\Models;
use Illuminate\Database\Query\Builder;

/**
 * Wipon\Models\ClusterType
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|Cluster[] $clusters
 * @property-read mixed $name
 * @property-read mixed $scanned_at
 * @property-read mixed $created_at
 * @property-read mixed $deleted_at
 * @method static Builder | I18nModel withName($cityName)
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kz
 * @method static Builder|ClusterType whereId($value)
 * @method static Builder|ClusterType whereNameRu($value)
 * @method static Builder|ClusterType whereNameEn($value)
 * @method static Builder|ClusterType whereNameKz($value)
 * @property string $name_kk
 * @property integer $accuracy
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ClusterType whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ClusterType whereAccuracy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 */
class ClusterType extends I18nModel
{
    public $timestamps = false;
    
    protected $visibleI18n = ['name'];
    
    protected $fillable = ['name_kk', 'name_ru','name_en'];

    /** Ge cluster 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clusters()
    {
        return $this->hasMany(Cluster::class);
    }
    
}
