<?php

namespace Wipon\Models;

use Illuminate\Database\Query\Builder;
/**
 * Wipon\Models\Region
 *
 * @property-read \Wipon\Models\Country $country
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\City[] $cities
 * @property-read \Wipon\Models\RegionalCenter $regional_center
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\GeoAlias[] $aliases
 * @property-read mixed $scanned_at
 * @property-read mixed $created_at
 * @property-read mixed $deleted_at
 * @method static Builder | I18nModel withName($cityName)
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $country_id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kz
 * @method static Builder|Region whereId($value)
 * @method static Builder|Region whereCountryId($value)
 * @method static Builder|Region whereNameRu($value)
 * @method static Builder|Region whereNameEn($value)
 * @method static Builder|Region whereNameKz($value)
 * @property string $name_kk
 * @property integer $regional_center_city_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\City[] $clusters
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\City[] $organizations
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Region whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\Region whereRegionalCenterCityId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 */
class Region extends I18nModel
{
    public $timestamps = false;
    
    protected $visibleI18n = ['name'];
    
    protected $visible = [
        'regional_center', 'regional_center_city_id', 'country', 'aliases', 'id', 'name_kk', 'name_ru', 'name_en'
    ];
    
    protected $fillable = [
        'name_kk', 'name_ru', 'name_en', 'country_id', 'regional_center_city_id'
    ];

    public $cacheable = [
        'regional_center', 'country'
    ];

    /** Get region's Country
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /** Get region's cities
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    }

    /** Get region's regional_center
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function regional_center()
    {
        return $this->hasOne(City::class, 'id', 'regional_center_city_id');
    }

    /** Get region's clusters
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function clusters()
    {
        return $this->hasManyThrough(City::class, Cluster::class);
    }

    /** Get region's companies
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function organizations()
    {
        return $this->hasManyThrough(City::class, Organization::class);
    }

    /** Get region's  aliases
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function aliases()
    {
        return $this->morphMany(Alias::class, 'alias');
    }
    
}
