<?php

namespace Wipon\Models;

use Illuminate\Database\Query\Builder;

/**
 * Wipon\Models\ProductItem
 *
 * @property label
 * {
 *      excise_code: string?
 *      serial_number: string?
 *      facet_number: string?
 * }
 *
 * @property Object sticker
 * @property string status
 * @property integer $product_id
 * @property bool $is_duplicated
 * @property Product product
 * @method static Builder|Item whereId($value)
 * @method static Builder|Item whereStatus($value)
 * @method static Builder|Item whereIsDuplicated($value)
 * @mixin \Eloquent
 */
class Item extends BaseModel
{
    public $timestamps = false;
    
    protected $fillable = [
        'status', 'product_id', 'is_duplicated', 'sticker'
    ];
    
    protected $visible = [
      'id',  'status', 'product', 'is_duplicated', 'label', 'sticker' , 'personal_items'
    ];

    /**
     * @proterty label
     * {
     *      excise_code: string?
     *      serial_number: string?
     *      facet_number: string?
     * }
     * @var array
     */
    protected $casts = [
        "sticker" => 'array',
        "common_info" => 'array',
        'is_duplicated' => 'boolean'
    ];

    /** Get Item's Checks
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function checks()
    {
        return $this->hasMany(Check::class);
    }

    /** Get Item's product
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    /** Get Item's personal_items
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function personal_items()
    {
        return $this->hasMany(PersonalItem::class);
    }

    /** Get items's item_device_statuses
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function item_device_statuses()
    {
        return $this->hasMany(ItemDeviceStatus::class);
    }
}
