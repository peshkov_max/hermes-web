<?php

namespace Wipon\Models;

use Illuminate\Database\Eloquent\Model;

class ScheduledJob extends Model
{
    public $timestamps = false;

    protected $guarded = ['id'];
    
    protected $casts = [
        'params'=>'array'
    ]; 
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
