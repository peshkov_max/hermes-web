<?php

namespace Wipon\Models\Scopes;

use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/** According to project docs
 * @link http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5932740
 * @link http://docs.wiponapp.com:8090/pages/diffpagesbyversion.action?pageId=5932740&selectedPageVersions=8&selectedPageVersions=7
 */
class TestScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     * @return void
     */
    public function apply( Builder $builder, Model $model )
    {
        return str_is(config('app.env'), "production") ? $builder->where('is_testing', false) : $builder ;
    }
}