<?php

namespace Wipon\Models;

use Illuminate\Database\Query\Builder;

/**
 * Wipon\Models\ProductType
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Product[] $products
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\Suggestion[] $suggestions
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $name_kz
 * @property-read mixed $scanned_at
 * @property-read mixed $created_at
 * @property-read mixed $deleted_at
 * @property mixed name_kk
 * @method static Builder|ProductType whereId($value)
 * @method static Builder|ProductType whereNameRu($value)
 * @method static Builder|ProductType whereNameEn($value)
 * @method static Builder|ProductType whereNameKz($value)
 * @method static Builder| I18nModel withName($cityName)
 * @property integer $parent_id
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ProductType whereNameKk($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\ProductType whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wipon\Models\I18nModel orderedWithDirection($direction)
 * @property-read \Illuminate\Database\Eloquent\Collection|\Wipon\Models\ProductType[] $children
 */
class ProductType extends I18nModel
{
    public $timestamps = false;
    
    protected $fillable = [
        'name_ru','name_en','name_kk', 'parent_id'
    ];
    
    protected $visibleI18n = ['name', 'name_ru','name_en','name_kk', ];
    
    protected $visible = [
        'name', 'id', 'parent_id', 'children', 'parent'
    ];
    
    /** Get products
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /** Get suggestions 
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function suggestions()
    {
        return $this->morphMany(Suggestion::class, 'suggestion');
    }

    public function children()
    {
        return $this->hasMany(ProductType::class, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo(ProductType::class,  'parent_id' ,'id');
    }

    /**
     * Get all of the Product's aliases.
     */
    public function aliases()
    {
        return $this->morphMany(Alias::class, 'aliasable');
    }
}
