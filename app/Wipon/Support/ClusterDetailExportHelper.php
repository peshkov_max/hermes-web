<?php
namespace Wipon\Support;

use Excel;
use File;
use Wipon\Repos\CheckRepository;
use Wipon\Repos\CheckStatisticRepository;

/** Формирует отчет в Excel со страницы детализации по кластеру
 *
 * Для вставки изображений в Excel отчёт необходимо чтобы изображения находились на локальной машине.
 * Изображения из incorrect_items хранятся на статическом сервере.
 *
 * Данный класс загружает картинки для incorrect items со статического сервера, создает отчет и удаляет картинки
 *
 * Class ClusterDetailExportHelper
 * @package Wipon\Support
 */
class ClusterDetailExportHelper
{

    /** Сканирования
     * @var
     */
    private $checks;

    /** Personal Items
     * @var
     */
    private $personal_items;

    /**
     * @var $this
     */
    private $check_aggregation;

    public function __construct(
        CheckStatisticRepository $aggregation,
        CheckRepository $checks,
        int $clusterId, $params
    )
    {
        $this->check_aggregation = $aggregation->findByClusterId($clusterId);
        $this->checks = $checks->getScanChecksByClusterId($clusterId, $params)->get();;

        $this->checkTempDirOrNew();
        $this->collectPersonalItemsFromChecks();
        $this->downloadPersonalItemImages();
    }

    /**
     * Выгружате personal_items
     */
    private function collectPersonalItemsFromChecks()
    {
        $personal_items = collect([]);
        foreach ($this->checks as $check) {

            if ($check->retailer != null) {

                $collection = $check->item->personal_items->filter(function ( $element ) use ( $check ) {
                    return $element->device_uuid == $check->device_uuid && $element->incorrect_sticker != null;
                });

                if ($collection->count() > 0) {
                    foreach ($collection->all() as $item) {
                        $array = $item->incorrect_sticker;
                        $array['check_id'] = $check->id;
                        $personal_items->push($array);
                    }
                }
            }
        }

        $this->personal_items = $personal_items;;
    }

    /**
     * Очищает директорию storage/app/temp
     */
    private function clearTempDir()
    {
        if (File::exists(storage_path('app/temp')))
            File::deleteDirectory(storage_path('app/temp'), true);
    }

    /**
     * Загружает картинки personal items со статического сервера
     */
    private function downloadPersonalItemImages()
    {
        foreach ($this->personal_items as $key => $personal_item) {
            $this->createDirPerCheck($personal_item['check_id']);
            $this->downloadImage('sticker_uri', $personal_item['sticker_uri'], $personal_item['check_id']);
            $this->downloadImage('label_uri', $personal_item['label_uri'], $personal_item['check_id']);
        }
    }

    /** Загрузка картинки
     *
     * @param string $type
     * @param string $url
     * @param int $check_id
     */
    private function downloadImage( string $type, string $url, $check_id )
    {
        try {

            $url = img_static('images/' . $url);
            $img = storage_path('app/temp/' . $check_id) . "/$type.jpg";

            file_put_contents($img, file_get_contents($url));
        } catch (\Exception $e) {

            $url = img_static(config('wipon.default_image_path'));
            $img = storage_path('app/temp/' . $check_id) . "/$type.jpg";

            file_put_contents($img, file_get_contents($url));
        }
    }

    /** Проверяет существует ли директория с указанным check_id
     * и если не существует - создает её
     *
     * @param $check_id
     */
    private function createDirPerCheck( $check_id )
    {
        try {
            if ( ! File::exists(storage_path('app/temp/' . $check_id)))
                File::makeDirectory(storage_path('app/temp/' . $check_id));
        } catch (\Exception $e) {
            log_e($e);
        }
    }

    /**
     * Проверяет существует ли директория Temp - и если не существует - создает её
     */
    private function checkTempDirOrNew()
    {
        try {
            if ( ! File::exists(storage_path('app/temp')))
                File::makeDirectory(storage_path('app/temp'));
        } catch (\Exception $e) {

            log_e($e);
        }
    }

    /** Создает отчет и сохраняет её в storage/app/excel
     * @return array
     */
    public function createExcelReport()
    {
        $filePath = storage_path('app/public/excel');
        $fileName = trans('app.detail_file_name');

        $cluster = $this->check_aggregation;
        $checks = $this->checks;
        $personal_items = $this->personal_items;

        Excel::create($fileName, function ( $excel ) use ( $cluster, $checks, $personal_items ) {

            $excel->setTitle('Scan in region');
            $excel->setCompany('Wipon');
            $excel->sheet(trans('app.detail_title'), function ( $sheet ) use ( $cluster, $checks, $personal_items ) {

                $sheet->loadView('reports.checks.map.cluster_detail', [
                    'checks'         => $checks,
                    'cluster'        => $cluster,
                    'personal_items' => $personal_items,
                    'userName'       => auth()->user()->name,
                ]);
                wipon_image_to_excel($sheet);
            });
            
        })->store('xls', $filePath);

        // удаляем картинки из storage/app/temp
        $this->clearTempDir();

        return [$filePath, $fileName];
    }

}