<?php

use Illuminate\Support\Facades\Input;

if ( ! function_exists('img_static')) {
    /**
     * Generate an asset path for the static server.
     *
     * @param  string $path
     * @return string
     */
    function img_static( $path )
    {
        return config('wipon.static_server') . '/images/' . $path;
    }
}

if ( ! function_exists('trans_bold')) {

    /** Return string wrapped with <b> html tag
     *
     * @param string locale path
     * @return string
     */
    function trans_bold( $localePath )
    {
        return '<b>' . trans($localePath) . '</b>';
    }
}

if ( ! function_exists('localized_route_name')) {
    /** Add localization prefix before route name
     *
     * @param $routeName - route name
     * @return string    - localized route name
     */
    function localized_route_name( $routeName )
    {

        if (App::getLocale() == 'kk')
            $routeName = 'kk.' . $routeName;
        else
            $routeName = App::getLocale() . '.' . $routeName;

        return $routeName;
    }
}

if ( ! function_exists('localized_url')) {
    /**
     * Returns an URL adapted to $locale
     *
     * @param  string|boolean $locale Locale to adapt, false to remove locale
     * @param  string|false $url URL to adapt in the current language. If not passed, the current url would be taken.
     * @param  array $attributes Attributes to add to the route, if empty, the system would try to extract them from the url.
     *
     * @throws \Mcamara\LaravelLocalization\Exceptions\UnsupportedLocaleException
     *
     * @return string|false             URL translated, False if url does not exist
     */
    function localized_url( $locale = null, $url = null, $attributes = [] )
    {

        return LaravelLocalization::getLocalizedURL($locale, $url, $attributes);
    }
}

if ( ! function_exists('localized_route')) {
    /**
     * Generate a URL to a named route.
     *
     * @param  string $name
     * @param  array $parameters
     * @param  bool $absolute
     * @param  \Illuminate\Routing\Route $route
     * @return string
     */
    function localized_route( $name, $parameters = [], $absolute = true, $route = null )
    {

        if (App::getLocale() == 'kk')
            $name = 'kk.' . $name;
        else
            $name = App::getLocale() . '.' . $name;

        return app('url')->route($name, $parameters, $absolute, $route);
    }
}

if ( ! function_exists('current_locale')) {

    /**
     * Get current app locale
     *
     * @return string
     */
    function current_locale()
    {

        if (App::getLocale() == 'kk')
            $i18nLocale = 'kz';
        else
            $i18nLocale = App::getLocale();

        return $i18nLocale;
    }
}

if ( ! function_exists('get_i')) {

    /**  Get input data from request
     *
     * @param $paramName
     *
     * @return boolean
     */
    function get_i( $paramName )
    {
        return Input::get($paramName);
    }
}

if ( ! function_exists('has_i')) {

    /**  Check if input has such param
     *
     * @param $paramName
     * @return boolean
     */
    function has_i( $paramName )
    {
        return Input::has($paramName);
    }
}

if ( ! function_exists('log_i')) {

    /**  Log::info implementation
     *   Write logs
     *
     * @param string $message
     * @param array $context
     */
    function log_i( $message, array $context = [] )
    {
        Log::info($message, $context);
    }
}


if ( ! function_exists('log_e')) {

    /**  Log::error implementation
     *   Write logs
     *
     * @param string $message
     * @param array $context
     */
    function log_e( $message, array $context = [] )
    {
        Log::error($message, $context);
    }
}

if ( ! function_exists('log_w')) {

    /**  Log::warning implementation
     *   Write warning logs
     *
     * @param string $message
     * @param array $context
     */
    function log_w( $message, array $context = [] )
    {
        Log::warning($message, $context);
    }
}

if ( ! function_exists('log_on_dev')) {

    /**  Log::warning implementation
     *   Write warning logs
     *
     * @param string $message
     * @param array $context
     */
    function log_on_dev( $message, array $context = [] )
    {
        if (in_array(config('app.env'), ['local', 'testing', 'staging']))
        {
            Log::info($message, $context);
        }
    }
}

if ( ! function_exists('ddq')) {

    /**  Log query
     *
     *
     * @param \Illuminate\Database\Query\Builder $query
     */
    function ddq( $query )
    {
        DB::connection(config('database.default'))->enableQueryLog();
        $query->get();
        dd(DB::connection(config('database.default'))->getQueryLog());
    }
}

if ( ! function_exists('ddl')) {

    /**  Log query
     *
     *
     * @param \Illuminate\Database\Query\Builder $query
     * @param null $classCaller
     */
    function ddl( $query, $classCaller = null )
    {
        DB::connection(config('database.default'))->enableQueryLog();
        
        $query->get();
        
        if ($classCaller){
            Log::info($classCaller);
        }
        
        Log::info(DB::connection(config('database.default'))->getQueryLog());
    }
}

if ( ! function_exists('is_li_active')) {

    /**  Define whether we should open sub menu or not
     * if there is already one opened menu - do not open current
     *
     * @param $items
     * @param $item
     *
     * @return boolean
     */
    function is_li_active( $items, $item )
    {
        foreach ($items as $i) {
            if (str_contains($i->link->attr('class'), config('laravel-menu.settings.default.active_class')))
                return true;
        }

        /** @noinspection PhpUndefinedMethodInspection */
        return $item->hasChildren() && $item->parent == null && str_contains($item->link->attr('class'), config('laravel-menu.settings.default.active_class'));
    }
}

if ( ! function_exists('ten_minutes')) {

    /**  Get ten minutes in Carbon representation
     *
     * @return boolean
     */
    function ten_minutes()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return \Carbon\Carbon::now()->addMinutes(10);
    }
}

if ( ! function_exists('get_table_index')) {

    /** get index for table
     *
     * @param \Illuminate\Contracts\Pagination\Paginator $resource
     * @return bool
     */
    function get_table_index( $resource )
    {
        return ($resource->currentPage() - 1) * $resource->perPage() + 1;
    }
}

if ( ! function_exists('button_cancel')) {

    /**  Add link to route with icon
     * @param null $url
     * @return string
     */
    function button_cancel( $url = null )
    {
        $url = $url ?? URL::previous();

        return '<a href="' . $url . '" id="button-cancel" class="btn btn-default ">' . trans('app.cancel') . '</a>';
    }
}

if ( ! function_exists('button_save')) {

    /**  Get save button */
    function button_save()
    {
        return Form::button('<span class="glyphicon glyphicon-save"></span> ' . trans('app.save'), ['id' => 'save-item-button', 'type' => 'submit', 'class' => ' btn btn-primary']);
    }
}

if ( ! function_exists('get_sort_icon')) {

    /**  Get sort icon for table headers sorting
     * @param $resourceName - Resource type (singular), Examples: organization, user, product
     * @param string $sort_field - Fiels used for sorting
     * @return string
     */
    function get_sort_icon( $resourceName, $sort_field = 'sort' )
    {
        $filter = Session::has("{$resourceName}Filter") ? Session::get("{$resourceName}Filter") : null;

        if ($filter) {
            if (isset($filter[ $sort_field ]) && $filter[ $sort_field ] == 'desc')
                return '<p class="pull-right"><a href="#"  class="' . $sort_field . '" title="' . trans('app.sort_by_asc_now') . '" id="sort-by-' . $sort_field . '-asc-icon">
                        <i class="fa fa-sort-asc sort-icon-both sort-icon-active"></i>
                        <i class="fa fa-sort-desc sort-icon-both sort-icon-not-active" ></i>
                    </a></p>';

            if (isset($filter[ $sort_field ]) && $filter[ $sort_field ] == 'asc')
                return '<p class="pull-right"><a href="#" class="' . $sort_field . '" title="' . trans('app.sort_by_decs_now') . '" id="sort-by-' . $sort_field . '-desc-icon">
                        <i class="fa fa-sort-asc sort-icon-both sort-icon-not-active"></i>
                        <i class="fa fa-sort-desc sort-icon-both sort-icon-active" ></i>
                    </a></p>';
        }

        return '<p class="pull-right"><a href="#"  class="' . $sort_field . '" title="' . trans('app.sort_by_decs_now') . '" id="sort-by-' . $sort_field . '-asc-icon">
                        <i class="fa fa-sort-asc sort-icon-both" title="' . trans('app.sort_by_ascending') . '"></i>
                        <i class="fa fa-sort-desc sort-icon-both" title="' . trans('app.sort_by_decs_now') . '"></i>
                    </a></p>';
    }
}

if ( ! function_exists('next_sort_type')) {

    /**  Get sort url for table headers sorting
     * @param $resourceName - Resource type (singular), Examples: organization, user, product
     * @param string $sort_field - Fiels used for sorting
     * @return string
     */
    function next_sort_type( $resourceName, $sort_field = 'sort' )
    {
        $filter = Session::get("{$resourceName}Filter");

        $url = Session::has("{$resourceName}Filter") ? Session::get("{$resourceName}IndexPageUrl") : null;

        if ($filter && $url) {
            if (isset($filter[ $sort_field ]) && in_array($filter[ $sort_field ], ['desc', 'asc'])) {
                return $filter[ $sort_field ] == 'desc' ? 'asc' : 'desc';
            }
        }

        return 'desc';
    }
}

if ( ! function_exists('array_to_std_object')) {

    /**  Change the syntax $array['field'] with $array->field
     *
     * Sometimes it's more comfortable to use stdObject instead array
     * In order to change the following syntax $array['field'] with $array->field this function was created
     * @param $array
     * @return stdClass
     */
    function array_to_std_object( $array )
    {
        $object = new \stdClass();

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = array_to_std_object($value);
            }
            $object->$key = $value;
        }

        return $object;
    }
}

if ( ! function_exists('is_json')) {

    /**  Check if string valid json
     *
     * @param $args
     * @return bool
     */
    function is_json( ...$args )
    {
        json_decode(...$args);

        return (json_last_error() === JSON_ERROR_NONE);
    }
}

if ( ! function_exists('format_timezone')) {

    /**
     * Format PHP timezone string to the human readable format
     *
     * @param $timezone
     * @return string
     */
    function format_timezone( $timezone )
    {
        $timezones = [
            '(UTC-11:00) Midway Island'                   => 'Pacific/Midway',
            '(UTC-11:00) Samoa'                           => 'Pacific/Samoa',
            '(UTC-10:00) Hawaii'                          => 'Pacific/Honolulu',
            '(UTC-09:00) Alaska'                          => 'US/Alaska',
            '(UTC-08:00) Pacific Time (US &amp; Canada)'  => 'America/Los_Angeles',
            '(UTC-08:00) Tijuana'                         => 'America/Tijuana',
            '(UTC-07:00) Arizona'                         => 'US/Arizona',
            '(UTC-07:00) Chihuahua'                       => 'America/Chihuahua',
            '(UTC-07:00) La Paz'                          => 'America/La_Paz',
            '(UTC-07:00) Mazatlan'                        => 'America/Mazatlan',
            '(UTC-07:00) Mountain Time (US &amp; Canada)' => 'US/Mountain',
            '(UTC-06:00) Central America'                 => 'America/Managua',
            '(UTC-06:00) Central Time (US &amp; Canada)'  => 'US/Central',
            '(UTC-06:00) Guadalajara'                     => 'America/Mexico_City',
            '(UTC-06:00) Mexico City'                     => 'America/Mexico_City',
            '(UTC-06:00) Monterrey'                       => 'America/Monterrey',
            '(UTC-06:00) Saskatchewan'                    => 'Canada/Saskatchewan',
            '(UTC-05:00) Bogota'                          => 'America/Bogota',
            '(UTC-05:00) Eastern Time (US &amp; Canada)'  => 'US/Eastern',
            '(UTC-05:00) Indiana (East)'                  => 'US/East-Indiana',
            '(UTC-05:00) Lima'                            => 'America/Lima',
            '(UTC-05:00) Quito'                           => 'America/Bogota',
            '(UTC-04:00) Atlantic Time (Canada)'          => 'Canada/Atlantic',
            '(UTC-04:30) Caracas'                         => 'America/Caracas',
            '(UTC-04:00) La Paz'                          => 'America/La_Paz',
            '(UTC-04:00) Santiago'                        => 'America/Santiago',
            '(UTC-03:30) Newfoundland'                    => 'Canada/Newfoundland',
            '(UTC-03:00) Brasilia'                        => 'America/Sao_Paulo',
            '(UTC-03:00) Buenos Aires'                    => 'America/Argentina/Buenos_Aires',
            '(UTC-03:00) Georgetown'                      => 'America/Argentina/Buenos_Aires',
            '(UTC-03:00) Greenland'                       => 'America/Godthab',
            '(UTC-02:00) Mid-Atlantic'                    => 'America/Noronha',
            '(UTC-01:00) Azores'                          => 'Atlantic/Azores',
            '(UTC-01:00) Cape Verde Is.'                  => 'Atlantic/Cape_Verde',
            '(UTC+00:00) Casablanca'                      => 'Africa/Casablanca',
            '(UTC+00:00) Edinburgh'                       => 'Europe/London',
            '(UTC+00:00) Greenwich Mean Time : Dublin'    => 'Etc/Greenwich',
            '(UTC+00:00) Lisbon'                          => 'Europe/Lisbon',
            '(UTC+00:00) London'                          => 'Europe/London',
            '(UTC+00:00) Monrovia'                        => 'Africa/Monrovia',
            '(UTC+00:00) UTC'                             => 'UTC',
            '(UTC+01:00) Amsterdam'                       => 'Europe/Amsterdam',
            '(UTC+01:00) Belgrade'                        => 'Europe/Belgrade',
            '(UTC+01:00) Berlin'                          => 'Europe/Berlin',
            '(UTC+01:00) Bern'                            => 'Europe/Berlin',
            '(UTC+01:00) Bratislava'                      => 'Europe/Bratislava',
            '(UTC+01:00) Brussels'                        => 'Europe/Brussels',
            '(UTC+01:00) Budapest'                        => 'Europe/Budapest',
            '(UTC+01:00) Copenhagen'                      => 'Europe/Copenhagen',
            '(UTC+01:00) Ljubljana'                       => 'Europe/Ljubljana',
            '(UTC+01:00) Madrid'                          => 'Europe/Madrid',
            '(UTC+01:00) Paris'                           => 'Europe/Paris',
            '(UTC+01:00) Prague'                          => 'Europe/Prague',
            '(UTC+01:00) Rome'                            => 'Europe/Rome',
            '(UTC+01:00) Sarajevo'                        => 'Europe/Sarajevo',
            '(UTC+01:00) Skopje'                          => 'Europe/Skopje',
            '(UTC+01:00) Stockholm'                       => 'Europe/Stockholm',
            '(UTC+01:00) Vienna'                          => 'Europe/Vienna',
            '(UTC+01:00) Warsaw'                          => 'Europe/Warsaw',
            '(UTC+01:00) West Central Africa'             => 'Africa/Lagos',
            '(UTC+01:00) Zagreb'                          => 'Europe/Zagreb',
            '(UTC+02:00) Athens'                          => 'Europe/Athens',
            '(UTC+02:00) Bucharest'                       => 'Europe/Bucharest',
            '(UTC+02:00) Cairo'                           => 'Africa/Cairo',
            '(UTC+02:00) Harare'                          => 'Africa/Harare',
            '(UTC+02:00) Helsinki'                        => 'Europe/Helsinki',
            '(UTC+02:00) Istanbul'                        => 'Europe/Istanbul',
            '(UTC+02:00) Jerusalem'                       => 'Asia/Jerusalem',
            '(UTC+02:00) Kyiv'                            => 'Europe/Kiev',
            '(UTC+02:00) Pretoria'                        => 'Africa/Johannesburg',
            '(UTC+02:00) Riga'                            => 'Europe/Riga',
            '(UTC+02:00) Sofia'                           => 'Europe/Sofia',
            '(UTC+02:00) Tallinn'                         => 'Europe/Tallinn',
            '(UTC+02:00) Vilnius'                         => 'Europe/Vilnius',
            '(UTC+03:00) Baghdad'                         => 'Asia/Baghdad',
            '(UTC+03:00) Kuwait'                          => 'Asia/Kuwait',
            '(UTC+03:00) Minsk'                           => 'Europe/Minsk',
            '(UTC+03:00) Nairobi'                         => 'Africa/Nairobi',
            '(UTC+03:00) Riyadh'                          => 'Asia/Riyadh',
            '(UTC+03:00) Volgograd'                       => 'Europe/Volgograd',
            '(UTC+03:30) Tehran'                          => 'Asia/Tehran',
            '(UTC+04:00) Abu Dhabi'                       => 'Asia/Muscat',
            '(UTC+04:00) Baku'                            => 'Asia/Baku',
            '(UTC+04:00) Moscow'                          => 'Europe/Moscow',
            '(UTC+04:00) Muscat'                          => 'Asia/Muscat',
            '(UTC+04:00) St. Petersburg'                  => 'Europe/Moscow',
            '(UTC+04:00) Tbilisi'                         => 'Asia/Tbilisi',
            '(UTC+04:00) Yerevan'                         => 'Asia/Yerevan',
            '(UTC+04:30) Kabul'                           => 'Asia/Kabul',
            '(UTC+05:00) Islamabad'                       => 'Asia/Islamabad',
            '(UTC+05:00) Karachi'                         => 'Asia/Karachi',
            '(UTC+05:00) Tashkent'                        => 'Asia/Tashkent',
            '(UTC+05:30) Chennai'                         => 'Asia/Calcutta',
            '(UTC+05:30) Kolkata'                         => 'Asia/Calcutta',
            '(UTC+05:30) Mumbai'                          => 'Asia/Calcutta',
            '(UTC+05:30) New Delhi'                       => 'Asia/Calcutta',
            '(UTC+05:30) Sri Jayawardenepura'             => 'Asia/Calcutta',
            '(UTC+05:45) Kathmandu'                       => 'Asia/Katmandu',
            '(UTC+06:00) Almaty'                          => 'Asia/Almaty',
            '(UTC+06:00) Astana'                          => 'Asia/Almaty',
            '(UTC+06:00) Dhaka'                           => 'Asia/Dhaka',
            '(UTC+06:00) Ekaterinburg'                    => 'Asia/Yekaterinburg',
            '(UTC+06:30) Rangoon'                         => 'Asia/Rangoon',
            '(UTC+07:00) Bangkok'                         => 'Asia/Bangkok',
            '(UTC+07:00) Hanoi'                           => 'Asia/Hanoi',
            '(UTC+07:00) Jakarta'                         => 'Asia/Jakarta',
            '(UTC+07:00) Novosibirsk'                     => 'Asia/Novosibirsk',
            '(UTC+08:00) Beijing'                         => 'Asia/Hong_Kong',
            '(UTC+08:00) Chongqing'                       => 'Asia/Chongqing',
            '(UTC+08:00) Hong Kong'                       => 'Asia/Hong_Kong',
            '(UTC+08:00) Krasnoyarsk'                     => 'Asia/Krasnoyarsk',
            '(UTC+08:00) Kuala Lumpur'                    => 'Asia/Kuala_Lumpur',
            '(UTC+08:00) Perth'                           => 'Australia/Perth',
            '(UTC+08:00) Singapore'                       => 'Asia/Singapore',
            '(UTC+08:00) Taipei'                          => 'Asia/Taipei',
            '(UTC+08:00) Ulaan Bataar'                    => 'Asia/Ulan_Bator',
            '(UTC+08:00) Urumqi'                          => 'Asia/Urumqi',
            '(UTC+09:00) Irkutsk'                         => 'Asia/Irkutsk',
            '(UTC+09:00) Osaka'                           => 'Asia/Tokyo',
            '(UTC+09:00) Sapporo'                         => 'Asia/Tokyo',
            '(UTC+09:00) Seoul'                           => 'Asia/Seoul',
            '(UTC+09:00) Tokyo'                           => 'Asia/Tokyo',
            '(UTC+09:30) Adelaide'                        => 'Australia/Adelaide',
            '(UTC+09:30) Darwin'                          => 'Australia/Darwin',
            '(UTC+10:00) Brisbane'                        => 'Australia/Brisbane',
            '(UTC+10:00) Canberra'                        => 'Australia/Canberra',
            '(UTC+10:00) Guam'                            => 'Pacific/Guam',
            '(UTC+10:00) Hobart'                          => 'Australia/Hobart',
            '(UTC+10:00) Melbourne'                       => 'Australia/Melbourne',
            '(UTC+10:00) Port Moresby'                    => 'Pacific/Port_Moresby',
            '(UTC+10:00) Sydney'                          => 'Australia/Sydney',
            '(UTC+10:00) Yakutsk'                         => 'Asia/Yakutsk',
            '(UTC+11:00) Vladivostok'                     => 'Asia/Vladivostok',
            '(UTC+12:00) Auckland'                        => 'Pacific/Auckland',
            '(UTC+12:00) Fiji'                            => 'Pacific/Fiji',
            '(UTC+12:00) International Date Line West'    => 'Pacific/Kwajalein',
            '(UTC+12:00) Kamchatka'                       => 'Asia/Kamchatka',
            '(UTC+12:00) Magadan'                         => 'Asia/Magadan',
            '(UTC+12:00) Marshall Is.'                    => 'Pacific/Fiji',
            '(UTC+12:00) New Caledonia'                   => 'Asia/Magadan',
            '(UTC+12:00) Solomon Is.'                     => 'Asia/Magadan',
            '(UTC+12:00) Wellington'                      => 'Pacific/Auckland',
            '(UTC+13:00) Nuku\'alofa'                     => 'Pacific/Tongatapu',
        ];

        foreach ($timezones as $tzn => $tz) {
            if ($tz == $timezone) {
                return $tzn;
            }
        }

        return $timezone;
    }
}

if ( ! function_exists('get_month_ru')) {

    /**  Return Russian month representation
     *
     * @param $monthName
     * @return bool
     */
    function get_month_ru( $monthName )
    {
        $allMonths = [
            'january'   => "Январь",
            'february'  => "Февраль",
            'march'     => "Март",
            'april'     => "Апрель",
            'may'       => "Май",
            'june'      => "Июнь",
            'july'      => "Июль",
            'august'    => "Август",
            'september' => "Сентябрь",
            'october'   => "Октябрь",
            'november'  => "Ноябрь",
            'december'  => "Декабрь",
        ];

        return $allMonths[ mb_strtolower($monthName) ];
    }
}

if ( ! function_exists('wipon_image_to_excel')) {

    /**  Insert custom Wipon logo to excel sheet
     *
     * @param $sheet
     * @return bool
     * @throws PHPExcel_Exception
     */
    function wipon_image_to_excel( $sheet )
    {
        $objDrawing = new \PHPExcel_Worksheet_Drawing();
        $objDrawing->setPath(public_path('images/components/report_logo.png')); //your image path
        $objDrawing->setCoordinates('A1');
        $objDrawing->setOffsetX(10);
        $objDrawing->setOffsetY(5);
        $objDrawing->setWorksheet($sheet);
    }
}

if ( ! function_exists('php_process_owner')) {

    /** Get the username of the php process owner
     *
     * @throws PHPExcel_Exception
     */
    function php_process_owner()
    {
        $processUser = posix_getpwuid(posix_geteuid());

        return $processUser['name'];
    }
}

if ( ! function_exists('get_current_time')) {

    /** Get the username of the php process owner
     *
     * @throws PHPExcel_Exception
     */
    function get_current_time()
    {
        return \Carbon\Carbon::now()->setTimeZone(
            ! empty(config('wipon.user_time_zone'))
                ? new \DateTimeZone(config('wipon.user_time_zone'))
                : new \DateTimeZone(config('app.timezone'))
        )->format(config('wipon.pss_date_format'));
    }
}

if ( ! function_exists('set_user_timezone')) {

    /** Get the username of the php process owner
     *
     * @param $date
     * @return string
     */
    function set_user_timezone($date)
    {
        $new_str = new \DateTime($date, new \DateTimeZone('UTC'));

        $new_str->setTimeZone(
            ! empty(config('wipon.user_time_zone'))
                ? new \DateTimeZone(config('wipon.user_time_zone'))
                : new \DateTimeZone(config('app.timezone'))
        );

        return $new_str->format(config('wipon.dt_format_d_m_y_h_i'));
    }
}

if ( ! function_exists('hasCheckItems')) {

    function hasCheckItems( $check, $personal_items )
    {
        if ($check->retailer == null)
            return false;

        $status = false;
        foreach ($personal_items as $personal_item) {
            if ($check->id == $personal_item['check_id']) {
                $status = true;
            }
        }

        return $status;
    }
}

if ( ! function_exists('carbon_user_tz_now')) {

    /** Возвращает текущее время в временной зоне пользователя
     *
     * @return \Carbon\Carbon static
     */
    function carbon_user_tz_now() :\Carbon\Carbon
    {
        return \Carbon\Carbon::now()->setTimezone(config('wipon.user_time_zone', 'UTC'));
    }
}

if ( ! function_exists('format_coordinates')) {

    /** Формирует координаты в нужном формате
     *
     * @param string $latitude
     * @param string $longitude
     * @return \Carbon\Carbon static
     */
    function format_coordinates( $latitude = null, $longitude = null )
    {
        if (is_null($latitude) && is_null($longitude))
            return '-';

        return sprintf("%.6f, %.6f", $latitude, $longitude);
    }
}

if ( ! function_exists('str_for_search')) {
    
    /**
     * Подготавливает строку для поиска в БД 
     * (для использования конструкции like) 
     *
     * @param  string $str
     * @return string
     */
    function str_for_search( $str )
    {
        $trimmed = trim($str);
        
        return $trimmed !== '' ? '%' . $trimmed . '%' : '';
    }
}


if ( ! function_exists('push_cant_be_send')) {

    /**
     * Проверяет может ли пуш уведомление быть отправлено
     * 
     * На всех серверах КРОМЕ PRODUCTION пуш уведомления должны 
     * отправляться только на устройства из whitelist-a (config/wipon.php:91) 
     *
     * @param $uuid
     * @return bool
     */
    function push_cant_be_send( $uuid )
    {
        $result = config('app.env') !== 'production' && ! in_array($uuid, config('wipon.whitelist'));

        if ($result) {
            log_w('Can not send push notification. App env is not production and given device uuid is out of Wipon whitelist.'.PHP_EOL.
                '# See whitelist here: config/wipon.php:91');
        }
        
        return $result;
    }
}
