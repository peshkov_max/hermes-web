<?php

namespace Wipon\Observers;

use Wipon\Models\VisualFeature;

class VisualFeatureObserver
{
    /**
     * @param VisualFeature $model
     */
    public function deleted($model)
    {
        $model->image->delete();

        $image_fake = $model->image_fake;

        if ($image_fake)
            $image_fake->delete();
    }
}