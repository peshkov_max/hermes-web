<?php

namespace Wipon\Observers;

use File;

class ImageObserver
{
    /**
     * @param \Wipon\Models\Image $model
     */
    public function deleting( $model )
    {
        $this->deleteFileByAttributeName($model, 'original_uri');
        $this->deleteFileByAttributeName($model, 'thumb_uri');

        $dir = substr($model->getRawAttribute('original_uri'), strlen('images/') + 1, config('wipon.image_dirs_length'));
        $dir = public_path("images/cabinet/$dir");
        
        /** delete directory if it's empty */
        $countFiles = File::allFiles($dir);
        
        try {
            if (count($countFiles) === 0 && File::exists($dir)) {
                File::deleteDirectory($dir);
            }
        } catch (\Exception $e) {
            log_e('Cant remove dir in path: '.$dir, [$e->getMessage()]);
        }
    }

    /**
     * @param $model
     * @param string $name
     */
    private function deleteFileByAttributeName( $model, $name = 'original_uri' )
    {
        $path = public_path('images/' . $model->getRawAttribute($name));

        if (File::exists($path))
            File::delete($path);
        else
            log_e('Trying to remove not existed image. Path: ' . $path);
    }


}