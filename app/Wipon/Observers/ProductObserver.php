<?php

namespace Wipon\Observers;

use Wipon\Models\Check;
use Wipon\Models\DuplicationSession;
use Wipon\Models\PersonalItem;

class ProductObserver
{
    /**
     * @param \Wipon\Models\Image $model
     */
    public function deleting($model)
    {
       
        foreach ($model->items as $item) {
            
            Check::whereItemId($item->id)->delete();
            PersonalItem::whereItemId($item->id)->delete();
            PersonalItem::whereItemId($item->id)->delete();
            DuplicationSession::whereItemId($item->id)->delete();
            
            $item->delete();
        }
        
        
    }

}