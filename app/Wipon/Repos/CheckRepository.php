<?php
namespace Wipon\Repos;

use DB;
use Wipon\Models\Country;

class CheckRepository extends EloquentRepository
{

    protected $modelName = 'Check';

    public function update( int $id, array $data )
    {
    }

    public function stats( array $data )
    {
        $regions = $this->getStat($data);

        $array = $this->convertTableToArray($regions);

        return $array;
    }

    private function getStat( $data )
    {
        $yearCondition = "AND q1.year = {$data['year']}";

        $checkTypeCondition = $this->getCheckTypeCondition($data);

        $ukmTypeCondition = $this->getUkmTypeCondition($data);

        $sortCondition = $this->getSortCondition($data);

        $nameLocalized = "regions.name_" . current_locale();

        $kazakhstanId = Country::whereNameEn(config('wipon.base_country_name'))->firstOrFail()->id;

        DB::enableQueryLog();
        $rs = DB::select(DB::raw(
            "SELECT 
                q4.region_name  as region_name,
                SUM(q4.january) AS january,
                SUM(q4.february) AS february,
                SUM(q4.march) AS march,
                SUM(q4.april) AS april,
                SUM(q4.may) AS may,
                SUM(q4.june) AS june,
                SUM(q4.july) AS july,
                SUM(q4.august) AS august,
                SUM(q4.september) AS september,
                SUM(q4.october) AS october,
                SUM(q4.november) AS november,
                SUM(q4.december) AS december,
                SUM(q4.total) AS total
                
                FROM (
                    SELECT
                        q3.region_name, 
                        q3.month,
                        q3.region_id,
                        SUM(CASE WHEN q3.month = 1 THEN q3.per_region_month ELSE 0 END) AS january,
                        SUM(CASE WHEN q3.month = 2 THEN q3.per_region_month ELSE 0 END) AS february,
                        SUM(CASE WHEN q3.month = 3 THEN q3.per_region_month ELSE 0 END) AS march,
                        SUM(CASE WHEN q3.month = 4 THEN q3.per_region_month ELSE 0 END) AS april,
                        SUM(CASE WHEN q3.month = 5 THEN q3.per_region_month ELSE 0 END) AS may,
                        SUM(CASE WHEN q3.month = 6 THEN q3.per_region_month ELSE 0 END) AS june,
                        SUM(CASE WHEN q3.month = 7 THEN q3.per_region_month ELSE 0 END) AS july,
                        SUM(CASE WHEN q3.month = 8 THEN q3.per_region_month ELSE 0 END) AS august,
                        SUM(CASE WHEN q3.month = 9 THEN q3.per_region_month ELSE 0 END) AS september,
                        SUM(CASE WHEN q3.month = 10 THEN q3.per_region_month ELSE 0 END) AS october,
                        SUM(CASE WHEN q3.month = 11 THEN q3.per_region_month ELSE 0 END) AS november,
                        SUM(CASE WHEN q3.month = 12 THEN q3.per_region_month ELSE 0 END) AS december,
                        SUM(q3.per_region_month) AS total
                        
                    FROM (
                        SELECT *
                            FROM (
                                SELECT *
                                    FROM (
                                        SELECT
                                            $nameLocalized as region_name,
                                            EXTRACT(YEAR FROM checks.created_at) AS year,
                                            EXTRACT(MONTH FROM checks.created_at) AS month,
                                            cities.region_id AS region_id,
                                            count(*) OVER (PARTITION BY EXTRACT(MONTH FROM checks.created_at), cities.region_id) AS per_region_month,
                                            row_number() OVER (PARTITION BY EXTRACT(MONTH FROM checks.created_at), cities.region_id ORDER BY checks.created_at DESC) AS rank_region_id
                                        FROM checks
                                        JOIN clusters ON clusters.id = checks.cluster_id
                                        JOIN cities ON cities.id = clusters.city_id 
                                        JOIN items ON items.id = checks.item_id $checkTypeCondition $ukmTypeCondition
                                        JOIN regions on regions.id = cities.region_id and regions.country_id = $kazakhstanId 
                                    ) AS q1
                                WHERE q1.rank_region_id = 1 $yearCondition 
                            ) AS q2
                    ) AS q3
               
                    group by q3.month, q3.region_id, q3.region_name
                    order by q3.month, q3.region_id, q3.region_name
                ) as q4
            
                group by q4.region_id, q4.region_name
                order by $sortCondition"));
        //log_i(DB::getQueryLog());

        return $rs;
    }

    /**
     * @param $regions
     * @return array
     */
    private function convertTableToArray( $regions )
    {
        $array = [];
        foreach ($regions as $region) {
            $array[] = (array) $region;
        }

        return $array;
    }

    private function getCheckTypeCondition( $data )
    {
        if ($data['checkType'] == 'nothing')
            return " and jsonb_exists_any(items.sticker, array['nothing'])";

        if ($data['checkType'] == 'both')
            return " and jsonb_exists_any(items.sticker, array['excise_code' , 'serial_number'])";

        if ($data['checkType'] == 'scan')
            return " and jsonb_exists_any(items.sticker, array['excise_code'])";

        if ($data['checkType'] == 'hand')
            return " and jsonb_exists_any(items.sticker, array['serial_number'])";
    }

    private function getUkmTypeCondition( $data )
    {
        if ($data['ukmType']['all'] === 'true')
            return " ";

        $condition = [];
        if (filter_var(array_get($data, 'ukmType.valid'), FILTER_VALIDATE_BOOLEAN))
            $condition [] = "items.status = 'valid'";

        if (filter_var(array_get($data, 'ukmType.fake'), FILTER_VALIDATE_BOOLEAN))
            $condition [] = "items.status = 'fake'";

        if (filter_var(array_get($data, 'ukmType.atlas'), FILTER_VALIDATE_BOOLEAN))
            $condition [] = "items.status = 'atlas'";

        $hasDuplicate = filter_var(array_get($data, 'ukmType.duplicate'), FILTER_VALIDATE_BOOLEAN);

        if ( ! $condition && ! $hasDuplicate)
            return ' and items.status = \'undefined\'';

        if ($condition) {
            $appendix = " and (" . implode(" or ", $condition) . ")";

            return $hasDuplicate
                ? $appendix . " and (items.is_duplicated = true or items.is_duplicated = false)"
                : $appendix . " and items.is_duplicated = false";
            
        } elseif ($hasDuplicate) {
            return " and items.is_duplicated = true";
        }
    }

    private function getSortCondition( $data )
    {
        return "{$data['sort']['name']} {$data['sort']['direction']}";
    }

    /** Выгружает проверки(сканирования и ввод вручкую) по ID кластера
     *
     * @param int $clusterId
     * @param array $params
     * @return mixed
     */
    public function getChecksByClusterId( int $clusterId, array $params )
    {
        $model = $this->getModelClass();

        return $model::where('cluster_id', $clusterId)
            ->orderBy('created_at', array_has($params, 'sort.direction') ? array_get($params, 'sort.direction') : 'decs')
            ->with('item.product.organization', 'item.personal_items', 'item.product.type', 'cluster.city');
    }

    /** Выгружает проверки(только сканирования) по ID кластера
     *
     *
     * @param int $clusterId
     * @param array $params
     * @return mixed
     */
    public function getScanChecksByClusterId( int $clusterId, array $params )
    {
        $model = $this->getModelClass();

        return $model::where('cluster_id', $clusterId)
            ->join('items', 'items.id', '=', 'checks.item_id')
            ->where("items.sticker->excise_code", '!=', '')
            ->orderBy('created_at', array_has($params, 'sort.direction') ? array_get($params, 'sort.direction') : 'asc')
            ->with('item.product.organization', 'item.personal_items', 'item.product.type', 'cluster.city');
    }

}