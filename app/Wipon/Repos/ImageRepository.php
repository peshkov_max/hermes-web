<?php
namespace Wipon\Repos;

use Wipon\Models\Image as WiponImage;

class ImageRepository extends EloquentRepository  
{

    protected $modelName = 'Image';

    public function update(int $id , array $data ) : WiponImage
    {
        $this->getModel()->whereId($id)->update($data);

        return $this->findById($id);
    }
}