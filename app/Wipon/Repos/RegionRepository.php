<?php
namespace Wipon\Repos;

use Cache;
use Illuminate\Support\Collection;
use Wipon\Models\City;
use Wipon\Models\Country;

class RegionRepository extends EloquentRepository
{
    protected $modelName = 'Region';

    public function update( int $id, array $data )
    {
        //todo implement this
    }

    /** Get default region for authorised user
     *
     * @return mixed
     */
    public function getDefaultRegion()
    {
        return Cache::remember('user_' . auth()->user()->id . '_default_region', config('wipon.minute_to_store_in_cache', 30),
            function () {
                return auth()->user()->organization && auth()->user()->organization->city 
                    ? auth()->user()->organization->city->region 
                    : City::whereNameEn('Astana')->get()->first()->region;
            });
    }

    /** Get Kazakhstan Region
     *
     * @return Collection
     */
    public function getKazakhstanRegion() :Collection
    {
        return Cache::remember('kazakhstanRegions', config('wipon.minute_to_store_in_cache', 30), function () {

            // We need to take first Astana then Almaty then all others with alphabetical order
            $regions = Country::where('name_ru', 'Казахстан')->get()->first()->regions()->orderBy('name_' . current_locale());

            // we take Astana and Almaty separately
            $astana = $regions->get()->where('name_en', 'Astana')->first();
            $almaty = $regions->get()->where('name_en', 'Almaty')->first();

            // take all regions without Astana and Almaty
            $regions = $regions->where('name_en', '!=', 'Almaty')->where('name_en', '!=', 'Astana')->get();

            // prepend Almaty and Astana
            $regions->prepend($almaty);
            $regions->prepend($astana);

            return $regions;
        });
    }

    public function filter( array $filter )
    {
        /** @var \Illuminate\Database\Query\Builder $query */
        $query = $this->getModel()->select('regions.*')->with('country', 'regional_center');

        $searchRegion = array_get($filter, 'search.name.value');

        if ( ! empty($searchRegion)) {
            $query = $query->where(function ( $q ) use ( $searchRegion ) {
                /** @var \Illuminate\Database\Query\Builder $q */

                $q->where("regions.name_ru", "ilike", "%$searchRegion%")
                    ->orWhere("regions.name_en", "ilike", "%$searchRegion%")
                    ->orWhere("regions.name_kk", "ilike", "%$searchRegion%");
            });
        }

        if (filter_var(array_get($filter, 'checkboxes.kz_regions_only.checked'), FILTER_VALIDATE_BOOLEAN)) {
            $query = $query->join("countries", "countries.id", "=", "regions.country_id", "left")
                ->where("countries.name_en", "Kazakhstan");
        } else {
            $searchCountry = array_get($filter, 'search.country.value');

            if ( ! empty($searchCountry)) {
                $query = $query->join("countries", "countries.id", "=", "regions.country_id", "left")
                    ->where(function ( $q ) use ( $searchCountry ) {
                        /** @var \Illuminate\Database\Query\Builder $q */

                        $q->where("countries.name_ru", "ilike", "%$searchCountry%")
                            ->orWhere("countries.name_en", "ilike", "%$searchCountry%")
                            ->orWhere("countries.name_kk", "ilike", "%$searchCountry%");
                    });
            }
        }

        switch (array_get($filter, 'sort.name', 'id')) {
            case 'region':
                $query = $query->orderBy('regions.name_ru', array_get($filter, 'sort.direction', 'asc'));
                break;
            case 'regional_center':
                $query = $query->join("cities", "cities.id", "=", "regions.regional_center_city_id", "left")
                    ->orderBy('cities.name_ru', array_get($filter, 'sort.direction', 'asc'));
                break;
            default:
                $query = $query->orderBy('regions.id', array_get($filter, 'sort.direction', 'asc'));
        }

        foreach (array_get($filter, 'radios', []) as $radio) {
            if (array_get($radio, 'name') == 'regional_center') {
                switch (array_get($radio, 'selected')) {
                    case 'with_reg_center':
                        $query = $query->whereNotNull('regional_center_city_id');
                        break;
                    case 'without_reg_center':
                        $query = $query->whereNull('regional_center_city_id');
                        break;
                    default:
                        break;
                }
            }
        }

        return $query;
    }

    public function searchByName( string $querySearch )
    {
        $model = $this->getModelClass();

        /** @noinspection PhpUndefinedMethodInspection
         * scopeWithName - > Only for models which extends Wipon\Models/I18nModel */
        return $model::withName($querySearch)->orderBy('name_' . current_locale());
    }

    public function countrySearch( $querySearch, $countryId )
    {
        $model = $this->getModel();

        /** @noinspection PhpUndefinedMethodInspection
         * scopeWithName - > Only for models which extends Wipon\Models/I18nModel */
        if ($querySearch) {
            $model = $model::withName($querySearch);
        }

        if ($countryId && filter_var($countryId, FILTER_VALIDATE_INT)) {
            $model = $model->whereCountryId($countryId);
        }

        return $model->orderBy('name_' . current_locale());
    }

}