<?php
namespace Wipon\Repos;

use Illuminate\Database\Eloquent\Builder;
use Wipon\Models\Organization;
use Wipon\Models\Product;
use Wipon\Traits\Repos\MergeTrait;

class OrganizationRepository extends EloquentRepository
{
    use MergeTrait;

    protected $modelName = 'Organization';


    public function create( array $data )
    {
        if ( ! $this->userIsSuperAdmin()) {
            log_e('User with not super-admin role trying to create organization');

            return new $this->modelName;
        }

        /*** @var Organization $model */
        $model = $this->getModel();

        $model->city_id = $data['city_id'] !== '' ? $data['city_id'] : null;
        $model->image_id = $data['image_id'] ?? null;

        $model->name_en = $data['name_en'] ?? null;
        $model->name_ru = $data['name_ru'] ?? null;
        $model->name_kk = $data['name_kk'] ?? null;
        $model->is_supplier = $data['is_supplier'] ?? null;

        $model->is_moderated = $data['is_moderated'] ?? false;
        $model->legal_name = $data['legal_name'] ?? null;
        $model->email = $data['email'] ?? null;

        $model->contract_number = $this->fieldIsNotEmpty($data, 'contract_number') ? $data['contract_number'] : null;
        $model->contact_person = $this->fieldIsNotEmpty($data, 'contact_person') ? $data['contact_person'] : null;

        $model->save();

        return $model;
    }

    public function update( int $id, array $data )
    {
        /*** @var Organization $model */
        $model = $this->findById($id);

        $model->city_id = $data['city_id'] !== '' ? (int) $data['city_id'] : $model->city_id;
        $model->image_id = $data['image_id'];
        $model->is_moderated = $data['is_moderated'] ?? false;

        /** Изменять название может только супер администратор
         * @see http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5931283
         **/
        if ($this->userIsSuperAdmin()) {
            // Имя Wipon - неизменяемо
            $model->name_en = $model->name_en == config('wipon.wipon_name') ? config('wipon.wipon_name') : $data['name_en'];
            $model->name_ru = $model->name_ru == config('wipon.wipon_name') ? config('wipon.wipon_name') : $data['name_ru'];
            $model->legal_name = $data['legal_name'];
        }

        $model->email = $this->fieldIsNotEmpty($data, 'email') ? $data['email'] : null;
        $model->is_supplier = $data['is_supplier'];
        $model->contract_number = $this->fieldIsNotEmpty($data, 'contract_number') ? $data['contract_number'] : null;
        $model->contact_person = $this->fieldIsNotEmpty($data, 'contact_person') ? $data['contact_person'] : null;
        $model->legal_name = $data['legal_name'];

        $model->save();

        return $model;
    }

    public function filter( $params ) : Builder
    {
        $model = $this->getModelClass();

        $builder = $model::query();
        
        if (isset($params['search'])) {
            $builder->withName($params['search']);
        }

        if (array_has($params, 'radios.moderation.use')
            && array_has($params, 'radios.moderation.selected')
            && filter_var(array_get($params, 'radios.moderation.use'), FILTER_VALIDATE_BOOLEAN)
        ) {
            $boolean = array_get($params, 'radios.moderation.selected') === 'is_moderated' ? true : false;
            $builder->whereIsModerated($boolean);
        }

        if (array_has($params, 'sort.direction')) {
            $builder->orderedWithDirection(array_get($params, 'sort.direction'));
        }

        return $builder->with('city');
    }


    public function moderate( int $id, bool $status = false )
    {
        /*** @var Organization $model */
        $organization = $this->findById((int) $id);

        $organization->is_moderated = $status === 'moderated' ? true : false;

        return $organization->save() ? true : false;
    }

    /**
     * @param array $data
     * @param $field
     * @return bool
     */
    private function fieldIsNotEmpty( array $data, $field )
    {
        return isset($data[ $field ]) && $data[ $field ] !== '';
    }


    /**
     * @param string $query
     * @return Builder
     */
    public function searchByName( string $query ) : Builder
    {
        list($query, $model) = $this->prepareForSearch($query);

        /** @noinspection PhpUndefinedMethodInspection
         * scopeWithName - > Only for models which extends Wipon\Models/I18nModel */
        return $model->withName($query)->orderBy('name_' . current_locale());
    }

    /**
     * @param int $id
     * @param string $query
     * @param array $slave_ids
     * @return Builder
     */
    public function mergingFilter( int $id, string $query, array $slave_ids = [] ) : Builder
    {
        list($query, $model) = $this->prepareForSearch($query);

        return $model::where('id', '!=', $id)
            ->whereNotIn('id', $slave_ids)
            ->withName($query)
            ->doesntHave('users')
            ->orderBy('name_' . current_locale());
    }

    /**
     * @param string $query
     * @return array
     */
    private function prepareForSearch( string $query )
    {
        $query = trim($query);
        $query = "%$query%";
        $model = $this->getModelClass();

        return [$query, $model];
    }

    /**
     * @param int $masterId
     * @param array $slaveIds
     * @return Organization
     * @throws \Error
     */
    public function merge( int $masterId, array $slaveIds ) : Organization
    {

        if ( ! $this->userIsSuperAdmin()) {
            log_w('Cant merge items only super-admin can. User: ' . auth()->user()->name);
            throw new \Error('Cant merge items only super-admin can. User: ' . auth()->user()->name);
        }

        $model = $this->getModelClass();

        /*** @var \Illuminate\Database\Eloquent\Collection|| Organization[] $model */
        $slaves = $model::whereIn('id', $slaveIds)->get();

        /*** @var Organization $model */
        $master = $this->findById($masterId);

        /*** @var Organization $slave */
        foreach ($slaves as $slave) {

            // если у организации есть пользователи - переходим к следующей организации, и пишем в логи что была попытка объединить организацию с
            // с пользователями
            if ($slave->users()->count() > 0) {
                log_w('Trying to merge organization with users!!! Current-user: ' . auth()->user()->name);
                continue;
            }

            // перенос псевдонимов
            // берём все имена name_ru name_kz name_en
            // делаем их них массив добавляя в них альясы
            $aliases = $this->getEntityAliases($slave);

            $this->updateMasterAliases($aliases, $slave, $master);

            // перенос продуктов
            // находим все продукты slave организации и обновляем у них поле organization_id
            Product::whereOrganizationId($slave->id)->update(['organization_id' => $masterId]);

            // удалить организацию
            $slave->forcedelete();
        }

        return $master;
    }


}