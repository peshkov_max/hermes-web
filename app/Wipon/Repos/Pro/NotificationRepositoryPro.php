<?php

namespace Wipon\Repos\Pro;

class NotificationRepositoryPro extends EloquentRepository
{
    protected $modelName = 'Notification';

    /**
     * Фильтр
     * @param array $params
     * @return mixed
     */
    public function filter( array $params )
    {
        $builder = $this->getModel();
        $builder = $builder::query();

        $user_id = array_get($params, 'user_id', null);

        if ($user_id && filter_var($user_id, FILTER_VALIDATE_INT)) {
            $builder->whereUserId($user_id);
        }

        $text = array_get($params, 'text', null);

        if ($text && is_string($text)) {
            $builder->where('text', 'ilike', str_for_search($text));
        }

        $direction = array_get($params, 'sort.direction', 'desc');

        $builder->orderBy('created_at', $direction);

        return $builder->with('user')->withCount('feedback_messages');
    }
}