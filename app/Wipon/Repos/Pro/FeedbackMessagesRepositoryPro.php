<?php
namespace Wipon\Repos\Pro;

use Wipon\Models\Pro\FeedbackMessage;

class FeedbackMessagesRepositoryPro extends EloquentRepository
{
    protected $modelName = 'FeedbackMessage';

    public function update( int $id, array $data ) : FeedbackMessage
    {
        $this->getModel()->whereId($id)->update($data);

        return $this->findById($id);
    }

    public function filter( array $params )
    {
        /**
         * @var $model \Eloquent
         */
        $builder = $this->getQueryBuilder();

        $builder->select('feedback_messages.id',
            'feedback_messages.user_id',
            'feedback_messages.name',
            'feedback_messages.phone_number',
            'feedback_messages.email',
            'feedback_messages.text',
            'feedback_messages.created_at',
            'feedback_messages.is_unread'
        );

        $this->searchFilters($params, $builder);

        $this->applyReadability($params, $builder);

        $this->applySorting($params, $builder);

        return $builder->with('user');
    }

    private function applyReadability( $params, $builder )
    {
        $use = array_get($params, 'radios.readUnread.use');
        $was_read = array_get($params, 'radios.readUnread.selected');

        if ($use != true) {
            return;
        }

        if ($was_read == 'read') {
            $builder->where('is_unread', false);
        }

        if ($was_read == 'unread') {
            $builder->where('is_unread', true);
        }
    }
    
    /**
     * @param array $params
     * @param \Illuminate\Database\Query\Builder $builder
     */
    protected function searchFilters( array $params, $builder )
    {
        if (array_has($params, 'id') && ! empty(array_get($params, 'id'))) {
            $builder->where("feedback_messages.id", '=', trim($params['id']));
        }

        if (array_has($params, 'user_id') && ! empty(array_get($params, 'user_id'))) {
            $builder->where("feedback_messages.user_id", '=', trim($params['user_id']));
        }

        if (array_has($params, 'search') && ! empty(array_get($params, 'search'))) {

            $builder->where(function ( $q ) use ( $params ) {

                $query = "%" . trim($params['search']) . "%";

                /*** @var \Eloquent $query */
                $q->where("feedback_messages.name", 'ilike', $query)
                    ->orWhere("feedback_messages.email", 'ilike', $query)
                    ->orWhere("feedback_messages.phone_number", 'ilike', $query);
            });
        }

        if (array_has($params, 'platform') && ! empty(array_get($params, 'platform'))) {

            $builder->join('users', function ( $join ) use ( $params ) {

                $query = '%' . trim($params['platform']) . '%';

                $join->on('feedback_messages.user_id', '=', 'users.id')
                    ->where('users.device->platform', 'ilike', $query);
            });
        }
    }

    /**
     * @param $id
     * @param $device_uuid
     * @return $this
     */
    public function getOtherDeviceMessages( $id, $device_uuid )
    {
        return $this->getQueryBuilder()->where('device_uuid', $device_uuid)->where('id', '!=', $id);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getQueryBuilder()
    {
        /**
         * @var $model \Eloquent
         */
        $model = $this->getModelClass();

        /*** @var \Illuminate\Database\Query\Builder $builder */
        $builder = $model::query();

        return $builder;
    }

    /**
     * @param array $params
     * @param $builder
     */
    private function applySorting( array $params, $builder )
    {
        if (isset($params['sort']['direction']) && isset($params['sort']['name'])) {
            
            switch ($params['sort']['name']) {
                case 'customer_name':
                    $builder->orderBy('name', $params['sort']['direction']);
                    break;
                case 'email':
                    $builder->orderBy('email', $params['sort']['direction']);
                    break;
                case 'created_at':
                    $builder->orderBy('created_at', $params['sort']['direction']);
                    break;
                default:
                    $builder->orderBy('created_at', 'desc');
                    break;
            }
        }
    }
}