<?php

namespace Wipon\Repos\Pro;

use Illuminate\Database\Eloquent\Builder;

class SubscriptionRepositoryPro extends EloquentRepository
{
    protected $modelName = 'Subscription';

    /**
     * Фильтрация данных
     *
     * @param array $params - Параметры из запроса
     * @return mixed
     */
    public function filter( array $params )
    {
        $builder = $this->getModel();
        $builder = $builder::query()->select(
            'subscriptions.id',
            'subscriptions.user_id',
            'subscriptions.created_at',
            'subscriptions.expires_at',
            'subscriptions.is_active',
            'users.name',
            'users.phone_number',
            'users.work_phone_number',
            'users.email');

        $str = array_get($params, 'search', null);
        $builder->join('users', function ( $join ) use ( $str ) {
            $join->on('subscriptions.user_id', '=', 'users.id');
            if ($str) {
                $str = str_for_search($str);
                $join->where(function ( $query ) use ( $str ) {
                    $query->where('users.name', 'ilike', $str)
                        ->orWhere('users.phone_number', 'ilike', $str)
                        ->orWhere('users.work_phone_number', 'ilike', $str)
                        ->orWhere('users.email', 'ilike', $str);
                });
            }
        });

        $this->applyStatusFilter($params, $builder);

        if ( ! empty(array_get($params, 'user_id', null))) {
            $builder->where('subscriptions.user_id', array_get($params, 'user_id'));
        }

        $this->applySorting($params, $builder);

        return $builder->with('user');
    }

    /**
     *
     * @param $params
     * @param Builder $builder
     * @return void
     */
    private function applyStatusFilter( $params, Builder $builder )
    {
        $use = array_get($params, 'radios.subscriptions_activation.use');
        $value = array_get($params, 'radios.subscriptions_activation.selected');

        if ($use != true) {
            return;
        }

        switch ($value) {
            case 'active':
                $builder->where('subscriptions.is_active', true);
                break;
            case 'not_active':
                $builder->where('subscriptions.is_active', false);
                break;
            default:
                break;
        }
    }

    private function sortParamsAreValid( $params )
    {
        return ! empty(array_get($params, 'sort.name'))
        && ! empty(array_get($params, 'sort.direction'))
        && in_array(array_get($params, 'sort.direction'), ['desc', 'asc'])
        && in_array(array_get($params, 'sort.name'), ['expires_at', 'created_at', 'email', 'customer_name']);
    }

    private function applySorting( $params, $builder )
    {
        if ( ! $this->sortParamsAreValid($params)) {
            return;
        }

        $direction = array_get($params, 'sort.direction');

        switch (array_get($params, 'sort.name')) {
            case 'customer_name':
                $builder->orderBy('users.name', $direction);
                break;
            case 'email':
                $builder->orderBy('users.email', $direction);
                break;
            case 'created_at':
                $builder->orderBy('subscriptions.created_at', $direction);
                break;
            case 'expires_at':
                $builder->orderBy('subscriptions.expires_at', $direction);
                break;
            default:
                $builder->orderBy('subscriptions.created_at', $direction);
                break;
        }
    }
}