<?php
namespace Wipon\Repos\Pro;

use Wipon\Repos\EloquentRepository as Repo;

class EloquentRepository extends Repo
{
    protected $modelNameSpace = "Wipon\\Models\\Pro\\";
    
    public function getModel()
    {
        $model = $this->modelNameSpace . $this->modelName;

        return new $model();
    }
}