<?php

namespace Wipon\Repos\Pro;

class UserRepositoryPro extends EloquentRepository
{
    protected $modelName = 'User';

    /**
     * Фильтр списка пользователей
     *
     * @param array $params
     * @return mixed
     */
    public function filter( array $params )
    {
        $builder = $this->getModel();
        $builder = $builder::query();

        $builder->select('id', 'name', 'phone_number', 'email', 'device');
        
        if ( ! empty(array_get($params, 'search'))) {
            
            $builder->where(function ( $q ) use ( $params ) {
                $str = str_for_search(array_get($params, 'search'));
                $q->where('name', 'ilike', $str)
                    ->orWhere('phone_number', 'ilike', $str)
                    ->orWhere('email', 'ilike', $str);
            });
        }

        if ( ! empty(array_get($params, 'id'))) {
            $builder->whereId(array_get($params, 'id', null));
        }
        
        if ( ! empty(array_get($params, 'sort.name')) && ! empty(array_get($params, 'sort.direction'))) {
            $builder->orderBy(array_get($params, 'sort.name'), array_get($params, 'sort.direction'));
        }

        return $builder->withCount('feedback_messages');
    }

}