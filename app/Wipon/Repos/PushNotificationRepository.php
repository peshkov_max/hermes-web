<?php

namespace Wipon\Repos;

class PushNotificationRepository extends EloquentRepository
{
    protected $modelName = 'DeviceNotification';

    
    public function update( int $id, array $data )
    {
        // TODO: Implement update() method.
    }

    public function filter( array $data )
    {
        /**
         * Use JOIN instead Laravel Eloquent lazy loading fore higher performance
         */
        $builder = $this->getModel()
            ->select('device_notifications.id',
                'device_notifications.device_uuid',
                'device_notifications.is_unread',
                'device_notifications.text',
                'device_notifications.deleted_at', 
                'device_notifications.created_at',
                'devices.app_version',
                'devices.push_token',
                'customers.phone_number')
            ->join('devices', 'devices.uuid', '=', 'device_notifications.device_uuid')
            ->leftJoin('customers', 'customers.id', '=', 'devices.customer_id');


        if (array_has($data, 'search') and  array_get($data, 'search')!='') {
            $builder->where(function($q) use ($data) {
                $query = str_for_search(array_get($data, 'search'));

                $q->where('device_notifications.device_uuid', 'ilike', $query);
                $q->orWhere('device_notifications.text', 'ilike', $query);
            });
        }

        if (array_has($data, 'sort') && array_has($data, 'sort.direction') && in_array(array_get($data, 'sort.direction'), ['desc', 'asc'])) {
            $builder->orderBy('device_notifications.created_at', array_get($data, 'sort.direction'));
        }
        
        return $builder;
    }
}