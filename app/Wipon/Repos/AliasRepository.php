<?php
namespace Wipon\Repos;

use Illuminate\Database\Eloquent\Relations\Relation;

class AliasRepository extends EloquentRepository
{
    private $morthMap;

    protected $modelName = 'Alias';

    public function __construct()
    {
        $this->morthMap = Relation::morphMap();
    }

    /**
     * Получение псевдонимов
     *
     * @param $model
     * @param array|null $params
     * @return mixed
     */
    public function filter( $model, array $params = null )
    {
        $builder = $model->aliases();

        if ( ! $params)
            return $builder;

        if (isset($params['search_query']) && $params['search_query'] != '') {
            $query = '%' . trim($params['search_query']) . '%';
            $builder->where('name', 'ilike', $query);
        }

        $builder->orderBy('name', 'asc');

        return $builder;
    }

    /**
     * Изменить псевдонима
     *
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function update( int $id, array $data )
    {
        $model = $this->findById($id);

        $model->name = $data['name'];

        return $model->save();
    }

    /**
     * Удаление псевдонима
     * 
     * @param int $id
     */
    public function destroy( $id = 0 )
    {
        $builder = $this->getModelClass();

        $builder::whereId($id)->delete();
    }

    /**
     * Найти morph model по $aliasable_type  $aliasable_id
     *
     * @param $aliasable_type
     * @param $aliasable_id
     * @return mixed
     */
    public function findMorthModel( $aliasable_type, $aliasable_id )
    {
        $model = $this->morthMap[ $aliasable_type ]::findOrFail($aliasable_id);

        return $model;
    }

    /**
     * Создать псевдоним
     *
     * @param $aliasable_type
     * @param $aliasable_id
     * @param $name
     * @return mixed
     */
    public function createMorth( $aliasable_type, $aliasable_id, $name )
    {
        $model = $this->findMorthModel($aliasable_type, $aliasable_id);

        $model->aliases()->create(['name' => $name]);

        return $model;
    }

}