<?php
namespace Wipon\Repos;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Wipon\Models\Alcohol\CheckStatistic;
use Wipon\Models\Check;

class CheckStatisticRepository extends EloquentRepository
{
    protected $modelName = 'CheckStatistic';

    public function update( int $id, array $data )
    {
    }

    /** Filter statistics
     *
     * @param array $params
     * @return Collection
     */
    public function filter( array $params = [] ) : Collection
    {
        if ( ! $this->paramsAreExists($params))
            return collect([]);

        if ($this->ukmTypeIsEmpty($params) || str_is($params['checkType'], 'nothing'))
            return collect([]);

        return $this->getRequestResult($params);
    }

    /** vValidate params for filter
     *
     * @param array $params - Filter parameters
     * @return bool
     */
    private function paramsAreExists( array  $params ) : bool
    {
        return
            array_has($params, 'period') &&
            array_has($params, 'ukmType') &&
            array_has($params, 'regionId') &&
            array_has($params, 'checkType');
    }

    /** Checks if ukm type is not set
     *
     * @param array $params - Filter parameters
     * @return bool
     */
    private function ukmTypeIsEmpty( array $params ) : bool
    {
        if ( ! filter_var(array_get($params, 'ukmType.all'), FILTER_VALIDATE_BOOLEAN)
            && ! filter_var(array_get($params, 'ukmType.valid'), FILTER_VALIDATE_BOOLEAN)
            && ! filter_var(array_get($params, 'ukmType.fake'), FILTER_VALIDATE_BOOLEAN)
            && ! filter_var(array_get($params, 'ukmType.duplicate'), FILTER_VALIDATE_BOOLEAN)
            && ! filter_var(array_get($params, 'ukmType.atlas'), FILTER_VALIDATE_BOOLEAN)
        ) {
            return true;
        }

        return false;
    }

    /**
     * Выполняет запрос фильтрации
     *
     * Из-за нестандартного запросы было решено сделать Raw запрос
     *
     * Метод mapFilterQueryResults преобразует массив результата запроса
     * в Eloquent коллекцию типа CheckStatistic. Это сделано для удобства, т.к.
     * после мапинга мы можем подгружать связи мидели методом load. Подобная подгрузка методов происходит
     * при формировании отчётов в классе CheckMapRequest, в методе generateMapReport стр.322.
     *
     * @param array $params - Filter parameters
     * @return Collection
     */
    private function getRequestResult( array $params ) : Collection
    {
        $ukmCondition = $this->getUkmCondition($params);
        $enterTypeCondition = $this->getEnterTypeCondition($params);
        $regionCondition = $this->getRegionCondition($params);
        $periodCondition = $this->getPeriodCondition($params);
        
        /**
         * we need array - not stdClass
         */
        DB::setFetchMode(\PDO::FETCH_ASSOC);
        // DB::enableQueryLog();

        list($selectFields, $subQueryFields) = CheckStatistic::getQueryRequiredFields();

        $queryResult = DB::select(DB::raw(
            "SELECT {$selectFields}
                FROM (SELECT
                    {$subQueryFields}
                    FROM kgd_alcohol.check_statistics
                    JOIN core.clusters ON core.clusters.id = kgd_alcohol.check_statistics.cluster_id
                         {$regionCondition}
                         {$periodCondition}        
                      
                      WINDOW w AS (
                        PARTITION BY kgd_alcohol.check_statistics.cluster_id
                        ORDER BY kgd_alcohol.check_statistics.cluster_id )
                        
                     ) AS sub_query
                     join  core.cities on core.cities.id = sub_query.city_id
                     join  core.regions on core.regions.id = core.cities.region_id
                     
                WHERE sub_query.row_number = 1 and {$ukmCondition} {$enterTypeCondition}
                ORDER BY id"));

        DB::setFetchMode(\PDO::FETCH_CLASS);
          // log_i(DB::getQueryLog());

        return $this->mapFilterQueryResults($queryResult);
    }

    /** Преобразует массив в коллекцию CheckStatistic объектов
     *
     * @param array $queryResults
     * @return EloquentCollection
     */
    private function mapFilterQueryResults( array $queryResults ) : EloquentCollection
    {
        $collection = EloquentCollection::make();

        foreach ($queryResults as $queryResult) {
            $collection->add(new CheckStatistic($queryResult));
        }

        return $collection;
    }

    /** Get ukm condition for filter
     *
     * @param array $params - Filter parameters
     * @return string | ''
     */
    private function getUkmCondition( array $params = ['ukmType' => ['all' => true]] ) : string
    {
        $array = $this->getUkmConditionArray(array_get($params, 'ukmType'));

        return count($array) > 0 ? '(' . implode(' or ', $array) . ')' : '';
    }

    /** Construct ukm conditions
     *
     * @param array $ukmTypes -  Ukm type: atlas | valid | fake | duplicate
     * @return array
     */
    private function getUkmConditionArray( array $ukmTypes = ['all' => true] ) : array
    {
        $condition = [];
        foreach ($ukmTypes as $key => $value) {

            if (str_is($key, 'all') && filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                $condition = [];
                $condition[] = "check_total > 0";
                break;
            }

            if (filter_var($value, FILTER_VALIDATE_BOOLEAN))
                $condition[] = "check_{$key}_sum > 0";
        }

        return $condition;
    }

    /** Get Enter type condition
     *
     * @param array $params - Filter params
     * @return string | ''
     */
    private function getEnterTypeCondition( array $params ) : string
    {
        if ($this->isScanOrHand($params)) {

            $whereClause = $this->getEnterTypeByUkmArray(array_get($params, 'checkType'), array_get($params, 'ukmType'));

            return ! empty($whereClause) ? ' and (' . $whereClause . ')' : ' and id is null';
        }

        // return all enter types
        if ($this->isBoth($params))
            return '';

        // clean query result - return nothing
        if ($this->isNothing($params))
            return ' and id is null';

        return '';
    }

    /** Construct part of query? which filter by ukm
     *
     * @param string $enterType - Check type: hand | scan
     * @param array $ukmTypes - Ukm type: all | atlas | valid | fake | duplicate
     * @return string | ''
     */
    private function getEnterTypeByUkmArray( string $enterType = 'scan', array $ukmTypes = ['all' => true] ) : string
    {
        $condition = [];

        foreach ($ukmTypes as $key => $value) {

            if (str_is($key, 'all') && filter_var($value, FILTER_VALIDATE_BOOLEAN)) {
                $condition = [];
                $condition[] = "check_by_{$enterType}_sum > 0";
                break;
            }

            if (filter_var($value, FILTER_VALIDATE_BOOLEAN))
                $condition[] = "check_{$key}_{$enterType} > 0";
        }

        return implode(' or ', $condition);
    }

    /** Get region condition
     *
     * @param array $params - Filter params
     * @return string | '
     */
    private function getRegionCondition( array $params ) : string
    {
        // return all regions by default
        if ( ! array_has($params, 'regionId'))
            return '';

        return "JOIN core.cities ON core.cities.id = core.clusters.city_id and cities.region_id = " . array_get($params, 'regionId');
    }

    /** Get period condition
     *
     * @param array $params
     * @return string | ''
     * @throw Exception
     */
    private function getPeriodCondition( array  $params ) : string
    {
        try {

            $start = Carbon::createFromFormat(config('wipon.pss_date_format'), array_get($params, 'period.start'), config('wipon.user_time_zone'))
                ->timezone('UTC')
                ->setTime(0, 0, 0)
                ->format(config('wipon.date_time_format'));

            $end = Carbon::createFromFormat(config('wipon.pss_date_format'), array_get($params, 'period.end'), config('wipon.user_time_zone'))
                ->timezone('UTC') 
                ->setTime(23, 59, 59)
                ->format(config('wipon.date_time_format'));

            return "where scanned_at between '{$start}' and '{$end}'";
        } catch (\Exception $e) {

            log_e($e);

            return '';
        }
    }

    /**
     * Check if check type is:
     * NOT: scan or hand
     * BUT: nothing or both
     *
     * @param $params
     * @return bool
     */
    private function isScanOrHand( array $params ) : bool
    {
        return isset($params['checkType']) && $params['checkType'] != 'both' && $params['checkType'] != 'nothing';
    }

    /** Check if check type is BOTH
     * @param $params
     * @return bool
     */
    private function isBoth( array $params ) : bool
    {
        return isset($params['checkType']) && $params['checkType'] === 'both';
    }

    /** Check if check type is NOTHING
     *
     * @param $params
     * @return bool
     */
    private function isNothing( array $params ) : bool
    {
        return isset($params['checkType']) && $params['checkType'] === 'nothing';
    }

    /** Get participants for KGD competition
     *
     * @param $start
     * @param $end
     * @param $regionId
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getKgdCompetitionParticipants( $start, $end, $regionId )
    {
        $sub_query = Check::selectRaw("distinct
                     checks.device_uuid,
                     clusters.id as cluster_id,
                     clusters.name_ru as cluster_name,
                     clusters.street,  
                     clusters.house,  
                     clusters.latitude,
                     clusters.longitude,  
                     products.name_ru as product_name,
                     product_types.name_ru as product_type,
                     cities.name_ru as city_name,
                     organizations.name_ru as organization_name,
                     items.sticker->>'excise_code' as excise_code, 
                     checks.created_at, 
                     items.status, 
                     items.is_duplicated,
                     row_number() over (PARTITION BY items.sticker->>'excise_code'
                     ORDER BY items.sticker->>'excise_code' ) as r_n")
            ->join('items', 'checks.item_id', '=', 'items.id')
            ->join('clusters', 'checks.cluster_id', '=', 'clusters.id')
            ->join('cities', 'clusters.city_id', '=', 'cities.id')
            ->join('products', 'items.product_id', '=', 'products.id')
            ->join('organizations', 'products.organization_id', '=', 'organizations.id')
            ->join('product_types', 'products.product_type_id', '=', 'product_types.id')
            ->where("items.sticker->excise_code", '!=', '')
            ->where("checks.created_at", '>=', Carbon::createFromFormat(config('wipon.date_time_format'), $start))
            ->where("checks.created_at", '<=', Carbon::createFromFormat(config('wipon.date_time_format'), $end))
            ->where("cities.region_id", '=',  (int) $regionId);

        foreach (config('wipon.whitelist') as $uuid)
            $sub_query->where("checks.device_uuid", '<>', $uuid);

        $builder = Check::from(DB::raw('( ' . $sub_query->toSql() . ' ) AS sub_query'))
            ->mergeBindings($sub_query->getQuery())
            ->where("sub_query.r_n", 1)
            ->orderBy('created_at');
        
        return $builder->get();
    }
    
    /** Выполняет запрос фильтрации для выгрузки одного кластера
     *
     * @param int $clusterId
     * @return $this
     * @throws \Exception
     */
    public function findByClusterId( int $clusterId )
    {
        if ( ! $clusterId)
            throw new \Exception("ClusterId was expected but $clusterId given");

        /**
         * we need array - not stdClass
         */
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        list($selectFields, $subQueryFields) = CheckStatistic::getQueryRequiredFields();

        $queryResult = DB::select(DB::raw(
            "SELECT {$selectFields}
                FROM (SELECT
                      {$subQueryFields}
                      FROM kgd_alcohol.check_statistics
                      JOIN core.clusters ON core.clusters.id = kgd_alcohol.check_statistics.cluster_id
                      and core.clusters.id = {$clusterId} 
                      WINDOW w AS (
                        PARTITION BY kgd_alcohol.check_statistics.cluster_id
                        ORDER BY kgd_alcohol.check_statistics.cluster_id )
                     ) AS sub_query
                    join  core.cities on core.cities.id = sub_query.city_id
                    join  core.regions on core.regions.id = core.cities.region_id 
                WHERE sub_query.row_number = 1
                ORDER BY id"));

        DB::setFetchMode(\PDO::FETCH_CLASS);

        return (new CheckStatistic(isset($queryResult[0]) ? $queryResult[0] : []))->load('checks.item');
    }

    /** Выгрузка только сканирований отсортированных по убыванию даты сканирований
     *
     * @param array $params - Параметры фильтра
     * @return array
     */
    public function getChecksForReport( array $params )
    {
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $subQueryFields = CheckStatistic::SUB_QUERY_FIELDS;
        $ukmCondition = $this->getUkmCondition($params);
        $enterTypeCondition = $this->getEnterTypeCondition($params);
        $periodCondition = $this->getPeriodCondition($params);
     
        $queryResult = DB::select(DB::raw(
            "SELECT 
                        sub_query.id,
                        sub_query.check_id,
                        sub_query.city_id,
                        sub_query.name as cluster_name,
                        sub_query.latitude,
                        sub_query.longitude,
                        sub_query.street,
                        sub_query.house,
                   
                        core.items.sticker as item_sticker,
                        core.items.status as item_status,
                        core.products.name_ru as product_name,
                        core.checks.retailer,
                        core.product_types.name_ru as product_type_name,
                        core.organizations.name_ru as organization_name,
                       
                        sub_query.check_valid_hand,
                        sub_query.check_valid_scan,
                        sub_query.check_fake_hand,
                        sub_query.check_fake_scan,
                        sub_query.check_atlas_hand,
                        sub_query.check_atlas_scan,
                        sub_query.check_duplicate_hand,
                        sub_query.check_duplicate_scan,
                        sub_query.item_valid_hand,
                        sub_query.item_valid_scan,
                        sub_query.item_fake_hand,
                        sub_query.item_fake_scan,
                        sub_query.item_atlas_hand,
                        sub_query.item_atlas_scan,
                        sub_query.item_duplicate_hand,
                        sub_query.item_duplicate_scan,
                       
                        sub_query.check_valid_sum,
                        sub_query.check_fake_sum,
                        sub_query.check_atlas_sum,
                        sub_query.check_duplicate_sum,
                       
                        sub_query.item_valid_sum,
                        sub_query.item_fake_sum,
                        sub_query.item_atlas_sum,
                        sub_query.item_duplicate_sum,
                        sub_query.item_total,
                        sub_query.check_total,
                        sub_query.check_by_hand_sum,
                        sub_query.check_by_scan_sum,
                        sub_query.item_by_hand_sum,
                        sub_query.item_by_scan_sum,
                        sub_query.unique_devices,
                   
                        core.cities.name_ru as city_name,
                        core.regions.name_ru as region_name,
                        scanned_at
        
                FROM (SELECT
               
                     {$subQueryFields} ,
                     kgd_alcohol.check_statistics.scanned_at
                     FROM kgd_alcohol.check_statistics
                         JOIN core.clusters ON core.clusters.id = kgd_alcohol.check_statistics.cluster_id
                         JOIN core.cities ON core.cities.id = core.clusters.city_id and cities.region_id = {$params['regionId']}
                           {$periodCondition}        
                              WINDOW w AS (
                                PARTITION BY kgd_alcohol.check_statistics.cluster_id
                                ORDER BY kgd_alcohol.check_statistics.cluster_id )
                             ) AS sub_query
        
                    join core.cities on core.cities.id = sub_query.city_id
                    join core.regions on core.regions.id = core.cities.region_id
                    join core.checks on sub_query.check_id = core.checks.id
                    join core.items on core.checks.item_id = core.items.id
                    left join core.products on core.items.product_id = core.products.id
                    left join core.product_types on core.product_types.id = core.products.product_type_id
                    left join core.organizations on core.organizations.id = core.products.organization_id
        
                 WHERE {$ukmCondition} {$enterTypeCondition}
                 ORDER BY scanned_at"));
        
        DB::setFetchMode(\PDO::FETCH_CLASS);

        return $queryResult;
    }

}