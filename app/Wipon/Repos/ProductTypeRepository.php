<?php
namespace Wipon\Repos;

use Wipon\Models\ProductType;

class ProductTypeRepository extends EloquentRepository
{

    protected $modelName = 'ProductType';

    public function update( int $id, array $data )
    {
        /*** @var ProductType $model */
        $model = $this->findById($id);
        $model->name_en = $data['name_en'] ??  $model->name_en;
        $model->name_ru = $data['name_ru'] ??  $model->name_ru;
        $model->name_kk = $data['name_kk'] ??  $model->name_kk;

        $model->save();

        return $model;
    }

    public function filter( array $params )
    {
        $model = $this->getModelClass();
        $builder = $model::query();

        if (array_has($params, 'searchByName') && array_get($params, 'searchByName') !== '') {

            $str = str_for_search(array_get($params, 'searchByName', 'stringIsEmpty'));

            $builder->where(function ( $q ) use ( $str ) {
                $q->where('name_en', 'ilike', $str);
                $q->orWhere('name_kk', 'ilike', $str);
                $q->orWhere('name_ru', 'ilike', $str);
            });
        }

        if (array_has($params, 'searchByParentName') && array_get($params, 'searchByParentName') !== '') {
            $str = str_for_search(array_get($params, 'searchByParentName', 'stringIsEmpty'));

            $builder->whereHas('parent', function ($query) use ( $str ){
                $query->where(function ( $q ) use ( $str ) {
                    $q->where('name_en', 'ilike', $str);
                    $q->orWhere('name_kk', 'ilike', $str);
                    $q->orWhere('name_ru', 'ilike', $str);
                });
            });
        }

        list ($name, $direction) = $this->getSortParams($params);

        return $builder->orderBy($name, $direction);
    }

    /**
     * @param array $params
     * @return bool
     */
    private function getSortParams( array $params )
    {
        return array_has($params, 'sort')
        && array_has($params, 'sort.name')
        && array_has($params, 'sort.direction')
        && in_array(array_get($params, 'sort.name'), ['name_ru', 'name_kz', 'name_en',])
        && in_array(array_get($params, 'sort.direction'), ['asc', 'desc',])

            ? [array_get($params, 'sort.name'), array_get($params, 'sort.direction')]
            : ['name_ru', 'desc'];
    }
}