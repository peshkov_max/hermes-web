<?php
namespace Wipon\Repos;

use DB;
use Wipon\Models\ApiRequest;

class ApiRequestRepository extends EloquentRepository
{
    protected $modelName = 'ApiRequest';

    public function update( int $id, array $data )
    {
    }

    public function filter( array $data )
    {
        /**
         * @var $model \Eloquent
         */
        $model = $this->getModelClass();

        /*** @var \Illuminate\Database\Query\Builder $builder */
        $builder = $model::query();
        
        $query = $data['select_query'];
        dd($query->get());  
        if ( ! $query) {
            return $builder->orderBy('created_at', 'decs');
        }
 
        $pattern = '/(DROP|TRUNCATE|DELETE|INSERT|UPDATE|CREATE)/mi';
        preg_match($pattern, $query, $matches);

        if (count($matches) > 0) {
            throw new \Exception("Invalid select query, One of you must not use one of these words: 
                                  DROP, TRUNCATE, DELETE, INSERT, UPDATE, CREATE. $query Given.");
        }

        return $builder->whereRaw(DB::raw($query))->orderBy('created_at', 'decs');
    }
    
}