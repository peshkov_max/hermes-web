<?php namespace Wipon\Repos;

use Carbon\Carbon;

abstract class EloquentRepository implements RepositoryInterface
{
    protected $modelNameSpace = "Wipon\\Models\\";

    public function getModel()
    {
        $model = $this->modelNameSpace . $this->modelName;

        return new $model();
    }

    public function getModelClass() : string
    {
        return get_class($this->getModel());
    }

    public function perPage()
    {
        return $this->getModel()->getPerPage();
    }

    public function getAll()
    {
        return $this->getModel()->paginate($this->perPage());
    }

    public function search( $searchQuery )
    {
        $search = "%{$searchQuery}%";

        return $this->getModel()->where('name', 'like', $search)
            ->orWhere('email', 'like', $search)
            ->paginate($this->perPage());
    }

    public function findById( $id )
    {
        return $this->getModel()->findOrFail($id);
    }

    public function findBy( $key, $value, $operator = '=' )
    {
        return $this->getModel()->where($key, $operator, $value)->paginate($this->perPage());
    }

    public function delete( $id )
    {
        $model = $this->findById($id);

        if ( ! is_null($model)) {
            $model->delete();

            return true;
        }

        return false;
    }

    public function create( array $data )
    {
        return $this->getModel()->create($data);
    }

    /**
     * Проверка запроса на существование джоина таблицы
     *
     * @param   \Illuminate\Database\Eloquent\Builder $query
     * @param   string $joinTable
     * @return  bool
     */
    protected function joinExists( $query, $joinTable )
    {
        if ( ! $query->getQuery()) {
            return false;
        }

        $joins = $query->getQuery()->joins;

        if ( ! $joins || empty($joins)) {
            return false;
        }

        foreach ($joins as $join) {
            if ($join->table == $joinTable) {
                return true;
            }
        }

        return false;
    }

    public function update( int $id, array $data )
    {
        try {
            $this->findById($id)->update($data);
        } catch (\Exception $e) {
            log_e($e->getMessage());
        }
    }   

    /**
     * Возвращает начальную дату периода и конечную дату периода в виде Carbon объектов
     *
     * @param array - Массив содержащий два поля: $params['start'], $params['end']
     * @param string $fromFormat - Формат, используя который создается объект Carbon
     * @param string $toFormat - Формат, к которому приводится дата
     * @param int $subMonth - Количество месяцев, вычитаемое для начальной даты в значении
     *                              по умолчанию(в случае,если парсинг привел к ошибке)
     * @return array              - Массив из двух элементов, начальной даты, и конечной даты
     */
    public static function getPeriods( array $params, string $fromFormat = 'Y-m-d H:i:s', string $toFormat = 'Y-m-d H:i:s', int $subMonth = 1 )
    {
        try {
            return [
                Carbon::createFromFormat($fromFormat, array_get($params, 'period.start'), config('wipon.user_time_zone'))
                    ->timezone('UTC')
                    ->format($toFormat),
                Carbon::createFromFormat($fromFormat, array_get($params, 'period.end'), config('wipon.user_time_zone'))
                    ->timezone('UTC')
                    ->format($toFormat),
            ];
        } catch (\Exception $e) {

            log_w('Period default values are empty', [$e->getMessage()]);

            return [
                Carbon::now()
                    ->subMonth($subMonth)
                    ->timezone('UTC')
                    ->format($toFormat),
                Carbon::now()
                    ->timezone('UTC')
                    ->format($toFormat),
            ];
        }
    }


    public function userIsSuperAdmin()
    {
        return auth()->user()->hasRole('super-admin');
    }
}