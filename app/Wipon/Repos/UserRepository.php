<?php
namespace Wipon\Repos;

use Wipon\Models\User;

class UserRepository extends EloquentRepository
{

    protected $modelName = 'User';

    public function create( array $data )
    {
        $currentDateTime = date(config('wipon.date_time_format'));

        $newUser = $this->getModel()->create([
            "name"            => $data['name'],
            "email"           => $data['email'],
            "api_token"       => bcrypt($data['email']),
            "organization_id" => (int) $data['organization'],
            "created_at"      => $currentDateTime,
        ]);

        // attach roles
        $newUser->roles()->attach($data['roles']);

        return $newUser;
    }

    public function update( int $id, array $data )
    {
        /** @var User $user */
        $user = $this->findById($id);

        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->organization_id = isset($data['organization']) && $data['organization'] != 0 ? $data['organization'] : $user->organization_id;
        $user->save();

        // attach roles
        if ($data['roles'] && count($data['roles']) > 0)
            $user->roles()->sync($data['roles']);

        return $user;
    }

    public function getAll()
    {
        return $this->getModel()->with('organization', 'roles')->paginate($this->perPage());
    }

    public function delete( $id )
    {
        $model = $this->findById($id);
        if ( ! is_null($model)) {
            $model->forceDelete();

            return true;
        }

        return false;
    }


    public function filter( $params )
    {
        $model = $this->getModelClass();

        $builder = $model::query();

        $builder->whereIsTesting(false);

        if ($params['search']) {

            $query =  str_for_search($params['search']);
            $builder->where(function ( $q ) use ( $query ) {
                $q->where('name', 'ilike', $query)
                    ->orWhere('email', 'ilike', $query);
            });
        }

        if (array_has($params, 'organizationId') && array_get($params, 'organizationId') !== '') {
            $builder->where('organization_id', array_get($params, 'organizationId'));
        }

        if (array_has($params, 'roleId') && array_get($params, 'roleId') !== '') {
            $builder->whereHas('roles', function ( $query ) use ( $params ) {
                $query->where('id', array_get($params, 'roleId'));
            });
        }

        if (array_has($params, 'sort') && array_has($params, 'sort.name') && array_has($params, 'sort.direction')) {
            $builder->orderBy(array_get($params, 'sort.name') , array_get($params, 'sort.direction'));
        } 
        
        return $builder->with('organization', 'roles');
    }
}