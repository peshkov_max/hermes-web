<?php
namespace Wipon\Repos;

use Wipon\Models\Role;

class RoleRepository extends EloquentRepository
{
    protected $modelName = 'Role';
    
    public function update( int $id, array $data )
    {
        /*** @var Role $role */
        $role = $this->findById($id);
        $role->name = $data['name'];
        $role->display_name = $data['display_name'];
        $role->description = $data['description'];
        $role->save();

        return $role;
    }

    public function withName($searchQuery)
    {  //    todo: Need refactor -> change to ->where(function ( $q ) use ( $query ) {
        //   todo:  $q->where('name_ru', 'ilike', $query)
        //   todo:       ->orWhere('name_en', 'ilike', $query)
        //   todo:      ->orWhere('name_kk', 'ilike', $query);
        //   todo:     }) 
       return Role::whereRaw("(name ilike '%" . $searchQuery . "%' or display_name ilike '%" . $searchQuery . "%' or description ilike '%" . $searchQuery . "%')")
            ->with('users');
    }


}