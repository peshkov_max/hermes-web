<?php

namespace Wipon\Repos;

use Wipon\Models\Customer;
use DB;

class CustomerRepository extends EloquentRepository
{
    protected $modelName = 'Customer';

    public function update( int $id, array $data )
    {
        try {

            /**
             * @var $model Customer
             */
            $model = $this->findById($id);
            $model->name = $data['name'];
            $model->phone_number = $data['phone_number'];
            $model->email = $data['email'] ?? null;

            return $model->save();
        } catch (\Exception $e) {
            log_e('Error while saving product', [$e->getMessage()]);

            return false;
        }
    }

    /**
     * Фильтр списка пользователей
     *
     * @param array $params
     * @return mixed
     */
    public function filter( array $params )
    {
        $builder = $this->getModel();
        $builder = $builder::query();
        
        $builder->selectRaw(DB::raw(' 
                 max(customers.id) as id, 
                 max(customers.name) as name, 
                 max(customers.phone_number) as phone_number, 
                 max(devices.uuid) as device_uuid,
                 max(customers.email) as email'));
        $builder->join('devices', 'devices.customer_id', '=', 'customers.id');
        
        if ( ! empty(array_get($params, 'search'))) {
            
            $builder->where(function ( $q ) use ( $params ) {
                $str = str_for_search(array_get($params, 'search'));
                $q->where('name', 'ilike', $str)
                    ->orWhere('phone_number', 'ilike', $str)
                    ->orWhere('email', 'ilike', $str)
                    ->orWhere('devices.uuid', 'ilike', $str);
            });
        }
        
        $builder->groupBy('phone_number');
        
        if ( ! empty(array_get($params, 'sort.name')) && ! empty(array_get($params, 'sort.direction'))) {
            $builder->orderBy(array_get($params, 'sort.name'), array_get($params, 'sort.direction'));
        }

        return $builder->with('devices');
    }

}