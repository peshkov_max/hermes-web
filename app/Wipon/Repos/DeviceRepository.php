<?php
namespace Wipon\Repos;

use Wipon\Models\Device;
use Wipon\Models\FeedbackMessage;

class DeviceRepository extends EloquentRepository
{
    protected $modelName = 'Device';

    public function update( int $id, array $data ) : Device
    {
        $this->getModel()->whereId($id)->update($data);

        return $this->findById($id);
    }

    /**
     * Получение устройств пользователя, отсортированных по дате последней активности пользователя
     *
     * @param array $params
     * @return mixed
     */
    public function filter( array $params )
    {
        $builder = $this->getModel()
            ->select('devices.platform', 'devices.model', 'devices.deleted_at', 'push_token', 'customer_id', 'app_version', 'uuid', 'devices.updated_at', 'customers.phone_number')
            ->leftJoin('customers', 'customers.id', '=', 'devices.customer_id')
            ->whereBetween('devices.updated_at', $this->getPeriods($params, config('wipon.dt_format_d_m_y_h_i'), 'Y-m-d H:i:s', 240));
       
        // выборка по версии устройства
        if ($this->mustBeFilteredByVersion($params)) {
            $version = trim(array_get($params, 'version'));
            $builder->where('devices.app_version', $version);
        }

        if (array_has($params, 'searchByUuid') && array_get($params, 'searchByUuid') !== '') {
            $builder->where('devices.uuid', '=', array_get($params, 'searchByUuid'));
        }
     
        if (array_has($params, 'searchByModel') && array_get($params, 'searchByModel') !== '') {
            $builder->where('devices.model', 'ilike', str_for_search(array_get($params, 'searchByModel')));
        }

        if ($params['sort']) {
            
            if ($this->needAttributeSorting($params))
                $builder->orderBy($params['sort']['name'], $params['sort']['direction']);
            
            if ($this->needActivitySorting($params)) {
                $direction = $this->needActivitySorting($params) ? $params['sort']['direction'] : 'desc';
                $builder->orderBy('devices.updated_at', $direction);
            }
        }

        return $builder;
    }

    /**
     * @param $params
     * @return bool
     */
    private function needActivitySorting( $params )
    {
        if (array_has($params, 'sort') &&
            array_has($params, 'sort.name') &&
            array_get($params, 'sort.name') == 'updated_at'
        ) {
            return true;
        }

        return false;
    }

    /***
     * @param $params
     * @param $field
     * @return bool
     */
    private function needSearch( $params, $field )
    {
        if (array_has($params, 'search') &&
            array_has($params, "search.{$field}") &&
            array_has($params, "search.{$field}.value")
            && array_get($params, "search.{$field}.value") !== ''
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param array $params
     * @return bool
     */
    private function needAttributeSorting( array $params )
    {
        return array_has($params, 'sort.direction') &&
        in_array(array_get($params, 'sort.name'), ['uuid', 'model', 'platform']);
    }

    /**
     * @param array $params
     * @return bool
     */
    private function mustBeFilteredByVersion( array $params )
    {
        return array_has($params, 'version')
        && array_get($params, 'version') != 'all'
        && array_get($params, 'version') != '';
    }

    public function getCountDeviceMessages( $device_uuid )
    {
        return FeedbackMessage::where('device_uuid', $device_uuid)->count();
    }
}