<?php

namespace Wipon\Repos;

use Wipon\Models\Region;

class ClusterRepository extends EloquentRepository
{
    protected $modelName = 'Cluster';

    /** @var \Wipon\Models\Cluster query */
    protected $query;

    /** @inheritdoc */
    public function filter(array $filter)
    {
        $this->query = $this->getModel()->with('city', 'cluster_type')->select('clusters.*');

        $region_id = array_get($filter, 'region');
        $city_id = array_get($filter, 'city');
        $without_city_id = array_get($filter, 'without_city_id');
        $without_address_only = array_get($filter, 'without_address_only');
        $without_type_only = array_get($filter, 'without_type_only');
        $without_name_only = array_get($filter, 'without_name_only');
        $type = array_get($filter, 'type');
        $fixed_status = array_get($filter, 'fixed_status');
        $moderated_status = array_get($filter, 'moderated_status');
        $licensed_status = array_get($filter, 'licensed_status');

        if ($without_city_id !== "true") {
            if (empty($region_id)) {
                $region_id = Region::whereNameEn('Astana')->first()->id;
            }

            if (!$this->joinExists($this->query, 'cities')) {
                $this->query = $this->query->join("cities", "cities.id", "=", "clusters.city_id");
            }

            $this->query = $this->query->where('cities.region_id', $region_id);

            if (! empty($city_id)) {
                $this->query = $this->query->where('clusters.city_id', $city_id);
            }
        } else {
            $this->query = $this->query->whereNull('clusters.city_id');
        }

        if ($without_address_only === "true") {
            // WHERE += ((street = '' OR street IS NULL) AND (house = '' OR house IS NULL))
            $this->query = $this->query->where(function($q) {
                /** @var \Wipon\Models\Cluster $q */
                $q->where(function($q) {
                    /** @var \Wipon\Models\Cluster $q */
                    $q->whereNull('clusters.street')->orWhere('clusters.street', '');
                });

                $q->where(function($q) {
                    /** @var \Wipon\Models\Cluster $q */
                    $q->whereNull('clusters.house')->orWhere('clusters.house', '');
                });
            });
        }

        if ($without_name_only === "true") {
            $this->query = $this->query->where(function($q) {
                // WHERE += ((name_ru = '' OR name_ru IS NULL) AND (name_en = '' OR name_en IS NULL)
                //  AND (name_kk = '' OR name_kk IS NULL))

                $this->query = $this->query->where(function($q) {
                    /** @var \Wipon\Models\Cluster $q */
                    $q->where(function($q) {
                        /** @var \Wipon\Models\Cluster $q */
                        $q->whereNull('clusters.name_ru')->orWhere('clusters.name_ru', '');
                    });

                    $q->where(function($q) {
                        /** @var \Wipon\Models\Cluster $q */
                        $q->whereNull('clusters.name_en')->orWhere('clusters.name_en', '');
                    });

                    $q->where(function($q) {
                        /** @var \Wipon\Models\Cluster $q */
                        $q->whereNull('clusters.name_kk')->orWhere('clusters.name_kk', '');
                    });
                });
            });
        }

        if ($without_type_only === "true") {
            $this->query = $this->query->whereNull("clusters.cluster_type_id");
        }

        if (! empty($type)) {
            $this->query = $this->query->whereClusterTypeId($type);
        }

        if (! empty($fixed_status) && in_array($fixed_status, ["fixed_only", "unfixed_only"])) {
            $this->query = $this->query->whereIsFixed($fixed_status === "fixed_only");
        }

        if (! empty($moderated_status) && in_array($moderated_status, ["moderated_only", "not_moderated_only"])) {
            $this->query = $this->query->whereIsModerated($moderated_status === "moderated_only");
        }

        if (! empty($licensed_status) && in_array($licensed_status, ["licensed_only", "not_licensed_only"])) {
            $this->query = $this->query->whereIsLicensed($licensed_status === "licensed_only");
        }

        return $this->query;
    }

    public function update(int $id, array $data)
    {
        // TODO: Implement update() method.
    }
}