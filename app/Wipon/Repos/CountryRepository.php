<?php

namespace Wipon\Repos;

use Wipon\Models\Country;

class CountryRepository extends EloquentRepository 
{

    protected $modelName = 'Country';

    /** @inheritdoc */
    public function filter(array $filter)
    {
    }


    public function update(int $id , array $data ) : Country
    {
        $this->getModel()->whereId($id)->update($data);

        return $this->findById($id);
    }

    public function searchByName(string $querySearch ) 
    {
        $model = $this->getModelClass();
        
        /** @noinspection PhpUndefinedMethodInspection
         * scopeWithName - > Only for models which extends Wipon\Models/I18nModel */
        return  $model::withName($querySearch)->orderBy('name_'.current_locale());
    }
}