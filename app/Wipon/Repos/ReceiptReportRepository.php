<?php

namespace Wipon\Repos;

use Illuminate\Database\Eloquent\Builder;

class ReceiptReportRepository extends EloquentRepository
{
    protected $modelName = 'ReceiptReport';

    public function update( int $id, array $data )
    {
    }

    public function filter( array $params = [], bool $withOrdering = true ): Builder
    {
        $builder = $this->getModel()->with('device.customer', 'region', 'type');

        if (array_has($params, 'regionId') && array_get($params, 'regionId') !== 'all' && array_get($params, 'regionId') !== '')
            $builder->whereRegionId((int) array_get($params, 'regionId'));

        $typeId = array_get($params, 'typeId');

        if (array_has($params, 'typeId') && $typeId !== 'all' && $typeId !== '')
            $builder->whereReceiptReportTypeId($typeId);

        if (array_has($params, 'period') && array_has($params, 'period.start') && array_has($params, 'period.end'))
            $builder->whereBetween('created_at', $this->getPeriods($params, config('wipon.dt_format_d_m_y_h_i')));

        if ($withOrdering)
            $builder
                ->orderBy('created_at', $params['sort']['direction'] ?? 'asc')
                ->orderBy('id', 'desc');

        return $builder;
    }

}