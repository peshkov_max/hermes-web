<?php
namespace Wipon\Repos;

use Illuminate\Database\Eloquent\Builder;
use Wipon\Models\Product;
use Wipon\Traits\Repos\MergeTrait;

class ProductRepository extends EloquentRepository
{
    use MergeTrait;

    protected $modelName = 'Product';

    public function create( array $data )
    {

        if ( ! $this->userIsSuperAdmin()) {
            log_e('User with not super-admin role trying to create organization');

            return new $this->modelName;
        }

        try {

            $product = $this->getModel();

            $product->product_type_id = $data['product_type_id'] ?? null;
            $product->organization_id = $data['organization_id'] ?? null;

            $product->name_en = $data['name_en']?? null;
            $product->name_kk = $data['name_kk']?? null;
            $product->name_ru = $data['name_ru']?? null;

            $product->barcode = $data['barcode'];
            $product->legal_name = $data['legal_name'];

            $product->is_enabled = $data['is_enabled'] ?? true;
            $product->is_moderated = $data['is_moderated']; // auth()->user()->hasRole(['super-admin', 'product-moderator']) ? true : false

            $product->image_id = $data['image_id'] ?? null;
            $product->background_image_id = $data['background_image_id'] ?? null;

            $product->common_info = $data['common_info'];

            $product->save();
        } catch (\Exception $e) {

            log_e('Product was not saved! Error while storing product' . PHP_EOL, [$e->getMessage()]);

            return null;
        }

        return $product;
    }


    public function update( int $id, array $data )
    {
        try {

            /*** @var Product $product */
            $product = $this->findById($id);

            $product->product_type_id = $data['product_type_id'] ?? null;
            $product->organization_id = $data['organization_id'] ?? null;
            $product->barcode = $data['barcode'];

            /** Изменять название может только супер администратор
             * @see http://docs.wiponapp.com:8090/pages/viewpage.action?pageId=5931459
             **/
            if ($this->userIsSuperAdmin()) {
                $product->name_en = $data['name_en'];
                $product->name_kk = $data['name_kk'];
                $product->name_ru = $data['name_ru'];
                $product->legal_name = $data['legal_name'];
            }

            $product->is_enabled = $data['is_enabled'] && $data['is_enabled'] === 'true' ? (bool) $data['is_enabled'] : false;
            $product->is_moderated = $data['is_moderated'] && $data['is_moderated'] === 'true' ? (bool) $data['is_moderated'] : false;

            $product->image_id = $data['image_id'] ?? $product->image->id ?? null;
            $product->background_image_id = $data['background_image_id'] ?? $product->background_image->id ?? null;

            $product->common_info = $data['common_info'] ?? $product->common_info;
            $product->save();
        } catch (\Exception $e) {

            log_e('Product was not saved! Error while updating product' . PHP_EOL, [$e->getMessage()]);

            return null;
        }

        return $product;
    }

    public function setStatus( int $id, array $data, $field )
    {

        /*** @var Product $product */
        $product = $this->findById($id);
        $product->{$field} = isset($data[ $field ]) && $data[ $field ] === 'true' ? true : false;
        $product->save();

        return $product;
    }


    public function getAll()
    {
        $model = $this->getModelClass();

        return $model::filterAccess();
    }


    public function filter( $params )
    {
        $model = $this->getModelClass();
        $builder = $model::query();

        $this->applySearchForMerging($params, $builder);

        $this->applySearch($params, $builder);

        $this->applyModeration($params, $builder);

        $this->setOrder($params, $builder);

        return $builder->filterAccess();
    }

    /**
     * This is for merging products
     *
     * @param $filterData
     * @param $builder
     */
    private function applySearchForMerging( $filterData, $builder )
    {
        /*  Когда выбирается родитель при объединении продуктов -
          |
          | См. передача параметров в VueJS компонент: 
          |     resources/assets/js/vue/views/products/vm-merge.vue:16
          | См. пример определения метода вызываемого для передачи параметра:   
          |     resources/assets/js/vue/views/products/vm-merge.vue:169 methods.excludeProducts  
          | Cм. вызов переданного метода: 
          |     resources/assets/js/vue/components/v-select.vue:94 methods.initSelect
        */
        if ( ! isset($filterData['params']) || ! is_json($filterData['params'])) {
            return;
        }

        try {

            $productId = json_decode($filterData['params'])[0];

            // Не включая продукты с ID
            if (isset($productId)) {
                $builder->where('id', '!=', $productId);
            }

            //  С именем
            if (array_has($filterData, 'search') && array_get($filterData, 'search') != '') {
                $builder->withName(array_get($filterData, 'search'));
            }
        } catch (\Exception $e) {
            log_e(__CLASS__ . ': ' . __METHOD__ . ' -  Error while transforming search params. ', [$e->getMessage()]);
        }
    }

    /**
     * @param array - $params
     * @param Builder - $builder
     * @return mixed
     */
    private function applySearch( $params, $builder )
    {
        if ( ! array_has($params, 'searchByName') && ! array_has($params, 'searchByOrganization')) {
            return;
        }

        if (array_has($params, 'searchByName') && array_get($params, 'searchByName') !== '') {
            /** @noinspection PhpUndefinedMethodInspection
             * scopeWithName - > Only for models which extends Wipon\Models/I18nModel */
            $builder->withName(array_get($params, 'searchByName'));
        }

        if (array_has($params, 'searchByOrganization') && array_get($params, 'searchByOrganization') !== '') {
            $name = str_for_search(array_get($params, 'searchByOrganization'));
            $builder->whereHas('organization', function ( $query ) use ( $name ) {
                $query->where(function ( $q ) use ( $name ) {
                    $q->where('name_ru', 'ilike', $name);
                    $q->orWhere('name_en', 'ilike', $name);
                    $q->orWhere('name_ru', 'ilike', $name);
                });
            });
        }
    }


    /**
     * @param array $params
     * @param Builder $builder
     */
    private function setOrder( array $params, Builder $builder )
    {
        if (array_has($params, 'sort.name') && array_has($params, 'sort.direction')) {
            $builder->orderBy(array_get($params, 'sort.name'), array_get($params, 'sort.direction'));
        } else {
            $builder->orderBy('name_' . current_locale(), 'asc');
        }
        $builder->orderBy('id');
    }

    /**
     * @param $params
     * @param Builder $builder
     * @return void
     */
    private function applyModeration( $params, Builder $builder )
    {
        $use = array_get($params, 'radios.is_moderated.use');
        $value = array_get($params, 'radios.is_moderated.selected');

        if ($use != true) {
            return;
        }

        if ($value === 'is_moderated') {
            $builder->whereIsModerated(true);
        }

        if ($value === 'is_not_moderated') {
            $builder->whereIsModerated(false);
        }
    }

    /** Requirement http://jira.wiponapp.com:8090/pages/viewpage.action?pageId=6684899
     *
     * @param int $masterId
     * @param array $slavesIds
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Error
     */
    public function merge( int $masterId, array $slavesIds )
    {
        if ( ! $this->userIsSuperAdmin()) {
            log_w('Cant merge items only super-admin can. User: ' . auth()->user()->name);
            throw new \Error('Cant merge items only super-admin can. User: ' . auth()->user()->name);
        }

        $model = $this->getModelClass();

        /*** @var \Illuminate\Database\Eloquent\Collection|| Product[] $slaves */
        $slaves = $model::whereIn('id', $slavesIds)->get();

        /*** @var Product $master */
        $master = $this->findById($masterId);

        $firstLegalMame = null;

        /*** @var Product $slave */
        foreach ($slaves as $slave) {

            if ( ! $firstLegalMame) {
                $firstLegalMame = $this->setFirstLegalName($slave, $firstLegalMame);
            }

            // если у продукта есть признаки подлиности - переходим к следующему продукту,
            // и пишем в логи что была попытка объеденить продукт с продуктом с визуальными чертами
            if ($slave->visual_features()->count() > 0) {
                log_w('Trying to merge products with visual features!!! Current-user: ' . auth()->user()->name);
                continue;
            }

            // перенос псевдонимов
            // берём все имена name_ru name_kz name_en
            // делаем их них массив добавляя в них альясы
            $aliases = $this->getEntityAliases($slave);

            if ( ! empty($slave->legal_name))
                array_push($aliases, $slave->legal_name);

            $this->updateMasterAliases($aliases, $slave, $master);

            // Все Items «продукта раба» преходят к «мастер продукт»
            $this->redefineProductItems($slave->items, $master->id);

            // удалить продукт
            $slave->forceDelete();
        }

        if ( ! empty($firstLegalMame)) {
            $this->updateMasterLegalName($master, $firstLegalMame);
        }

        return $master;
    }

    /** @link http://jira.wiponapp.com:8090/pages/viewpage.action?pageId=6684899
     *
     * @param $slaveItems
     * @param int $masterId
     */
    private function redefineProductItems( $slaveItems, int $masterId )
    {
        foreach ($slaveItems as $item) {
            $item->product_id = $masterId;
            $item->save();
        }
    }

    /** @link http://jira.wiponapp.com:8090/pages/viewpage.action?pageId=6684899
     *
     * @param $master
     * @param $firstLegalMame
     */
    private function updateMasterLegalName( $master, $firstLegalMame )
    {
        // legal_name «мастер продукта» делаем  псевдонимом «мастер продукта»
        if ( ! empty($master->legal_name))
            $master->aliases()->create(['name' => $master->legal_name]);

        // сохраняем перый попавшийся legal_name
        $master->legal_name = $firstLegalMame;
        $master->save();
    }

    /**
     * @param $slave
     * @param $firstLegalMame
     * @return mixed
     */
    private function setFirstLegalName( $slave, $firstLegalMame )
    {
        if ( ! empty($slave->legal_name)) {
            $firstLegalMame = $slave->legal_name;
        }

        return $firstLegalMame;
    }

}