<?php
namespace Wipon\Repos;


use Wipon\Models\VisualFeature;

class VisualFeatureRepository extends EloquentRepository
{

    protected $modelName = 'VisualFeature';

    /**
     * @param array $data
     * @return mixed|VisualFeature
     */
    public function create( array $data )
    {
     
        $feature = $this->getModel();

        $feature->product_id = $data['product_id'];
        $feature->image_id = (int)$data['image_id'];

        $feature->fake_image_id = $data['fake_image_id'] ?? null;
        $feature->description_ru = $data['description_ru'];
        $feature->description_en = $data['description_en'];
        $feature->description_kk = $data['description_kk'] ?? null;

        $feature->order_number = $data['order_number'];

        $feature->save();

        return $feature;
    }

    /** Update
     * @param int $id
     * @param array $data
     * @return VisualFeature
     */
    public function update( int $id, array $data )
    {
        /*** @var VisualFeature $feature */
        $feature = $this->findById($id);

        $feature->product_id = $data['product_id'] ??  $feature->product_id;
        $feature->image_id = $data['image_id'] ??  $feature->image_id;

        $feature->fake_image_id = $data['fake_image_id'] ??  $feature->fake_image_id;
        $feature->description_ru = $data['description_ru'] ??  $feature->description_ru;
        $feature->description_en = $data['description_en'] ??  $feature->description_en;
        $feature->description_kk = $data['description_kk'] ??  $feature->description_kk;

        $feature->order_number = $data['order_number'] ?? $feature->order_number;
        $feature->save();

        return $feature;
    }

    /**  
     * Delete item
     * 
     * @param int $id
     * @return bool|null
     * @throws \Exception
     */
    public function delete( $id )
    {
        /*** @var VisualFeature $featureToDelete */
        $featureToDelete = $this->findById($id);

        $features = $this->getModel()->where('product_id', $featureToDelete->product_id)->where('order_number', '>', $featureToDelete)->orderBy('order_number'.'asc')->get();

        foreach ($features  as $feature){
            /*** @var VisualFeature $feature */
            $order = $feature->order_number;
            $order++ ;
            $feature->order_number = $order;

            $feature->save();
        }

        return $featureToDelete->delete();
    }

    /** Set new order to feature
     * @param int $featureId
     * @param int $new_order_number
     * @return bool
     */
    public function setOrder( int $featureId, int $new_order_number )
    {
        /*** @var VisualFeature $featureToMove */
        $featureToMove = $this->findById($featureId);

        $old_order_number = $featureToMove->order_number;
        
        $features = $this->getModel()->where('product_id', $featureToMove->product_id)->get();
        
        if (count($features)===0) {
            log_e(__METHOD__."Can't find features with such product_id: {$featureToMove->product_id}");
            return false;
        }
        
        foreach ($features as $feature){
            /*** @var VisualFeature $feature */
            if ($feature->order_number === $new_order_number && $feature->id !== $featureToMove->id){
                $feature->order_number = $old_order_number;
                $feature->save();
                continue;
            }

            if ($feature->order_number === $old_order_number && $feature->id === $featureToMove->id){
                $feature->order_number = $new_order_number;
                $feature->save();
                continue;
            }
        }
        
        return true;
    }
}