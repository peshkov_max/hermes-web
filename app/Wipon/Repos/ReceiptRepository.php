<?php

namespace Wipon\Repos;

use App\Jobs\PushNotifications\SendPushSingle;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Validator;
use Wipon\Models\Device;
use Wipon\Models\Receipt;

class ReceiptRepository extends EloquentRepository
{
    protected $modelName = 'Receipt';

    public function update( int $id, array $data )
    {
        // TODO: Implement update() method.
    }

    /**
     * Фильтр для страницы "Реестр чеков"
     *
     * @param array $params
     * @return mixed
     */
    public function filter( array $params = [] ) 
    {
        $builder = $this->getModel()
            ->selectRaw(DB::raw(' 
             receipts.id, 
             receipts.device_uuid, 
             receipts.bin, 
             receipts.sum, 
             receipts.status, 
             receipts.photo, 
             receipts.created_at,
             regions.name_ru as region_name, 
             customers.name as member_name,
             customers.email as member_email, 
             customers.phone_number as member_phone_number'))
            ->join('devices', 'devices.uuid', '=', 'receipts.device_uuid')
            ->join('customers', 'customers.id', '=', 'devices.customer_id')
            ->join('regions', 'receipts.region_id', '=', 'regions.id');
        
        $this->searchById($params, $builder);

        $this->searchByUUID($params, $builder);

        $this->filterByRegion($params, $builder);

        $this->filterStatuses($params, $builder);

        $this->filterByPeriod($params, $builder);

        $this->setOrdering($params, $builder);

        return $builder;
    }

    /** 
     * Фильтр для страницы "Розыгрыш чеков"
     * 
     * @param array $params
     * @return mixed
     */
    public function playPrizeFilter( array $params )
    {
        $builder = $this->getModel()
            ->selectRaw(DB::raw('
            receipts.id, 
            receipts.device_uuid, 
            receipts.bin, 
            receipts.sum, 
            receipts.status, 
            receipts.photo, 
            receipts.created_at, 
            regions.name_ru as region_name, 
            customers.name as member_name, 
            customers.phone_number as member_phone_number, 
            row_number() over (order by receipts.created_at) as row_number'))
            ->join('devices', 'devices.uuid', '=', 'receipts.device_uuid')
            ->join('customers', 'customers.id', '=', 'devices.customer_id')
            ->join('regions', 'receipts.region_id', '=', 'regions.id');

        // нам нужны чеки со статусами "Участник" и "Не фискальный"
        $builder->where(function ( $q ) {
            $q->orWhere('receipts.status', 'member');
            $q->orWhere('receipts.status', 'not-fiscal');
        });
        
        $this->filterByRegion($params, $builder);

        $this->filterByPeriod($params, $builder);

        if ($this->findingWinner($params)) {
            $builder = $this->findWinner($params, $builder);
        }
        
        $builder->orderBy('row_number', 'asc');    
        
        return $builder;
    }

    /**
     * Были ли переданы параметры статуса для фильтрации
     *
     * @param $params
     * @return bool
     */
    private function canBeFilteredByStatus( $params )
    {
        $rules = [
            'statuses.processing'         => 'required|in:true,false',
            'statuses.empty_bin'          => 'required|in:true,false',
            'statuses.invalid_date'       => 'required|in:true,false',
            'statuses.empty_sum'          => 'required|in:true,false',
            'statuses.not_fiscal'         => 'required|in:true,false',
            'statuses.duplicate'          => 'required|in:true,false',
            'statuses.empty_date_time'    => 'required|in:true,false',
            'statuses.blurred'            => 'required|in:true,false',
            'statuses.member'             => 'required|in:true,false',
            'statuses.winner'             => 'required|in:true,false',
            'statuses.doesnt_match_terms' => 'required|in:true,false',
            'onlyWinners'                 => 'in:true,false',
        ];

        if (array_get($params, 'regionId') != 'all')
            $rules['regionId'] = 'required|exists:regions,id';

        $v = Validator::make($params, $rules);

        if ($v->fails()) {
            log_w('Params for ReceiptRepository filter method are wrong');

            return false;
        }

        return true;
    }

    /**
     * Проверяет есть ли статусы в параметрах
     *
     * @param $params
     * @return bool
     */
    private function isEmpty( $params )
    {
        return ! filter_var(array_get($params, 'statuses.processing'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.invalid_date'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.empty_bin'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.not_fiscal'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.empty_date_time'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.duplicate'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.empty_sum'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.blurred'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.winner'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.doesnt_match_terms'), FILTER_VALIDATE_BOOLEAN)
        && ! filter_var(array_get($params, 'statuses.member'), FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * Правильный ли параметры сортировки
     *
     * @param array $params
     * @return bool
     */
    private function sortParamsAreValid( array $params )
    {
        return isset($params['sort']['direction']) && in_array($params['sort']['name'], ['created_at', 'id', 'member_name', 'member_phone']);
    }

    /**
     * Фильтрация статусов
     *
     * @param array $params
     * @param $builder
     */
    protected function filterStatuses( array $params, $builder )
    {
        if ($this->isEmpty($params)) {

            $builder->where('receipts.status', 'unknown');
            
        } elseif ($this->canBeFilteredByStatus($params)) {

            $builder->where(function ( $q ) use ( $params ) {

                foreach ($params['statuses'] as $status => $bool) {

                    // перевод от вида empty_sum(формат используется в локализации, и при передаче параметров)
                    //          к виду empty-sum(используется при хранении в БД)
                    $status = implode('-', preg_split('/[_,]+/', $status));

                    if (filter_var($bool, FILTER_VALIDATE_BOOLEAN))
                        $q->orWhere('receipts.status', $status);
                }
            });
        }
    }

    /**
     * Сортировка
     *
     * @param array $params
     * @param $builder
     */
    protected function setOrdering( array $params, $builder )
    {
        if ($this->sortParamsAreValid($params))
            $builder->orderBy('receipts.'.$params['sort']['name'], $params['sort']['direction']);
        else
            $builder->orderBy('receipts.created_at', 'desc');

        $builder->orderBy('receipts.id', 'desc');
    }

    /**
     * Поиск по ID
     *
     * @param array $params
     * @param $builder
     */
    protected function searchById( array $params, $builder )
    {
        if ( ! array_has($params, 'search') || array_get($params, 'search') === '') {
            return;
        }



        $builder->where('receipts.id', '=', (int) array_get($params, 'search'));
    }

    /**
     * Поиск по региону
     *
     * @param array $params
     * @param $builder
     */
    protected function filterByRegion( array $params, $builder )
    {
        if (array_get($params, 'regionId') == 'all') {
            return;
        }

        if (array_has($params, 'regionId') && array_get($params, 'regionId') != '') {
            $builder->where('receipts.region_id',(int) array_get($params, 'regionId'));

            return;
        }

        $builder->where('receipts.region_id', (new RegionRepository())->getDefaultRegion()->id);
    }

    /**
     * Поиск за период
     *
     * @param array $params
     * @param $builder
     */
    protected function filterByPeriod( array $params, $builder )
    {
        if (array_has($params, 'period') && array_has($params, 'period.start') && array_has($params, 'period.end')) {
            $builder->whereBetween('receipts.created_at', $this->getPeriods($params, config('wipon.dt_format_d_m_y_h_i')));
        }
    }

    /**
     * Статистика по чекам
     *
     * @param array $params
     * @return array
     */
    public function getStatistics( array $params = [] )
    {
        /**
         * we need array - not stdClass
         */
        DB::setFetchMode(\PDO::FETCH_ASSOC);

        $total = trans('app.total');

        DB::enableQueryLog();

        $queryResult = DB::select(DB::raw(
            "with table_content as 
                (
                    select * from (
                        SELECT
                             q3.region_name  as region_name,
                             SUM(january) OVER (PARTITION BY q3.region_id) AS january,
                             SUM(february) OVER (PARTITION BY q3.region_id) AS february,
                             SUM(march) OVER (PARTITION BY q3.region_id) AS march,
                             SUM(april) OVER (PARTITION BY q3.region_id) AS april,
                             SUM(may) OVER (PARTITION BY q3.region_id) AS may,
                             SUM(june) OVER (PARTITION BY q3.region_id) AS june,
                             SUM(july) OVER (PARTITION BY q3.region_id) AS july,
                             SUM(august) OVER (PARTITION BY q3.region_id) AS august,
                             SUM(september) OVER (PARTITION BY q3.region_id) AS september,
                             SUM(october) OVER (PARTITION BY q3.region_id) AS october,
                             SUM(november) OVER (PARTITION BY q3.region_id) AS november,
                             SUM(december) OVER (PARTITION BY q3.region_id) AS december,
                             SUM(total) OVER (PARTITION BY q3.region_id) AS total,
                             row_number() OVER (PARTITION BY q3.region_id) AS rank_region_id
                            from (
                                  SELECT
                                        q2.region_name as region_name,
                                        q2.month,
                                        q2.region_id,
                                        SUM(CASE WHEN q2.month = 1 THEN q2.per_region_month ELSE 0 END) AS january,
                                        SUM(CASE WHEN q2.month = 2 THEN q2.per_region_month ELSE 0 END) AS february,
                                        SUM(CASE WHEN q2.month = 3 THEN q2.per_region_month ELSE 0 END) AS march,
                                        SUM(CASE WHEN q2.month = 4 THEN q2.per_region_month ELSE 0 END) AS april,
                                        SUM(CASE WHEN q2.month = 5 THEN q2.per_region_month ELSE 0 END) AS may,
                                        SUM(CASE WHEN q2.month = 6 THEN q2.per_region_month ELSE 0 END) AS june,
                                        SUM(CASE WHEN q2.month = 7 THEN q2.per_region_month ELSE 0 END) AS july,
                                        SUM(CASE WHEN q2.month = 8 THEN q2.per_region_month ELSE 0 END) AS august,
                                        SUM(CASE WHEN q2.month = 9 THEN q2.per_region_month ELSE 0 END) AS september,
                                        SUM(CASE WHEN q2.month = 10 THEN q2.per_region_month ELSE 0 END) AS october,
                                        SUM(CASE WHEN q2.month = 11 THEN q2.per_region_month ELSE 0 END) AS november,
                                        SUM(CASE WHEN q2.month = 12 THEN q2.per_region_month ELSE 0 END) AS december,
                                        SUM(q2.per_region_month) AS total
                                  FROM (
                                         SELECT *
                                         FROM (
                                                SELECT
                                                  regions.name_ru as region_name,
                                                  receipts.region_id AS region_id,
                                                  EXTRACT(MONTH FROM receipts.created_at) as month,
                                                  EXTRACT(YEAR FROM receipts.created_at) as year,
                                                  count(*) OVER (PARTITION BY EXTRACT(MONTH FROM receipts.created_at), receipts.region_id) AS per_region_month,
                                                  row_number() OVER (PARTITION BY EXTRACT(MONTH FROM receipts.created_at), receipts.region_id ORDER BY receipts.created_at DESC) AS rank_region_id
                                                FROM receipts
                                                  JOIN regions on regions.id = receipts.region_id and regions.country_id = 45
                                                  {$this->getReceiptStatusCondition($params)}
                                              ) AS q1
                                          WHERE q1.rank_region_id = 1 and {$this->getYearCondition($params)}
                                        ) as q2
                                  group by q2.month, q2.region_id, q2.region_name
                                ) as q3
        
                           group by q3.month,  q3.region_name, q3.january, q3.february, q3.march, q3.april, 
                                    q3.may, q3.june, q3.july, q3.september, q3.november, q3.december, q3.august, 
                                    q3.october, q3.total, q3.region_id) as q4
                    where q4.rank_region_id=1
                    order by {$this->getOrdering($params)}
                ) 
                  
                select * from table_content
                union all
                select 
                  '{$total}',
                  sum(january),
                  sum(february),
                  sum(march),
                  sum(april),
                  sum(may),
                  sum(june),
                  sum(july),
                  sum(august),
                  sum(september),
                  sum(october),
                  sum(november),
                  sum(december),
                  sum(total), 
                  count(*) 
                from table_content;"));

        DB::setFetchMode(\PDO::FETCH_CLASS);

        return count($queryResult) > 1 ? $queryResult : [];
    }

    /**
     * Фильтрация статистики - установка года
     *
     * @param array $params
     * @return string
     */
    private function getYearCondition( array $params = [] )
    {
        if ( ! array_has($params, 'year'))
            return 'q1.year=2016';

        $year = array_get($params, 'year');

        return "q1.year={$year}";
    }

    /**
     * Установка условий выбора статусов при формировании статистики по чекам
     *
     * @param array $params
     * @return string
     */
    private function getReceiptStatusCondition( array $params = [] )
    {
        if ( ! array_has($params, 'statuses'))
            return '';

        return "where {$this->getStatuses(array_get($params, 'statuses'))}";
    }

    /**
     * Получение списка статусов для условия
     *
     * @param array $statuses
     * @return mixed
     */
    private function getStatuses( array $statuses = [] )
    {
        $stack = [];
        foreach ($statuses as $key => $value) {

            // перевод от вида empty_sum(формат используется в локализации, и при передаче параметров)
            //          к виду empty-sum(используется при хранении в БД)
            $key = implode('-', preg_split('/[_,]+/', $key));

            if (filter_var($value, FILTER_VALIDATE_BOOLEAN))
                $stack[ $key ] = "receipts.status = '$key'";
        }

        return implode(' or ', $stack);
    }

    /**
     * Установка сортировки при формировании статистики чеков
     *
     * @param array $params
     * @return string
     */
    private function getOrdering( array $params = [] )
    {
        if ( ! array_has($params, 'sort') || ! array_has($params, 'sort.name') || ! array_has($params, 'sort.direction'))
            return 'q4.region_name desc';

        return "q4.{$this->getSortByName($params)} {$this->getSortDirection($params)}";
    }

    /**
     * Проверка является переданное в параметрах имя для
     * сортировки одним из полей выбираемых их базы данных
     *
     * @param array $params
     * @return string
     */
    private function getSortByName( array $params )
    {
        $sortBy = array_get($params, 'sort.name');

        return in_array($sortBy, [
            'region_name',
            'january',
            'february',
            'march',
            'april',
            'may',
            'june',
            'july',
            'august',
            'september',
            'october',
            'november',
            'december',
            'total',
        ]) ? $sortBy : 'region_name';
    }

    /**
     * Получение порядка сортировки
     *
     * @param array $params
     * @return string
     */
    private function getSortDirection( array $params = [] )
    {
        $direction = array_get($params, 'sort.direction');

        return in_array($direction, ['asc', 'desc']) ? $direction : 'desc';
    }

    /** 
     * Поиск по UUID устройства 
     * 
     * @param $params
     * @param $builder
     */
    private function searchByUUID( $params, $builder )
    {
        if (array_has($params, 'deviceUUID') && array_get($params, 'deviceUUID') != '')
        {
            $builder->where('receipts.device_uuid', '=', trim(array_get($params, 'deviceUUID')));
        }
    }

    /**
     * Получение списка участников
     *
     * @param string $regionId
     * @return $this|Builder|static
     */
    public function getParticipants( $regionId = 'all' )
    {
        try {

            $conditions = [];

            if ($regionId !== 'all') {
                $v = Validator::make(['region_id' => $regionId], ['region_id' => 'required|exists:regions,id']);

                if ( ! $v->fails()) {
                    $conditions [] = ['regions.id', '=', $regionId];
                } else {
                    log_w(__CLASS__ . ':' . __METHOD__ . ' Wrong region id: ' . $regionId);
                }
            }

            $builder = Device::select(
                \DB::raw("max(devices.uuid) as uuid, 
                      count(devices.uuid) as receipts_count, 
                      max(customers.name) as member_name,
                      max(customers.phone_number) as member_phone_number, 
                      max(regions.name_ru) as region_name"))
                ->join('receipts', 'receipts.device_uuid', '=', 'devices.uuid')
                ->join('regions', 'regions.id', '=', 'receipts.region_id')
                ->join('customers', 'customers.id', '=', 'devices.customer_id')
                ->where($conditions)
                ->groupBy('receipts.device_uuid')
                ->groupBy('regions.id')
                ->orderBy('receipts_count', 'desc');

            return $builder;
        } catch (\Exception $e) {
            log_e([$e->getMessage()]);

            return Device::whereNull('uuid');
        }
    }


    /**
     * Установка статуса чека
     *
     * @param int $id
     * @param string $status
     * @return bool
     */
    public function setStatus( $id = 0, $status = 'processing' )
    {
        try {
            /**
             * @var Receipt $receipt
             */
            $receipt = $this->findById($id);
            $receipt->status = $status;
            $receipt->save();

            log_i("Status {$status} was set for ReceiptID: {$id}");
        } catch (\Exception $e) {

            log_e('Error while setting receipt status', [$e->getMessage()]);

            return false;
        }

        return true;
    }

    /**
     * Ищем ли мы сейчас победителя
     *
     * @param $params
     * @return bool
     */
    private function findingWinner( $params )
    {
        return array_has($params, 'search') && array_get($params, 'search') != '';
    }

    /**
     * Поиск победителя по номеру чека в общем списке чеков
     *
     * @param array $params
     * @param $builder
     * @return mixed
     */
    private function findWinner( array $params, $builder )
    {
        /*
         | We need to perform the query 
         |
         | SELECT *
         |   FROM
         |     (SELECT receipts.*,
         |             row_number() over ( ORDER BY receipts.created_at) AS row_number
         |      FROM "receipts"
         |      WHERE ("status" = 'member' OR "status" = 'not-fiscal')
         |        AND "created_at" BETWEEN '2016-09-20 08:38:00' AND '2016-10-20 09:38:00'
         |      ORDER BY "created_at" ASC, "id")AS dd
         |   WHERE "row_number" = 1
         * */
        $builder = $this->getModel()
            ->select('*')
            ->from(DB::raw('( ' . $builder->toSql() . ' ) AS sub_query'))
            ->mergeBindings($builder->getQuery())
            ->where("sub_query.row_number", (int) array_get($params, 'search'));

        return $builder;
    }

    /**
     * Отправка всем устройствам пользователя уведомления о выигрыше
     *
     * @param string $uuid
     * @param string $msg
     */
    public function notifyWinner( string $uuid, string $msg = 'You are a winner!!' )
    {
        try {

            foreach (Device::find($uuid)->customer->devices as $device) {
                dispatch((
                new SendPushSingle([
                    'uuid' => $device->uuid,
                    'message' => $msg
                ]))->onQueue('cabinet_push'));
            }
        } catch (\Exception $e) {
            log_e('Error while pushing push notification to queue', [$e->getMessage()]);
        }
    }
}