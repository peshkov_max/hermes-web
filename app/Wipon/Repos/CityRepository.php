<?php

namespace Wipon\Repos;

use Wipon\Models\City;

class CityRepository extends EloquentRepository
{

    protected $modelName = 'City';

    /** @inheritdoc */
    public function filter(array $filter)
    {
        /** @var \Illuminate\Database\Query\Builder $query */
        $query = City::select('cities.*')->with('region.country');

        $searchCity = array_get($filter, 'search.name.value');

        if (! empty($searchCity)) {
            $query = $query->where(function ($q) use ($searchCity) {
                /** @var \Illuminate\Database\Query\Builder $q */

                $q->where("cities.name_ru", "ilike", "%$searchCity%")
                    ->orWhere("cities.name_en", "ilike", "%$searchCity%")
                    ->orWhere("cities.name_kk", "ilike", "%$searchCity%");
            });
        }
        
        if (filter_var(array_get($filter, 'checkboxes.kz_cities_only.checked'), FILTER_VALIDATE_BOOLEAN)) {
            $query = $query->join("regions", "regions.id", "=", "cities.region_id", "left")
                ->join("countries", "countries.id", "=", "regions.country_id", "left")
                ->where("countries.name_en", "Kazakhstan");
            
        }
        else {
            $searchCountry = array_get($filter, 'search.country.value');

            if (! empty($searchCountry)) {
                $query = $query->join("regions", "regions.id", "=", "cities.region_id", "left")
                    ->join("countries", "countries.id", "=", "regions.country_id", "left")
                    ->where(function ($q) use ($searchCountry) {
                        /** @var \Illuminate\Database\Query\Builder $q */

                        $q->where("countries.name_ru", "ilike", "%$searchCountry%")
                            ->orWhere("countries.name_en", "ilike", "%$searchCountry%")
                            ->orWhere("countries.name_kk", "ilike", "%$searchCountry%");
                    });
            }
        }

        $searchRegion = array_get($filter, 'search.region.value');

        if (! empty($searchRegion)) {
            if (! $this->joinExists($query, 'regions')) {
                $query = $query->join("regions", "regions.id", "=", "cities.region_id", "left");
            }

            $query = $query->where(function ($q) use ($searchRegion) {
                /** @var \Illuminate\Database\Query\Builder $q */

                $q->where("regions.name_ru", "ilike", "%$searchRegion%")
                    ->orWhere("regions.name_en", "ilike", "%$searchRegion%")
                    ->orWhere("regions.name_kk", "ilike", "%$searchRegion%");
            });
        }

        switch (array_get($filter, 'sort.name', 'id')) {
            case 'city':
                $query = $query->orderBy('cities.name_ru', array_get($filter, 'sort.direction', 'asc'));
                break;
            default:
                $query = $query->orderBy('cities.id', array_get($filter, 'sort.direction', 'asc'));
        }

        if (filter_var(array_get($filter, 'checkboxes.regional_centers_only.checked'), FILTER_VALIDATE_BOOLEAN)) {
            $query = $query->where('cities.is_regional_center', true);
        }

        if (filter_var(array_get($filter, 'checkboxes.without_coords_only.checked'), FILTER_VALIDATE_BOOLEAN)) {
            $query = $query->whereNull('cities.latitude')->whereNull('cities.longitude');
        }

        if (filter_var(array_get($filter, 'checkboxes.without_timezones_only.checked'), FILTER_VALIDATE_BOOLEAN)) {
            $query = $query->whereNull('cities.timezone');
        }

        if (filter_var(array_get($filter, 'checkboxes.without_regions_only.checked'), FILTER_VALIDATE_BOOLEAN)) {
            $query = $query->whereNull('cities.region_id');
        }

        return $query;
    }


    public function update(int $id , array $data ) : City
    {
        $this->getModel()->whereId($id)->update($data);

        return $this->findById($id);
    }

    public function searchByName(string $querySearch ) 
    {
        $model = $this->getModelClass();
        
        /** @noinspection PhpUndefinedMethodInspection
         * scopeWithName - > Only for models which extends Wipon\Models/I18nModel */
        return  $model::withName($querySearch)->orderBy('name_'.current_locale());
    }
}