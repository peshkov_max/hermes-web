<?php
namespace Wipon\Repos;

use Wipon\Models\FeedbackMessage;

class SupportRepository extends EloquentRepository
{
    protected $modelName = 'FeedbackMessage';

    public function update( int $id, array $data ) : FeedbackMessage
    {
        $this->getModel()->whereId($id)->update($data);

        return $this->findById($id);
    }

    public function filter( array $params )
    {
        /**
         * @var $model \Eloquent
         */
        $builder = $this->getQueryBuilder();

        $builder->select('device_uuid', 'name', 'phone_number', 'email', 'text', 'created_at', 'read_at', 'id');

        $this->searchFilters($params, $builder);

        $this->applyReadability($params, $builder);
        $this->applySourceType($params, $builder);

        if ($this->sortParamsAreValid($params)) {
            $builder->orderBy($params['sort']['name'] === 'sent_at' ? 'created_at' : $params['sort']['name'], $params['sort']['direction']);
        };

        return $builder->with('device.customer');
    }

    public function read( int $id, bool $setUnRead = false )
    {
        $message = $this->findById($id);

        $message->read_at = $setUnRead === false ? carbon_user_tz_now() : null;

        if ($message->save())
            return true;

        return false;
    }

    /**
     * @param array $params
     * @param \Illuminate\Database\Query\Builder $builder
     */
    protected function searchFilters( array $params, $builder )
    {
        if (array_has($params, 'search') && ! empty(array_get($params, 'search'))) {
            
            $query = '%' . trim($params['search']) . '%';
            $builder->where(function ( $q ) use ( $query ) {
                /*** @var \Eloquent $query */
                $q->where("device_uuid", 'ilike', $query)
                    ->orWhere("email", 'ilike', $query)
                    ->orWhere("name", 'ilike', $query);
            });
        }

        if (array_has($params, 'platform') && ! empty(array_get($params, 'platform'))) {
            
            $query = '%' . trim($params['platform']) . '%';
            $builder->whereHas("device", function ( $q ) use ( $query ) {
                $q->where('platform', 'ilike', $query);
            });
        }
    }

    /**
     * @param array $params
     * @return bool
     */
    protected function sortParamsAreValid( array $params )
    {
        return isset($params['sort']['direction']) && in_array($params['sort']['name'], ['name', 'email', 'sent_at', 'read_at']);
    }


    public function getOtherDeviceMessages( $id, $device_uuid )
    {
        return $this->getQueryBuilder()->where('device_uuid', $device_uuid)->where('id', '!=', $id);
    }

    /**
     * @return \Illuminate\Database\Query\Builder
     */
    protected function getQueryBuilder()
    {
        /**
         * @var $model \Eloquent
         */
        $model = $this->getModelClass();

        /*** @var \Illuminate\Database\Query\Builder $builder */
        $builder = $model::query();

        return $builder;
    }

    private function applyReadability( $params, $builder )
    {
        $use = array_get($params, 'radios.readUnread.use');
        $was_read = array_get($params, 'radios.readUnread.selected');
       
        if ($use != true) {
            return;
        }

        if ($was_read == 'read') {
            $builder->whereNotNull('read_at');
        }

        if ($was_read == 'unread') {
            $builder->whereNull('read_at');
        }
    }

    private function applySourceType( $params, $builder )
    {
        $use = array_get($params, 'radios.mobileWeb.use');
        $source_type = array_get($params, 'radios.mobileWeb.selected');

        if ($use != true) {
            return;
        }

        if ($source_type == 'mobile') {
            $builder->whereNotNull('device_uuid');
        }

        if ($source_type == 'web') {
            $builder->whereNull('device_uuid');
        }
    }
}