<?php

namespace App\Jobs\PushNotifications;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Wipon\Models\Device;
use Wipon\Models\Pro\User;
use Wipon\Traits\LifeTimeCounterTrait;

class SendPushMass extends Job implements ShouldQueue
{
    use InteractsWithQueue, LifeTimeCounterTrait {
        LifeTimeCounterTrait::__construct as startClassLifeTimeCounting;
        LifeTimeCounterTrait::__destruct as endClassLifeTimeCounting;
    }

    /**
     * @var array
     */
    private $params;

    /**
     * Create a new job instance.
     *
     * @param array $params
     */
    public function __construct( array $params )
    {
        $this->params = $params;
        $this->startClassLifeTimeCounting();
    }
    
    public function __destruct()
    {
        $this->endClassLifeTimeCounting();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->params) || $this->attempts() > 3) {
            $this->delete();
        }

        $message = array_get($this->params, 'message', 'Wipon push notification');

        if (array_has($this->params, 'is_pro') && array_get($this->params, 'is_pro') === 'true') {

            $query = $this->getDevicesBuilderWiponPro($this->params);
            
            $query->chunk(config('wipon.push_notification_bunch_length'), function ( $users ) use ( $message ) {
                dispatch((new SendPushToUserBunch($users->pluck('id')->toArray(), $message))->onQueue('cabinet_push'));
            });

        } else {

            $query = $this->getDevicesBuilderWipon($this->params);
            
            $query->chunk(config('wipon.push_notification_bunch_length'), function ( $devices ) use ( $message ) {
                dispatch((new SendPushToDeviceBunch($devices->pluck('uuid')->toArray(), $message))->onQueue('cabinet_push'));
            });
        }

        $this->delete();
    }

    /**
     * Возвращает Query Builder в соответствии с переданными параметрами
     *
     * @param array $params
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function getDevicesBuilderWipon( array $params )
    {
        $query = (new Device())->newQuery();

        // Требуй чек
        if (filter_var(array_get($params, 'receipts_require'), FILTER_VALIDATE_BOOLEAN)) {
            $query->has('receipts');
        }

        // Платформа
        if (array_has($params, 'platform')) {
            $query->where('devices.platform', 'ilike', str_for_search(array_get($params, 'platform', 'android')));
        }

        // Версия
        if (array_has($params, 'versions')) {
            $query->whereIn('devices.app_version', array_get($params, 'versions', []));
        }

        // Регион
        if (array_has($params, 'regions')) {
            $query->whereIn('devices.region_id', array_get($params, 'regions', []));
        }

        return $query;
    }

    private function getDevicesBuilderWiponPro( $params )
    {
        $query = (new User())->newQuery();

        // Платформа
        if (array_has($params, 'platform')) {
            $query->where('devices->platform', 'ilike', str_for_search(array_get($params, 'platform', 'android')));
        }

        // Версия
        if (array_has($params, 'versions') && count(array_get($params, 'versions')) > 0) {
            $query->whereIn('app_version', array_get($params, 'versions', []));
        }

        // Регион
        if (array_has($params, 'regions') && count(array_get($params, 'regions')) > 0) {
            $query->whereIn('devices.region_id', array_get($params, 'regions', []));
        }

        return $query;
    }

    /**
     * Количество устройств, на которые будет отправлено уведомление
     * в соответствии с переданными параметрами
     *
     * @param array $params
     * @return int
     */
    public static function getDeviceNumber( array $params )
    {
        if (array_has($params, 'is_pro') && array_get($params, 'is_pro') === 'true') {
            return (new self($params))->getDevicesBuilderWiponPro($params)->count();
        }

        return (new self($params))->getDevicesBuilderWipon($params)->count();
    }


}
