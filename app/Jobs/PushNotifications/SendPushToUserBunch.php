<?php

namespace App\Jobs\PushNotifications;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Validator;
use Wipon\Models\Pro\User;
use Wipon\Traits\LifeTimeCounterTrait;
use Wipon\Traits\PushNotificationTrait;

class SendPushToUserBunch extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, PushNotificationTrait, LifeTimeCounterTrait {
        LifeTimeCounterTrait::__construct as startClassLifeTimeCounting;
        LifeTimeCounterTrait::__destruct as endClassLifeTimeCounting;
    }


    /**
     *  List of device uuids
     *
     * @var string
     */
    private $user_ids;

    /**
     * @var string
     */
    private $message;

    /**
     * Create a new job instance.
     *
     * @param array $user_ids
     * @param string $message
     */
    public function __construct( array $user_ids, string $message )
    {
        $this->user_ids = $user_ids;
        $this->message = $message;

        $this->startClassLifeTimeCounting();
    }

    function __destruct()
    {
        $this->endClassLifeTimeCounting();
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->message) || empty($this->user_ids) || ! is_array($this->user_ids) || $this->attempts() > 3) {
            log_w('Push notification cant be send. Message or device uuid list is empty');
            $this->delete();

            return;
        }

        $deviceToHandleLength = config('wipon.push_notification_bunch_length');

        if (count($this->user_ids) > $deviceToHandleLength) {
            log_w("Push notification cant be send. SendPushToUserBunch cant handle more than $deviceToHandleLength records");
            $this->delete();

            return;
        }

        $v = Validator::make(['user_ids' => $this->user_ids], [
            'user_ids'   => "required|array",
            'user_ids.*' => "exists:pgsql_pro.users,id",
        ]);

        if ($v->fails()) {
            log_w('Push notification cant be send. Some given uuids does not exists in devices table');
            $this->delete();

            return;
        }

        $this->sendToUserList();

        $this->delete();
    }


    /**
     * Отправка уведомления списку пользователей
     */
    private function sendToUserList()
    {
        $users = User::whereIn('id', $this->user_ids)->withCount(["notifications" => function ( $q ) {
            $q->where("is_unread", true);
        }])->get();

        foreach ($users as $user) {
            $this->sendToUser($user);
        }
    }
}
