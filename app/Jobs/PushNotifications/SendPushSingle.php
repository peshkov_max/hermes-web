<?php

namespace App\Jobs\PushNotifications;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Wipon\Models\Device;
use Wipon\Models\Pro\User;
use Wipon\Traits\LifeTimeCounterTrait;
use Wipon\Traits\PushNotificationTrait;

class SendPushSingle extends Job implements ShouldQueue
{
    use InteractsWithQueue, PushNotificationTrait, LifeTimeCounterTrait {
        LifeTimeCounterTrait::__construct as startClassLifeTimeCounting;
        LifeTimeCounterTrait::__destruct as endClassLifeTimeCounting;
    }


    /**
     * Из таблицы pgsql.core.users
     *
     * [DBconnection].[scheme].[table]
     * @var string - Идентификатор устройства.
     */
    private $uuid;

    /**
     * @var string - Сообщение
     */
    private $message;

    /**
     * Из таблицы pgsql_pro.public.users
     *
     * [DBconnection].[scheme].[table]
     * @var integer - Идентификатор пользователя
     */
    private $user_id;

    /**
     * Из таблицы pgsql_pro.public.users
     *
     * [DBconnection].[scheme].[table]
     * @var integer - Номер телефона пользователя
     */
    private $phone_number;

    /**
     * Create a new job instance.
     *
     * @param array $params
     */
    public function __construct( array $params )
    {
        $this->uuid = array_get($params, 'uuid', null);
        $this->user_id = array_get($params, 'user_id', null);
        $this->phone_number = array_get($params, 'phone_number', null);
        $this->message = array_get($params, 'message', null);

        $this->startClassLifeTimeCounting();
    }

    public function __destruct()
    {
        $this->endClassLifeTimeCounting();
    }

    /**
     * Отправить PUSH-сообщение на устройство по UUID
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->uuid) && empty($this->user_id) && empty($this->phone_number)) {
            log_w('Cant send push notification. Empty uuid, user_id, phone number, so - delete the task.');
            $this->delete();

            return;
        }

        if (empty($this->message)) {
            log_w('Cant send push notification. Empty message, so - delete the task');
            $this->delete();

            return;
        }

        if ($this->attempts() > 3) {
            log_w('Cant send push notification. More than 3 attempts to execute the task, so - delete the task');
            $this->delete();

            return;
        }

        if ( ! is_null($this->uuid)) {
            $this->sendToWiponUser();
        }

        if ( ! is_null($this->user_id) || ! is_null($this->phone_number)) {
            $this->sendToWiponProUser();
        }

        $this->delete();
    }

    /**
     * Отправка уведомлений пользователям проекта Wipon
     */
    private function sendToWiponUser()
    {
        $device = Device::whereUuid($this->uuid)
            ->withCount(["notifications" => function ( $q ) {
                $q->where("is_unread", true);
            }])->first();

        if (is_null($device)) {
            log_w('Cant send push notification! Device not found.');

            return;
        }

        /** @var Device $device */
        $this->sendToDevice($device);
    }

    /** Отправка уведомления пользователю проекта Wipon Pro */
    private function sendToWiponProUser()
    {
        $user = null;

        if ($this->user_id) {

            /** @var User $user */
            $user = User::whereId($this->user_id)
                ->withCount(["notifications" => function ( $q ) {
                    $q->where("is_unread", true);
                }])->first();
        }

        if ($this->phone_number) {

            /** @var User $user */
            $user = User::wherePhoneNumber($this->phone_number)
                ->withCount(["notifications" => function ( $q ) {
                    $q->where("is_unread", true);
                }])->first();
        }

        if (is_null($user)) {
            log_w("Cant send push notification! User not found. Phone Number: {$this->phone_number}. User id: {$this->user_id}");
            
            return;
        }

        $this->sendToUser($user);
    }

}
