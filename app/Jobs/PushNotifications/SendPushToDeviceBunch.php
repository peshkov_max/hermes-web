<?php

namespace App\Jobs\PushNotifications;

use App\Jobs\Job;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Validator;
use Wipon\Models\Device;
use Wipon\Traits\LifeTimeCounterTrait;
use Wipon\Traits\PushNotificationTrait;

class SendPushToDeviceBunch extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, PushNotificationTrait, LifeTimeCounterTrait {
        LifeTimeCounterTrait::__construct as startClassLifeTimeCounting;
        LifeTimeCounterTrait::__destruct as endClassLifeTimeCounting;
    }
    
    /**
     *  List of device uuids
     *
     * @var string
     */
    private $uuids;

    /**
     * @var string
     */
    private $message;

    /**
     * Create a new job instance.
     *
     * @param array $uuids
     * @param string $message
     */
    public function __construct( array $uuids, string $message )
    {
        $this->uuids = $uuids;
        $this->message = $message;

        $this->startClassLifeTimeCounting();
    }

    function __destruct()
    {
        $this->endClassLifeTimeCounting();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (empty($this->message) || empty($this->uuids) || ! is_array($this->uuids) || $this->attempts() > 3) {

            log_w('Push notification cant be send. Message or device uuid list is empty');
            $this->delete();
        }

        $deviceToHandleLength = config('wipon.push_notification_bunch_length');

        if (count($this->uuids) > $deviceToHandleLength) {

            log_w("Push notification cant be send. SendPushToDeviceBunch cant handle more than $deviceToHandleLength records");
            $this->delete();
        }

        $v = Validator::make(['uuids' => $this->uuids,], [
            'uuids'   => "required|array",
            'uuids.*' => "exists:devices,uuid",
        ]);

        if ($v->fails()) {
            log_e('Push notification cant be send. Some given uuids does not exists in devices table', $v->errors()->toArray());
            $this->delete();
        }

        $this->sendToDeviceList();

        $this->delete();
    }


    /**
     * Отправить на список устройств
     *
     */
    private function sendToDeviceList()
    {
        $devices = Device::whereIn('uuid', $this->uuids)->withCount(["notifications" => function ( $q ) {
            $q->where("is_unread", true);
        }])->get();

        /** @var Device $device */
        foreach ($devices as $device) {
            $this->sendToDevice($device);
        }
    }

}
