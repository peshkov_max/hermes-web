<?php


namespace App\Providers;


use DaveJamesMiller\Breadcrumbs\ServiceProvider as BreadcrumbsServiceProvider;

class BreadcrumbsProvider extends  BreadcrumbsServiceProvider
{
   
   // This method can be overridden in a child class
    public function registerBreadcrumbs()
    {
        // Load the app breadcrumbs if they're in app/Http/breadcrumbs.php
        if (file_exists($file = $this->app['path'].'/Wipon/breadcrumbs.php'))
        { 
            require $file;
        } 
        else
            throw new \Exception('Breadcrumbs.php file not found!');
    }
}