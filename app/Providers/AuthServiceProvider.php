<?php

namespace App\Providers;

use App\Policies\AliasPolicy;
use App\Policies\ClusterPolicy;
use App\Policies\ProductPolicy;
use App\Policies\UserPolicy;
use Auth;
use Cache;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Schema;
use Wipon\Guard\UserProvider;
use Wipon\Models\Alias;
use Wipon\Models\Cluster;
use Wipon\Models\Permission;
use Wipon\Models\Product;
use Wipon\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Cluster::class => ClusterPolicy::class,
        Product::class => ProductPolicy::class,
        User::class    => UserPolicy::class,
        Alias::class   => AliasPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate $gate
     * @return void
     */
    public function boot( GateContract $gate )
    {
        $this->registerPolicies($gate);
        $this->registerPermissions($gate);
    }


    /**
     * @param GateContract $gate
     */
    private function registerPermissions( GateContract $gate )
    {
        $gate->before(function ( $user ) {
            /**@var User $user */
            if ($user->isSuperAdmin()) {
                return true;
            }
        });

        if ($this->schemaHasTable()) {
            foreach ($this->getPermissions() as $permission) {
                $gate->define($permission->name, function ( $user ) use ( $permission ) {

                    /**@var User $user */
                    return $user->hasRole($permission->roles);
                });
            }
        }
    }

    /**
     * Возвращает все users permissions
     *
     * @return mixed
     */
    private function getPermissions()
    {
        return Cache::remember('user_permissions', config('wipon.minute_to_store_in_cache', 30), function () {
            return Permission::with('roles')->get();
        });
    }

    /**
     * Проверяет есть ли в БД
     *
     * @return mixed
     */
    private function schemaHasTable()
    {
        return Cache::remember('schema_has_table', config('wipon.minute_to_store_in_cache', 30), function () {
            return Schema::hasTable('permissions');
        });
    }
}
