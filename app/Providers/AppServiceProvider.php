<?php

namespace App\Providers;

use Auth;
use Cache;
use Config;
use DB;
use Faker\Factory as FakerFactory;
use Faker\Generator as FakerGenerator;
use File;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Log;
use Validator;
use Wipon\Models\City;
use Wipon\Models\Organization;
use Wipon\Models\Product;
use Wipon\Models\User;

class AppServiceProvider extends ServiceProvider
{

    private $imagesPath = 'images';

    private $cabinetImagesPath = 'images/cabinet';

    private $cacheableModels = [
        'Organization',
        'Image',
        'Region',
        'User',
    ];


    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('password_old', function ( $attribute, $value, $parameters, $validator ) {

            $user = User::findOrFail((int) $parameters[0]);

            return Auth::attempt([
                'email'    => $user->email,
                'password' => $value,
            ]);
        });

        $this->bootDevServices();

        $this->initCacheableModelObservers();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Set morph relations
        Relation::morphMap([
            'products'      => Product::class,
            'cities'        => City::class,
            'organizations' => Organization::class,
        ]);

        // Set app image dirs
        $this->checkDirOrNew($this->imagesPath);
        $this->checkDirOrNew($this->cabinetImagesPath);
    }


    /**
     * Check if the the given path exists
     *
     * @param  string $path
     * @return string mixed
     */
    public static function checkDirOrNew( string $path ) :string
    {
        $publicDir = public_path($path);

        if ( ! File::exists($publicDir))
            File::makeDirectory($publicDir);

        return $path;
    }

    /**
     * Если запуск приложения осуществляется при окружении local
     *
     * 1. Регистрирует IdeHelperServiceProvider
     * 2. Устанавливает русский язык для FakerFactory
     * 3. Регистрирует слушатель запросов к БД
     */
    protected function bootDevServices()
    {
        if ( in_array(config('app.env'), ['local', 'testing', 'staging']) ) {

            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');

            // Use russian locale in faker
            $this->app->singleton(FakerGenerator::class, function () {
                return FakerFactory::create('ru_RU');
            });

            // Log all SQL queries
            // DB::listen(function ( $query ) {
            //     Log::info(' ---- START Query Logging ---- ');
            //     Log::info([$query->sql, $query->bindings, $query->time]);
            //     Log::info(' ---- END Query Logging ---- ');
            // });
        }
    }

    /**
     * Clear cacheable models after model was saved
     */
    protected function initCacheableModelObservers()
    {
        foreach ($this->cacheableModels as $cacheableModel) {
            $model = "Wipon\\Models\\{$cacheableModel}";

            $model::saved(function ( $model ) {
                if (method_exists($model, 'removeModelFromCache')) {
                    $model->removeModelFromCache();
                }

                return true;
            });
        }
    }
}
