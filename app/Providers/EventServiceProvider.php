<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Wipon\Models\Image;
use Wipon\Models\Product;
use Wipon\Models\VisualFeature;
use Wipon\Observers\ImageObserver;
use Wipon\Observers\ProductObserver;
use Wipon\Observers\VisualFeatureObserver;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\KgdStatGenerationStarted' => [
            'App\Listeners\ReportToSlackKgdStatStart',
        ],
        'App\Events\KgdStatWasGenerated' => [
            'App\Listeners\ReportStatResultsToEmails',
            'App\Listeners\ReportStatResultsToSlack',
            'App\Listeners\StoreKgdStatisticsMetaInfo',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot( DispatcherContract $events )
    {
        parent::boot($events);

        Image::observe(new ImageObserver());
        Product::observe(new ProductObserver());
        VisualFeature::observe(new VisualFeatureObserver());
    }
}
