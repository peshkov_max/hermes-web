<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Wipon\Models\City;
use Wipon\Models\Organization;
use Wipon\Models\Product;
use Wipon\Models\ProductType;
use Wipon\Models\Role;
use Wipon\Models\User;
use Wipon\Models\VisualFeature;
use Wipon\Models\Pro\FeedbackMessage as FeedbackMessagePro;
use Wipon\Models\Pro\Store as StorePro;
use Wipon\Models\Pro\User as UserPro;
use Wipon\Models\Pro\Subscription as SubscriptionPro;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $namespace_pro = 'App\Http\Controllers\Pro';


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->pattern('id', '[0-9]+');
        
        parent::boot($router);

        $router->model('products', Product::class);
        $router->model('visual-features', VisualFeature::class);
        $router->model('roles', Role::class);
        $router->model('users', User::class);
        $router->model('cities', City::class);

        $router->model('organizations', Organization::class);
        $router->model('product-types', ProductType::class);
        
        // Wipon Pro binding
        $router->model('feedback-messages-pro', FeedbackMessagePro::class);
        $router->model('users-pro', UserPro::class);
        $router->model('stores-pro', StorePro::class);
        $router->model('subscriptions-pro', SubscriptionPro::class);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        // wipon
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });

        // wipon pro
        $router->group([
            'namespace' => $this->namespace_pro, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes_pro.php');
        });
    }
}
