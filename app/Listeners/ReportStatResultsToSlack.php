<?php

namespace App\Listeners;

use App\Events\KgdStatWasGenerated;

class ReportStatResultsToSlack
{
    /**
     * Handle the event.
     *
     * @param  KgdStatWasGenerated $event
     * @return void
     */
    public function handle( KgdStatWasGenerated $event )
    {
        app('support_slack')->sendStatistics($event->statData);
        log_i('Notification was sent to #analitics chanel in slack');

        if ($event->statData['clusters_number'] > config('wipon.cluster_creating_limit')) {
            app('support_slack')->sendClusterAlert();
            log_i('Notification was sent to #alert chanel in slack');
        }
    }
}
