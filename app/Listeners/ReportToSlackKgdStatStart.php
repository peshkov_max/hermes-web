<?php

namespace App\Listeners;

use App\Events\KgdStatGenerationStarted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class ReportToSlackKgdStatStart
{
    /**
     * Handle the event.
     *
     * @param  KgdStatGenerationStarted  $event
     * @return void
     */
    public function handle(KgdStatGenerationStarted $event)
    {
        // создание PID файла который обозначает состояние генерации статистики
        if ( ! Storage::exists('stat_updating_pid')) {
            Storage::put('stat_updating_pid', 'start', 'public');
        } else {
            log_w('Statistics has already been started' );
            return;
        }
        
        log_i('Started KGD stat regeneration. Started at: ' . $event->time);
        
        app('support_slack')->startSgdStatisticsAggregation();
    }
}
