<?php

namespace App\Listeners;

use App\Events\KgdStatWasGenerated;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Wipon\Models\Alcohol\StatisticLog;

class StoreKgdStatisticsMetaInfo
{
    /**
     * Handle the event.
     *
     * @param  KgdStatWasGenerated $event
     * @return void
     */
    public function handle( KgdStatWasGenerated $event )
    {
        StatisticLog::create([
            'aggregation' =>$event->aggregationTime ?? 0,
            'mapping'     => $event->mappingTime ?? 0,
            'saving_stat' => $event->savingStatTime ?? 0,
            'memory_peak' => $event->memoryPeak,
            'total'       => $event->timeSpent,
            'created_at'  => Carbon::now(),
            'action'      => 'generate',
        ]);

        log_i("Ended KGD stat regeneration. Ended at: " . carbon_user_tz_now()->toDateTimeString() . " Time spent: {$event->timeSpent}");

        Storage::delete('stat_updating_pid');
    }

   
}
