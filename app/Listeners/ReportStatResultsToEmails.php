<?php

namespace App\Listeners;

use App\Events\KgdStatWasGenerated;
use Illuminate\Support\Facades\Mail;

class ReportStatResultsToEmails
{
    /**
     * Email адреса на которые будут отправлены 
     * отчеты о результатах сканирования
     * @var array
     */
    private $kgd_person_emails_to_notify = [
        'zhsuraganov@mgd.kz',
        'dnurgaliev@mgd.kz',
        'musa.zh-y@mail.ru',
    ];

    /**
     * Handle the event.
     *
     * @param  KgdStatWasGenerated $event
     * @return void
     */
    public function handle( KgdStatWasGenerated $event )
    {
        if (in_array(config('app.env'), ['local', 'testing', 'staging'])) {
            log_i("Forbidden to send report about statistics on local/staging/testing environment");
            return;
        }

        foreach ($this->kgd_person_emails_to_notify as $email) {

            Mail::queueOn('cabinet_email', 'emails.kgd_analytic_short', ['messageFields' => $event->statData], function ( $message ) use ( $email ) {

                $message->from('partnership@wiponapp.com', config('mail.from.name'));
                $message->to(in_array(config('app.env'), ['local', 'testing', 'staging']) ? config('wipon.dev_email') : $email)
                    ->subject('Ежедневный отчет от Wipon');

                log_i("Short stat about alcohol checks was send to email: $email");
            });

            log_i("Short stat about alcohol checks was pushed to email queue");
        }
    }
}
