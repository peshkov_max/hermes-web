<?php

namespace App\Console;

use App\Console\Commands\ClearBeanstalkdQueueCommand;
use App\Console\Commands\DevCommand;
use App\Console\Commands\ExecuteScheduledJobCommand;
use App\Console\Commands\GenerateVueJSI18nCommand;
use App\Console\Commands\InitDBSchemasCommand;
use App\Console\Commands\InitPostgis;
use App\Console\Commands\InitSupervisorCommand;
use App\Console\Commands\RefreshKgdStatCommand;
use App\Console\Commands\ReportRemoveCommand;
use App\Console\Commands\MigrateWiponDbCommand;
use App\Console\Commands\SeedUserCommand;
use App\Console\Commands\UpdateLogRightsCommand;
use App\Jobs\SendAlmatyReceiptReport;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        ClearBeanstalkdQueueCommand::class,
        GenerateVueJSI18nCommand::class,
        ReportRemoveCommand::class,
        ExecuteScheduledJobCommand::class,
        MigrateWiponDbCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule( Schedule $schedule )
    {
        $schedule->command('report-remove')->dailyAt($this->getTimeToStart('01:00'));
        $schedule->command('execute-scheduled')->everyMinute();
    }


    private function getTimeToStart( $time )
    {
        return Carbon::createFromFormat('H:i', $time, 'Asia/Almaty')->setTimezone('UTC')->format('H:i');
    }

    /**
     * Относится к проекту "Требуй чек - выиграй приз"
     * Отправлять отчет только в период проведения акции: с 2016-10-1 по 2016-12-16
     * 
     * @return bool
     */
    protected function competitionIsActive( )
    {
        $competitionStarts = Carbon::create(2016, 10, 1, 1, null, null, 'Asia/Almaty')->setTimezone('UTC');
        $competitionEnds = Carbon::create(2016, 12, 16, 1, null, null, 'Asia/Almaty')->setTimezone('UTC');
        
        return Carbon::createFromFormat('H:i', '00:00', 'Asia/Almaty')
            ->setTimezone('UTC')
            ->between($competitionStarts, $competitionEnds);
    }
}
