<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MigrateWiponDbCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:wipon-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Wipon and Wipon Pro migrations';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('migrate', [
            '--database' => 'pgsql',
            '--path' => 'database/migrations/wipon',
        ]);

        $this->call('migrate', [
            '--database' => 'pgsql_pro',
            '--path' => 'database/migrations/wipon_pro',
        ]);
    }
}
