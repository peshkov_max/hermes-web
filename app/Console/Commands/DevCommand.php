<?php

namespace App\Console\Commands;

use App\Jobs\SendPushMass;
use Illuminate\Console\Command;


use Wipon\Models\City;
use Wipon\Models\Device;
use Wipon\Models\FeedbackMessage;
use Wipon\Models\Organization;
use Wipon\Models\Pro\ApiRequest;
use Wipon\Models\Pro\Notification;
use Wipon\Models\Pro\Store;
use Wipon\Models\Pro\StoreType;
use Wipon\Models\Pro\Subscription;
use Wipon\Models\Pro\User;
use Wipon\Models\ProductType;
use Wipon\Models\Receipt;
use Wipon\Models\ScheduledJob;


class DevCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dev';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test some functional in dev purposes';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      
    }

}
