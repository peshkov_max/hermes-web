<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Illuminate\Support\Facades\File;

/** This class - command:
 * 1/ */
class ReportRemoveCommand extends Command
{

    protected $name = 'report-remove';

    protected $description = 'Remove all Excel files in app/public/excel';

    public function handle()
    {
        if (File::exists(storage_path(config('wipon.excel_files_path'))))
            File::cleanDirectory(storage_path(config('wipon.excel_files_path')));
         else
            File::makeDirectory(storage_path(config('wipon.excel_files_path')));


       $this->info("ClearAppStorageCache@handle: Storage/Cache directory was cleaned");
       log_i("ClearAppStorageCache@handle: Storage/Cache directory was cleaned");
    }

}
