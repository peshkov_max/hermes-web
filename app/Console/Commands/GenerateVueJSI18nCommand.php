<?php

namespace App\Console\Commands;

use File;
use Storage;
use Illuminate\Console\Command;

class GenerateVueJSI18nCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vue-i18n:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create localization files concatenated in json file ';

    /**
     * Execute the console command.
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $messages_all = [];
        foreach ($this->getCurrentLocales() as $locale) {

            $files = File::allFiles(resource_path("lang/$locale"));

            foreach ($files as $file) {

                $messages = $this->loadMessages($file);
                $messages = $this->clearMessages($messages);

                if (count($messages) > 0) {
                    $messages_all[ $locale ][ $file->getBaseName('.php') ] = $this->changeArrayKeysTypeToString($messages);
                    continue;
                }

                $this->warn('File with name ' . $file->getBaseName('.php') . ' is empty');
            }
        }

        //store file 
        File::put($this->getJsonFilePath(), json_encode($messages_all));
    }

    /**
     * @param $file
     * @return mixed
     */
    private function loadMessages( $file )
    {
        try {
            // import content
            $messages = include $file->getRealPath();

            if (is_numeric($messages))
                throw new \Exception('File type mismatch. Localization file must return valid php array.');

            return $messages;

        } catch (\Exception $e) {
            $this->error($e);
            dd('Error');
        }
    }

    /**
     * @param $messages
     * @return mixed
     */
    private function clearMessages( $messages )
    {
        $messages = array_filter($messages, function ( $k ) {
            return $k != '';
        }, ARRAY_FILTER_USE_KEY);

        return $messages;
    }


    private function changeArrayKeysTypeToString( $data )
    {
        $newArray = [];

        foreach ($data as $key => $value) {
            $key = strVal($key);
            $value = $this->replaceAttributes($value);
            $newArray[ is_numeric($key) ? "_$key" : "$key" ] = $value;
        }

        return $newArray;
    }


    /**
     * @param $message
     * @return array
     */
    private function replaceAttributes( $message )
    {
        try {

            if (is_array($message)) {

                $newArray = [];
                foreach ($message as $key => $var) {
                    $newArray[ $key ] = is_array($var) ? $this->replaceAttributes($var) : $this->replace($var);
                }

                return $newArray;

            } elseif (is_string($message)) {

                return $this->replace($message);

            } else
                throw new \Exception('Type error');

        } catch (\Exception $e) {
            $this->error($e);
        }
    }

    /** Replace var 
     * @param $value
     * @return mixed
     */
    private function replace( $value )
    {

        if ( ! str_contains($value, ':'))
            return $value;

        $colonNumber = substr_count($value, ':');

        if ($colonNumber == 0)
            return $value;

        $cursorCounter = 0;

        for ($i = 0;$i <= $colonNumber;$i++) {

            // find first var position or next var position
            $colonPos = stripos($value, ':', $cursorCounter == 0 ? 0 : $cursorCounter);

            // if not found - continue
            if ($colonPos === false) continue;

            $cursorCounter = $colonPos = $colonPos + 1;

            // if it is not php inserted variable - continue (expected - :variable)
            if (substr($value, $colonPos, 1) == " ") continue;

            // check colon for an escape
            $prevChar = $colonPos <= 0 ? null : substr($value, $colonPos - 2, 1);

            if ($prevChar == "\\") {
                $value = substr_replace($value, "", $colonPos - 2, 1);
                continue;
            }

            $varName = $this->extractVariableName($value, $colonPos);

            if ($varName == '') continue;

            $value = str_replace(":{$varName}", "{" . $varName . "}", $value);
        }

        return $value;
    }

    /**
     * @return string
     */
    private function getJsonFilePath()
    {
        $pathToJson = public_path('js/i18n');

        if ( ! File::exists($pathToJson)) {
            File::makeDirectory($pathToJson);
        }

        File::cleanDirectory($pathToJson);

        return "$pathToJson/localization.json";
    }

    /**
     * @return mixed
     */
    private function getCurrentLocales()
    {
        return array_map('basename', File::directories(resource_path('lang')));
    }

    /**
     * @param $value
     * @param $colonPos
     * @return mixed
     */
    private function extractVariableName( $value, $colonPos )
    {
        $words = preg_split('/[^\w]/', substr($value, $colonPos, strlen($value)));
        $varName = $words[0];

        return $varName;
    }

}
