<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Wipon\Models\ScheduledJob;

class ExecuteScheduledJobCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute-scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute scheduled job command';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // взять все работы, время выполнения которых настало
        $jobsToExecute = ScheduledJob::where('is_executed', false)->where('execute_at', '=', Carbon::now()->second(0))->get();

        foreach ($jobsToExecute as $job) {

            // Запускаем работу
            dispatch(new $job->name($job->params));

            // Обновляем модель ScheduledJon
            $job->update(['is_executed' => true]);

            $this->info("Job {$job->name} was pushed to the queue!");
            log_i("Job {$job->name} was pushed to the queue!");
        }

        $jobNumber = $jobsToExecute->count();

        if ($jobNumber) {
            $this->info("ExecuteScheduledJobCommand was executed. $jobNumber jobs was handled!");
            log_i("ExecuteScheduledJobCommand was executed. $jobNumber jobs was handled!");
        }
    }
}
