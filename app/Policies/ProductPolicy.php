<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;


    /**
     *  If user own this product
     *
     *
     * @param $user
     * @param $product
     * @return bool
     */
    public function own_product($user, $product)
    {
       return $user->company_id === $product->company_id;
    }
}
