<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Wipon\Models\Alias;
use Wipon\Models\User;

class AliasPolicy
{
    use HandlesAuthorization;

    public function index( User $user, Alias $alias, $type)
    {
        return $this->checkType($user, $type);
    }

    public function store( User $user, Alias $alias, $type)
    {
        return $this->checkType($user, $type);
    }

    public function destroy(User $user, Alias $alias, $type)
    {
        return $this->checkType($user, $type);
    }

    public function update( User $user, Alias $alias, $type)
    {
        return $this->checkType($user, $type);
    }

    /**
     * @param User $user
     * @param $type
     * @return bool
     */
    private function checkType( User $user, $type )
    {
        switch ($type) {
            case 'products':
                return $user->hasRole(['super-admin', 'product-moderator']);
                break;
            case 'cities':
                return $user->hasRole(['super-admin']);
                break;
            case 'organizations':
                return $user->hasRole(['super-admin', 'organization-moderator']);
                break;
            default:
                return false;
                break;
        }
    }
}

