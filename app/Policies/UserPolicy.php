<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;
    
    public function index($user)
    {
        return $user->isSuperAdmin();
    }

    public function destroy($user)
    {
        return $user->isSuperAdmin();
    }
    
    /**
     *  If user can view  to this user
     *
     *
     * @param $user
     * @param $user_show
     * @return bool
     */
    public function show($user, $user_show)
    {
        return $user->id === $user_show->id || $user->isSuperAdmin();
    }
    
    public function edit( $user, $user_update)
    {
        return $user->id === $user_update->id;
    }

    public function update( $user, $user_update)
    {
        return $user->id === $user_update->id;
    }


    public function update_password($user, $user_update)
    {
        return $user->id === $user_update->id || $user->isSuperAdmin();
    }


}
