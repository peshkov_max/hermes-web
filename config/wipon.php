<?php

// push_url
// http://wipon.net:8183/v2/push/  production
// http://172.17.26.236:8181/v2/push/ vagrant
// http://172.17.26.6:8183/v2/push/ test

return [
    'name'                       => 'Wipon',
    'pass_prefix'                => "19.03.2015-13:29",
    'date_time_format'           => 'Y-m-d H:i:s',
    'dt_format_d_m_y_h_i'        => 'd.m.Y H:i',
    'dt_format_d_m_y'            => 'd.m.Y',
    'default_city'               => 'Astana',
    'wipon_name'                 => 'Wipon',
    'logo'                       => 'images/components/wipon.png',
    'support_email'              => 'support@wipon.net',
    'dev_email'                  => 'peshkov-maximum@yandex.ru',
    'image_dirs_length'          => 2,
    'original_image_per_feature' => 10,
    'fake_image_per_feature'     => 20,
    'default_image_path'         => 'images/components/nopic.jpg',
    'default_image_deleted'      => 'images/components/image_was_deleted.jpg',
    'default_image_deleted_sync' => 'images/components/image_was_deleted_sync.jpg',
    'default_sync_image_path'    => 'images/components/nopic_sync.jpg',
    'image_path'                 => 'images/components/wipon.png',
    'excel_files_path'           => 'app/public/excel',
    'base_country_name'          => 'Kazakhstan',
    'static_server'              => env('APP_URL_STATIC', 'http://static'),

    /**
     * Post-Soviet states date format is  DD.MM.YYYY format
     * Used in http://jira.wiponapp.com:8090/pages/viewpage.action?pageId=5932945
     */
    'pss_date_format'            => 'd.m.Y',

    'models' => [
        'date_format' => 'Y-m-d H:i:s',
        'per_page'    => 15,
    ],

    /*
      |--------------------------------------------------------------------------
      | User Time Zone
      |--------------------------------------------------------------------------
      | This value is configured when due to
      | app loading in App\Http\Middleware\Authenticate middleware
      |
    */
    'user_time_zone'       => null,

    /*
      |--------------------------------------------------------------------------
      | Wipon Organization id in DB
      |--------------------------------------------------------------------------
      | Configured in App\Http\Middleware\Authenticate middleware
      |
    */
    'organization_name_en' => 'Wipon',

 
    // Лимит создания кластеров
    'cluster_creating_limit'         => 2000,

    // Количество устройств которое может обработать Job - SendPushToDevices
    'push_notification_bunch_length' => 200,

    // Цена подписки за пользование сервисом Wipon Pro
    'wipon_pro_subscription_price'   => [
        'per_month' => env('PRO_PRICE_PER_MONTH', 500),
        'per_year'  => env('PRO_PRICE_PER_YEAR', 6000),
    ],
];
