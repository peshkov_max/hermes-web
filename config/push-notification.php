<?php

return [

    'wiponIOS'     => [ 
        'environment' => env('APP_ENV', 'development') === 'production' ? 'production' : 'development',
        'certificate' => env('PUSH_IOS_CERT_PATH', '/path/to/certificate.pem'),
        'passPhrase'  => env('PUSH_IOS_CERT_PASS', 'qwerty'),
        'service'     => 'apns',
    ],
    'wiponAndroid' => [
        'environment' => env('APP_ENV', 'development'),
        'apiKey'      => env('PUSH_DROID_API_KEY', 'abcdef'),
        'service'     => 'gcm',
    ],

];