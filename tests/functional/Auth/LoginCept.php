<?php
$I = new FunctionalTester($scenario);
$I->wantTo('ensure that login works');
//Action
//$I->signIn('user', false);
$I->signIn('user', false, 'super-admin', 'name@email.com');
//Result
$I->amOnPage('/home');
$I->seeAuthentication();
$I->wantTo('perform actions and see result');