<?php
class PasswordRecoveryCept //Вообще это в функциональных тестах по идее должно быть, хотя хз
{
    function resetEmails(FunctionalTester $I)
    {
        $I->wantTo('delete all emails in debugmail');
        $I->amOnPage('https://debugmail.io/projects/wipon');
        if ($I->see('This project is empty. Send mails using these settings to start mail debugging:', 'div.mail-list_list__empty p')){
           // выполнить функцию sendRecoveryPassword
        } else {
        $I->seeElement('.mail-list_item');
        $I->click('#checkbox-all');
        $I->click(['class' => 'button-submit']);
        $I->see('This project is empty. Send mails using these settings to start mail debugging:', 'div.mail-list_list__empty p');
        }
            
    }
    
    function sendRecoveryPassword(FunctionalTester $I)
    {
        $I->wantTo('ensure that UI has warning before user deletion');
        //Action
        $I->amOnPage('/login');
        $I->click('#login-forget-link');
        $I->seeCurrentUrlEquals('/password/email');
        $wipon = $I->grabRecord('users', ['name' => 'wipon']);
        $I->fillField('email', $wipon->email);
        $I->click(['class' => 'btn-success']);
        //Result
        $I->seeCurrentUrlEquals('/login');
        $I->see(trans('passwords.sent'), 'div.alert-info');
        $I->wantTo('perform actions and see result');
    }
                       
    function checkEmail(FunctionalTester $I)
    {
        $I->wantTo('check sended email');
        $I->amOnPage('https://debugmail.io/projects/wipon');
        $I->click(['link' => '.mail-list_link']);
        $I->wait(5);
        $I->see('Your Password Reset Link', 'h2.mail-content_subject');
    }  
                       
    function resetPassword(FunctionalTester $I)
    {
        $I->wantTo('reset password');
        $I->amOnPage('https://debugmail.io/projects/wipon');
        $I->click(['link' => '.mail-list_link']);
        $I->wait(5);
        $I->click(['link' => '.mail-content_link']);
        //создаем переменную в которую сохраняем ссылку с 'iframe.mail-content_content_safe a';
        $I->click(['link' => 'iframe.mail-content_content_safe a']);
        $I->see('Your Password Reset Link', 'h2.mail-content_subject');
    }   
}
?>


