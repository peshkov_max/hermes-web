<?php
$I = new FunctionalTester($scenario);
$I->wantTo('ensure that login remember');

//Action
$I->signIn('user', true);
$user = $I->grabRecord('users', ['name' => 'user']);

//Result
$I->assertTrue($user->remember_token != null); //remember token is null when you login with unchecked "remember_me"

$I->wantTo('perform actions and see result');
