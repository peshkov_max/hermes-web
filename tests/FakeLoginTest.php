<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FakeLoginTest extends TestCase
{
    public function testLocalarea()
    {
        $this->visit('/login');
        $this->type('abs', 'login');
        $this->type('123', 'password');
        $this->press('Войти');
        $this->seePageIs('/login');
        $this->see('Пользователя с таким Логином/Email не существует');
    }
}
