<?php

class SubscriptionCest
{
    /**
     *  Создание новой подписки для пользователя
     *
     * @param AcceptanceTester $I
     */
    function createSubscriptionTest( AcceptanceTester $I )
    {
        $I->wantTo('Add new subscription to user by user ID');
        $I->amOnPage('/subscriptions-pro');
        $I->wait(3);
       
        $I->expectTo('See index page');
        $I->see(trans('app.subscriptions_index'));
        $I->see(trans('app.add_subscription'));
       
        $I->wantTo('Open modal window for adding user subscription');
        $I->click('#add-subscription-button');
        $I->wait(2);
       
        $I->expectTo('Modal window is opened');
        $I->see(trans('app.add_subscription'));
       
        $I->expectTo('See an error if i do not fil anything');
        $I->fillField('#user-id', '');
        $I->click('#add-new-subscription-modal-button');
        $I->wait(3);
        $I->see(strip_tags(trans('validation.required', ['attribute' => trans('validation.attributes.user_id')])));
       
        $I->expectTo('See an error if string was given instead of user id(integer)');
        $I->fillField('#user-id', 'test');
        $I->click('#add-new-subscription-modal-button');
        $I->wait(3);
        $I->see(strip_tags(trans('validation.numeric', ['attribute' => trans('validation.attributes.user_id')])));
       
        $I->expectTo('See an error if the given user id does not exist in users table');
        $I->fillField('#user-id', 32);
        $I->click('#add-new-subscription-modal-button');
        $I->wait(3);
        $I->see(strip_tags(trans('validation.exists', ['attribute' => trans('validation.attributes.user_id')])));
       
        $I->expectTo('See subscription is created');
        $I->fillField('#user-id', \Wipon\Models\Pro\User::first()->id);
        $I->click('#add-new-subscription-modal-button');
        $I->wait(3);
        $I->see(strip_tags(trans('app.subscriptions_index')));
        $I->saveScreenshot();
    }

    /**
     *  Создание новой подписки для пользователя со страницы пользователей
     *
     * @param AcceptanceTester $I
     */
    function createSubscriptionTestFromUserPage( AcceptanceTester $I )
    {
        $I->wantTo('Add new subscription to user');
        $I->amOnPage('/users-pro');
        $I->wait(3);

        $I->expectTo('See index page');
        $I->see("Пользователи приложения");

        $I->wantTo('Open modal window for adding user subscription');
        $I->click('div.align-info-table:nth-child(2) > div.div-table-col > button.button-table-cell:nth-child(1)');
        $I->wait(2);
        $I->expectTo('See modal window');
        $I->see('Добавить подписку для пользователя'); //.. //*[@id="add-subscription-button-10"]
        $I->click("div.radio:nth-child(1) > label");
        $I->wait(1);
        $I->saveScreenshot();
        $I->expectTo('Save subscription');
        $I->click('#save-new-subscription-button');
        $I->wait(5);
        $I->saveScreenshot(1);
        // $I->acceptPopup();
        // $I->saveScreenshot();
    }



}








