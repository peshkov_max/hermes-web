<?php

class FeedbackCest
{
    public function  _before(AcceptanceTester $I, \Page\Login $loginPage){

        $loginPage ->login('wipon-dev', 'wrD3CiEL5yyyk6Gb5');

    }

    public function _after(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $loginPage ->logout();
    }

    // tests // Проверяем поиск по имени "Обратная связь"
    public function searchAnNameFeedback(AcceptanceTester $I)
    {
        $I->wantTo('check search an name on page feedback');
        $I->maximizeWindow();
        $I->wait(4);
        $I->click('#menu-support-toggle');
        $I->wait(2);
        $I->click('#menu-support-index');
        $I->wait(2);
        $I->see('Обратная связь');
        $I->see('John', 'a');
        $I->see('Mike', 'a');
        $I->see('Samhanta', 'a');
        $I->see('Vitalya', 'a');
        $I->see('Стас', 'a');
    // test // Проверяем поиск по имени "Обратная связь"
        $I->fillField('#search-by-everyWhere-input', 'Mike');
        $I->click('#search-by-everyWhere-button');
        $I->wait(2);
        $I->see('Mike', 'a');
        $I->dontSee('John', 'a');
        $I->dontSee('Samhanta', 'a');
        $I->dontSee('Vitalya', 'a');
        $I->dontSee('Стас', 'a');
        $I->saveScreenshot('(Обратная связь) Поиск только имя Mike');

    // test  // Проверяем поиск по UUid "Обратная связь"

        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->fillField('#search-by-everyWhere-input', '49DE8311-F5C0-4E88-985A-DE3CD59F9A5A');
        $I->pressKey('#search-by-everyWhere-input',WebDriverKeys::ENTER);
        $I->wait(2);
        $I->dontSee('Mike', 'a');
        $I->see('John', 'a');
        $I->dontSee('Samhanta', 'a');
        $I->dontSee('Vitalya', 'a');
        $I->dontSee('Стас', 'a');
        $I->saveScreenshot('(Обратная связь) Поиск только uuid 49DE8311-F5C0-4E88-985A-DE3CD59F9A5A');

    // test  // Проверяем поиск по email "Обратная связь"

        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->fillField('#search-by-everyWhere-input', 'Real_cake@gmail.com');
        $I->click('#search-by-everyWhere-button');
        $I->wait(2);
        $I->dontSee('Mike', 'a');
        $I->dontSee('John', 'a');
        $I->see('Samhanta', 'a');
        $I->see('Vitalya', 'a');
        $I->dontSee('Стас', 'a');
        $I->saveScreenshot('(Обратная связь) Поиск только email Real_cake@gmail.com');

    // test // Проверяем поиск по платформе ios "Обратная связь"

        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->fillField('#search-by-platform-input', 'ios');
        $I->click('#search-by-platform-button');
        $I->wait(1);
        $I->see('IOS');
        $I->dontSee('Android');
        $I->saveScreenshot('(Обратная связь) Поиск только платформа ios');

    // test // Проверяем поиск по платформе android "Обратная связь"

        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->fillField('#search-by-platform-input', 'android');
        $I->click('#search-by-platform-button');
        $I->wait(1);
        $I->see('android');
        $I->dontSee('ios');
        $I->saveScreenshot('(Обратная связь) Поиск только платформа андроид');


    // test //  Проверяем Фильтры "Прочитано/ не прочитано"
    // Только прочитанные
        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->click('#use-readUnread');
        $I->wait(1);
        $I->click('div.display-radio:nth-child(1) > label.self-written-style');
        $I->wait(2);
        $I->see('Samhanta', 'a');
        $I->see('Стас', 'a');
        $I->dontSee('John', 'a');
        $I->dontSee('Mike', 'a');
        $I->dontSee('Vitalya', 'a');
        $I->saveScreenshot('(Обратная связь) Только прочитанные');

    // Только не прочитанные

        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->click('#use-readUnread');
        $I->wait(1);
        $I->click('div.display-radio:nth-child(2) > label.self-written-style');
        $I->wait(2);
        $I->dontSee('Samhanta', 'a');
        $I->dontSee('Стас', 'a');
        $I->see('John', 'a');
        $I->see('Mike', 'a');
        $I->see('Vitalya', 'a');
        $I->saveScreenshot('(Обратная связь) Только не прочитанные');

    // test //  Проверяем Фильтры "С сайта/ с мобильного приложения"
        // Только из приложения
        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->click('#use-mobileWeb');
        $I->click('div.display-radio:nth-child(1) > label.self-written-style');
        $I->wait(2);
        $I->see('John', 'a');
        $I->see('Mike', 'a');
        $I->see('Samhanta', 'a');
        $I->dontSee('Vitalya', 'a');
        $I->dontSee('Стас', 'a');
        $I->saveScreenshot('(Обратная связь) Только из приложения');

        // Только из приложения
        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->click('#use-mobileWeb');
        $I->click('div.display-radio:nth-child(2) > label.self-written-style');
        $I->wait(2);
        $I->dontSee('John', 'a');
        $I->dontSee('Mike', 'a');
        $I->dontSee('Samhanta', 'a');
        $I->see('Vitalya', 'a');
        $I->see('Стас', 'a');
        $I->saveScreenshot('(Обратная связь) Только c сайта ');

    // test // проверяем фильтры в таблице
        // Фильтр по наименованию
        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->click('div.style-table-cell:nth-child(1) > div.style-description-table > p.align-description > a');
        $I->wait(2);
        $I->see('John', 'div.align-info-table:nth-child(2) > div.div-table-col:nth-child(1) > a');
        $I->see('Mike', 'div.align-info-table:nth-child(3) > div.div-table-col:nth-child(1) > a');
        $I->see('Samhanta', 'div.align-info-table:nth-child(4) > div.div-table-col:nth-child(1) > a');
        $I->see('Vitalya', 'div.align-info-table:nth-child(5) > div.div-table-col:nth-child(1) > a');
        $I->see('Стас', 'div.align-info-table:nth-child(6) > div.div-table-col:nth-child(1) > a');
        $I->wait(2);
        $I->saveScreenshot('(Обратная связь) Первая сортировка по имени');

        // Сортеруем данные в обратную сторону

        $I->click('div.style-table-cell:nth-child(1) > div.style-description-table > p.align-description > a');
        $I->wait(2);
        $I->see('John', 'div.align-info-table:nth-child(6) > div.div-table-col:nth-child(1) > a');
        $I->see('Mike', 'div.align-info-table:nth-child(5) > div.div-table-col:nth-child(1) > a');
        $I->see('Samhanta', 'div.align-info-table:nth-child(4) > div.div-table-col:nth-child(1) > a');
        $I->see('Vitalya', 'div.align-info-table:nth-child(3) > div.div-table-col:nth-child(1) > a');
        $I->see('Стас', 'div.align-info-table:nth-child(2) > div.div-table-col:nth-child(1) > a');
        $I->wait(2);
        $I->saveScreenshot('(Обратная связь) Вторая сортировка по имени');

        // Фильтр по Времени отправки.

        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->click('div.style-table-cell:nth-child(2) > div.style-description-table > p.align-description > a');
        $I->wait(2);
        $I->see('Время: 10:00. Дата: 01.01.2017', 'div.align-info-table:nth-child(2) > div.div-table-col:nth-child(2)');
        $I->see('Время: 10:00. Дата: 02.01.2017', 'div.align-info-table:nth-child(3) > div.div-table-col:nth-child(2)');
        $I->see('Время: 10:00. Дата: 03.01.2017', 'div.align-info-table:nth-child(4) > div.div-table-col:nth-child(2)');
        $I->see('Время: 11:00. Дата: 03.01.2017', 'div.align-info-table:nth-child(5) > div.div-table-col:nth-child(2)');
        $I->see('Время: 12:00. Дата: 03.01.2017', 'div.align-info-table:nth-child(6) > div.div-table-col:nth-child(2)');
        $I->saveScreenshot('(Обратная связь) Первая сортировка по времени отправки');

        // Сортеруем данные в обратную сторону

        $I->click('div.style-table-cell:nth-child(2) > div.style-description-table > p.align-description > a');
        $I->wait(2);
        $I->see('Время: 10:00. Дата: 01.01.2017', 'div.align-info-table:nth-child(6) > div.div-table-col:nth-child(2)');
        $I->see('Время: 10:00. Дата: 02.01.2017', 'div.align-info-table:nth-child(5) > div.div-table-col:nth-child(2)');
        $I->see('Время: 10:00. Дата: 03.01.2017', 'div.align-info-table:nth-child(4) > div.div-table-col:nth-child(2)');
        $I->see('Время: 11:00. Дата: 03.01.2017', 'div.align-info-table:nth-child(3) > div.div-table-col:nth-child(2)');
        $I->see('Время: 12:00. Дата: 03.01.2017', 'div.align-info-table:nth-child(2) > div.div-table-col:nth-child(2)');
        $I->saveScreenshot('(Обратная связь) Вторая сортировка по времени отправки');

        // Фильтр по Электронной почте

        $I->click('#reset-filter-button');
        $I->wait(2);
        $I->click('div.style-table-cell:nth-child(4) > div.style-description-table > p.align-description > a');
        $I->wait(2);
        $I->see('Happy_sun@mail.ru', 'div.align-info-table:nth-child(2) > div.div-table-col:nth-child(4)');
        $I->see('Light_work@gmail.com', 'div.align-info-table:nth-child(3) > div.div-table-col:nth-child(4)');
        $I->see('Real_cake@gmail.com', 'div.align-info-table:nth-child(4) > div.div-table-col:nth-child(4)');
        $I->see('Real_cake@gmail.com', 'div.align-info-table:nth-child(5) > div.div-table-col:nth-child(4)');
        $I->see('Salam@gmail.com', 'div.align-info-table:nth-child(6) > div.div-table-col:nth-child(4)');
        $I->saveScreenshot('(Обратная связь) Первая сортировка по электронной почте');

        // Сортеруем данные в обратную сторону

        $I->click('div.style-table-cell:nth-child(4) > div.style-description-table > p.align-description > a');
        $I->wait(2);
        $I->see('Happy_sun@mail.ru', 'div.align-info-table:nth-child(6) > div.div-table-col:nth-child(4)');
        $I->see('Light_work@gmail.com', 'div.align-info-table:nth-child(5) > div.div-table-col:nth-child(4)');
        $I->see('Real_cake@gmail.com', 'div.align-info-table:nth-child(4) > div.div-table-col:nth-child(4)');
        $I->see('Real_cake@gmail.com', 'div.align-info-table:nth-child(3) > div.div-table-col:nth-child(4)');
        $I->see('Salam@gmail.com', 'div.align-info-table:nth-child(2) > div.div-table-col:nth-child(4)');
        $I->saveScreenshot('(Обратная связь) Вторая сортировка по электронной почте');

        // сбрасываем фильтры на усходную позицию

        $I->click('#reset-filter-button');
        $I->wait(2);
    }


    // tests // Проверяем пометить необработанным и просмотр сообщения "Обратная связь"
    public function seeAndProccessingMessage(AcceptanceTester $I)
    {
        $I->wantTo('checked message feedback and proccessing message');
        $I->maximizeWindow();
        $I->wait(2);
        $I->amOnPage('/supports');
        $I->wait(2);

        // Помечаем обработанным сообщение изначально необработанное потом при просмотре сообщения мы должны увидеть что сообщение обработанно!

        $I->click('div.align-info-table:nth-child(5) > div.div-table-col:nth-child(9) button:nth-child(3)');
        $I->wait(1);
        $I->click('#continue-cancel-modal-button');
        $I->wait(2);
        $I->saveScreenshot('(Обратная связь) Сообщения Mike помеченно обработанным');

        // Теперь переходим к просмотру и так же проверяем будет ли там помечено как обработанное сообщение от mike

        $I->click('div.align-info-table:nth-child(5) > div.div-table-col:nth-child(9) button:nth-child(1)');
        $I->wait(2);
        $I->see('Mike');
        $I->see('Время: 10:00. Дата: 02.01.2017');
        $I->see('Light life');
        $I->see('Light_work@gmail.com');
        $I->see('+77078762244');
        $I->see('nokia (Android)');
        $I->see('2.5.5');
        $I->see('4a6996739fd30f40');
        $I->see('Пометить не обработанным', '#open-set-read-modal-button');
        $I->saveScreenshot('(Обратная связь) Просмотр инфармации о сообщения обратной связи и статус обработки сообщения');

        // Все данные которые есть у Mike совподают теперь мы должны пометить необработанным сообщение для проверки работоспособности данной кнопки

        $I->click('div.block-button-inline > button#open-set-read-modal-button');
        $I->wait(1);
        $I->click('#set-message-status-modal-button');
        $I->wait(1);
        $I->see('Пометить обработанным', 'button');
        $I->dontSee('Пометить не обработанным', '#open-set-read-modal-button');
        $I->saveScreenshot('(Обратная связь) Просмотр инфармации о сообщения статус не обработанное сообщение');

        // Переходим в показать устройства и проверяем данные

        $I->click('#open-device-detail-button');
        $I->wait(2);
        $I->see('4a6996739fd30f40');
        $I->see('nokia');
        $I->see('Android');
        $I->see('2.5.5');
        $I->see('Нет информации');
        $I->saveScreenshot('(Обратная связь) Просмотр инфармации о устройстве');

        // Переходим по ссылке помотреть сообщение переходим обратно на страницу обратная связь.

        $I->click('#show-messages-button');
        $I->wait(2);
        $I->see('Обратная связь');
        $I->see('Mike');
        $I->saveScreenshot('(Обратная связь)  Просмотр сообщения (отсортированная страница по uuid 4a6996739fd30f40)');
    }
}
