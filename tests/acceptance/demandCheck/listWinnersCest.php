<?php

class listWinnersCest
{
    public function  _before(AcceptanceTester $I, \Page\Login $loginPage){

        $loginPage ->login('wipon-dev', 'wrD3CiEL5yyyk6Gb5');

    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function listWinners(AcceptanceTester $I)
    {
        $I->wantTo('test filter and search on page list of winners');
        $I->maximizeWindow();
        $I->wait(5);
        $I->click('#menu-receipt-moderation');
        $I->wait(1);
        $I->click('#menu-receipt-winners');
        $I->wait(2);
        $I->see('Список победителей');
        $I->see('qwe');
    }
}
