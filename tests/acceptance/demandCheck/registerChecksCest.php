<?php

class registerChecksCest
{
    public function  _before(AcceptanceTester $I, \Page\Login $loginPage){

        $loginPage ->login('wipon-dev', 'wrD3CiEL5yyyk6Gb5');

    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function registerChecksTest(AcceptanceTester $I)
    {
        $I->wantTo('register checks test filter and search');
        $I->maximizeWindow();
        $I->wait(5);
        $I->click('#menu-receipt-moderation');
        $I->wait(1);
        $I->click('#menu-receipt-registry');
        $I->wait(2);
        $I->see('Реестр чеков');
        $I->see('qwe');
    }
}
