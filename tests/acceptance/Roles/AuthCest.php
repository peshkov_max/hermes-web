<?php
class AuthCest
{
    function authSuperAdmin(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can see his profile');

        //Action
        $loginPage->signInAsRoles('super-admin');
        $I->amOnPage('/home');
        $I->maximizeWindow();
        $I->click('#organization-menu');
        $I->click('#show-profile');

        //Result
        $I->see('wipon', '#user-name');
        $I->wantTo('perform actions and see result');
    }

    function authContentManager(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can see his profile');

        //Action
        $loginPage->signInAsRoles('content-manager');
        $I->amOnPage('/home');
        $I->maximizeWindow();
        $I->click('#organization-menu');
        $I->click('#show-profile');

        //Result
        $I->see('content-manager', '#user-name');
        $I->wantTo('perform actions and see result');
    }

    function authSupport(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can see his profile');

        //Action
        $loginPage->signInAsRoles('support');
        $I->amOnPage('/home');
        $I->maximizeWindow();
        $I->click('#organization-menu');
        $I->click('#show-profile');

        //Result
        $I->see('support', '#user-name');
        $I->wantTo('perform actions and see result');
    }

    function authProductModerator(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can see his profile');

        //Action
        $loginPage->signInAsRoles('product-moderator');
        $I->amOnPage('/home');
        $I->maximizeWindow();
        $I->click('#organization-menu');
        $I->click('#show-profile');
        
        //Result
        $I->see('product-moderator', '#user-name');
        $I->wantTo('perform actions and see result');
    }

    function authInspectorAdmin(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can see his profile');

        //Action
        $loginPage->signInAsRoles('inspector-admin');
        $I->amOnPage('/home');
        $I->maximizeWindow();
        $I->click('#organization-menu');
        $I->click('#show-profile');

        //Result
        $I->see('inspector-admin', '#user-name');
        $I->wantTo('perform actions and see result');
    }

    function authInspector(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can see his profile');

        //Action
        $loginPage->signInAsRoles('inspector');
        $I->amOnPage('/home');
        $I->maximizeWindow();
        $I->click('#organization-menu');
        $I->click('#show-profile');
        
        //Result
        $I->see('inspector', '#user-name');
        $I->wantTo('perform actions and see result');
    }

    function authRaimbek(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can see his profile');

        //Action
        $loginPage->signInAsRoles('commercial');
        $I->amOnPage('/home');
        $I->maximizeWindow();
        $I->click('#organization-menu');
        $I->click('#show-profile');

        //Result
        $I->see('raimbek', '#user-name');
        $I->wantTo('perform actions and see result');
    }
}
?>