<?php


class FirstCest
{
    public function  _before(AcceptanceTester $I, \Page\Login $loginPage){

        $loginPage ->login('wipon-dev', 'wrD3CiEL5yyyk6Gb5');

    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function tryToTest(AcceptanceTester $I)
    {
        $I->amOnPage('/product-types');

       $I->see('Типы продуктов');
        $I->fillField('#search-by-name-input', 'Табак');
      $I->click('#search-by-name-button');
        $I->wait(5);
        $I->see('Табак (id = 5)');
        $I->dontSee('Парфюм (id = 4)');
        $I->click('#reset-filter-button');
        $I->wait(5);
        $I->see('Парфюм (id = 4)');
        $I->click('#open-product-type-link-5');
        $I->see('Просмотр типа');
        $I->saveScreenshot(__METHOD__);



        //$I->click('#menu-support-index');


        // $I->wait(5);
        // $I->saveScreenshot(__METHOD__);
        // $I->click('#menu-support-index');


        // $I->see('Обратная связь');
        //
        // $I->click('Обратная связь');
        // $I->saveScreenshot();

        //
        //
        // $I->checkOption('#use-readUnread');

        // $I->see('Только прочитанное');
        //
        // $I->click('#radio-unread');
    }
}
