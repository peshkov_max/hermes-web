<?php
class AddOrganizationCest
{
    public $_pathSelectorsForDelete = 'div.list-container > div:last-child > div.list-column-button'; //
//        $I->click('div.list-container > div:last-child > div.list-column-button:last-child');
  

    //Page name " Создание новой организации ": Не видны элементы ждем пока разрабы поправят.
   function GoToAddOrganization(AcceptanceTester $I){
    
       $I->wantTo('Add organization go path to add organization page');

       $I->amOnPage('/vue-products');
       $I->wait(5);
       $I->saveScreenshot();
       // $I->see('Создание новой организации');
       // Дошли до создания новой организации.//
       // Перед заполнением порверим работают ли обязательные поля то есть просто пытаемся сохронить данные на что//
       // Проверяем кнопку "отмена" и "закрыть"//
       // $I->click('#cancel-edit-button');
       // $I->wait(1);
       // $I->click('#modal-close');
       // $I->wait(1);
       // $I->see('Создание новой организации');
       // Получаем сообщения о незаполненых обязательных полях//
       // $I->fillField('#email', 'Test');
       // $I->click('#save-organization-button');
       // $I->wait(3);
       // $I->see('Поле Наименование (EN) обязательно для заполнения.');
       // $I->see('Поле Наименование (RU) обязательно для заполнения.');
       // $I->see('Поле Юридическое обязательно для заполнения.');
       // $I->see('Поле Email должно быть содержать электронный адрес верного формата');
       // $I->see('Поле Город должно быть числом');
       // // Видим что подсказки о том что объязательные поля не заполнены.//
       // // Теперь заполняем форму и сохроняем (цепляемся к компонентам с помощью id)//
       // $I->fillField('#name_en', 'Test_Organization');
       // $I->fillField('#name_ru', 'Тестовая_Организация');
       // $I->fillField('#name_kk', 'Тестовая Организация kk');
       // $I->fillField('#contract_number', '568');
       // $I->fillField('#legal_name', 'Тестовое Юридическое имя');
       // $I->fillField('#email', 'Test@mail.ru');
       // // работаем с селектом
       // $I->click('#select2-city-select-container');
       // $I->fillField('.select2-search__field', 'Астана');
       // $I->wait('2');
       // $I->pressKey('.select2-search__field',WebDriverKeys::ENTER);
       // $I->executeJS("$('.select2-search__field').trigger(jQuery.Event('keypress', {keyCode: 13}));");
       // $I->click('#save-organization-button');
       // $I->wait(5);
       // $I->see('Тестовая_Организация');
       // $I->see('Тестовое Юридическое имя');
       // $I->see('568');
       // $I->see('Test@mail.ru');
       // $I->amOnPage('/login');
   }

    //Page name " Организации B2B, B2G ".
//    function organizationB2BTest(AcceptanceTester $I){
//        $I ->wantTo(' check organization B2B, B2G page ');
//        $I->maximizeWindow();
//        $I ->amOnPage('/organizations');
//        $I->see('Организации B2B, B2G');
//        // Проверка фильтров и поиска на работоспасобность //
          // Сперва вводим в поиск данные за тем открываем фильтры промодерированных и не промодерированных организация //
          // То есть после того как мы нажимаем на фильтры фокус с строки ввода спадает и происходит поиск //
//        $I->fillField('#search-by-name-input', 'KGD');
//        $I->click('#use-moderation');
//        $I->wait(2);
//        $I->see('KGD');
//        $I->dontSee('#open-device-log-link-1', 'Wipon');
//        // Проверяем фильтры //
//        $I->click('div.display-radio:nth-child(1) > label.self-written-style');
//        $I->wait(2);
//        $I->see('KGD');
//        $I->dontSeeInSource('<div class="alert alert-info info ">
//            Записи не найдены<!--v-html-->
//        </div>');
//        $I->click('div.display-radio:nth-child(2) > label.self-written-style');
//        $I->wait(2);
//        $I->see('Записи не найдены');
//        $I->dontSee('KGD');
//        // Проверяем сброс фильтров
//        $I->click('#reset-filter-button');
//        $I->wait(2);
//        $I->dontSee('Записи не найдены');
//        $I->amOnPage('/login');
//    }

//    //Page name " Прикрепление городов к организации ".
//    function addCityForOrganizationTest(AcceptanceTester $I){
//        $I->wantTo('add city for organization');
//        $I->maximizeWindow();
//        $I->amOnPage('/organizations/1/cities');
//        // Добавляем город //
//        // Выбираем и добавляем город //
//        $I->see('Прикрепление городов к организации');
//        $I->click('#add-alias-button');
//        $I->click('.select2-selection--single');
//        $I->fillField('.select2-search__field', 'Тараз');
//        $I->wait(2);
//        $I->pressKey('.select2-search__field',WebDriverKeys::ENTER);
//        /////////////////$I->executeJS("$('.select2-search__field').trigger(jQuery.Event('keypress', {keyCode: 13}));");//////////////////// Не раскоменчиваем ////
//        $I->click('#store-alias-button');
//        $I->wait(2);
//        $I->see('Астана');
//        // Проверяем удаление города //
//        // Для начала проверем кнопку отмена //
//        $I->click($this->_pathSelectorsForDelete);
//        $I->wait(1);
//        $I->see('Внимание!', '.modal-content');
//        $I->click('#modal-close');
//        $I->dontSee('Внимание!', '.modal-content');
//        $I->see('Тараз');
//        // Проверяем удаление //
//        $I->click('$this->_pathSelectorsForDelete');
//        $I->wait(1);
//        $I->click('#continue-modal-button');
//        $I->wait(1);
//        $I->dontSee('Тараз');
//        $I->dontSee('Внимание!', '.modal-content');
//        $I->amOnPage('/login');
//    }


    //Page name " Пользователи/Прикрепленные пользователи (прикрепление пользователся к орагнизации) ".
//    function seeAndAddUserForOrganizations(AcceptanceTester $I){
//
//        $I->wantTo('See and add user for organizations');
//        $I->maximizeWindow();
//        $I->amOnPage('/organizations/1/manage-users');
//        $I->see('Пользователи');
//        // Добавляем пользователя //
//        $I->click('#user-attach');
//        $I->wait(1);
//        $I->click('.select2-selection--single');
//        $I->wait(1);
//        $I->fillField('.select2-search__field', 'support');
//        $I->wait(1);
//        $I->pressKey('.select2-search__field',WebDriverKeys::ENTER);
//        $I->wait(1);
//        $I->click('#modal-confirm');
//        $I->wait(1);
//        $I->see('support');
//        // После добавления необходимо проверить работает ли удаление пользователей (пользователя которого мы добавили ранее). //
//        // Проверяем кнопку отмены //
//        $I->click($this->_pathSelectorsForDelete);
//        $I->see('Внимание!', '.modal-content');
//        $I->click('#modal-delete-close');
//        $I->see('support');
//        $I->dontSee('Внимание!', '.modal-content');
//        // удаляем добавленного пользователя //
//        $I->click($this->_pathSelectorsForDelete);
//        $I->see('Внимание!', '.modal-content');
//        $I->click('div.modal-footer > div > button#modal-confirm-detach-close'); // пишем точный путь селекторов иба если указать просто id открываться будет другая модалка "добавление пользователей"
//        $I->wait(1);
//        $I->dontSee('support');
//        $I->amOnPage(/login);
//    }


    //Page name " Псевдонимы организации ".
//    function pseudoOrganizations(AcceptanceTester $I){
//
//        $I->wantTo('pseudo of the organization');
//        $I->maximizeWindow();
//        $I->amOnPage('/aliases/organizations/1');
//        $I->see('Псевдонимы организации');
//        $I->click('div.block-button-inline > button#add-alias-button');
//        $I->wait(2);
//        $I->fillField('div.form > #alias-input', 'neon');
//        $I->wait(1);
//        $I->click('div.form-buttons > div.block-button-inline > button#store-alias-button');
//        $I->wait(1);
//        $I->see('neon');
//        $I->click($this->_pathSelectorsForDelete);
//        $I->wait(1);
//        $I->click('div.modal-dialog > div.modal-content > div.modal-footer > div > div.block-button-inline > button#continue-modal-button'); // пишем точный путь селекторов иба если указать просто id открываться будет другая модалка "добавление пользователей"
//        $I->wait(2);
//        $I->dontSee('neon');
//        $I->amOnPage('/login');
//    }
    //Page name " Редактирование организации ": Не видны элементы ждем пока разрабы поправят.
//    function editOrganizations(AcceptanceTester $I){
//
//        $I->wantTo('edit organization');
//        $I->maximizeWindow();
//        $I->amOnPage('/organizations/1/edit');
//        $I->wait(30);
//        $I->see('qwe');
//    }
}
?>







