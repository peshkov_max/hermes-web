<?php
class EditOrganizationCest
{
    //User-story:
    function editOrganization(AcceptanceTester $I, \Page\Login $loginPage)
    {
        $I->wantTo('ensure that user can add organization');
        $loginPage->signInAsUser('wipon');
        $I->maximizeWindow();
        $I->click('#menu-users');

        $I->click('#menu-organizations');
        $I->amOnPage('/organizations');
        $I->click(['class'=>'.testing-edit-organization']);

    }

}
?>