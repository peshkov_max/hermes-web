<?php
namespace Stat;
use \AcceptanceTester;

class StatYkmCest
{
    public function  _before(AcceptanceTester $I, \Page\Login $loginPage){

        $loginPage ->login('wipon-dev', 'wrD3CiEL5yyyk6Gb5');

    }
    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function FilterYear(AcceptanceTester $I)
    {
        $I->amOnPage('/checks/stats');
        $I->see('Статистика');

        $I->click('#select2-year-select-container');
        $I->wait(2);
        $I->fillField('.select2-search__field','2017');
        $I->wait(2);
        $I->pressKey('.select2-search__field',\WebDriverKeys::ENTER);
        $I->wait(5);
        $I->see('Область');
    }

    public function resetFilters(AcceptanceTester $I)
    {
       /* $I->click('Сбро');*/
    }
}
