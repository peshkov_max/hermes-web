<?php


class FirstCest
{
    public function  _before(AcceptanceTester $I, \Page\Login $loginPage){

        $loginPage ->login('wipon-dev', 'wrD3CiEL5yyyk6Gb5');

    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests

    public function SearchByName(AcceptanceTester $I)
    {
        $I->amOnPage('/product-types');

        $I->see('Типы продуктов');
        $I->fillField('#search-by-name-input', 'Табак');
        $I->click('#search-by-name-button');
        $I->wait(5);
        $I->see('Табак');
        $I->dontSee('Парфюм');
        $I->click('#reset-filter-button'); //Сбрасываю фильтр
        $I->wait(5);
        $I->see('Парфюм (id = 39)');
    }

    public function CreateProduct(AcceptanceTester $I)
    {
        $I->amOnPage('/product-types');

        $I->click('#add-type-button');
        $I->see('Добавить');
        $I->fillField('#name_ru','Элемент списка');
        $I->fillField('#name_en','List item');
        $I->click('#select2-parent-type-select-container');
        $I->wait(2);
        $I->fillField('.select2-search__field','Табак');
        $I->wait(2);
        $I->pressKey('.select2-search__field',WebDriverKeys::ENTER);
        $I->saveScreenshot(__METHOD__);
        $I->click('#next-button');
        $I->wait(5);
        $I->see('Типы продуктов');
    }


    public function SearchByNameParentType(AcceptanceTester $I)
    {
        $I->amOnPage('/product-types');

        $I->see('Типы продуктов');
        $I->fillField('#search-by-parent_type_name-input', 'Табак');
        $I->click('#search-by-parent_type_name-button');
        $I->wait(5);
        $I->see('Табак');
        $I->dontSee('Парфюм');
        $I->click('#reset-filter-button'); //Сбрасываю фильтр
        $I->wait(5);
        $I->see('Парфюм');
    }

    public function TypeView(AcceptanceTester $I)
    {
        $I->amOnPage('/product-types');
        $I->click('#open-product-type-link-40');
        $I->see('Просмотр типа');
    }

    public function TypeViewEdit(AcceptanceTester $I)
    {
        $I->amOnPage('/product-types');
        $I->click('#open-product-type-link-40');
        $I->see('Просмотр типа');
        //---------ЖДУ ИСПРАВЛЕНИЯ БАГА---------
        /*$I->click('#edit-type');
        $I->see('Редактирование типа');
        $I->fillField('#name_ru','Табак');
        $I->fillField('#name_en','Tobacco');
        $I->click('#next-button');
        $I->wait(5);
        $I->see('Просмотр типа');*/
        //----------ЖДУ ИСПРАВЛЕНИЯ БАГА----------
        $I->click('#edit-type');
        $I->see('Редактирование типа');
        $I->click('#skip-button');
        $I->wait(5);
        $I->see('Внимание!');
        $I->click('#continue-button');
        $I->wait(5);
        $I->see('Просмотр типа');
        $I->click('#go-back-button');
        $I->see('Типы продуктов');
    }

    public function ShowProduct(AcceptanceTester $I)
    {
       $I->amOnPage('/product-types');
        $I->see('Типы продуктов');
        $I->click('#show-product-type-button-40');
        $I->see('Просмотр типа');
        $I->click('#go-back-button');
        $I->see('Типы продуктов');
    }

    public function EditProduct(AcceptanceTester $I)
    {
        $I->amOnPage('/product-types');
        $I->see('Типы продуктов');
        $I->click('#edit-product-type-button-40');
        $I->see('Редактирование типа');
        $I->click('#skip-button');
        $I->wait(10);
        $I->see('Внимание!');
        $I->click('#continue-button');
        $I->wait(5);
        $I->see('Типы продуктов');
    }

}
