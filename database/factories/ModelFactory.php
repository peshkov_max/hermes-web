<?php

use Illuminate\Support\Facades\File;
use Wipon\Models\{
    Alias, ApiRequest, Check, Cluster, Customer, Device, DeviceNotification, FeedbackMessage, Item, Organization, Product, Receipt, ReceiptReport, ReceiptReportType, Role, User
};
use Wipon\Models\Pro\ApiRequest as ApiRequestPro;
use Wipon\Models\Pro\FeedbackMessage as FeedbackMessagePro;
use Wipon\Models\Pro\Region as RegionPro;
use Wipon\Models\Pro\Store as StorePro;
use Wipon\Models\Pro\User as UserPro;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Organization::class, function ( Faker\Generator $faker ) {

    $name = $faker->company;

    return [
        'name_ru' => $name,
        'name_en' => $name,
        'email'   => $faker->safeEmail,
    ];
});

$factory->define(Alias::class, function ( Faker\Generator $faker ) {

    return [
        'name' => $faker->company,
    ];
});

$factory->define(User::class, function ( Faker\Generator $faker ) {

    return [
        'organization_id' => function () {
            return factory(Organization::class)->create()->id;
        },
        'name'            => str_random(10),
        'email'           => $faker->safeEmail,
        'password'        => bcrypt(str_random(10)),
        'api_token'       => bcrypt(str_random(10)),
        'remember_token'  => str_random(10),
        'is_testing'      => false,
    ];
});

$factory->define(Role::class, function ( Faker\Generator $faker ) {

    return [
        'name'         => str_random(10),
        'display_name' => $faker->word,
    ];
});

$factory->define(Cluster::class, function ( Faker\Generator $faker ) {

    $name = $faker->company;

    return [
        'city_id'   => \Wipon\Models\City::whereNameEn($faker->randomElement(['Astana', 'Almaty', 'Villach', 'Klagenfurt', 'Mistelbach', 'Surgut']))->first()->id,
        "name_ru"   => $name,
        "name_en"   => $name,
        "name_kk"   => $name,
        "latitude"  => $faker->latitude,
        "longitude" => $faker->longitude,
        "accuracy"  => $faker->randomDigit,
        "street"    => $faker->streetName,
        "house"     => $faker->randomDigit,
    ];
});

$factory->define(Device::class, function ( Faker\Generator $faker ) {
    return [
        'uuid'        => $faker->uuid,
        'model'       => $faker->randomElement(['105w', 'U213', 'UI1000', 'IU544', 'U3000', 'U2000']),
        'platform'    => $faker->randomElement(['iOS', 'Android', 'Windows']),
        'app_version' => $faker->randomElement(['2.1.0', '2.1.1', '2.2.1', '2.3.1']),
        'push_token'  => bcrypt(str_random(30)),
        'customer_id' => function () use ( $faker ) {
            return Customer::create(['name' => $faker->firstName, 'phone_number' => $faker->phoneNumber, 'email' => $faker->email])->id;
        },
    ];
});

$factory->define(Check::class, function ( Faker\Generator $faker ) {

    return [
        'item_id'       => function () {
            return factory(Item::class)->create()->id;
        },
        'cluster_id'    => function () {
            return factory(Cluster::class)->create()->id;
        },
        'device_uuid'   => function () {
            return factory(Device::class)->create()->uuid;
        },
        'geo_longitude' => $faker->longitude(48.0196, 77),
        'geo_latitude'  => $faker->latitude(48.0196, 77),
        'geo_accuracy'  => $faker->randomDigit,
        'created_at'    => $faker->dateTimeBetween($startDate = '-1 days', $endDate = 'now'),
    ];
});

$factory->define(ApiRequest::class, function ( Faker\Generator $faker ) {

    list($path, $content, $response) = getRequestData($faker);

    return [
        'method'      => $faker->randomElement(['POST', 'GET']),
        'client_ip'   => $faker->ipv4,
        'url'         => $faker->url,
        'path'        => $path,
        'device_uuid' => function () {
            return factory(Device::class)->create()->uuid;
        },
        'header'      => [
            'slug' => $faker->slug,
        ],
        'content'     => $content,
        'response'    => $response,
        'created_at'  => $faker->dateTime,
    ];
});

$factory->define(FeedbackMessage::class, function ( Faker\Generator $faker ) {
    $int = mt_rand(0, 1262055681);

    return [
        'device_uuid'  => function () {

            $device = factory(Device::class)->create();
            factory(ApiRequest::class, 20)->create(['device_uuid' => $device->uuid]);
            factory(Check::class, 20)->create(['device_uuid' => $device->uuid]);

            return $device->uuid;
        },
        'created_at'   => \Carbon\Carbon::createFromTimestamp($int)->format('Y-m-d H:i:s'),
        'name'         => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'email'        => $faker->email,
        'text'         => $faker->text(200),
        'app_log'      => [
            'url'        => 'post',
            'domainName' => $faker->domainName,
            'ipv4'       => $faker->ipv4,
        ],
    ];
});

$factory->define(Product::class, function ( Faker\Generator $faker ) {

    $name = $faker->word;

    return [
        'organization_id' => function () {
            return factory(Organization::class)->create()->id;
        },
        'name_ru'         => $name,
        'name_en'         => $name,
        'common_info'     => [
            'labeled_at'  => $faker->date(),
            'description' => $faker->sentence(30),
        ],
    ];
});

$factory->define(Item::class, function ( Faker\Generator $faker ) {

    return [
        'product_id'    => function () {
            return factory(Product::class)->create()->id;
        },
        'status'        => $faker->randomElement(['valid', 'fake', 'atlas']),
        'is_duplicated' => $faker->boolean(),
        'sticker'       => [
            $faker->randomElement(['excise_code', 'serial_number']) => 'U01' . str_random(40),
        ],
        'common_info'   => [],
    ];
});

$factory->define(DeviceNotification::class, function ( Faker\Generator $faker ) {

    return [
        'device_uuid' => function () {
            return factory(Device::class)->create()->uuid;
        },
        'text'        => $faker->text(),
        'is_unread'   => $faker->boolean(),
    ];
});

try {
    define('KAZ_REGIONS_IDS', \Wipon\Models\Region::whereCountryId(\Wipon\Models\Country::whereNameRu('Казахстан')->first()->id)->get()->pluck('id'));
} catch (\Exception $e) {
    log_e([$e->getMessage()]);
}

$factory->define(Receipt::class, function ( Faker\Generator $faker ) {

    copyReceiptImage();

    return [
        'device_uuid'    => $faker->randomElement(getDeviceUUIDs()),
        'region_id'      => $faker->randomElement(json_decode(KAZ_REGIONS_IDS)),
        'bin'            => $faker->numberBetween(10000000, 358888581243),
        'sum'            => $faker->randomFloat(null, 10, 50000),
        'status'         => $faker->randomElement(['processing', 'member', 'blurred', 'invalid-date', 'empty-bin', 'empty-sum', 'empty-sum', 'empty-date-time', 'winner']),
        'photo'          => 'receipts/check.jpg',
        'given_datetime' => $faker->dateTimeBetween($startDate = '-1 days', $endDate = 'now'),
        'created_at'     => $faker->dateTimeBetween($startDate = '-1 days', $endDate = 'now'),
    ];
});

$factory->define(ReceiptReport::class, function ( Faker\Generator $faker ) {

    copyReceiptImage();

    return [
        'device_uuid'            => $faker->randomElement(getDeviceUUIDs()),
        'region_id'              => $faker->randomElement(json_decode(KAZ_REGIONS_IDS)),
        'organization_name'      => $faker->company,
        'organization_address'   => $faker->address,
        'message'                => $faker->text,
        'photo'                  => 'receipts/check.jpg',
        'receipt_report_type_id' => function () use ( $faker ) {
            return ReceiptReportType::create(['name_en' => $faker->company, 'dev_name' => $faker->slug, 'name_ru' => $faker->company,])->id;
        },
        'created_at'             => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
    ];
});

/** Получение всех devise_uuid устройств, у которых есть customer_id
 *
 * @return mixed|string
 */
function getDeviceUUIDs()
{
    // Возвращаем созданную константу
    if (defined('DEVICE_UUIDs'))
        return DEVICE_UUIDs;

    // Если есть устройства с customer_id - определяем константу с массивом uuid устройств
    if (Device::whereNotNull('customer_id')->count() > 0) {
        define('DEVICE_UUIDs', Device::whereNotNull('customer_id')->take(30)->pluck('uuid')->toArray());

        return DEVICE_UUIDs;
    }

    // Если нету устройств с customer_id - создаем их и возвращаем значения  
    factory(Device::class)->create()->uuid;
    define('DEVICE_UUIDs', Device::whereNotNull('customer_id')->take(30)->pluck('uuid')->toArray());

    return DEVICE_UUIDs;
}

/**
 * Copy receipt image  from seeds/images/receipts/check.jpg tp public/images/receipts/check,jpg
 */
function copyReceiptImage()
{
    $receiptImagesDir = public_path('images/receipts');

    if ( ! File::exists($receiptImagesDir))
        File::makeDirectory($receiptImagesDir);

    if ( ! File::exists($receiptImagesDir . '/check.jpg'))
        File::copy(database_path('seeds/images/receipts/check.jpg'), $receiptImagesDir . '/check.jpg');
}

/**
 * @param \Faker\Generator $faker
 * @return array
 */
function getRequestData( Faker\Generator $faker )
{
    $path = $faker->randomElement(['v3/checks', 'v3/feedback', 'v3/notifications']);

    switch ($path) {
        case 'v3/checks':
            $content = [
                "geo"  => [
                    "latitude"  => $faker->latitude,
                    "longitude" => $faker->longitude,
                    "accuracy"  => $faker->randomDigit,
                ],
                "type" => $faker->randomElement(['excise_code', 'serial_number']),
                "code" => 'U01' . str_random(40),
            ];
            $response = getCheckResponse();
            break;
        case 'v3/feedback':
            $content = [
                "name"        => $faker->lastName . ' ' . $faker->firstName,
                "phoneNumber" => $faker->phoneNumber,
                "email"       => $faker->email,
                "text"        => $faker->text(200),
                "appLog"      => '{\"some_useful_technical_info\": [\"transactions\", \"migrations\", \"OOP\", \"polymorphism\", \"injection\"]}',

            ];
            $response = ['status' => 'success'];
            break;
        case 'v3/notifications':
            $content = [];
            $response = factory(DeviceNotification::class, 2)->create()->toArray();
            break;
        default:
            $content = [];
            $response = ['status' => 'success'];
            break;
    }

    return [$path, $content, $response];
}

function getCheckResponse()
{
    $faker = Faker\Factory::create();

    $item = factory(Item::class)->create()->toArray();
    $check = factory(Check::class, 5)->create()->toArray();

    return [
        "item"          => $item,
        "personal_item" => [
            "is_duplicated" => $faker->boolean,
        ],
        "common_info"   => [
            "bottling_date" => $faker->dateTime,
        ],
        "scans"         => $check,
    ];
}

// Wipon Pro

$factory->define(FeedbackMessagePro::class, function ( Faker\Generator $faker ) {
    $int = mt_rand(0, 1262055681);

    return [
        'user_id'      => function () {
            return factory(UserPro::class)->create()->id;
        },
        'name'         => $faker->name,
        'phone_number' => $faker->phoneNumber,
        'email'        => $faker->email,
        'text'         => $faker->text(200),
        'app_log'      => [
            'url'        => 'post',
            'domainName' => $faker->domainName,
            'ipv4'       => $faker->ipv4,
        ],
        'created_at'   => \Carbon\Carbon::createFromTimestamp($int)->format('Y-m-d H:i:s'),
    ];
});

$factory->define(UserPro::class, function ( Faker\Generator $faker ) {
    $int = mt_rand(0, 1262055681);

    return [
        'api_token'         => $faker->domainName,
        'phone_number'      => $faker->phoneNumber,
        'device'            => [
            'model'      => $faker->randomElement(['SP34', 'SG 45', 'SPF 34']),
            'platform'   => $faker->randomElement(['IOS', 'Android', 'Windows']),
            'push_token' => $faker->randomAscii,
        ],
        'name'              => str_random(10),
        'email'             => $faker->safeEmail,
        'app_version'       => $faker->randomElement(['2.0.1', '2.3.4', '2.1.4']),
        'app_language'      => 'ru',
        'work_phone_number' => $faker->phoneNumber,
        'created_at'        => \Carbon\Carbon::createFromTimestamp($int)->format('Y-m-d H:i:s'),
    ];
});

$factory->define(ApiRequestPro::class, function ( Faker\Generator $faker ) {

    list($path, $content, $response) = getRequestData($faker);

    return [
        'method'     => $faker->randomElement(['POST', 'GET']),
        'client_ip'  => $faker->ipv4,
        'url'        => $faker->url,
        'path'       => $path,
        'user_id'    => function () {
            return factory(UserPro::class)->create()->id;
        },
        'header'     => [
            'slug' => $faker->slug,
        ],
        'content'    => $content,
        'response'   => $response,
        'created_at' => $faker->dateTime,
    ];
});

$factory->define(StorePro::class, function ( Faker\Generator $faker ) {
    return [
        'user_id' => UserPro::first()->id,

        'region_id'      => function () {
            return RegionPro::first()->id;
        },
        'city'           => $faker->city,
        'name'           => $faker->word,
        'bin'            => $faker->word,
        'legal_name'     => $faker->word,
        'license_number' => $faker->uuid,
        'latitude'       => $faker->latitude,
        'longitude'      => $faker->longitude,
        'radius'         => $faker->numberBetween(1, 100),
        'street'         => $faker->word,
        'house'          => $faker->word,
        'created_at'     => $faker->dateTime,
    ];
});

$factory->define(\Wipon\Models\Pro\Notification::class, function ( Faker\Generator $faker ) {
    return [
        'user_id'    => UserPro::inRandomOrder()->id,
        'text'       => $faker->sentence(5),
        'is_unread'  => $faker->boolean(20),
        'created_at' => $faker->dateTime,
    ];
});

$factory->define(\Wipon\Models\Pro\Subscription::class, function ( Faker\Generator $faker ) {
    try {
        $id = UserPro::inRandomOrder()->first()->id;
    } catch (\Exception $e) {
        log_e($e);
        $id= 1;
    }

    return [
        'user_id'    => $id,
        'is_active'  => $faker->boolean(20),
        'expires_at' => \Carbon\Carbon::now()->addDays(2)->format('Y-m-d H:i:s'),
        'created_at' => $faker->dateTime,
    ];
});
