// Import modules
var elixir = require( 'laravel-elixir' );
require('laravel-elixir-vueify');
require('./elixir-extensions');

// DISABLE gulp-notify notifications
if (process.argv[2] == '--production') {
    process.env.DISABLE_NOTIFIER = true;
}

var bowerDir = './resources/assets/bower/';

// Without this config browserify does nor watch changes
elixir.config.js.browserify.watchify.options.poll = true;

elixir( function(mix) {
  
    mix.styles( ['app.css', 'products.css' ], 'public/css/app.css' )
        .styles( 'error.css', 'public/css/error.css' )
        .copy( 'resources/assets/bower/font-awesome/fonts', 'public/build/fonts' )
        .copy( 'resources/assets/bower/bootstrap/fonts', 'public/build/fonts' )
        .copy( 'resources/assets/fonts/bebas', 'public/build/fonts' )
        .copy( 'resources/assets/fonts/lato/Lato-Hairline.ttf', 'public/build/fonts/Lato-Hairline.ttf' )
        .styles( [
            // Шаблон который мы используем - centaurus - использует определённую версию bootstrap
            '../css/bootstrap.css',
            '../css/theme_styles.css',
            'awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
            'font-awesome/css/font-awesome.css',
            'fuelux/dist/css/fuelux.css',
            'cropperjs/dist/cropper.css',
            'datatables.net-dt/css/jquery.dataTables.css',
            'select2/dist/css/select2.css',
            'sweetalert/dist/sweetalert.css',
            'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.min.css',
            'eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
        ], 'public/css/vendor.css', bowerDir )
        .scripts( [
            'jquery/dist/jquery.js',
            'jquery-ui/jquery-ui.js',
            'bootstrap/dist/js/bootstrap.min.js',
            'fuelux/dist/js/fuelux.js',
            'fuelux/js/wizard.js',
            'select2/dist/js/select2.min.js',
            'select2/dist/js/i18n/ru.js',
            'cropperjs/dist/cropper.js',
            'sweetalert/dist/sweetalert-dev.js',
            'datatables.net/js/jquery.dataTables.min.js',
            'bootstrap3-wysihtml5-bower/dist/bootstrap3-wysihtml5.all.min.js',
            'bootstrap3-wysihtml5-bower/dist/locales/bootstrap-wysihtml5.ru-RU.js',
            'moment/min/moment-with-locales.js',
            'moment-timezone/builds/moment-timezone-with-data.js',
            // Set app moment/locale to "ru" before eonasdan-bootstrap-datetimepicker- IMPORTANT!!!
            '../js/vue/config/setMomentLocale.js',
            'eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js',
            'i18n-phonenumbers/dist/i18n.phonenumbers.min.js',
            
        ], 'public/js/vendor.js', bowerDir )
        .scripts( [
            'libs/html5shiv.js',
            'libs/respond.js'
        ], 'public/js/forIE9.js' )
        .babel( [
            'libs/theme.js',
            'pure_js/services/Helper.js',
            'pure_js/services/AjaxLoader.js',
            'pure_js/services/ImageService.js',
            'pure_js/services/Server.js',

            'pure_js/App.js',
            'pure_js/IndexPage.js',

            'pure_js/products/Product.js',
            'pure_js/products/ProductIndex.js',
            'pure_js/products/ProductWizard.js',
            'pure_js/products/VisualFeature.js'

        ], 'public/js/app.js')
        .browserify('vue/main.js', 'public/js/vue_app.js' )
        .generate_i18n()
        .version( ['css/app.css', 'css/vendor.css', 'js/vendor.js', 'js/app.js', 'js/forIE9.js', 'js/vendor.js', 'js/vue_app.js', 'js/i18n/localization.json',] );
} );
