import mFilter from "../mixins/m-filter-logic";

export default {
    mixins: [ mFilter ],
    components: {
        'v-header': require( "../components/v-header.vue" ),
        'v-table': require( "../components/v-table.vue" ),
        'v-cell': require( "../components/v-cell.vue" ),
        'v-table-button': require( "../components/v-table-button.vue" ),
        'v-bootstrap-modal': require( '../components/v-bootstrap-modal.vue' ),
        'v-button': require( '../components/v-button.vue' ),
    },
    events: {
        /**
         * События history-back загружает данные предыдущей страницы вызывая метод getItems из ../mixins/m-filter-logic.
         * Вызов события осуществляется в файле ../main.js
         * 
         * Событие срабатывает по умолчанию на всех страницах куда импортируется данный миксин
         * Метод getItems определён в ../mixins/m-filter-logic. 
         * Если вам необходимо выполнить кастомный запрос на получение данных(не getItems),
         * то необходимо в компоненте в filterData определить кастомное событие и название события записать в 
         * поле filterData.historyEventName
         *
         * @param event
         */
        'history-back': function(event={}) {
            this.getItems( event );
        }
    },
}