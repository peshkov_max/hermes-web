var URI = require( 'urijs' );

/**
 Реализует логику фильтра
 - Делает запрос с параметрами filterData или params.filter
 - Организует хранение истории в historyApi
 - Организует формирование URL для отображения в адресной строке браузера
 - Парсинг URL из адресной строки браузера для заполнения vm.filterData
 - Реализует сброс фильтра до первоначального состояния

 Как использовать этот миксин?
 1. Подключаем этот миксин:
 - import mFilter from '../../mixins/m-filter-logic'

 2. Включаем его в компонент:
 - mixins: [mFilter],

 2. В модели(методе data возвращающий объект) определяем свойство filterData
 (примеры смотрите в supports/vm-index, devices/vm-index, checks/vm-stat и др.)

 3. Привязываем объекты filterData к элементам в шаблоне
 (примеры смотрите в supports/vm-index, devices/vm-index, checks/vm-stat и др.)

 4. По умолчанию миксин ожидает, что в ответ на запрос будет возвращен объект Laravel - pagination
 (@see https://laravel.com/docs/5.2/pagination#converting-results-to-json)

 Но если у вас специфический запрос, вы можете установить возвращаемы значения в
 модель самостоятельно. Для этого нужно в компоненте, куда вставляется микмин m-filter-logic,
 создать метод который будет вызываться от всей страницы для запроса на бекенд за данными (вместо getItems из  m-filter-logic).
 В этом методе будет осуществляться вызов метода getItems и туда будет прокинут каллбек
 success (пример реализации в vm-stats, vm-maps - метод getStat)

 5. По умолчанию запрос осуществляется с данными filterData. Но вы можете
 сконфигурировать данные для отправки параметров запроса. При этом нужно реализовать
 такую же структуру как в п 4(см. пункт выше) и передать объект filter
 (пример реализации в vm-stats, vm-maps - метод getStat)

 Как работает история и переходы назад в браузере?

 Для организации логики хранения истории используется History API
 @see https://developer.mozilla.org/ru/docs/Web/API/History_API

 После каждого запроса осуществляется запись текущего состояния filterData
 и соответствующий этому состоянию URL в браузере. При этом URL записывается
 как текущий в строку запроса.

 Когда пользователь нажимает на кнопку "Назад" в браузере - осуществляется событие
 onpopstate (определение этого метода смотрите в m-filter-logic, метод created)
 Если состояние в истории есть - вызывается событие trigger-component-event
 главного инстанса приложения (см. main.js). В событие в качестве первого
 параметра передается название события которое нужно вызывать, а в качестве второго -
 объект { historyCall: true }. Для всех index страниц вызывается событие по умолчанию -
 history-back. Оно реализовано в миксине m-index. Это событие вызывает метод getItems({ historyCall: true }),
 где historyCall - сигнализирует о том, что происходит запрос, который не нужно записывать в историю
 */
import mNavigator from './m-navigator';

export default {
    mixins:[mNavigator],
    created(){
        let vm = this;

        // Установка значений фильтров при первоначальной загрузке страницы
        if ( vm.hasGetParamsInUrl() )
            this.setFilterParams( URI.parseQuery( URI().query() ) );
        
        window.onpopstate = function( e ) {

            vm.pullFromHistory();


            if ( e.state ) {
                
                // Установка значений фильтров и загрузка данных предыдущей страницы 
                vm.filterData = e.state;
                
                /*
                 |    Если вы не отправляете на бэкенд весь filterData, а формируете
                 |    отправляемые параметры с помощью метода configureSendData то
                 |    для корректной работы истории необходимо на вашей странице определить
                 |    событие, которое будет отвечать за переход назад по истории. 
                 |    см. resources/assets/js/vue/views/receipts/vm-winners.vue:365
                 |    
                 |    Также в filterData нужно передать название этого события. 
                 |    см. resources/assets/js/vue/views/receipts/vm-winners.vue:171
                 |    
                 |    а в методе get{название сущности} реализовать передачу параметра historyCall
                 |    см. resources/assets/js/vue/views/receipts/vm-winners.vue:232
                 |      
                 * */
                vm.$dispatch( 'trigger-component-event', vm.getHistoryBackEventName(), {historyCall: true} );
            }
            else
                window.history.back();
        };
    },
    methods: {
        /**
         * Определяет, есть ли get параметры в запросе
         *
         * @returns {boolean}
         */
        hasGetParamsInUrl(){
            let query       = URI.parseQuery( URI().query() );
            let queryLength = 0;

            for (let o in query) queryLength++;

            return queryLength > 0;
        },

        /***
         * Выполняет запрос фильтра
         *
         * Значение url берутся из:
         * 1 параметра params.url,
         * 2 через название модели в tableData.model,
         * 3 vm.tableData.useModelInUrl.alternativeUrl
         *
         * Если в параметрах метода есть свойство filter - оно будет использовано для как
         * данные отправляемые на бекенд, иначе параметрами запроса будет vm.filterData
         *
         * Если не прокинута функция success то по умолчанию ожидается, что с бекенда
         * придет объект pagination? иначе response будет передан методу success
         *
         * Если в аргументе метода прокинуто свойство historyCall= true - это означает,
         * что вызов getItems произведён после нажатия на кнопку Назад браузера(см метод created).
         * Ожидается что мы перешли на рпедыдущую страницу и пожтому записывать историю не нужно
         *
         *
         *
         * @param params = { 
         *                     url?: String,
         *                     historyCall?: Boolean,
         *                     success?: Function,
         *                     filter?: Object 
         *                }
         */

        getItems( params = {} ) {

            let vm = this;
            let url = params.url ? params.url : (vm.tableData.useModelInUrl.status ? vm.tableData.model : vm.tableData.useModelInUrl.alternativeUrl);
            
            vm.$http.get( url, params.filter ? params.filter : vm.filterData ).then( function( response ) {

                if ( typeof params.success === 'function' )
                    params.success( response );
                else
                    vm.pagination = response.data;

                if ( !params.historyCall )
                    window.history.pushState( vm.filterData, '', URI().query( vm.configureUrlForHistory() ).href() );

                // установка стилей hover для шапки таблицы
                vm.$dispatch( 'trigger-component-event', 'init-header-hover-styles' );

            }, function( response ) {

                this.$set( 'pagination', { data: [] } );

                if ( response.status == 401 )
                    window.open( URI( URI().origin() ).segmentCoded( [ 'login' ] ).toString(), "_self" );

                if ( typeof params.error === 'function' )
                    params.error( response );

                console.error( response );
            } );
        },

        /**
         * Возвращает строку get параметров, которая в дальнейшем будет
         * отображаться в качестве текущего url страницы
         *
         * @returns string
         */
        configureUrlForHistory(){

            let vm        = this;
            let urlParams = {};

            if ( ! vm.filterData ) {
                return;
            }

            Object.keys( vm.filterData ).forEach( function( key ) {

                if ( key == 'page' && vm.filterData ) {
                    urlParams[ 'page' ] = vm.filterData.page;
                }

                if ( vm.filterData.hasOwnProperty( key ) && typeof vm.filterData[ key ] === 'object' )

                    switch (key) {
                        case 'search':
                            urlParams = vm.configureSearchForHistory( urlParams );
                            break;
                        case 'sort':
                            urlParams = vm.configureSortForHistory( urlParams );
                            break;
                        case 'checkboxes':
                            urlParams = vm.configureCheckboxForHistory( urlParams );
                            break;
                        case 'radios':
                            urlParams = vm.configureRadioForHistory( urlParams );
                            break;
                        case 'selects':
                            urlParams = vm.configureSelectForHistory( urlParams );
                            break;
                        case 'period':
                            urlParams = vm.configurePeriodForHistory( urlParams );
                            break;
                        case 'switches':
                            urlParams = vm.configureSwitchesForHistory( urlParams );
                            break;
                        default:
                            if ( key != 'buttons' )
                                console.warn( `Type of filter: ${key} is not supported by the m-filter-logic mixin` );
                            break
                    }
            } );

            return jQuery.param( urlParams );
        },

        /**
         * Настройка полей search для записи в текущий url
         * @param urlParams
         * @returns {{}}
         */
        configureSearchForHistory( urlParams = {} ){

            let vm = this;

            Object.keys( vm.filterData.search ).forEach( function( field ) {

                if (
                    vm.objectHasObjectField( vm.filterData.search, field ) &&
                    vm.filterData.search[ field ][ 'value' ] !== ''
                ) {
                    urlParams[ 'search' ]          = urlParams[ 'search' ] || {};
                    urlParams[ 'search' ][ field ] = vm.filterData.search[ field ][ 'value' ];
                }
            } );

            return urlParams;
        },

        /**
         * Настройка полей switches для записи в текущий url
         * @param urlParams
         * @returns {{}}
         */
        configureSwitchesForHistory( urlParams = {} ){

            let vm = this;

            if ( !vm.filterData.switches )
                return urlParams;

            Object.keys( vm.filterData.switches ).forEach( function( field ) {
                if (
                    vm.objectHasObjectField( vm.filterData.switches, field ) &&
                    vm.filterData.switches[ field ].selected !== ''
                ) {
                    urlParams[ 'switch' ]          = urlParams[ 'switch' ] || {};
                    urlParams[ 'switch' ][ field ] = vm.filterData.switches[ field ].selected;
                }
            } );

            return urlParams;
        },

        /**
         * Настройка полей sort для записи в текущий url
         * @param urlParams
         * @returns {{}}
         */
        configureSortForHistory( urlParams = {} ){

            urlParams[ 'sort' ]                = {};
            urlParams[ 'sort' ][ 'name' ]      = this.filterData.sort.name;
            urlParams[ 'sort' ][ 'direction' ] = this.filterData.sort.direction;

            return urlParams;
        },
        /**
         * Настройка полей checkbox для записи в текущий url
         * @param urlParams
         * @returns {{}}
         */
        configureCheckboxForHistory( urlParams = {} ){

            let vm = this;

            Object.keys( vm.filterData.checkboxes ).forEach( function( field ) {
                if ( vm.objectHasObjectField( vm.filterData.checkboxes, field ) ) {
                    urlParams[ 'checkbox' ]          = urlParams[ 'checkbox' ] || {};
                    urlParams[ 'checkbox' ][ field ] = vm.filterData.checkboxes[ field ][ 'checked' ];
                }
            } );
            return urlParams;
        },
        /**
         * Настройка полей radio для записи в текущий url
         * @param urlParams
         * @returns {{}}
         */
        configureRadioForHistory( urlParams = {} ){

            let vm = this;

            vm.filterData.radios.forEach( function( radioObject ) {
                if ( radioObject.hasOwnProperty( 'use' ) &&
                    radioObject.use === true &&
                    radioObject.hasOwnProperty( 'name' ) &&
                    radioObject.hasOwnProperty( 'selected' ) &&
                    radioObject.selected !== '' ) {
                    urlParams[ 'radio' ]                          = urlParams[ 'radio' ] || {};
                    urlParams[ 'radio' ][ radioObject[ 'name' ] ] = radioObject[ 'selected' ];
                }
            } );

            return urlParams;
        },
        /**
         * Настройка полей select для записи в текущий url
         * @param urlParams
         * @returns {{}}
         */
        configureSelectForHistory( urlParams = {} ){

            let vm = this;

            Object.keys( vm.filterData.selects ).forEach( function( field ) {
                if ( vm.objectHasObjectField( vm.filterData.selects, field ) ) {
                    urlParams[ 'select' ]          = urlParams[ 'select' ] || {};
                    urlParams[ 'select' ][ field ] = {};
                    urlParams[ 'select' ][ field ]['value'] = vm.filterData.selects[ field ][ 'selected' ];
                    
                    if ( vm.filterData.selects[ field ].useAjax
                        && vm.filterData.selects[ field ][ 'selected' ] != null
                        && vm.filterData.selects[ field ][ 'selected' ] !== ''
                        && vm.filterData.selects[ field ][ 'initialState' ] ) {
                        urlParams[ 'select' ][ field ]['text'] = vm.filterData.selects[ field ][ 'initialState' ]['text'];
                    }
                }
            } );
 
            return urlParams;
        },
        /**
         * Настройка полей period для записи в текущий url
         * @param urlParams
         * @returns {{}}
         */
        configurePeriodForHistory( urlParams = {} ){
            let vm = this;

            Object.keys( vm.filterData.period ).forEach( function( field ) {
                urlParams[ 'period' ]          = urlParams[ 'period' ] || {};
                urlParams[ 'period' ][ field ] = vm.filterData.period[ field ];
            } );
            return urlParams;
        },
        /**
         * Проверяет есть ли у объекта object дочерний объект с именем childObject
         *
         * @param object
         * @param childObjectName
         * @returns {boolean}
         */
        objectHasObjectField( object = {}, childObjectName = '' ){
            return object.hasOwnProperty( childObjectName ) && typeof object[ childObjectName ] === 'object';
        },

        /**
         * Устанавливает значения из URL в модель vm.filterData
         * @param query
         */
        setFilterParams( query = '' ){

            let vm = this;

            let initialState = vm.initialState();

            if ( !vm.filterData )
                return;
            
            Object.keys( query ).forEach( function( field ) {

                try {
                    if ( field == 'page' ) {
                        vm.filterData.page = query[ field ];
                        return;
                    }

                    const { filterType, fieldName } = vm.parseFieldName( field );

                    switch (filterType) {
                        case 'sort':
                            switch (fieldName) {
                                case 'name':
                                    if ( vm.filterData.sort )
                                        vm.filterData.sort.name = query[ field ] || initialState.sort.name;
                                    break;
                                case 'direction':
                                    if ( vm.filterData.sort )
                                        vm.filterData.sort.direction = query[ field ] || initialState.sort.direction;
                                    break;
                                default:
                                    if ( vm.filterData.sort ) {
                                        vm.filterData.sort.name = initialState.sort.name;
                                        vm.filterData.sort.direction = initialState.sort.direction;
                                    }
                                    break;
                            }
                            break;
                        case 'search':
                            if ( vm.objectHasObjectField( vm.filterData.search, fieldName ) )
                                vm.filterData.search[ fieldName ][ 'value' ] = query[ field ];
                            break;
                        case 'checkbox':
                            if ( vm.objectHasObjectField( vm.filterData.checkboxes, fieldName ) )
                                vm.filterData.checkboxes[ fieldName ][ 'checked' ] = query[ field ] === 'true';
                            break;
                        case 'radio':
                            vm.filterData.radios.forEach( function( radio ) {
                                if ( radio.hasOwnProperty( 'name' ) && radio.name === fieldName ) {
                                    radio[ 'use' ]      = true;
                                    radio[ 'selected' ] = query[ field ];
                                }
                            } );
                            break;
                        case 'select':
                            if (! vm.filterData.selects[ fieldName ]) {
                                return;
                            }

                            vm.handleSettingSelect2(fieldName, query, field);
                            break;
                        case 'period':
                            if ( vm.filterData.period )
                                vm.filterData.period[ fieldName ] = query[ field ];
                            break;
                        case 'switch':
                            if ( vm.filterData.switches )
                                vm.filterData.switches[ fieldName ].selected = query[ field ];
                            break;
                        default:
                            console.warn( `Тип фильтра: ${filterType}  не поддерживается` );
                            break;
                    }
                }catch(e){
                  console.error(e);
                }
                
            } )
        },


        handleSettingSelect2(fieldName, query, field){
            
            let vm = this;
            
            if (_.includes(field, 'value') || _.includes(field, 'text')){

                if (vm.filterData.selects[ fieldName ].hasOwnProperty('initialState')){

                    if (_.includes(field, 'value')){
                        vm.filterData.selects[ fieldName ][ 'initialState' ]['id'] = query[ field ];
                        vm.filterData.selects[ fieldName ][ 'selected' ] = query[ field ];
                    }

                    if (_.includes(field, 'text')){
                        vm.filterData.selects[ fieldName ][ 'initialState' ]['text'] = query[ field ];
                    }


                }else {
                    if (_.includes(field, 'value') ){
                        vm.filterData.selects[ fieldName ][ 'selected' ] = query[ field ];
                    }
                }
            }

            else  if ( vm.objectHasObjectField( vm.filterData.selects, fieldName ) ) {
                vm.filterData.selects[ fieldName ][ 'selected' ] = query[ field ];
            }
        },
        /**
         *  Функция URI.parseQuery( ) возвращает объект вида:
         *  {
         *      checkbox[checkTypeHand]:"true"
         *      checkbox[checkTypeScan]:"false"
         *      checkbox[ukmTypesAll]:"false"
         *      checkbox[ukmTypesAtlas]:"false"
         *      checkbox[ukmTypesDuplicated]:"true"
         *      checkbox[ukmTypesFake]:"false"
         *      checkbox[ukmTypesValid]:"false"
         *      select[selectYear]:"2016"
         *      sort[direction]:"asc"
         *      sort[name]:"region_name"
         *  }
         *  где checkbox[checkTypeHand] - ключь, а "true" - значение свойства объекта
         *
         *  Данная функция парсит строку checkbox[checkTypeHand] и возвращает тип (checkbox)
         *  и название поля этого типа (checkTypeHand)
         *
         *  Используется деструктуризация при возвращении значений
         *  @see  https://learn.javascript.ru/destructuring
         *
         * @param filterFieldName
         * @returns {{filterType: *, fieldName: *}}
         */
        parseFieldName( filterFieldName = '' )
        {
            let regexResult = filterFieldName.split( /([a-z]+)(\[)([a-zA-Z_]+)(\])/ );

            let filterType = regexResult[ 1 ];

            if ( !filterType )
                console.warn( filterFieldName, filterType, 'Filter type was not presented in given params' );

            let fieldName = regexResult[ 3 ];

            if ( !fieldName )
                console.warn( filterFieldName, filterType, 'Filter name was not presented in given params' );

            return { filterType, fieldName };
        },
        /***
         * Сброс значений фильтра
         *
         * Установка vm.filterData в первоначальное положение и выполнения запроча getItems
         */
        resetFilter( params = {} ){

            this.filterData.resetFilter = true;
            let initialState            = this.initialState();

            if ( typeof params.setParams === 'function' ) {
                params.setParams( initialState );
            }

            /*** Выставление фильтра - за период, если есть*/
            if ( initialState.filterData.period ) {
                this.$broadcast( 'set-filter-data-period', initialState.filterData.period );
            }

            this.$data = initialState;

            if ( typeof params.getData === 'function' ) {
                params.getData();
                return;
            }
            
            this.getItems( params );
        },
        /**
         * Возвращает текущее название события history-back
         * @returns {string}
         */
        getHistoryBackEventName(){
            return this.filterData.historyEventName
                ? this.filterData.historyEventName
                : 'history-back'
        },

        /*** Логика этого метода работает только в vm-maps и vm-stat */
        getUkmAllStat(){
            this.$set( 'filterData.checkboxes.ukmTypesFake.checked', false );
            this.$set( 'filterData.checkboxes.ukmTypesValid.checked', false );
            this.$set( 'filterData.checkboxes.ukmTypesDuplicated.checked', false );
            this.$set( 'filterData.checkboxes.ukmTypesAtlas.checked', false );
            this.getStat();
        },
        /*** Логика этого метода работает только в vm-maps и vm-stat */
        getUkmNotAllStat(){
            this.$set( 'filterData.checkboxes.ukmTypesAll.checked', false );
            this.getStat();
        },
        /*** Логика этого метода работает только в vm-maps и vm-stat */
        getFilterCheckType(){
            let checkType;

            if ( this.filterData.checkboxes.checkTypeHand.checked === true && this.filterData.checkboxes.checkTypeScan.checked === true )
                checkType = 'both';

            if ( this.filterData.checkboxes.checkTypeHand.checked !== true && this.filterData.checkboxes.checkTypeScan.checked !== true )
                checkType = 'nothing';

            if ( this.filterData.checkboxes.checkTypeHand.checked !== true && this.filterData.checkboxes.checkTypeScan.checked === true )
                checkType = 'scan';

            if ( this.filterData.checkboxes.checkTypeHand.checked === true && this.filterData.checkboxes.checkTypeScan.checked !== true )
                checkType = 'hand';

            return checkType;
        },
        
        /**
         * Преобразование radio элемента в объект
         */
        transformRadioForRequest( ){

            let vm    = this;
            let objectToReturn = {};

            vm.filterData.radios.forEach( function(key) {
                objectToReturn[key.name] = {
                    use: key.use,
                    selected: key.selected
                };
            } );

            return objectToReturn;
        },
    }
}