var URI = require( 'urijs' );

export default {
    data(){
        return {
            stackLength: 5,
            needHistoryPushing: true,
            needHistoryPulling: true
        }
    },
    methods: {

        /*
         |
         |  Секция с методами для работы с URL
         |
         */
        goTo( ...arg ){

            // save current url in navigation history before we wo to another page
            this.needHistoryPushing && this.pushToHistory( URI().toString() );

            // если вам НЕ НУЖНО сохранять URL в истории используйте данную переменную
            // установите в false перед вызовом goTo
            // пример: resources/assets/js/vue/views/product-types/vm-form.vue:173
            this.needHistoryPushing = true;

            // если возникает ситуация когда вам нужно перейти на предыдущую страницу и сделать так, 
            // чтобы эта предыдущая страница пропала из истории см resources/assets/js/vue/views/product-types/vm-form.vue:195
            let vm =this;
            
            arg.forEach(obj => {
            
                if ( typeof obj === 'object'
                    && obj.hasOwnProperty( 'navigationConfiguration' )
                    && obj.navigationConfiguration.hasOwnProperty( 'pullLatestUrl' )
                )
                {
                    (obj.navigationConfiguration.pullLatestUrl === true) && (vm.pullFromHistory());
                }
            });

            arg = arg.filter( element=> typeof element !== 'object' );

            // go to another page
            window.open( URI( URI().origin() ).segmentCoded( arg ).toString(), "_self" );
        },
        goBack( resource = null ){

            if ( !this.needHistoryPulling ) {

                this.needHistoryPulling = true;
                go_back();
                return
            }
            
            // for testing goals
            let vm = this;
            const url = vm.pullFromHistory()

            if ( url ) {
                go_back( url )
                return
            }

            if ( resource ) {
                location.replace( URI().origin() + '/' + resource );
                return
            }

            go_back();
        },
        goBackNotPull( resource = null ){
            if ( resource ) {
                location.replace( URI().origin() + '/' + resource );
                return
            }
            
            this.needHistoryPulling = true;
            go_back();
        },

        goToBlank( ...arg ){
            window.open( URI( URI().origin() ).segmentCoded( arg ).toString(), "_blank" );
        },
        goWithParamsToSegments( obj = {}, ...arg ){
            window.open( URI( URI().origin() ).segmentCoded( arg ).search( jQuery.param( obj ) ).toString(), "_self" );
        },
        goWithParamsToSegmentsBlank( obj = {}, ...arg ){
            window.open( URI( URI().origin() ).segmentCoded( arg ).search( jQuery.param( obj ) ).toString(), "_blank" );
        },
        getLinkTo( ...arg ){
            return URI( URI().origin() ).segmentCoded( arg ).toString();
        },
        segmentCodedSearchURL( obj = {}, ...arg ){
            return URI( URI().origin() ).segmentCoded( arg ).search( jQuery.param( obj ) ).toString();
        },

        getHistory(){

            let history = null

            try {
                history = JSON.parse( window.localStorage.getItem( 'cabinet_navigation_history' ) )
            } catch (e) {
                history = [];

                console.error( 'Cant get variable from cabinet_navigation_history', e )
            }

            !history && console.warn( 'History is no set' )

            return history === null ? [] : history
        },
        setHistory( history = null ){

            if ( !history ) {
                console.warn( 'You are trying to set empty history to localStorage.' )
                return;
            }

            window.localStorage.setItem( 'cabinet_navigation_history', JSON.stringify( history ) )
        },
        pushToHistory( url = 'You tried to push empty url to history' ){

            let history = this.getHistory()

            if ( history.length >= this.stackLength ) {
                history.shift()
            }

            history.push( url )

            this.setHistory( history )
        },
        pullFromHistory(){

            let history = this.getHistory()

            if (! history || history.length === 0 ) {
                console.warn( 'You try to pull url from history. But history is empty now' )
                return
            }

            const item = history.pop()

            this.setHistory( history )

            return item || null
        },
    }

}