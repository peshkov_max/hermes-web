/*
 | Блок заголовка с инструкцией ( inline help )
 | Страницы использующие данный миксин обычно вставляют кнопку 
 | вызова модального окна с инструкцией по странице 
 | См. пример: resources/assets/js/vue/views/inline-help/h-cities.vue
 | 
 */
import mGlobal from '../mixins/m-global';

export default {
    props: ['title', 'total'],
    mixins: [ mGlobal ],
    components: {
        'v-bootstrap-modal': require( '../components/v-bootstrap-modal.vue' ),
        'v-dropdown': require( '../components/v-dropdown.vue' ),
    },
    computed: {
        staticServer(){
            return this.$root.$refs.vm.staticServer;
        }
    },
    methods:{
        /**
         * Получить заголовок
         * 
         * @returns {string}
         */
        getPageTitle(){
            try {

                return this.total
                    ? translate(this.title.label, this.title.params) + ' (' + this.total + ')'
                    : translate(this.title.label, this.title.params);

            }catch(e){
                console.error(e);
                return translate('app.header_not_set') ;
            }
        },
        /**
         * Открыть модальное окно
         * 
         * @param { string } id - id модального окна 
         */
        open(id){
            if (!id) {return}

            $( '#' + id ).modal( 'show' );
        },
    },
}
