/*
 | Реализует логику формирования отчета
 | Используется в vm-registry, vm-statistics, vm-winners, vm-messages
 */
export default {

    data(){
        return {
            loaderId: 'excel_loader',
            downloadId: 'download-link'
        }
    },
    methods: {

        /**
         * Установить надпись о выгрузке отчета
         *
         * @param tipContainer - селектор блока, в который будет вставлена надпись
         */
        setExportLoader( tipContainer = '.filter-block' ){

            let loaderText = translate( 'app.report_is_forming' );
            let id         = 'excel_loader';
            let counter    = 0;
            let prefix     = '.';

            let vm = this;

            $( tipContainer ).append( `<h2 id="${vm.loaderId}">${loaderText}</h2>` );

            setInterval( () => {
                let $block = $( `#${vm.loaderId}` );

                if ( counter === 3 ) {
                    counter = 0;
                    $block.text( loaderText + _.repeat( prefix, counter ) );
                    return;
                }

                counter++;
                $block.text( loaderText + _.repeat( prefix, counter ) );
            }, 200 );
        },

        /**
         * Удаление надписи о выгрузке отчета и добавление кнопки - ссылки на отчет
         *
         * @param linkContainer - селектор блока, в который необходимо вставить ссылку на отчет
         * @param url
         */
        initReportDownloadLink( linkContainer = '.filter-block', url = '' ){

            let vm    = this;
            let title = translate( 'app.download_link' );

            $( `#${vm.loaderId}` ).remove();

            $( linkContainer ).append( `<a class="btn btn-success" 
                                             style="margin-bottom: 10px;"
                                             href="${url}" 
                                             id="${vm.downloadId}">
                                            <i class="fa fa-file-excel-o"></i> ${title}
                                          </a>` );

            $( `#${vm.downloadId}` ).on( 'click', e => $( `#${vm.downloadId}` ).remove() );
        },

        /**
         * Запрос на формирование отчет
         *
         * @param reportUrl - URL на который будет отправлены параметры для формирования отчета
         * @param params    - Параметры, которые будут переданы в запросе для формирования отчета
         */
        getReport( reportUrl = 'export-receipts', params = { divToInsertSelector: '.row.col-sm-12' } ){

            if ( this.pagination && this.pagination.data && this.pagination.data.length ===0 ){
                
                alert(translate( 'app.not_record_report' ));
                return
            }

            this.setExportLoader( params.divToInsertSelector );

            this.$http
                .get( reportUrl, this.configureSendData( { withReport: true } ) )
                .then( response => {

                        if ( response.data.url ) {

                            this.initReportDownloadLink( params.divToInsertSelector, response.data.url );

                            (typeof params.success === 'function') && params.success();
                        }
                        else {
                            console.error( 'Error during report generation' );
                        }
                    },
                    response => {
                        console.error( response );
                    }
                ).bind( this );
        }
    }
}
