var URI = require( 'urijs' );

import mNavigator from "./m-navigator";

export default {
    mixins: [ mNavigator ],
    props: {
        params: {
            type: Object, default: function() {
                return {};
            }
        },
        roles: { type: String },
        defaultImagePath: { type: String, default: '../images/components/nopic.jpg' },
        currentLocale: { type: String, default: 'ru' },
        staticServer: { type: String },
        userTimeZone: { type: String, default: 'Asia/Almaty' },
        momentFormats: {
            type: Object, default: function() {
                return {
                    date: 'DD.MM.YYYY',
                    full: 'DD.MM.YYYY HH:mm',
                    timestamp: 'DD.MM.YYYY HH:mm:ss'
                };
            }
        }
    },

    methods: {
      
        /**
         * Проверяет есть ли у пользовтеля определённая роль
         * В качестве аргументов принимает роли
         *
         * @param {array} arg         - Роли
         * @returns {boolean}
         */
        hasRole( ...arg ){

            // see resources/views/vue.blade.php:19
            let roles = window.Laravel.roles;

            return arg.some( function( v ) {
                return roles.indexOf( v ) >= 0;
            } );
        },

        /**
         * Осуществляет перенаправление на страницу авторизации в случае если сессия авторизации завершилать
         * Используется главным образом внутри http.$get, http.$post запросов
         *
         * @param  response - HTTP response
         */
        handleSessionTimeOut( response ){
            ( response && response.status == 401 ) && window.open( URI( URI().origin() ).segmentCoded( [ 'login' ] ).toString(), "_self" );
        },

        /**
         * Сравнивает объект фильтра вьюхи с пришедшим из URL объектом
         * и рекрусивно устанавливает объект для установки в форму
         *
         * @param filterFromVM
         * @param filterFromUrl
         * @returns {*}
         */
        setFilterRecursively( filterFromVM = {}, filterFromUrl = {} ){

            let vm = this;

            Object.keys( filterFromVM ).forEach( function( element ) {
                if ( filterFromUrl.hasOwnProperty( element ) ) {
                    filterFromVM[ element ] =
                        filterFromVM[ element ] && typeof filterFromVM[ element ] === 'object'
                            ? vm.setFilterRecursively( filterFromVM[ element ], filterFromUrl[ element ] )
                            : filterFromUrl[ element ];
                }
            } );

            return filterFromVM
        },

        /* Конец секции с методами для работы с URL */

        getLocalisedName(){
            return 'name_' + this.currentLocale;
        },
        getMomentMonthAgo(){
            return moment().subtract( 1, 'months' ).format( this.momentFormats.full );
        },
        getMomentYearAgo(){
            return moment().subtract( 1, 'years' ).format( this.momentFormats.full );
        },
        getMomentNow(){
            return moment().add( 1, 'hours' ).format( this.momentFormats.full );
        },
        tryCatch( callback = ()=> {}, message = 'Error:', handleErrorCallback = ()=> {} ){
            if ( !callback )
                return;

            try {
                if ( typeof callback === 'function' )
                    callback();

            } catch (e) {
                console.error( message, e );
                if ( typeof handleErrorCallback === 'function' )
                    handleErrorCallback();
            }
        },

        getCurrentTimeZone(){

            if ( this.userTimeZone === '' ) {

                console.warn( 'UserTimeZone is not specified' );

                return 'Asia/Almaty';

            } else {
                return this.userTimeZone;
            }
        },

        getYearsForSelectInput(){

            const startYear   = 2015;
            const endYear     = moment().get( 'year' );
            const arrayLength = endYear - startYear + 1;

            return new Array( arrayLength ).fill( null ).map( ( element, index ) => {
                const year = startYear + index;
                return {
                    id: year,
                    text: year
                }
            } );
        },
        setPrefixedId( title, id ){
            return title + this.trunc( id, 5 );
        },
        trunc( value, n ){
            return (value.length > n) ? value.substr( 0, n - 1 ) : value;
        },

        /**
         * Используется при инициализации полей формы с типом boolean
         * Ожидается что в параметре this.params есть поле item содержащий
         * модель сущности, а в ней - поле field_name
         *
         * Если вам необходимо инициализировать поле со значением по
         * умолчанию - передайте третий параметр
         *
         * @param {string} field_name - имя свойства в модели item_name
         * @param {string} item_name - имя переданного параметра
         * @param {boolean} default_value - значение по умолчанию
         * @returns {boolean} - значение поля field_name из item_name
         */
        initFormBoolValue( field_name = false, item_name = null, default_value = true ){

            if ( !field_name ) {
                console.warn( 'Form field name does were not given' );
                return default_value
            }

            const _item_name = item_name ? item_name : 'item';

            if ( !this.params.hasOwnProperty( _item_name ) ) {
                console.warn( 'Needed param was not given ' + _item_name + ' expected' );
                return default_value
            }

            if ( !this.params[ _item_name ].hasOwnProperty( field_name ) ) {
                console.warn( 'Item field name was expected but ' + _item_name + ' given' );
                return default_value
            }

            return this.params[ _item_name ][ field_name ] || default_value
        }
    },
    events: {

        /**
         * Определяет, есть ли у пользователя указанная роль(роли)
         *
         * Данный workaround используется в компонентах (см. компонент v-push-form, метод hasRole стр 105)
         * Чтобы не передавать в каждый компонент значение roles мы просто вызываем ивент define-user-role
         * в который прокидываем роли и callback функцию, которая в свою очередь передаст результат компоненту
         *
         * @param roles          - Роли
         * @param callback       - Callback функция, в которую передается результат вызова функции hasRole
         * @returns {boolean}
         */
        'define-user-role': function( roles, callback ) {

            if ( !roles )
                return false;

            if ( typeof callback == 'function' )
                callback( this.hasRole( roles ) );
        },
        'go-back': function() {
            go_back();
        },
    }
}
