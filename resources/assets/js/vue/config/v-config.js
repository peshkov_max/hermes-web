/**
 * Author: Peshkov Maxim
 * Created: 31.05.16 12:42
 * Email:  peshkov-maximum@yandex.ru
 *
 *
 * Require:
 * - meta tag in html's head section with content = 'locale'
 *   and data-path attribute with the path to localization.json file(now path is js/i18n/localization.json)
 *
 *  1 - js/i18n/localization.json File is generated from Laravel localization files locates in path: resources/lang.
 *  2 - js/i18n/localization.json File is generated by Laravel console command "php artisan vue-i18n:generate".
 *  3 - There is an gulp task called "generate_i18n". It's definition located in elixir-extensions.js file. This task runs
 *      command "php artisan vue-i18n:generate" for generation localization purpose.
 *  4 - Finally task generate_i18n runs from gulpfile.json.
 *  5 - File change watcher for Laravel localization files works as well.
 *      So if you run "gulp watch" command and then change files in any resources/lang/*
 *      files localization will be automatically regenerated.
 *
 */

var URI = require( 'urijs' );

export default class {

    constructor( Vue ) {

        /***
         * Нужно трекать события которые осуществляет пользователь
         * Т.к. большая часть запросов идет через Ajax - нужно в
         * интерсепторе перехватывать событие и отправлять данное
         * событие в Google Analytics
         *
         * см. resources/assets/js/vue/config/v-config.js:209
         *
         * @type {string[]}
         */
        this.urlToTrack = [
            'moderate',
            'suspicious-users',
            'registry',
            'play-prize',
            'vue-products',
            'products',
            'map-filter',
            'stats',
            'statistics',
            'messages',
            'educate-receipt',
            'supports',
            'devices',
            'customers',
            'push-notifications',
            'users',
            'suggestion',
            'send-feedback',
            'roles',
            'regions',
            'educate-receipt',
            'export-messages',
            'export-receipts',
            'export-statistics',
            'organizations',
            'devices',
        ];

        /*
         * ----- SET ENV -----
         */
        this.initVueJSConfig( Vue );

        /*
         * ----- INIT DIRECTIVES -----
         */
        this.initDirectives( Vue );

        /*
         * ----- LARAVEL X-CSRF-TOKEN -----
         * 
         * Laravel object is defined here: resources/views/vue.blade.php:11
         */
        Vue.http.headers.common[ 'X-CSRF-TOKEN' ] = window.Laravel.csrf_token;
        Vue.http.headers.common[ 'Content-Type' ] = 'application/json';
        Vue.http.headers.common[ 'Accept' ]       = 'application/json';
        /*
         * ----- INIT GOOGLE ANALYTICS -----
         */
        this.initGoogleAnalytic( Vue );

        /*
         * ----- INIT INTERCEPTOR -----
         */
        this.initInterceptor( Vue );

        /*
         * ----- INIT GLOBAL HELPERS -----  
         */
        this.registerHelperFunctions( Vue );
    }

    /**
     * ----- INIT DIRECTIVES -----
     */
    initDirectives( Vue ) {

        /**
         * Решение проблемы с Bootstrap styled radio button groups
         *
         * @link http://forum.vuejs.org/topic/135/problem-binding-bootstrap-styled-radio-button-groups-with-vue-vm/3
         */
        Vue.directive( 'radio', {
            twoWay: true,
            bind: function() {
                var self = this;
                var btns = $( self.el ).find( '.btn' );
                btns.each( function() {
                    $( this ).on( 'click', function() {
                        var v = $( this ).find( 'input' ).get( 0 ).value;
                        self.set( v );
                    } )
                } );
            },
            update: function() {
                var value = this._watcher.value;
                if ( value ) {
                    this.set( value );
                    var btns = $( this.el ).find( '.btn' );
                    btns.each( function() {
                        $( this ).removeClass( 'active' );
                        var v = $( this ).find( 'input' ).get( 0 ).value;

                        if ( v === value ) {
                            $( this ).addClass( 'active' );
                        }
                    } );
                } else {
                    var input = $( this.el ).find( '.active input' ).get( 0 );
                    if ( input ) {
                        this.set( input.value );
                    }
                }
            }
        } );

        // wysihtml5 directive
        Vue.directive( 'wysihtml5-editor', {
            twoWay: true,
            bind: function() {
                var self = this;

                $( self.el ).wysihtml5( {
                    toolbar: {
                        "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                        "emphasis": true, //Italics, bold, etc. Default true
                        "lists": false, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                        "html": false, //Button which allows you to edit the generated HTML. Default false
                        "link": false, //Button to insert a link. Default true
                        "image": false, //Button to insert an image. Default true,
                        "color": false, //Button to change color of font
                        "blockquote": false, //Blockquote
                        "size": 'xs' //default: none, other options are xs, sm, lg
                    },
                    locale: "ru-Ru",
                    "events": {
                        "load": function() {
                            if ( self.vm.field.value != '' ) {
                                $( "#" + this.textarea.element.id ).data( "wysihtml5" ).editor.setValue( self.vm.field.value );
                            }
                        },
                        "change": function() {
                            try {
                                self.set( this.textarea.element.value )
                            } catch (e) {
                                console.error( e );
                            }
                        }
                    }
                } );
            },
            unbind: function( newVal, oldVal ) {

                this.el.removeEventListener( 'input', this.handler )
            }
        } );
    }

    /**
     * ----- INIT GOOGLE ANALYTICS -----
     */
    initGoogleAnalytic() {

        (function( i, s, o, g, r, a, m ) {
            i[ 'GoogleAnalyticsObject' ] = r;
            i[ r ] = i[ r ] || function() {
                    (i[ r ].q = i[ r ].q || []).push( arguments )
                }, i[ r ].l = 1 * new Date();
            a = s.createElement( o ),
                m = s.getElementsByTagName( o )[ 0 ];
            a.async = 1;
            a.src   = g;
            m.parentNode.insertBefore( a, m )
        })( window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga' );

        ga( 'create', 'UA-74065417-1', 'auto' );
        ga( 'send', 'pageview' );
    }

    /**
     * ----- INIT INTERCEPTOR -----
     */
    initInterceptor( Vue ) {

        var vm = this;

        /**
         | Проблема - при одновременном выполнении нескольких Ajax запросов
         | может возникать ситуация, при которой первый запрос установил лоадер
         | перед началом выполнения запроса. Второй запрос начал выполняться
         | параллельно и выполнился быстрее первого. После того как он
         | выполнился - он удалил лоадер. И получается ситуация, при которой
         | первый запрос ещё не завершился, а лоадера уже нет.
         |
         | Для решения данной проблемы сохраняем все url запросов в объекта
         | xhr_requests текущего класса.
         |  Структура {
         |     'url' (url запроса) : boolean (состояние лоадера - нужно ли для текущего url-а устанавливать лоадер)
         |  }
         |
         |  Смотрите методы в которых используется:
         |   - setLoader: resources/assets/js/vue/config/v-config.js:279
         |   - destroyLoader: resources/assets/js/vue/config/v-config.js:315
         */
        vm.xhr_requests = {};

        Vue.http.interceptors.push( {

            request( request ) {

                vm.notifyGoogleAnalyticService( request.url );

                vm.setLoader( request );

                return request;
            },

            response( response ) {

                vm.handleError( response );

                vm.destroyLoader( response.request.url );

                if (response && typeof response.headers === 'function'){
                    
                    let headers = response.headers();

                    if ( response.status == 401 || (headers[ 'session-time' ] && headers[ 'session-time' ] === 'expired') ) {
                        window.open( URI( URI().origin() ).segmentCoded( [ 'login' ] ).toString(), "_self" );
                    }
                }
                

                delete vm.xhr_requests[ response.request.url ];

                return response;
            }

        } );
    };

    /**
     * Нужно ли навешать слой лоадер во время выполнения ajax запроса
     *
     * Если вам потребовалось отключить лоадер загрузки -
     * добавьте в объект отправки поле withoutLoader = true
     * @param request
     * @returns {boolean}
     */
    needLoader( request ) {
        return !(typeof request.data !== 'undefined'
        && typeof request.data.withoutLoader !== 'undefined'
        && request.data.withoutLoader == true);
    }

    /**
     * Установка страницы лоадера
     */
    setLoader( request ) {
        
        if ( !this.needLoader( request ) ) {
            this.xhr_requests[ request.url ] = false;
            return false;
        }

        const blockToOverlay = URI().path() === '/dashboard' ? '.main-box-body' : '.main-box';

        setTimeout( ()=> {
            $( "html, body" ).animate( { scrollTop: 0 }, "slow" );
            $( `<div id="loading-screen">
                    <div class="progress-element">
                        <img class="circle" src="${Laravel.static_server}/components/loader_circle.png" alt="">
                        <img class="logo" src="${Laravel.static_server}/components/loader_logo.png" alt="">
                    </div>
                </div>` ).appendTo( $( blockToOverlay ) );
        }, );

        this.xhr_requests[ request.url ] = true;
        return true;
    }

    /**
     *  Notify google analytic service
     */
    notifyGoogleAnalyticService( url = null ) {

        if ( !url ) {
            return;
        }

        if ( _.includes( this.urlToTrack, url ) ) {
            ga( 'send', 'event', url, 'clicked' );
        }
    }

    /**
     * Удаление страницы лоадера
     */
    destroyLoader( url ) {

        if ( this.xhr_requests[ url ] == false ) {
            return;
        }

        setTimeout( ()=> {
            $( '#loading-screen' ).remove();
        }, 200 );
    }

    /**
     * Включение свойств debug и devtools на НЕ продакшн серверах
     * (Дает возможность видеть свойства в devtools и просматривать stacktrace, при ошибках)
     *
     * Отключение свойств debug и devtools на продакшн сервера
     * (Отключает перечисленные выше возможности)
     *
     * @param Vue
     */
    initVueJSConfig( Vue ) {

        /*
         |  Laravel is defined  here: resources/views/vue.blade.php:11
         */
        
        if ( window.Laravel.env !== 'production' ) {
            Vue.config.debug    = true;
            Vue.config.devtools = true;

        } else if ( window.Laravel.env === 'production' ) {
            Vue.config.debug    = false;
            Vue.config.devtools = false;
        }
    };

    handleError( r ) {

        if ( r.data.errors && typeof r.data.errors === 'array' ) {

            r.data.errors.forEach( function( e ) {
                if ( $( '.main-box-header' ).length ) {
                    $( '.main-box-header' ).append( `<div class='alert alert-danger'><button type="button" class="close" data-dismiss="alert">x</button>${e}</div>` );
                }
            } );
        }
    }

    /**
     * Инициализация глобальных функций
     *
     * @param Vue
     */
    registerHelperFunctions( Vue ) {

        /*
         * ----- Перевод строки в нужную локализацию -----
         */
        window.translate = function( locale = '', objCome = {} ) {
            let obj = objCome || {};
            return Vue.t( locale, obj );
        };

        /*
         * ----- Переход на предыдущую страницу -----
         */
        window.go_back = function( url = null ) {
            
            if ( url ) {
                location.replace( url );
                return;
            }

            url = document.referrer ? document.referrer : location.pathname.substring( 0, location.pathname.indexOf( "/", 2 ) );
            
            location.replace( url );
        };
    }
}



