/**
 * При первоначальной загрузки скрипта
 * 
 * Пакет eonasdan-bootstrap-datetimepicker необходимый 
 * для отображении дат, использует локализацию moment.js при инициализации скрипта
 * @see resources/assets/bower/eonasdan-bootstrap-datetimepicker/src/js/bootstrap-datetimepicker.js:2392
 * 
 * По умолчанию у moment.js установлена локализация en и соответственно при первоначальной загрузке 
 * отображении устанавливается англиская локализация. 
 * Нам нужна русская локализация, поэтому мы устанавливаем её, ранее загрузки скрипта bootstrap-datetimepicker
 * @see gulpfile.js:55
 * 
 */
moment.locale('ru');