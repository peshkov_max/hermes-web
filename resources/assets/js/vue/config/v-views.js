// see resources/views/vue.blade.php:19
const current_view = Laravel.currentView;

const views = {
    'vm-dashboard': require( '../views/home/vm-dashboard.vue' ),
    'vm-city-index': require( '../views/cities/vm-index.vue' ),
    'vm-city-edit': require( '../views/cities/vm-edit.vue' ),
    'vm-city-create': require( '../views/cities/vm-edit.vue' ),
    'vm-city-merge': require( '../views/cities/vm-merge.vue' ),
    'vm-region-index': require( '../views/regions/vm-index.vue' ),
    'vm-region-edit': require( '../views/regions/vm-edit.vue' ),
    'vm-cluster-index': require( '../views/clusters/vm-index.vue' ),
    'vm-cluster-merge': require( '../views/clusters/vm-merge.vue' ),
    'vm-cluster-create': require( '../views/clusters/vm-edit.vue' ),
    'vm-cluster-edit': require( '../views/clusters/vm-edit.vue' ),
    'vm-cluster-link': require( '../views/clusters/vm-link.vue' ),
    'vm-device-index': require( '../views/devices/vm-index.vue' ),
    'vm-device-logs': require( '../views/devices/vm-logs.vue' ),

    // Support
    'vm-support-index': require( '../views/supports/vm-index.vue' ),
    'vm-support-show': require( '../views/supports/vm-show.vue' ),
    'vm-support-customers': require( '../views/supports/vm-customers.vue' ),
    'vm-support-customer': require( '../views/supports/vm-customer.vue' ),
    'vm-support-notifications': require( '../views/supports/vm-notifications.vue' ),

    // Support - pro
    'vm-support-pro-index': require( '../views/pro/supports/vm-index.vue' ),
    'vm-support-pro-show': require( '../views/pro/supports/vm-show.vue' ),

    // Users Pro
    'vm-users-pro': require( '../views/pro/users/vm-index.vue' ),
    'vm-users-pro-show': require( '../views/pro/users/vm-show.vue' ),
    'vm-users-pro-edit': require( '../views/pro/users/vm-form.vue' ),
    
    'vm-stores-pro-edit': require( '../views/pro/stores/vm-form.vue' ),
    'vm-notifications-pro': require( '../views/pro/notifications/vm-index.vue' ),

    // Subscription
    'vm-subscriptions-pro-index': require( '../views/pro/subscriptions/vm-index.vue' ),

    // PUSH
    'vm-push': require( '../views/push/vm-push.vue' ),

    // Products
    'vm-product-index': require( '../views/products/vm-index.vue' ),
    'vm-product-merge': require( '../views/products/vm-merge.vue' ),
    'vm-product-create': require( '../views/products/vm-form.vue' ),
    'vm-product-edit': require( '../views/products/vm-form.vue' ),

    // Products types
    'vm-product-type-index': require( '../views/product-types/vm-index.vue' ),
    'vm-product-type-create': require( '../views/product-types/vm-form.vue' ),
    'vm-product-type-edit': require( '../views/product-types/vm-form.vue' ),
    'vm-product-type-show': require( '../views/product-types/vm-show.vue' ),

    // UKM
    'vm-check-table': require( '../views/checks/vm-stats.vue' ),
    'vm-check-maps': require( '../views/checks/vm-maps.vue' ),
    'vm-check-detail': require( '../views/checks/vm-detail.vue' ),

    // Require receipt
    'vm-moderation': require( '../views/receipts/vm-moderation.vue' ),
    'vm-play-prize': require( '../views/receipts/vm-play-prize.vue' ),
    'vm-registry': require( '../views/receipts/vm-registry.vue' ),
    'vm-winners': require( '../views/receipts/vm-winners.vue' ),
    'vm-statistics': require( '../views/receipts/vm-statistics.vue' ),
    'vm-messages': require( '../views/receipts/vm-messages.vue' ),
    'vm-participants': require( '../views/receipts/vm-participants.vue' ),
    'vm-education': require( '../views/receipts/vm-education.vue' ),

    // Cabinet users
    'vm-user-index': require( '../views/users/vm-index.vue' ),
    'vm-user-create': require( '../views/users/vm-form.vue' ),
    'vm-user-edit': require( '../views/users/vm-form.vue' ),
    'vm-password': require( '../views/users/vm-password.vue' ),

    // Organizations
    'vm-organization-index': require( '../views/organizations/vm-index.vue' ),
    'vm-organization-create': require( '../views/organizations/vm-form.vue' ),
    'vm-organization-update': require( '../views/organizations/vm-form.vue' ),
    'vm-organization-users': require( '../views/organizations/vm-users.vue' ),
    'vm-organization-merge': require( '../views/organizations/vm-merge.vue' ),
    'vm-organization-city-editor': require( '../views/organizations/vm-organization-city-editor.vue' ),

    // Aliases
    'vm-aliases': require( '../views/aliases/vm-index.vue' ),

    // Roles
    'vm-role-index': require( '../views/roles/vm-index.vue' ),
    'vm-role-create': require( '../views/roles/vm-form.vue' ),
    'vm-role-edit': require( '../views/roles/vm-form.vue' ),
    'vm-role-show': require( '../views/roles/vm-show.vue' ),

};

let returnObject = {};

Object.keys( views ).forEach( function( key ) {
    if ( key == current_view ) {
        returnObject[ `${key}` ] = views[ key ];
    }
} );

if ( returnObject.length == 0 ) {
    throw new Error( `View ${key} does not exists!` );
}

console.info( `View ${current_view} will be loaded!` );

export default returnObject

