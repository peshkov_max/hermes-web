/**
 * Author: Peshkov Maxim
 * Created: 31.05.16 12:42
 * Email:  peshkov-maximum@yandex.ru
 *
 * Register filters
 */
export default class {

    constructor(Vue) {

        Vue.filter( 'str_limit', function(value, limit) {
            if (!value) return;
            
            if (value.length <= limit) {
                return value;
            }

            return value.substring( 0, limit) + '...';
        } );
        
    }
}