window._ = require('lodash');

import Vue from "vue";
import VueI18n from "vue-i18n";
import views from  "./config/v-views.js";
import Actions from  "./config/v-actions.js";
import Config from "./config/v-config";
import Filters from "./config/v-filters.js";

Vue.use( require( 'vue-resource' ) );
Vue.use( VueI18n );

/*
 |  Laravel is defined  here: resources/views/vue.blade.php:11
 */
Vue.config.lang = Laravel.current_locale;

/*
 |  Laravel is defined  here: resources/views/vue.blade.php:11
 */
function getLocales(params){
    "use strict";
    
    Vue.http.get( Laravel.path_to_i18n,
        function( data ) {
            Object.keys( data ).forEach( function(lang, index) {
                Vue.locale( lang, data[lang] );

                if ((index+1)==Object.keys( data ).length){
                    if (typeof params.success === 'function'){
                        params.success();
                    }
                }
            } );
        },
        function(error) {
            console.error( 'Error: ', error );
        }
    );
}

getLocales({ success: function () {
    "use strict";
    new Vue( {
        el: "body",
        created(){
            new Config( Vue );
            new Filters( Vue );
        },
        components: views,
        events:{
            /**
             * Вызывает событие eventName в дочерних компонентах перечисленных выше,
             * передает параметры eventData
             *
             * Событие может срабатывать в любом из компонентов
             * Проблема: Миксин m-filter-logic расширяет компонент vm-stats
             *           из миксина m-filter-logic необходимо вызвать событие,
             *           зарегистрированное в компоненте vm-stats. Т. к. m-filter-logic
             *           может быть расширять не только vm-stats, мы
             *           не можем обращятся к метдам vm-stats через this.[имя метода]
             *           Также нет возможности вызвать event через события $dispatch $broadcast,
             *           т.к. миксин встроен в компонент и находится на одном с компанентом уровне
             *           Существует необходимость в вызове event-ов компонентов vm из встроенных в них миксинов
             *
             * Решение:  Из миксина m-filter-logic вызывается событие trigger-component-event
             *           в который передается имя события, которое необходимо инициировать
             *           и данные, перевадваемые инициированному событию
             *
             *
             * @param eventName
             * @param eventData
             */
            'trigger-component-event': function(eventName, eventData){
                this.$broadcast(eventName, eventData);
            }
        },
        methods: Actions
    } );
}});
