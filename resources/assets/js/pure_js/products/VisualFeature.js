/**
 * Author: Peshkov Maxim
 * Email:  peshkov-maximum@yandex.ru
 */
class VisualFeature {

    static init(dict = {}, config = {}) {
        return new VisualFeature( dict, config );
    }

    constructor(dict, config) {

        this.dict   = dict;
        this.config = config;

        /** We have one modal form to work with features
         *  Form is configured according the following variable
         *  non-comparative - comparative icture
         *  comparative - means two pictures
         *
         * @type {string} */
        this.modalType = 'not-comparative';

        this.$fakeImageBlock     = $( '#fake-image-block' );
        this.$originalImageBlock = $( '#original-image-block' );
        this.$featureFormModal   = $( '#feature-form-modal' );
        this.$updateFeature      = $( '#update-feature' );

        this.fakeCropperOptions = {
            rendered: 'feature-fake-img-rendered',
            input: 'feature-fake-img-input',
            helper: 'feature-fake-img-help-block',
            change_image_checkbox: 'change-image-fake',
            change_image_button: 'change-image-fake',
            hiddenInputsIds: {
                x: 'feature-fake-img-data-x',
                y: 'feature-fake-img-data-y',
                width: 'feature-fake-img-data-width',
                height: 'feature-fake-img-data-height'
            }
        };

        this.originalCropperOptions = {
            rendered: 'feature-original-img-rendered',
            input: 'feature-original-img-input',
            helper: 'feature-original-img-help-block',
            change_image_checkbox: 'change-image-original',
            change_image_button: 'change-image-fake',
            hiddenInputsIds: {
                x: 'feature-original-img-data-x',
                y: 'feature-original-img-data-y',
                width: 'feature-original-img-data-width',
                height: 'feature-original-img-data-height'
            }
        };

        this.descriptions = {
            ru: {
                field: 'feature-description-ru',
                helper: 'feature-desc-ru-helper-block'
            },
            en: {
                field: 'feature-description-en',
                helper: 'feature-desc-en-helper-block'
            }
        };

        this.registerSortable();

        let trs = $( "#feature-list tr" );
        if (trs.length > 1)
            $( '#feature-list' ).show();
        else
            $( '#feature-list' ).hide();

        $( document ).ready( function(e) {
            $( "#change_image_original" ).prop( 'checked', false ).hide();
        } );

        let instance = this;

        $( '#add-features' ).on( 'click', function() {
            instance.$updateFeature.val( false );
            Helper.image_update( instance.originalCropperOptions.rendered, instance.config.default_image );

            instance.modalType = 'not-comparative';
            instance.initModal();
            VisualFeature.disableCheckboxes();
        } );

        $( '#add-comparison-features' ).on( 'click', function() {

            instance.$updateFeature.val( false );
            Helper.image_update( instance.originalCropperOptions.rendered, instance.config.default_image );
            Helper.image_update( instance.fakeCropperOptions.rendered, instance.config.default_image );

            instance.modalType = 'comparative';

            instance.initModal();
            VisualFeature.disableCheckboxes();
        } );

        $( '#save-feature' ).on( 'click', function() {

            if (!instance.formIsValid())
                return;

            if (instance.$updateFeature.val() === 'true')
                instance.update( {
                    success: function(response) {

                        instance.updateFeatureInDom( response );
                        instance.$featureFormModal.modal( 'hide' );

                    },
                    error: function(data, text, xhr) {
                        console.log( xhr.status );
                        instance.$featureFormModal.modal( 'hide' );
                    }
                } );
            else
                instance.store( {
                    success: function(response) {

                        if (response === 'limitReached') {
                            swal( instance.dict.error, instance.dict.feature_limit_reached, "error" )
                        } else {
                            instance.insertFeatureInDom( response );
                        }
                        instance.$featureFormModal.modal( 'hide' );
                    },
                    error: function(data, text, xhr) {
                        console.log( xhr.status );
                        instance.$featureFormModal.modal( 'hide' );
                    }
                } );

        } );

        $( '#cancel-adding-feature' ).on( 'click', function() {
            instance.$featureFormModal.modal( 'hide' );
        } );
    }

    store(obj = {}) {

        document.getElementById( 'feature-form-method' ).value = 'POST';

        Server.postFile( this.config.base_url, new FormData( $( '#feature-form' )[0] ), obj );
    }

    update(obj = {}) {

        document.getElementById( 'feature-form-method' ).value = 'PATCH';

        Server.postFile( this.config.base_url + '/' + $( '#feature-id' ).val(), new FormData( $( '#feature-form' )[0] ), obj );

    }

    edit(featureType, featureId, doNotUseStatic = false) {

        let instance   = this;
        this.modalType = featureType;

        Server.get( instance.config.base_url + '/' + featureId + '/edit', null, {
            success: function(data) {
                instance.$updateFeature.val( true );

                $( '#feature-id' ).val( data.id );
                $( `#${instance.descriptions.ru.field }` ).val( data.description_ru );
                $( `#${instance.descriptions.en.field }` ).val( data.description_en );

                let path = instance.getImagePath( data.image.original_uri, doNotUseStatic );
                $( `#${instance.originalCropperOptions.rendered}` ).attr( 'src', path );
                Helper.image_update( instance.fakeCropperOptions.rendered, path);

                if (data.image_fake !== null) {
                    
                    let pathFake = instance.getImagePath( data.image_fake.original_uri, doNotUseStatic );
                    $( `#${instance.fakeCropperOptions.rendered}` ).attr( 'src', pathFake);
                    Helper.image_update( instance.fakeCropperOptions.rendered, pathFake);
                }

                instance.initModal();
                instance.$featureFormModal.modal( 'show' );
            }
        } );
    }
    

    getImagePath(path, doNotUseStatic = false) {

        return doNotUseStatic
            ? this.config.app_path + path
            : this.config.static_path + path;
    }

    destroy(featureId, obj = {}) {
        Server.postFile( this.config.base_url + '/' + featureId, new FormData( $( '#delete-feature-form' )[0] ), obj );
    }

    insertFeatureInDom(feature) {

        $( '#feature-list' ).show();

        let instance          = this;
        let $featureListTbody = this.$featureListTbody;
        let secondImage       = '';
        let featureType       = 'not-comparative';

        if (instance.modalType === 'comparative') {
            secondImage = `&nbsp;<img class="img-thumbnail" src="" id="feature-list-image-${feature.fake_image_id}" style=" height: 60px;" 
                           onerror="this.src='${instance.config.default_image_loading}'">`;
        }

        if (instance.modalType === 'comparative') {
            featureType = 'comparative';
        }

        //noinspection JSUnresolvedVariable
        $featureListTbody.append( `<tr id="${feature.id}">
                <td class='priority text-center'>${feature.order_number}</td>
                <td>
                <img 
                    class="img-thumbnail" 
                    src=""
                    id="feature-list-image-${feature.image_id}" 
                    style=" height: 60px;"
                    onerror="this.src='${instance.config.default_image_loading}'"
                >${secondImage}
                
                </td>
                <td>${feature.description}</td>
                <td  class='text-center'>
                    <button class='btn btn-edit btn-primary' onclick="visualFeature.edit('${featureType}', '${feature.id}', true)">${instance.dict.edit}</button>
                    <button class='btn btn-delete btn-danger'>${instance.dict.deleteFeature}</button>
                </td>
            </tr>` );

        //noinspection JSUnresolvedVariable
        Helper.image_update( `feature-list-image-${feature.image_id}`, instance.getImagePath( feature.image.thumb_uri, true ));

        if (instance.modalType === 'comparative') { //noinspection JSUnresolvedVariable
            Helper.image_update( `feature-list-image-${feature.fake_image_id}`,  instance.getImagePath( feature.image_fake.thumb_uri, true ));
        }
    }

    initModal() {

        const instance = this;

        switch (instance.modalType) {
            case 'not-comparative':

                this.$fakeImageBlock.addClass( 'hide-block' );
                this.$originalImageBlock.removeClass( 'col-sm-6' );
                this.$originalImageBlock.addClass( 'col-sm-12' );

                instance.$featureFormModal.on( 'shown.bs.modal', function() {

                    $( '#feature-product-id' ).val( $( '#product-id' ).val() );

                    instance.initCropper( instance, instance.originalCropperOptions );
                } );

                break;
            case 'comparative':

                this.$fakeImageBlock.removeClass( 'hide-block' );
                this.$originalImageBlock.addClass( 'col-sm-6' );
                this.$originalImageBlock.removeClass( 'col-sm-12' );

                instance.$featureFormModal.on( 'shown.bs.modal', function() {

                    $( '#feature-product-id' ).val( $( '#product-id' ).val() );
                    instance.initCropper( instance, instance.originalCropperOptions );
                    instance.initCropper( instance, instance.fakeCropperOptions );

                } );

                break;
        }
    }

    initCropper(instance = {}, options = {}) {

        let dict = instance.dict;

        function setCropper(obj) {

            let $dataX      = $( '#' + options.hiddenInputsIds.x );
            let $dataY      = $( '#' + options.hiddenInputsIds.y );
            let $dataWidth  = $( '#' + options.hiddenInputsIds.width );
            let $dataHeight = $( '#' + options.hiddenInputsIds.height );

            $dataY.val( Math.round( obj.y ) );
            $dataX.val( Math.round( obj.x ) );
            $dataHeight.val( Math.round( obj.height ) );
            $dataWidth.val( Math.round( obj.width ) );
        }

        // Import image
        let inputImage = document.getElementById( options.input );

        /// Crop image section
        let image = document.getElementById( options.rendered );

        // this well reset loaded image in case we update image second time
        let $imageRendered = $( "#" + options.rendered );
        $imageRendered.attr( 'src', $imageRendered.attr( 'src' ) + '?' + new Date().getTime() );

        let cropper = null;

        cropper = new Cropper( image, {
            aspectRatio: 16 / 10,
            viewMode: 2,
            strict: false,
            crop: function() {
                setCropper( this.cropper.getData() );
            },
            built: function() {

                console.log( 23242342 );
                let $changeImage = $( "#" + options.change_image_checkbox );

                if (instance.$updateFeature.val() == 'true') {

                    VisualFeature.enableCheckboxes();

                    let obj = this;

                    if (inputImage.files.length !== 0) {

                        $( ".btn-upload" ).removeClass( 'disabled' );
                        $( "#" + options.input ).prop( 'disabled', false );

                        $( "#change-image-fake" ).prop( 'checked', true );
                        $( "#change-image-fake" ).parent().show();
                        $( "#change-image-original" ).prop( 'checked', true );
                        $( "#change-image-original" ).parent().show();

                        obj.cropper.enable();
                    } else {

                        $( ".btn-upload" ).addClass( 'disabled' );
                        $( "#" + options.input ).prop( 'disabled', true );

                        VisualFeature.enableCheckboxes();
                        obj.cropper.disable();
                    }

                    $changeImage.on( 'change', function(e) {
                        let checkbox = $( this );

                        if (checkbox.is( ':checked' )) {

                            let input = $( "#" + options.input );
                            input.closest( 'label' ).removeClass( 'disabled' );
                            input.prop( 'disabled', false );
                            obj.cropper.enable();
                        } else {
                            let input = $( "#" + options.input );
                            input.closest( 'label' ).addClass( 'disabled' );
                            input.prop( 'disabled', true );
                            obj.cropper.disable();
                        }
                    } )

                }
            }
        } );

        this.setImageUploader( inputImage, cropper, options, dict );

        instance.$featureFormModal.on( 'hidden.bs.modal', function() {
            cropper.destroy();
            instance.clearFeatureForm();

        } )
    }

    setImageUploader(inputImage, cropper, options, dict) {

        let $helpBlock = $( '#' + options.helper );

        $helpBlock.html( '' );

        //noinspection JSUnresolvedVariable
        let URL = window.URL || window.webkitURL;
        let blobURL;

        if (!URL) {
            inputImage.disabled = true;
            inputImage.parentNode.className += ' disabled';
            return;
        }

        inputImage.onchange = function() {

            let files = this.files;
            let file;

            if (!cropper || !files || !files.length) {
                return;
            }

            file = files[0];

            if (!ImageService.imageIsValid( file, $helpBlock, dict )) {
                Helper.reset( options.input );
                return;
            }

            //noinspection JSUnresolvedFunction
            blobURL = URL.createObjectURL( file );
            cropper.clear();
            cropper.reset().replace( blobURL );
        };
    }

    clearFeatureForm() {

        let $desc_ru        = $( `#${this.descriptions.ru.field}` );
        let $desc_ru_helper = $( `#${this.descriptions.ru.helper}` );

        let $desc_en        = $( `#${this.descriptions.en.field}` );
        let $desc_en_helper = $( `#${this.descriptions.en.helper}` );

        $desc_ru.val( '' );
        $desc_en.val( '' );

        $desc_ru.parent().removeClass( 'has-error' );
        $desc_ru_helper.empty();

        $desc_en.parent().removeClass( 'has-error' );
        $desc_en_helper.empty();

        this.$originalImageBlock.removeClass( 'has-error' );
        $( `#${this.originalCropperOptions.helper}` ).empty();

        this.$fakeImageBlock.removeClass( 'has-error' );
        $( `#${this.fakeCropperOptions.helper}` ).empty();

        document.getElementById( this.originalCropperOptions.input ).value = "";
        document.getElementById( this.fakeCropperOptions.input ).value     = "";

        $( "#" + this.originalCropperOptions.change_image_checkbox ).attr( 'checked', false );
        $( "#" + this.fakeCropperOptions.change_image_checkbox ).attr( 'checked', false );

        $( ".btn-upload" ).removeClass( 'disabled' );
        $( "#" + this.originalCropperOptions.input ).prop( 'disabled', false );
        $( "#" + this.fakeCropperOptions.input ).prop( 'disabled', false );

        VisualFeature.disableCheckboxes();

        this.$featureFormModal.off();
    }

    formIsValid() {

        let hasError;

        hasError = this.validateTextFields();

        hasError = this.validateImage( this.originalCropperOptions.helper, this.originalCropperOptions.input, this.$originalImageBlock, hasError );

        if (this.modalType === 'comparative') {
            hasError = this.validateImage( this.fakeCropperOptions.helper, this.fakeCropperOptions.input, this.$fakeImageBlock, hasError );
        }

        return !hasError;
    }

    validateTextFields(hasError) {

        let $desc_ru        = $( `#${this.descriptions.ru.field}` );
        let $desc_ru_helper = $( `#${this.descriptions.ru.helper}` );

        let $desc_en        = $( `#${this.descriptions.en.field}` );
        let $desc_en_helper = $( `#${this.descriptions.en.helper}` );

        if ($desc_ru.val() === '') {
            $desc_ru.parent().addClass( 'has-error' );
            $desc_ru_helper.html( this.dict.descriptionRuRequired );
            hasError = true;
        }
        else {
            $desc_ru.parent().removeClass( 'has-error' );
            $desc_ru_helper.empty();
        }

        if ($desc_en.val() === '') {
            $desc_en.parent().addClass( 'has-error' );
            $desc_en_helper.html( this.dict.descriptionEnRequired );
            hasError = true;
        }
        else {
            $desc_en.parent().removeClass( 'has-error' );
            $desc_en_helper.empty();
        }

        return hasError;
    };

    validateImage(helperId, inputId, $block, hasError) {

        let helper = $( `#${helperId}` );

        if ($( '#update-feature' ).val() === 'true') return hasError;

        if (document.getElementById( inputId ).files.length == 0) {
            $block.addClass( 'has-error' );
            helper.html( this.dict.imageRequired );
            helper.show();
            hasError = true;
        }
        else {
            $block.removeClass( 'has-error' );
            helper.empty();
            helper.hide();
        }

        return hasError;
    }

    registerSortable() {

        this.$featureListTbody = $( "#feature-table-tbody" );

        let instance = this;

        $( document ).ready( function() {

            //Helper function to keep table row from collapsing when being sorted
            var fixHelperModified = function(e, tr) {

                var $originals = tr.children();
                var $helper    = tr.clone();
                $helper.children().each( function(index) {
                    $( this ).width( $originals.eq( index ).width() )
                } );
                return $helper;
            };

            //Make diagnosis table sortable
            instance.$featureListTbody.sortable( {

                helper: fixHelperModified,
                stop: function(event, ui) {

                    renumber_table( '#feature-list', {
                        success: function() {

                            let feature_id = ui.item.attr( 'id' );
                            let new_order  = $( `#${feature_id}` ).find( ':first-child' ).html();

                            Server.patch( instance.config.base_url + '/' + feature_id + '/set-order/' + new_order, {
                                id: feature_id,
                                new_order_number: new_order
                            } );
                        }
                    } )
                }
            } ).disableSelection();

            //Delete button in table rows
            $( 'table' ).on( 'click', '.btn-delete', function() {

                let tableID = '#' + $( this ).closest( 'table' ).attr( 'id' );
                let click   = this;

                swal( {
                    title: instance.dict.attention,
                    text: instance.dict.itemWillBeRemoved,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: instance.dict.continue,
                    cancelButtonText: instance.dict.cancel,
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function(isConfirm) {

                    if (isConfirm) {
                        let $tr       = $( click ).closest( 'tr' );
                        let featureId = $tr.attr( 'id' );
                        $tr.remove();

                        renumber_table( tableID, {

                            success: function() {

                                instance.destroy( featureId, {
                                    success: ()=> {
                                        swal.close()
                                    },
                                    error: function(data) {
                                        console.log( data );
                                        swal( instance.dict.error, "error" )
                                    }
                                } );
                            }
                        } );
                    }
                } );

            } );

        } );

        // Renumber table rows
        function renumber_table(tableID, obj = {}) {
            let trs = $( tableID + " tr" );

            trs.each( function() {
                let count = $( this ).parent().children().index( $( this ) ) + 1;
                $( this ).find( '.priority' ).html( count );
            } );

            if (trs.length === 1) {
                $( tableID ).hide();
            }

            if (typeof obj.success === 'function') {
                obj.success();
            }

        }
    }

    updateFeatureInDom(feature) {

        let tds         = $( '#' + feature.id ).children();
        let image       = tds[1];
        let text        = tds[2];
        let imageFake   = '';
        let instance    = this;
        image.innerHTML = '';

        //noinspection JSUnresolvedVariable
        if (feature.image_fake !== null) {
            imageFake = `&nbsp;<img class="img-thumbnail" onerror="this.src='${instance.config.default_image_loading}'" src="" id="feature-list-image-${feature.fake_image_id}" style="height: 60px;">`;
        }
        image.innerHTML = `<img class="img-thumbnail" onerror="this.src='${instance.config.default_image_loading}'" src="" id="feature-list-image-${feature.image_id}" style="height: 60px;">${imageFake}`;

        //noinspection JSUnresolvedVariable
        Helper.image_update( `feature-list-image-${feature.image_id}`, instance.getImagePath( instance.config.default_image_loading, true ));

        //noinspection JSUnresolvedVariable
        if (feature.image_fake !== null)
            Helper.image_update( `feature-list-image-${feature.fake_image_id}`,  instance.getImagePath( instance.config.default_image_loading, true ));
        

        text.innerHTML = '';
        text.innerHTML = feature.description;
    }

    static disableCheckboxes() {
        $( "#change-image-fake" ).prop( 'checked', false );
        $( "#change-image-fake" ).parent().hide();
        $( "#change-image-original" ).prop( 'checked', false );
        $( "#change-image-original" ).parent().hide();
    }

    static enableCheckboxes() {
        $( "#change-image-fake" ).prop( 'checked', false );
        $( "#change-image-fake" ).parent().show();
        $( "#change-image-original" ).prop( 'checked', false );
        $( "#change-image-original" ).parent().show();
    }
}