/**
 * Author: Peshkov Maxim
 * Email:  peshkov-maximum@yandex.ru
 */
class ProductIndex {

    static init(dict = {}, config) {
        return new ProductIndex( dict, config );
    }

    constructor(dict, config) {
        
        this.dict = dict;
        this.config = config;

        let instance = this;
        
        
        $( '#product-organization' ).select2( {
            locale: 'ru',
            placeholder: {
                id: 0,
                text: instance.dict.chose_organization
            },
            ajax: {
                url: instance.config.organization_search_url,
                dataType: 'json',
                type: "GET",
                quietMillis: 50,
                data: function(data) {
                    return {
                        search: data.term
                    };
                },
                processResults: function(response) {

                    let organizations  =  response.data;
                    if (organizations === 'request_is_empty')
                        return null;
                  
                    return {
                        results: $.map( organizations, function(organization) {
                            return {
                                id: organization.id,
                                text: organization.name
                            }
                        } )
                    };
                }
            }
        } );

        $( '#filter-trigger-button' ).on( 'click', function(e) {
            e.preventDefault( e );

            if ($( '#product-filter-block' ).css( 'display' ) === 'none')
                $( '#product-filter-block' ).show( 400, ()=>$( "#target" ).focus() );
            else
                $( '#product-filter-block' ).hide( 400 );

        } );

        $( '.help-block' ).hide();

        function registerFilter(checkBox, block, firstRadio, firstValue, secondRadio, secondValue)  {
            
            $( '#' + checkBox ).on( 'click', function(e) {
                
                let checkbox = $( this );

                if (checkbox.is( ':checked' )) {

                    $( `#${block} .radio-inline` ).removeClass( 'disabled' ); 

                    $( `#${firstRadio}` ).prop( 'disabled', false );
                    $( `#${secondRadio}` ).prop( 'disabled', false ); 
                    
                    $( `#${firstRadio}` ).prop( 'value', firstValue );
                    $( `#${secondRadio}` ).prop( 'value', secondValue );
                    
                    $( `#${firstRadio}` ).prop( 'checked', false );
                    $( `#${secondRadio}` ).prop( 'checked', false );

                } else {
                    console.log( 55 );
                    $( `#${block} .radio-inline` ).addClass( 'disabled' );

                    $( `#${firstRadio}` ).prop( 'disabled', true );
                    $( `#${secondRadio}` ).prop( 'disabled', true );
                    
                    $( `#${firstRadio}` ).prop( 'value', null );
                    $( `#${secondRadio}` ).prop( 'value', null );

                    $( `#${firstRadio}` ).prop( 'checked', false );
                    $( `#${secondRadio}` ).prop( 'checked', false );

                }
            } );
        }

        registerFilter( 'use-enabled', 'enabled-block', 'is-enabled', 'is_enabled', 'is-not-enabled', 'is_not_enabled' );
        registerFilter( 'use-moderation', 'moderated-block', 'is-moderated', 'is_moderated', 'is-not-moderated', 'is_not_moderated' );
        registerFilter( 'use-sort', 'sort-block', 'use-desc', 'desc','use-asc' , 'asc');
        

        $( "#product-search" ).click( function() {
            $( "#filter-form" ).submit();
        } );

        $( "#do-filter" ).click( function() {
            $( "#filter-form" ).submit();
        } );

        $( "#do-reset" ).click( function() {

            $( "<input type='hidden' name='reset' value='true' />" )
                .appendTo( "#filter-form" );

            $( "#filter-form" ).submit();
        } );

        $( "#select-all-checkboxes" ).on( 'change', function() {
            if ($( this ).is( ':checked' )) {

                $( '#assign-all' ).prop( 'disabled', false );
                $( '#table-action-buttons' ).show();
                $( '#product-table' ).find( 'input[type=checkbox]' ).prop( 'checked', true );
                $( '#product-table' ).find( 'input[type=checkbox]:checked' ).closest( 'tr' ).css( 'background-color', '#f5f5f5' );
            } else {
                $( '#table-action-buttons' ).hide();
                $( '#assign-all' ).prop( 'disabled', true );
                $( '#product-table' ).find( 'input[type=checkbox]:checked' ).closest( 'tr' ).css( 'background-color', '#fff' );
                $( '#product-table' ).find( 'input[type=checkbox]' ).prop( 'checked', false );
            }
        } );

        $( '#product-table' ).find( 'input[type=checkbox]' ).on( 'change', function() {
            if ($( this ).is( ':checked' )) {

                $( '#table-action-buttons' ).show();

                $( this ).closest( 'tr' ).css( 'background-color', '#f5f5f5' );
            } else {

                if (!$( '#product-table' ).find( 'input[type=checkbox]:checked' ).length)
                    $( '#table-action-buttons' ).hide();

                $( this ).closest( 'tr' ).css( 'background-color', '#fff' );
            }
        } );

        $( '#assign-hide' ).on( 'click', function(e) {
            setStatus( 'is_enabled', $( this ), false );
            e.preventDefault( e );
        } );

        $( '#assign-show' ).on( 'click', function(e) {
            setStatus( 'is_enabled', $( this ), true );
            e.preventDefault( e );
        } );

        $( '#assign-set-not-moderated' ).on( 'click', function(e) {
            setStatus( 'is_moderated', $( this ), false );
            e.preventDefault( e );
        } );

        $( '#assign-set-is-moderated' ).on( 'click', function(e) {
            setStatus( 'is_moderated', $( this ), true );
            e.preventDefault( e );
        } );

        function setStatus(type, link, status) {
            
            let url = link.prop( 'href' );
            let ids = [];

            $( '#product-table' ).find( 'input[type=checkbox]:checked' ).each( function(index, element) {
                if (element.id.substr( 0, 9 ) === 'selected-')
                    ids.push( element.id.substr( 9, element.id.length ) );
            } );

            if (ids.length === 0) {
                alert( 'Select items please!!!' );
                e.preventDefault( e );
            }

            url = url + '?field=' + type + '&status=' + status + '&product_ids=' + JSON.stringify( ids );

            window.open( url, '_self' );
        }

    }
    
    static setFilterParam(firstRadio, firstValue, firstStatus, secondRadio, secondValue, secondStatus, disable){
        
        $( `#${firstRadio}` ).prop( 'disabled', disable );
        $( `#${firstRadio}` ).prop( 'value', firstValue );

        $( `#${secondRadio}` ).prop( 'disabled', disable );
        $( `#${secondRadio}` ).prop( 'value', secondValue);

        $( `#${firstRadio}` ).prop( 'checked', firstStatus );
        $( `#${secondRadio}` ).prop( 'checked', secondStatus );
    }

}