class Product {

    static init(dict, config) {
        return new Product( dict, config );
    }

    constructor(dict, config) {

        this.config = config;
        this.dict   = dict;

        this.$productForm = $( '#product-form' );
        this.$productId   = $( '#product-id' );

        this.fileChosen          = false;
        this.productImageCropper = null;

        this.product_types = JSON.parse( this.config.product_types );

        let instance = this;

        $( '#is-enabled' ).val( true );
        $( '#is-moderated' ).val( true );
        $( '#product-save-helper' ).hide();

        if (!$( ".wizard" ).length) {
            this.productImageCropper = this.initCropper();

            let $changeImage = $( "#change-image" );

            if ($changeImage.length) {
                $changeImage.attr( 'checked', false );
                $( ".btn-upload" ).addClass( 'disabled' );
                $( "#input-image" ).prop( 'disabled', true );
            }

            let $changeImageB = $( "#change-background-image" );

            if ($changeImageB.length) {
                $changeImageB.attr( 'checked', false );
                $( ".btn-upload" ).addClass( 'disabled' );
                $( "#input-image-background" ).prop( 'disabled', true );
            }
        }

        let $saveProductButton = $( "#save-item-button" );

        /// if we are not using wizard
        if ($saveProductButton.length) {

            $saveProductButton.on( 'click', function(e) {

                if (!instance.productFormIsValid( e )) {
                    e.preventDefault( e );
                }

                if (!instance.productImagesIsValid( e )) {
                    e.preventDefault( e );
                }

                let id = $( '#product-id' ).val();
                $( '#product-form-method' ).val( id === '' ? 'POST' : 'PATCH' );
            } );
        }

        $( function() {

            if (config.product && Helper.is_json_string( config.product )) {

                let product = JSON.parse( config.product );
                var data    = [
                    {
                        id: product.organization.id,
                        text: product.organization.name_ru
                    }
                ];
            } else
                var data = [];

            $( '#product-organization' ).select2( {
                locale: 'ru',
                data: data,
                ajax: {
                    url: instance.config.organization_search_url,
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function(data) {
                        return {
                            search: data.term
                        };
                    },
                    processResults: function(response) {

                        let organizations = response.data;
                        if (organizations === 'request_is_empty')
                            return null;

                        return {
                            results: $.map( organizations, function(organization) {

                                return {
                                    id: organization.id,
                                    text: organization.name
                                }
                            } )
                        };
                    }
                }
            } );
        } );

        instance.initProductTypeTree();
    }

    initProductTypeTree() {

        let instance = this;

        let dataSource = function(parentData, callback) {

            let yourData = [];

            if (parentData.attr !== undefined && parentData.attr.hasChildren !== undefined && parentData.attr.hasChildren == true) {
                let id = parentData.attr.id.split( '_' )[3];
                setWithParentId( id );
            } else {
                setWithParentId( null );
            }

            function setWithParentId($parentId) {

                instance.product_types.forEach( function(el) {
                    if (el.parent_id == $parentId) {
                        yourData.push( {
                            name: el.name,
                            type: hasChildren( el ) ? 'folder' : 'item',
                            attr: {id: 'product_type_id_' + el.id, hasChildren: hasChildren( el )}
                        } );
                    }
                } );
            }

            function hasChildren(checked) {

                let hasChildren = false;
                instance.product_types.forEach( function(el) {
                    if (el.parent_id === checked.id && el.id != checked.id)
                        hasChildren = true;
                } );

                return hasChildren;
            }

            // continue with rendering the tree
            callback( {data: yourData} );
        };

        let $productTypeTree = $( '#productTypeTree' );

        $productTypeTree.tree( {
            folderSelect: true,
            multiSelect: false,
            dataSource: dataSource
        } );

        $productTypeTree.on( 'selected.fu.tree', function(event, data) {
            $( 'input[name=product_type_id]' ).val( data.target.attr.id.split( '_' )[3] );
        } );

        if (instance.config.product !== undefined) {
            let product = JSON.parse( instance.config.product );

            if (product.type && product.type.parent_id == null) {
                $productTypeTree.tree( 'selectItem', $( '#product_type_id_' + product.type.id ) );
            }
            else if (product.type && product.type.parent_id != null) {

                /**
                 * Can be exported to class
                 */
                class TypeCounter {

                    // todo export this functionality 
                    constructor(product_types, select_id) {
                        this.elements      = [];
                        this.select_id     = select_id;
                        this.product_types = product_types;
                    }

                    setItem(type) {
                        let elements = [];

                        this.product_types.forEach( function(element) {
                            if (element.id == type.parent_id)
                                elements.push( element );
                        } );

                        if (elements.length && elements[0].parent_id != null) {
                            this.elements.push( elements[0] );
                            return this.setItem( elements[0] );
                        }
                        else {
                            if (elements.length)
                                this.elements.push( elements[0] );

                            this.elements = this.elements.reverse();

                            return this.openTree();
                        }
                    }

                    openTree() {
                        let instance = this;
                        this.elements.forEach( function(el, index, array) {
                            $productTypeTree.tree( 'openFolder', $( '#product_type_id_' + el.id ) );
                            if (array.length - 1 == index)
                                $productTypeTree.tree( 'selectItem', $( '#product_type_id_' + instance.select_id ) );

                        } )
                    }
                }

                let stack = new TypeCounter( instance.product_types, product.type.id );

                stack.setItem( product.type );

            }
        }

    }

    /**
     * Store new product
     * @param obj
     */
    store(obj = {}) {
        
        let instance = this;

        $( '#product-form-method' ).val( 'POST' );

        Server.post( '/products', instance.$productForm.serialize(), {
            
            success: function(data) {
              
                instance.$productId.val( data.id );
                
                $( '#product-id' ).val( data.id );
                
                $( '#product-info-block' ).append(
                    `<div class="alert alert-success" id="product-save-helper">
                        <span class="fa fa-check"></span>
                         ${instance.dict.productSaved}
                     </div>` );

                if (typeof obj.success === 'function') {
                    obj.success( data );
                }
            },
            error: function(data) {
                if (typeof obj.error === 'function') {
                    obj.success( data );
                }
            }
        } );
    }

    /**
     * Update product
     * @param obj
     */
    update(obj = {}) {

        // laravel REST need hidden _method field with value PATCH
        $( '#product-form-method' ).val( 'PATCH' );
     
        Server.post( this.config.base_url + '/' + this.$productId.val(), this.$productForm.serialize(), obj );
    }

    /**
     * Store product's image
     * @param obj
     */
    storeImage(obj = {}) {

        var form_data = new FormData( $( '#product-image-form' )[0] );
        var productId = this.$productId.val();
     
        form_data.append( 'product_id', productId );

        if ($( '#image-id' ).val() == '') {
            form_data.append( 'change_image', 'false' );
        }
        else {
            form_data.append( 'change_image', 'true' );
        }

        Server.postFile( this.config.base_url + '/' + productId + '/save-image', form_data, obj );
    }

    /**
     * Validate product form
     * @param event
     * @returns {boolean}
     */
    productFormIsValid(event) {

        let fieldsToCheck = {
            type: $( "#product-type" ),
            nameRu: $( "#name-ru" ),
            nameEn: $( "#name-en" ),
            legalName: $( "#legal-name" ),
            barcode: $( '#barcode' ),
            company: $( "#product-organization" ),
        };

        let isValid = true;

        for (var property in fieldsToCheck) {
            if (fieldsToCheck.hasOwnProperty( property )) {

                if (isValid) {
                    isValid = this.checkProductFormField( fieldsToCheck[property], property, event );
                }
                else {
                    this.checkProductFormField( fieldsToCheck[property], property, event );
                    isValid = false;
                }
            }
        }

        return isValid;
    }

    productImagesIsValid(e) {

        let instance = this;

        if ($( '#change-image' ).is( ':checked' ) && document.getElementById( 'input-image-background' ).files.length == 0) {

            swal( {
                title: instance.dict.attention,
                text: instance.dict.fill_background_image,
                type: "warning",
                confirmButtonText: instance.dict.continue,
                closeOnConfirm: true
            }, function() {
                swal.close();
            } );

            return false;
        }

        return true;
    }

    checkProductFormField($field, fieldName, event) {

        var setHtml = function() {
            if (fieldName === 'type' || fieldName === 'company')
                $( `#product-${fieldName}-helper-block` ).html( this.dict[fieldName] );
            else
                $field.next( ".help-block" ).html( this.dict[fieldName] );
        };

        var emptyField = function() {
            if (fieldName === 'type' || fieldName === 'company')
                $( `#product-${fieldName}-helper-block` ).empty();
            else
                $field.next( ".help-block" ).empty();
        };

        if ($field.val() === '') {

            $field.closest( ".form-group" ).addClass( 'has-error' );

            setHtml.call( this );

            event.preventDefault();
            return false;

        } else {

            if (fieldName === 'barcode' && $field.val().length !== 13) {

                $( `#barcode-helper-block` ).html( this.dict['barcode_max_length'] );

                $field.closest( ".form-group" ).addClass( 'has-error' );

                event.preventDefault();

                return false;
                
            } else {
                $field.closest( ".form-group" ).removeClass( 'has-error' );

                emptyField();

                return true;
            }
        }

    }

    initCropper() {

        let instance = this;

        function setCropRectangleOptions(obj) {

            let $dataX      = $( '#data-x' );
            let $dataY      = $( '#data-y' );
            let $dataHeight = $( '#data-height' );
            let $dataWidth  = $( '#data-width' );

            $dataY.val( Math.round( obj.y ) );
            $dataX.val( Math.round( obj.x ) );
            $dataHeight.val( Math.round( obj.height ) );
            $dataWidth.val( Math.round( obj.width ) );
        }

        /// Crop image section
        let image      = document.getElementById( 'image-rendered' );
        let $helpBlock = $( '#product-img-help-block' );

        $helpBlock.hide();

        let cropper;

        let inputImage       = document.getElementById( 'input-image' );

        var disablaButtons = function(styleClass, imageInputId, cropper) {
            $( "." + styleClass ).removeClass( 'disabled' );
            $( "#" + imageInputId ).prop( 'disabled', false );
            $( ".background-image" ).removeClass( 'disabled' );
            $( "#input-image-background" ).prop( 'disabled', false );
            $( "#change-background-image" ).val( true );
            cropper.cropper.enable();
        };

        var enableButtons = function(styleClass, imageInputId, cropper) {
            $( "." + styleClass ).addClass( 'disabled' );
            $( "#" + imageInputId ).prop( 'disabled', true );
            $( ".background-image" ).addClass( 'disabled' );
            $( "#input-image-background" ).prop( 'disabled', true );
            $( "#change-background-image" ).val( false );
            cropper.cropper.disable();
        };

        function toggleCheckbox(cropper, imageCheckboxId, imageInputId, styleClass = '') {

            let $changeImage = $( "#" + imageCheckboxId );

            let image = document.getElementById( imageInputId );

            if ($changeImage.length) {

                if (image.files.length !== 0) {
                    $changeImage.prop( 'checked', true );
                    disablaButtons( styleClass, imageInputId, cropper );
                }
                else {
                    $changeImage.prop( 'checked', false );
                    enableButtons( styleClass, imageInputId, cropper );
                }

                $changeImage.on( 'change', function() {
                    let checkbox = $( this );
                    if (checkbox.is( ':checked' )) {
                        disablaButtons( styleClass, imageInputId, cropper );
                    } else {
                        enableButtons( styleClass, imageInputId, cropper );
                    }
                } )
            }
        }

        cropper = new Cropper( image, {
            aspectRatio: 3 / 4,
            viewMode: 2,
            strict: false,
            cropBoxResizable: true,
            crop: function(e) {
                setCropRectangleOptions( this.cropper.getData() );
            },
            built: function(e) {
                
                let obj = this;
                toggleCheckbox( obj, 'change-image', 'input-image', 'main-image' );
            }
        } );

      

        inputImage.value = null;

        //noinspection JSUnresolvedVariable
        let URL = window.URL || window.webkitURL;
        let blobURL;

        if (!URL) {
            inputImage.disabled = true;
            inputImage.parentNode.className += ' disabled';
            return;
        }

        inputImage.onchange = function() {

            let files = this.files;
            let file;

            if (!cropper || !files || !files.length) {
                return;
            }

            file = files[0];

            if (!ImageService.imageIsValid( file, $helpBlock, instance.dict )) {
                Helper.reset( 'input-image' );

                return;
            }

            //noinspection JSUnresolvedFunction
            blobURL = URL.createObjectURL( file );
            cropper.reset().replace( blobURL );

            instance.fileChosen = true;
        };
        

        return cropper;
    }
}