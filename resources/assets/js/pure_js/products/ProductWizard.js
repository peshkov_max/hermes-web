class ProductWizard {

    static init(productInstance) {
        return new ProductWizard(productInstance);
    }

    constructor(productInstance) {
        
        this.productInstance = productInstance;
        
        this.$productWizard = $( '#product-wizard' );

        var wizard = this;
        
        window.onload = function() {

            $( '#first-step-next-button' ).on( 'click', (e)=> {
                wizard.$productWizard.wizard( 'next' );
            } );

            $( '#second-step-next-button' ).on( 'click', (e)=> {
                wizard.$productWizard.wizard( 'next' );
            } );

            $( '.wizard .actions .btn-next-product' ).on('click', function(e) {
                e.preventDefault();
                window.open(wizard.productInstance.config.base_url+'/create?wizard=true' , '_self');
            });

            $( '.wizard .actions .btn-wizard-exit' ).on('click', function(e) {
                e.preventDefault();
                window.open(wizard.productInstance.config.base_url , '_self');
            });

       
              
            

            wizard.$productWizard.wizard();

            let flag = false;

            wizard.$productWizard.on( 'actionclicked.fu.wizard', function(evt, data) {

                if (flag === true) {
                    flag = false; // reset flag
                    return; // let the event bubble away
                }
                evt.preventDefault();

                // manage  product
                if (wizard.needAndCanBeStored( data, evt, wizard )) {


                    // Product type is tree. so we check it separately
                if (!$('#product_type_id').val())
                {
                    swal( {title: wizard.productInstance.dict.attention,  text: wizard.productInstance.dict.selectProductType,});
                    return
                } 

                    // create product
                    if (wizard.productInstance.$productId.val() === '') {

                        swal( {
                            title: wizard.productInstance.dict.attention,
                            text: wizard.productInstance.dict.productWillBeSaved,
                            type: "warning",
                            confirmButtonText: wizard.productInstance.dict.continue,
                            cancelButtonText: wizard.productInstance.dict.cancel,
                            closeOnConfirm: true,
                            showCancelButton: true,
                        }, function(isConfirm) {
                            if (isConfirm) {
                                wizard.productInstance.store( {
                                    success:function (){
                                        flag = true;
                                        wizard.$productWizard.wizard( 'next' );
                                        if (!wizard.productInstance.productImageCropper)
                                            wizard.productInstance.productImageCropper = wizard.productInstance.initCropper(  );

                                        swal.close();
                                    },
                                    error: function(data) {console.log( data )}
                                });
                                
                            } else {
                                swal.close();
                            }
                        } );

                    }
                    else
                    // update product
                    {
                        wizard.productInstance.update( {
                            dict: wizard.productInstance.dict,
                            success: ()=> {
                                flag = true;
                                wizard.$productWizard.wizard( 'next' );
                            },
                            error: data => console.log( data )
                        } );
                    }

                }

                // manage product image
                if (data.step === 2 && data.direction === 'next' && wizard.productInstance.fileChosen) {

                    if(document.getElementById("input-image-background").value != "") {
                        // save product image
                        wizard.productInstance.storeImage( {
                            dict: wizard.productInstance.dict,
                            success: (product)=> {

                                flag = true;

                                $('.wizard .actions .btn-next-product').show();
                                $('.wizard .actions .btn-next').hide();

                                if (product.common_info.image_id)
                                    $('#image-id').val(product.common_info.image_id);

                                wizard.$productWizard.wizard( 'next' );
                            },
                            error: data => console.log( data )
                        } );
                    }
                    
                    else {
                        swal( {
                            title: wizard.productInstance.dict.attention,
                            text: wizard.productInstance.dict.fill_background_image,
                            type: "warning",
                            confirmButtonText: wizard.productInstance.dict.continue,
                            closeOnConfirm: true,
                        }, function(isConfirm) {
                            if (isConfirm) {
                                swal.close();
                            } else {
                                swal.close();
                            }
                        } );
                    }
                }

                if (data.step === 2 && data.direction === 'next' && !wizard.productInstance.fileChosen) {
                    $('.wizard .actions .btn-next-product').show();
                    $('.wizard .actions .btn-next').hide();
                    flag = true;
                    wizard.$productWizard.wizard( 'next' );
                }

                if (data.step === 3 && data.direction === 'previous') {
                    $('.wizard .actions .btn-next-product').hide();
                    $('.wizard .actions .btn-next').show();

                    flag = true;
                    wizard.$productWizard.wizard( 'previous' );
                }

                if (data.step === 2 && data.direction === 'previous') {
                    $('.wizard .actions .btn-next').show();
                    flag = true;
                    wizard.$productWizard.wizard( 'previous' );
                }
                
                if (data.step === 3){
                    $('.wizard .actions .btn-next-product').hide();
                    $('.wizard .actions .btn-next').show();
                }

            } );

            wizard.$productWizard.on('finished.fu.wizard', function (evt, data) {
                $('.wizard .actions .btn-next').hide();
                $('.wizard .actions .btn-next-product').show();
            });
        }
    }

    needAndCanBeStored(data, event) {
        
        let instance = this;

        return data.step === 1 && data.direction === 'next' && instance.productInstance.productFormIsValid( event ) ;
    };



}