/*
 * Author: Peshkov Maxim
 * Email:  peshkov-maximum@yandex.ru
 */
class App {

    static init(dict = {}) {
        return new App( dict );
    }

    constructor(dict) {
        this.dict     = dict;
        this.messages = {};
        
        let instance = this;

        window.paceOptions = {
            ajax: true
        };

        $( "div.required" ).keyup( this, function(event) {

            let $div = $( this );
            let [$input, $span, spanId] = event.data.defineVars( $div );
            event.data.reRenderErrors( $input, $div, spanId, $span );
        } );

        $.ajaxSetup( {
            headers: {
                'X-CSRF-TOKEN': Laravel.csrf_token,
            }
        } );

        App.initCheckboxes();

        this.initAjaxLoader();
        this.initMenu();

       
        if ($( "#button-cancel" ).length ){
            let flag = false;
            $( "#button-cancel" ).on('click', function(e) {
                if (flag === true) {
                    flag = false; // reset flag
                    return; // let the event bubble away
                }
                
                let button = this;
                e.preventDefault();
                
                  swal( {
                    title:instance.dict.attention,
                    text:instance.dict.cancel_changes,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#dd6b55",
                    confirmButtonText:instance.dict.continue,
                    cancelButtonText:instance.dict.cancel,
                    closeOnConfirm: false,
                    closeOnCancel: true
                }, function(isConfirm) {

                    if (isConfirm) {
                        flag = true;
                        button.click();
                        swal.close();
                    } else {
                        swal.close();
                    }
                } );
            })
        }
    }
    
    static initCheckboxes(){

        $('input:checkbox').click(function (event) {
            let checkbox = $( this );

            if (checkbox.is( ':checked' ))
                checkbox.val( true );
            else
                checkbox.val( false );
        });
    }

    defineVars($div) {

        let tryFields = [
            'input', 'textarea', 'select'
        ];

        let $input;

        tryFields.forEach( (key) => {
            let input = $div.find( key );
            if (input.length === 1) {
                $input = input;
                return false;
            }
        } );

        if ($input === undefined) {
            console.warn('Selected field is not supported!');
            return;
        }

        let [$span, spanId] = this.getSpanId( $div, $input );

        return [$input, $span, spanId];
    }

    getSpanId($div, $input) {
        let $span;
        let spanId;

        if ($input.prop( 'localName' ) === 'select') {

            $span  = $input.parent().children().last();
            spanId = $span.attr( 'id' );
        }
        else {
            $span  = $( 'span:first', $div );
            spanId = $span.prop( 'id' );
        }

        return [$span, spanId];
    }

    reRenderErrors($input, $div, spanId, $span) {

        if ($input.val() === '') {
            this.markAsValid( $div, $span, spanId );
        }
        else {
            this.markAsNotValid( $div, $span, spanId );
        }
    }

    markAsValid($div, $span, spanId) {

        $div.addClass( 'has-error' );

        if (this.messages.hasOwnProperty( spanId )) {
            $span.html( this.messages[spanId] );
            delete this.messages[spanId];
        }
    }

    markAsNotValid($div, $span, spanId) {

        $div.removeClass( 'has-error' );

        if (!this.messages.hasOwnProperty( spanId )) {
            this.messages[spanId] = $span.html();
        }

        $span.empty();
    }

    static checkSelect($select, message) {

        let $div = $select.first().parent().first().parent();

        let $span = $select.parent().children().last();

        if (!$select.val()) {

            $div.addClass( 'has-error' );
            $span.html( message );
            return;
        }

        $div.removeClass( 'has-error' );
        $span.empty();
    }

    initAjaxLoader() {

        /* Set Ajax loader  */
        $( document ).ajaxStart( function() {
            var overlay = $( '<div id="overlay" class="ajax_loader"></div>' );
            overlay.appendTo( document.body );

        } );

        $( document ).ajaxSuccess( function() {
            $( '#overlay' ).remove();
        } );

        $( document ).ajaxError( function() {
            $( '#overlay' ).remove();
        } );
    }
    
    initMenu(){
        
        $(function($) {
            setTimeout(function() {
                $('#content-wrapper > .row').css({
                    opacity: 1
                });
            }, 200);

            $('#sidebar-nav .dropdown-toggle').on('click', function (e) {
                e.preventDefault();

                var $item = $(this).parent();

                if (!$item.hasClass('open')) {
                    $item.parent().find('.open .submenu').slideUp('fast');
                    $item.parent().find('.open').toggleClass('open');
                }

                $item.toggleClass('open');

                if ($item.hasClass('open')) {
                    $item.children('.submenu').slideDown('fast');
                }
                else {
                    $item.children('.submenu').slideUp('fast');
                }
            });

            $('body').on('mouseenter', '#page-wrapper.nav-small #sidebar-nav .dropdown-toggle', function (e) {
                var $sidebar = $(this).parents('#sidebar-nav');

                if ($( document ).width() >= 992) {
                    var $item = $(this).parent();

                    $item.addClass('open');
                    $item.children('.submenu').slideDown('fast');
                }
            });

            $('body').on('mouseleave', '#page-wrapper.nav-small #sidebar-nav > .nav-pills > li', function (e) {
                var $sidebar = $(this).parents('#sidebar-nav');

                if ($( document ).width() >= 992) {
                    var $item = $(this);

                    if ($item.hasClass('open')) {
                        $item.find('.open .submenu').slideUp('fast');
                        $item.find('.open').removeClass('open');
                        $item.children('.submenu').slideUp('fast');
                    }

                    $item.removeClass('open');
                }
            });

            $('#make-small-nav').click(function (e) {
                $('#page-wrapper').toggleClass('nav-small');
            });

            $(window).smartresize(function(){
                if ($( document ).width() <= 991) {
                    $('#page-wrapper').removeClass('nav-small');
                }
            });

            $('.mobile-search').click(function(e) {
                e.preventDefault();

                $('.mobile-search').addClass('active');
                $('.mobile-search form input.form-control').focus();
            });
            $(document).mouseup(function (e) {
                var container = $('.mobile-search');

                if (!container.is(e.target) // if the target of the click isn't the container...
                    && container.has(e.target).length === 0) // ... nor a descendant of the container
                {
                    container.removeClass('active');
                }
            });
            

            // build all tooltips from data-attributes
            $("[data-toggle='tooltip']").each(function (index, el) {
                $(el).tooltip({
                    placement: $(this).data("placement") || 'top'
                });
            });
        });

        $.fn.removeClassPrefix = function(prefix) {
            this.each(function(i, el) {
                var classes = el.className.split(" ").filter(function(c) {
                    return c.lastIndexOf(prefix, 0) !== 0;
                });
                el.className = classes.join(" ");
            });
            return this;
        };

        (function($,sr){
            // debouncing function from John Hann
            // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
            var debounce = function (func, threshold, execAsap) {
                var timeout;

                return function debounced () {
                    var obj = this, args = arguments;
                    function delayed () {
                        if (!execAsap)
                            func.apply(obj, args);
                        timeout = null;
                    };

                    if (timeout)
                        clearTimeout(timeout);
                    else if (execAsap)
                        func.apply(obj, args);

                    timeout = setTimeout(delayed, threshold || 100);
                };
            }
            // smartresize 
            jQuery.fn[sr] = function(fn){	return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

        })(jQuery,'smartresize');
    }
}