/**
 * Author: Peshkov Maxim
 * Created: 17.05.16 13:13
 * Email:  peshkov-maximum@yandex.ru
 *
 * Class: IndexPage
 * Description: Store selected checkboxes in Map object
 *
 * Require on the page the following elements:
 *  - table
 *  - input:hidden:id:current-page              - current page number
 *  - div:id:content                            - block for rendering Ajax response
 *  - input:hidden:id:current-url               - For redirecting if first page is now displayed
 *  - input:checkbox:id:select-all-checkboxes   - checkbox, which gives as select all functionality
 *  - div:id:table-action-buttons                - set statuses block in div:id:filter-block
 *  - checkboxes                                - table > thead > tr > th > div > input:checkbox
 *  -
 *
 * This class mast be extended with others classes where Ajax requests are handling
 */
class IndexPage {

    static init(dict = {}, config = {}) {
        return new IndexPage( dict, config );
    }

    constructor(dict, config) {
        this.dict   = dict;
        this.config = config;

        this.allCheckboxesMap = new Map();
        this.currentPage      = parseInt( $( '#current-page' ).val() ) || 1;
        this.pushToHistory    = true;

        this.disabledButtons = {needDisabling: false};
    }
    
    
    meAlert(name){
        alert(name);
    }

    /**
     * Render contend from Ajax request
     * @param data
     * @param obj
     */
    reloadContent(data, obj = {}) {
        
        // render ajax page
        $( '#page-body-content' ).html( data );

        if (typeof this.initMergeListener === 'function') {
            this.initMergeListener();
        }

        this.currentPage = parseInt( $( '#current-page' ).val() ) || 1;

        this.restoreCheckboxesState();
        this.registerCheckboxesListeners();

        if (this.pushToHistory)
            window.history.pushState( {page_url: $( '#current-url' ).val()}, "title 1", $( '#current-url' ).val() );
        else
            this.pushToHistory = true;
        
        if (typeof obj.success === 'function') {
            obj.success( data );
        }
    }

    registerCheckboxesListeners() {

        let instance = this;

        $( 'table > tbody > tr > td ' ).on( 'click', function(e) {
            if (e.target !== this) {return;}

            instance.handleRowSelector( $( this ).parent().children().last().find( 'input:checkbox' ).first().get( 0 ) );
        } );

        $( "#select-all-checkboxes" ).on( 'change', function() {

            if ($( this ).is( ':checked' )) {
                $( 'table > tbody> tr> td > div > input' ).prop( 'checked', true );
                let checked = $( 'table > tbody> tr> td > div > input:checked' );
                checked.closest( 'tr' ).addClass( 'active' );
                checked.each( function(index, element) {
                    let id = parseInt( element.id.substr( 9, element.id.length ) );
                    instance.pushCheckboxInStack( id );
                } );

            } else {
                let checked = $( 'table > tbody> tr> td > div > input:checked' );
                checked.closest( 'tr' ).removeClass( 'active' );
                checked.each( function(index, element) {
                    let id = parseInt( element.id.substr( 9, element.id.length ) );
                    instance.pullCheckboxFromStack( id );
                } );

                $( 'table > tbody > tr > td > div > input:checkbox' ).prop( 'checked', false );
            }
        } );
    }
    
    handleRowSelector(element) {

        let id = parseInt( element.id.substr( 9, element.id.length ) );

        let $element = $( element );

        if ($element.is( ':checked' )) {

            $element.prop( 'checked', false );
            $element.closest( 'tr' ).removeClass( 'active' );

            if (!$( 'table > tbody> tr> td > div > input:checked' ).length)
                $( '#table-action-buttons' ).hide();

            this.pullCheckboxFromStack( id );

        } else {

            $element.prop( 'checked', true );
            $( '#table-action-buttons' ).show();
            $element.closest( 'tr' ).addClass( 'active' );

            this.pushCheckboxInStack( id );
        }

    }

    handleCheckboxSelector(element) {

        let id = parseInt( element.id.substr( 9, element.id.length ) );

        let $element = $( element );

        if ($element.is( ':checked' )) {
            
            $( '#table-action-buttons' ).show();
            $element.closest( 'tr' ).addClass( 'active' );
            
            this.pushCheckboxInStack( id );

        } else {
            
            $element.closest( 'tr' ).removeClass( 'active' );
            if (!$( 'table > tbody> tr> td > div > input:checked' ).length)
                $( '#table-action-buttons' ).hide();

            this.pullCheckboxFromStack( id );
        }

    }



    getSelectedEntityIds() {

        let ids = [];

        if (this.allCheckboxesMap.size <= 0)
            return this.getCurrentPageEntitiesIds();

        this.allCheckboxesMap.forEach( function(value) {

            if (Array.isArray( value ))
                value.forEach( value => ids.push( parseInt( value ) ) );
        } );

        return ids;
    }

    getCurrentPageEntitiesIds() {

        let ids = [];

        $( 'table > tbody> tr> td > div > input:checked' ).each( function(index, element) {
            if (element.id.substr( 0, 9 ) === 'selector-')
                ids.push( parseInt( element.id.substr( 9, element.id.length ) ) );
        } );

        return ids;
    };

    pushCheckboxInStack(id) {

        if (typeof  parseInt( id ) !== 'number') {
            console.error( 'Element id must be integer!' );
            return;
        }

        let ids_on_this_page = this.allCheckboxesMap.get( this.currentPage );

        if (!this.allCheckboxesMap.has( this.currentPage )) {
            ids_on_this_page = [];
            ids_on_this_page.push( id );
            this.allCheckboxesMap.set( this.currentPage, ids_on_this_page );
            this.countSelected();
            return;
        }

        if (!Array.isArray( ids_on_this_page )) {
            console.warn( 'Elements in checkboxes map must be type of array!' );
            return;
        }

        var index = ids_on_this_page.indexOf( id );
        if (index !== -1) {
            console.info( 'This item already selected!' );
            return;
        }

        // set value
        ids_on_this_page.push( id );
        this.allCheckboxesMap.set( this.currentPage, ids_on_this_page );
        
        this.countSelected();
    }

    pullCheckboxFromStack(id) {
        
        if (typeof  parseInt( id ) !== 'number') {
            console.error( 'Element id must be integer!' );
            return;
        }

        let ids_on_this_page = this.allCheckboxesMap.get( this.currentPage );

        if (ids_on_this_page === undefined) {
            console.info( 'Nothing to pull from table checked list!' );
            return;
        }

        if (!Array.isArray( ids_on_this_page )) {
            console.warn( 'Elements in checkboxes map must be type of array!' );
            return;
        }

        if (!ids_on_this_page.length) {
            console.info( 'Not item selected on this page!' );
            return;
        }

        let index = ids_on_this_page.indexOf( id );
        if (index === -1) {
            console.info( 'This item already pulled!' );
            return;
        }

        if (index > -1) {
            ids_on_this_page.splice( index, 1 );
        }

        // set value
        this.allCheckboxesMap.set( this.currentPage, ids_on_this_page );
        this.countSelected();
    }

    countSelected() {
        
        let instance = this;

        $( '#table-action-buttons' ).find( 'span.pull-right' )
            .html( this.dict.selected_items + this.countSelectedRows() +
                ' <button type="button" class="btn btn-default btn-xs" id="unselect-all" title="' + 
                this.dict.unselect_all + '"><i class="fa fa-remove"></i></button>' );

        $( '#unselect-all' ).on( 'click', ()=> {
            instance.unselectSelectedRows();
        } );
    };

    /**  get and set current page checked checkboxes */
    restoreCheckboxesState() {

        let ids = this.allCheckboxesMap.get( this.currentPage );

        // set each checkbox
        $( 'table > tbody > tr > td > div > input' ).each( function(index, element) {

            if (element.id.substr( 0, 9 ) === 'selector-') {

                let organization_id = element.id.substr( 9, element.id.length );

                if (Helper.in_array( parseInt( organization_id ), ids )) {
                    let $checkbox = $( '#selector-' + organization_id );
                    $checkbox.closest( 'tr' ).addClass( 'active' );
                    $checkbox.prop( 'checked', true );
                }
            }
        } );

        // set select-all selector
        if ($( 'table > tbody > tr > td > div > input:checked' ).length === $( 'table > tbody > tr ' ).length)
            $( 'table > thead > tr > th > div > input:checkbox' ).prop( 'checked', true );
        
    }

    /**
     * Count selected items in map
     *
     * @param number count */
    countSelectedRows() {

        var count = 0;

        this.allCheckboxesMap.forEach( function(value, key) {
            if (typeof  parseInt( key ) === 'number') {
                if (Array.isArray( value )) {
                    value.forEach( function(element) {
                        if (typeof  parseInt( element ) === 'number')
                            count++;
                    } );
                }
            } else console.error();

        } );

        if (count !== 0)
            $( '#table-action-buttons' ).show();
        else
            $( '#table-action-buttons' ).hide();

        this.disableElements(count);

        return count;
    }

    /**
     * Disable HTML element
     *
     * Require property:

         this.disabledButtons = {
                status:true,
                selector_type: 'class',
                selector_name:[
                    'merge-link'
                ]
         };

     * On each page there are can be HTML elements which must
     * be disabled if at least one element(row) selected
     *
     */
    disableElements() {

        if (this.disabledButtons.needDisabling === true) {
            
            $(this.disabledButtons.disable_selector).addClass('not-active');
            $(this.disabledButtons.enable_selector).removeClass('not-active');
        }
    }
    
    
    unselectSelectedRows(){
        
        let instance = this;
        
        instance.allCheckboxesMap.clear();

        $( 'table > thead> tr> th > div > input:checked' ).prop( 'checked', false );

        let $checked = $( 'table > tbody> tr> td > div > input:checked' );
        
        $checked.closest( 'tr' ).removeClass( 'active' );
        $checked.prop( 'checked', false );

        $( '#table-action-buttons' ).hide();
    }
}