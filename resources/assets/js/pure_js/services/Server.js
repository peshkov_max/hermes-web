/**
 * Author: Peshkov Maxim
 * Email:  peshkov-maximum@yandex.ru
 */
class Server {

    static init(dict = {}) {
        return new Server( dict );
    }

    constructor(dict) {
        this.dict = dict;
    }

    /**
     * Implement get request
     *
     * @param url
     * @param data
     * @param obj
     * @returns {void}
     */
    static get(url, data={}, obj = {}) {

        $.ajax( {
            type: "GET",
            url: url,
            data: data,
            success: (data) => {

                if (typeof obj.success === 'function') {
                    obj.success( data );
                }
            },
            error: (data) => {
                if (typeof obj.error === 'function') {
                    obj.error( data );
                }
                throw new Exception( `Get request exception. URL: ${url}` );
            }
        } );

    }

    /**
     * Implement get request
     *
     * @param url
     * @param data
     * @param obj
     * @returns {void}
     */
    static patch(url, data, obj = {}) {

        $.ajax( {
            type: "PATCH",
            url: url,
            data: data,
            success: (data) => {

                if (typeof obj.success === 'function') {
                    obj.success( data );
                }
            },
            error: (data) => {
                if (typeof obj.error === 'function') {
                    obj.error( data );
                }
                throw new Exception( `Get request exception. URL: ${url}` );
            }
        } );

    }

    /**
     *  Send file to server 
     * @param url
     * @param data
     * @param obj
     */
    static postFile(url, data, obj = {}) {
       
        $.ajax( {
            type: "POST",
            url: url,
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            },
            success: (data) => {
                if (typeof obj.success === 'function') {
                    obj.success( data );
                }
            },
            error: (data) => {
                if (typeof obj.error === 'function') {
                    obj.error( data );
                }
                console.log( `Get request exception. URL: ${url}` );
            }
        } );

    }

    /**
     * Implement post request
     * @param url
     * @param data
     * @param obj
     */
    static post(url, data, obj = {}){
       
        $.post(url, data, function(data) {
              
                if (typeof obj.success === 'function') {
                    obj.success( data );
                }
            }, 'json')
          
            .fail( function(data) {
                if (typeof obj.error === 'function') {
                    obj.error( data );
                }
                console.log( `Get request exception. URL: ${url}` );
            } );
    }
    
 
}