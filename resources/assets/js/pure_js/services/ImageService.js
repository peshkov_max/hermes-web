/**
 * Author: Peshkov Maxim
 * Email:  peshkov-maximum@yandex.ru
 */
class ImageService {

    /** @constructor */
    constructor(dict) {
        this.dict = dict;
    }

    /**
     * Init function 
     * @param dict
     * @returns {ImageService} 
     */
    static init(dict = {}) {
        return new ImageService( dict );
    }

    /** 
     * 
     * @param file
     * @param  $helpBlock
     * @param dictionary
     * @returns {boolean}
     */
    static imageIsValid(file, $helpBlock, dictionary) {
        console.log( 2342342342342 );
        let errorMessage = '';

        // check file extension
        if (!ImageService.isImage( file )) {
            errorMessage = dictionary.selectImage;
        }

        // check file size
        if (!ImageService.under1MB( file )) {
            errorMessage = dictionary.toBigImage;
        }

        if (errorMessage !== '') {
            $helpBlock.parent().addClass( 'has-error' );
            $helpBlock.html( errorMessage );
            $helpBlock.show();
            return false;
        }
        
        $helpBlock.parent().removeClass( 'has-error' );
        $helpBlock.empty();
        $helpBlock.hide();
        return true;
    }


    /**
     * If given file under 1 MB
     * @param file
     * @param messages
     * @returns {boolean}
     */
    static under1MB(file, messages = null){
   
        let fileGreaterThan1MB = file.size / 1024 / 1024 > 1;

        if (fileGreaterThan1MB) {

            if (messages !== null)
            swal({
                title: messages.title,
                text: messages.text,
                type: 'warning'
            });
            
            
            return false;
        }
        else
            return true;
    }

    /**
     *  If the given file is has image extension
     *
     * @param file
     * @param messages
     * @returns {boolean}
     */
    static isImage(file, messages = null){

        let fileIsImage = /^image\/\w+/.test( file.type );
        
        if (!fileIsImage ) {
            
            if (messages !== null)
            swal({
                title: messages.title,
                text: messages.text,
                type: 'warning'
            });
            
            return false;
        }
        else
            return true;
    }
}