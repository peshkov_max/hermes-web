/**
 * Author: Peshkov Maxim
 * Email:  peshkov-maximum@yandex.ru
 */
class Helper {

    init(dict = {}) {
        return new Helper( dict );
    }

    constructor(dict) {
        this.dict = dict;
    }

    static image_update(element_id, url) {
        $( `#${element_id}` ).attr( 'src', url + '?' + new Date().getTime() );
    }

    static reset(id) {
        $( "#" + id ).wrap( '<form>' ).closest( 'form' ).get( 0 ).reset();
        $( "#" + id ).unwrap();
    }

    static in_array(needle, haystack, strict) {

        var found = false, key, strict = !!strict;
        for (key in haystack) {
            if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
                found = true;
                break;
            }
        }
        return found;
    }

    static render_form_errors(errors, $form) {

        $form.find( '.form-group' ).removeClass( 'has-error' ).find( '.help-block' ).text( '' );

        $.each( errors, function(key, value) {
            var $input = $form.find( 'input[name=' + key + ']' );
            $input.next( 'span' ).html( value );
            $input.closest( '.form-group' ).addClass( 'has-error' );
        } );
    }

    /**
     * Gets the classname of an object or function if it can.  Otherwise returns the provided default.
     *
     * Getting the name of a function is not a standard feature, so while this will work in many
     * cases, it should not be relied upon except for informational messages (e.g. logging and Error
     * messages).
     */
    static class_name(object, defaultName = 'objectName') {

        var nameFromToStringRegex = /^function\s?([^\s(]*)/;
        var result                = "";

        if (typeof object === 'function') {
            result = object.name || object.toString().match( nameFromToStringRegex )[1];
        } else if (typeof object.constructor === 'function') {
            result = Helper.class_name( object.constructor, defaultName );
        }
        return result || defaultName;
    }

    /**
     * Check if the given string is valid json
     *
     * @param str
     * @returns {boolean}
     * @constructor
     */
    static is_json_string(str) {
        try {
            JSON.parse( str );
        } catch (e) {
            return false;
        }
        return true;
    }
    
}