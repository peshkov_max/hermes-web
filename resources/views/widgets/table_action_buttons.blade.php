
<div class="filter-block" id="table-action-buttons" style="display: none;">
    <div class='row'>
        <div class='col-lg-12 col-sm-12'>
            <div class="pull-right">

                @if(isset($config['links']))
                    @foreach($config['links'] as $link)
                        <a @if(isset($link['id'])) id="{{$link['id']}}" @endif
                            @if(isset($link['href'])) href="{{$link['href']}}" @endif
                            @if(isset($link['class'])) class="{{$link['class']}}" @endif   @if(isset($link['title']))  title="{{$link['title']}}"@endif>
                            @if(isset($link['icon']))<i class="fa fa-{{$link['icon']}} fa-lg"></i>@endif @if(isset($link['title'])) {{$link['title']}}@endif</a>
                    @endforeach
                @endif

                    @if(isset($config['buttons']))
                            @foreach($config['buttons'] as $button)
                            <button @if(isset($button['id'])) id="{{$button['id']}}" @endif
                            type="button"
                                @if(isset($button['class'])) class="{{$button['class']}}" @endif @if(isset($button['title'])) title="{{$link['title']}}"@endif>
                                @if(isset($button['icon']))<i class="fa fa-{{$button['icon']}} fa-lg"></i>@endif  @if(isset($button['title'])) {{$link['title']}}@endif</button>
                        @endforeach
                    @endif

            </div>
        </div>
        <div class='col-lg-12 col-sm-12' style="margin-top: 10px;">
            <span class="pull-right"> <span class="helper-block"></span> </span>
        </div>
    </div>
</div>