@extends('layouts.login')

@section('content')
    <div class="container-fluid" style="margin-top: 100px;">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">@lang('passwords.do_reset')</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                @lang('passwords.input_failed')<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                            {!! Form::open([  'url' => url('password/reset'), 'method'=>'POST',  "class"=>"form-horizontal" ]) !!}
                            {!! Form::hidden('token', $token)!!}

                            <div class="form-group">
                                <label class="col-md-4 control-label">@lang('passwords.email')</label>
                                <div class="col-md-6">
                                    {!! Form::email('email', $email ?? old('email'), ['id'=>'email', "class"=> "form-control", "required", "placeholder"=>"E-mail"])!!}

                                    @if (Session::has('email'))
                                        <div class="alert alert-info">
                                            {{Session::get('email')}}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">@lang('passwords.password_new')</label>
                                <div class="col-md-6">
                                    {!! Form::password('password', ['id'=>'password', "class"=> "form-control", "required",])!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">@lang('passwords.password_repeat')</label>
                                <div class="col-md-6">
                                    {!! Form::password('password_confirmation', ['id'=>'password-confirm', "class"=> "form-control", "required"])!!}
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    {!! Form::button(trans('passwords.do_reset'), ['id'=>'reset-password','type' => 'submit','class'=> 'btn btn-primary']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop