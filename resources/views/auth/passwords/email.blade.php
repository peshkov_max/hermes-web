@extends('layouts.login')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div id="login-box">
                    <div id="login-box-holder">
                        <div class="row">
                            <div class="col-xs-12">

                                <header id="login-header">
                                    <div id="login-logo">
                                        <img
                                                src="{{asset(config('wipon.image_path'))}}"
                                                onerror="this.src='{{ asset(config('wipon.image_path')) }}'"
                                                alt="CABINET"/> CABINET
                                    </div>
                                </header>

                                <div id="login-box-inner">
                                    {!! Form::open([  'url' => url('password/email'), 'method'=>'POST',  "class"=>"form-horizontal" ]) !!}
                                    @if (count($errors) > 0)
                                        <div class="input-group">
                                            <div class="alert alert-danger">
                                                @lang('auth.wrong_email')
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="input-group">
                                        @lang('auth.put_email')
                                    </div>

                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        {!! Form::email('email',  old('email'), ["class"=> "form-control", "required", "placeholder"=>"facebook@mail.com"])!!}
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-12">
                                            {{Form::button('<i class="fa fa-paper-plane-o"></i>  '.trans('auth.remind_password'), ['id'=>'send-email-button','type' => 'submit','class'=> 'btn btn-success col-xs-12'])}}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>

                    <div id="login-box-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                @include('partials._copyright')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
