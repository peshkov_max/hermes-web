@extends('layouts.login')

@section('content')

    <div class>
        <div class="row">
            <div class="col-xs-12">
                <div id="login-box">
                    <div id="login-box-holder">
                        <div class="row">
                            <div class="col-xs-12">
                                <header id="login-header">
                                    <div id="login-logo">
                                        <img src="{{ asset(config('wipon.image_path'))}}"
                                             onerror="this.src='{{ asset(config('wipon.image_path')) }}'"
                                             alt="CABINET"/> Аналитический центр Wipon
                                    </div>
                                </header>

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{!! $error !!}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                @if (Session::has('status'))
                                    <div class="alert alert-info">
                                        {{ Session::get('status') }}
                                    </div>
                                @endif

                                <div id="login-box-inner">
                                    {!! Form::open([  'url' => url('login'), 'method'=>'POST',  "class"=>"form-horizontal" , "id"=>"login-form"]) !!}
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        {!! Form::text('login',  old('login'), ["class"=> "form-control", "required", "placeholder"=>trans('app.login_or_email')])!!}
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                        {!! Form::password('password',   ["class"=> "form-control", "required",  "placeholder"=>trans('app.password')])!!}
                                    </div>
                                    <div id="remember-me-wrapper">
                                        <div class="row">
                                            <div class="col-xs-6">

                                                <div class="checkbox">
                                                    <input checked="checked" name="remember" type="checkbox" value="1"
                                                           id="checkbox" class="styled">
                                                    <label for="checkbox">
                                                        @lang('app.remember_me')
                                                    </label>
                                                </div>
                                            </div>


                                            <a href="{{ url('password/email') }}" id="login-forget-link"
                                               class="col-xs-6" style="padding-top: 7px; margin-top: 0px;">
                                                @lang('app.forget_password')
                                            </a>



                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            {{Form::button('<i class="fa fa-sign-in"></i> '.trans('app.login'), ['type' => 'submit','class'=> 'btn btn-success col-xs-12', 'id'=>'login-button', 'name' => 'submit_button'])}}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="login-box-footer">
                        <div class="row">
                            <div class="col-xs-12">
                                @include('partials._copyright')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
