<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
        height: 22px;
    }

    .custom-border-text-center{
        border: 1px solid #000000;
        text-align: center;
    }

    .custom-border-text-right{
        border: 1px solid #000000;
        text-align: right;
    }

</style>

<tr></tr>

<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>

<tr></tr>

<tr>
    <td  align="left" valign="middle" height="22">
        <b>@lang('app.report_for_almaty_region')</b>
    </td>
</tr>

<tr>
    <td  align="left" valign="middle" height="22"  colspan="13">
        @lang('app.report_for_almaty_region_info')
    </td>
</tr>

<tr></tr>

<tr>
    <th class="custom-border-text-left" width="15">@lang('app.phone_number')</th>
    <th class="custom-border-text-center" width="15">@lang('app.quantity')</th>
</tr>

@foreach($rows as $key => $item)
    <tr>
        <td height="20px" class="custom-border-text-left">
            {{ $item['phone'] }}
        </td>

        <td  class="custom-border-text-right">
            {{ $item['receipt_number'] }}
        </td>
    </tr>

@endforeach

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td  height="20" >
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>