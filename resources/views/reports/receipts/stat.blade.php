<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
        height: 22px;
    }

    .custom-border-text-center{
        border: 1px solid #000000;
        text-align: center;
    }

    .custom-border-text-right{
        border: 1px solid #000000;
        text-align: right;
    }

</style>

<tr></tr>

<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>
<tr>
    <td  align="left" valign="middle" height="22">
        <b>@lang('app.require_receipt_stat')</b>
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.by_year', ['year'=> $year ])
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        {{$receiptNumber}}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.form_data') {{ carbon_user_tz_now()->format(config('wipon.date_time_format'))  }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.user')  {{ $userName }}
    </td>
</tr>
<tr>
</tr>

<tr>
    <th class="custom-border-text-left" width="30">@lang('app.regions')</th>
    <th class="custom-border-text-center" width="15">@lang('app.january')</th>
    <th class="custom-border-text-center" width="15">@lang('app.february')</th>
    <th class="custom-border-text-center" width="15">@lang('app.march')</th>
    <th class="custom-border-text-center" width="15">@lang('app.april')</th>
    <th class="custom-border-text-center" width="15">@lang('app.may')</th>
    <th class="custom-border-text-center" width="15">@lang('app.june')</th>
    <th class="custom-border-text-center" width="15">@lang('app.july')</th>
    <th class="custom-border-text-center" width="15">@lang('app.august')</th>
    <th class="custom-border-text-center" width="15">@lang('app.september')</th>
    <th class="custom-border-text-center" width="15">@lang('app.october')</th>
    <th class="custom-border-text-center" width="15">@lang('app.november')</th>
    <th class="custom-border-text-center" width="15">@lang('app.december')</th>
    <th class="custom-border-text-right" width="15">@lang('app.total')</th>
</tr>

@foreach($items as $key => $item)
    <tr>
        <td height="20px" class="custom-border-text-left">
            {{ $item['region_name'] }}
        </td>

        <td  class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['january']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['february']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['march']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['april']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['january']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['june']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['july']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['august']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['september']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['october']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['november']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['december']) }}
        </td>

        <td class="custom-border-text-right">
            {{ sprintf("%01.2f", $item['total']) }}
        </td>
    </tr>

@endforeach

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td  height="20" >
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>