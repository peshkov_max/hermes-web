<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
        height: 22px;
    }

    .custom-border-text-right{
        border: 1px solid #000000;
        text-align: right;
        height: 22px;
    }

</style>

<tr></tr>

<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>
<tr>
    <td  align="left" valign="middle" height="22">
        <b>{{$title}}</b>
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.region') {{ $regionName }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.date_filter') {{
        \Carbon\Carbon::createFromFormat(config('wipon.date_time_format'), $period[0], 'UTC')
        ->timezone( config('wipon.user_time_zone'))
        ->format(config('wipon.dt_format_d_m_y')) }} - {{

        \Carbon\Carbon::createFromFormat(config('wipon.date_time_format'), $period[1],  'UTC')
        ->timezone( config('wipon.user_time_zone'))
        ->format(config('wipon.dt_format_d_m_y'))
        }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.receipts_quantity', ['number'=>$items->count()])
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.form_data') {{ carbon_user_tz_now()->format(config('wipon.date_time_format'))  }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.user')  {{ $userName }}
    </td>
</tr>
<tr>
</tr>

<tr>
    <th class="custom-border-text-left" width="8px">@lang('app.id')</th>
    <th class="custom-border-text-left" width="35">@lang('app.region')</th>
    <th class="custom-border-text-left" width="30">@lang('app.member_name')</th>
    <th class="custom-border-text-left" width="30">@lang('app.member_phone')</th>
    <th class="custom-border-text-left" width="30">@lang('app.email')</th>
    <th class="custom-border-text-left" width="50">@lang('app.device_uuid')</th>
    <th class="custom-border-text-left" width="30">@lang('app.receipt_status')</th>
    <th class="custom-border-text-left" width="25">@lang('app.given_datetime')</th>
</tr>

@foreach($items as $key => $item)
    <tr>
        <td class="custom-border-text-left">
            {{ $item->id }}
        </td>

        <td height="20px" class="custom-border-text-left">
            {{ $item->region_name }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->member_name ?? '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->member_phone_number ?? '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->member_email ?? '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->device_uuid }}
        </td>

        <td class="custom-border-text-left">
            {{ trans('app.receipt_'.implode('_', preg_split('/[-,]+/', $item->status) )) }}
        </td>

        <td class="custom-border-text-left">
            {{$item->created_at }}
        </td>
    </tr>

@endforeach

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td  height="20" >
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>