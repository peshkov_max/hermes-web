<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
        height: 22px;
    }

    .custom-border-text-center{
        border: 1px solid #000000;
        text-align: center;
    }

</style>

<tr></tr>

<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>
<tr>
    <td  align="left" valign="middle" height="22">
        <b>@lang('app.receipt_report_messages')</b>
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.region') {{ $regionName }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.date_filter')  {{
        \Carbon\Carbon::createFromFormat(config('wipon.date_time_format'), $period[0], 'UTC')
        ->timezone( config('wipon.user_time_zone'))
        ->format(config('wipon.dt_format_d_m_y')) }} - {{

        \Carbon\Carbon::createFromFormat(config('wipon.date_time_format'), $period[1],  'UTC')
        ->timezone( config('wipon.user_time_zone'))
        ->format(config('wipon.dt_format_d_m_y'))
        }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        {{$receiptNumber}}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.form_data') {{ carbon_user_tz_now()->format(config('wipon.date_time_format'))  }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.user')  {{ $userName }}
    </td>
</tr>
<tr>
</tr>

<tr>
    <th class="custom-border-text-left" width="5">@lang('app.number')</th>
    <th class="custom-border-text-center" width="15">@lang('app.message_id')</th>
    <th class="custom-border-text-center" width="35">@lang('app.region_name')</th>
    <th class="custom-border-text-center" width="45">@lang('app.address')</th>
    <th class="custom-border-text-center" width="25">@lang('app.company_name')</th>
    <th class="custom-border-text-center" width="25">@lang('app.member_name')</th>
    <th class="custom-border-text-center" width="25">@lang('app.member_phone')</th>
    <th class="custom-border-text-center" width="25">@lang('app.member_email')</th>
    <th class="custom-border-text-center" width="55">@lang('app.device_uuid')</th>
    <th class="custom-border-text-center" width="25">@lang('app.receipt_report_type')</th>
    <th class="custom-border-text-center" width="50">@lang('app.message')</th>
    <th class="custom-border-text-center" width="30">@lang('app.message_sent_at')</th>
</tr>

@foreach($items as $key => $item)
    <tr>
        <td height="20px" class="custom-border-text-left">
            {{ $key+1 }}
        </td>

        <td  class="custom-border-text-left">
            {{ $item->id }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->region->name }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->organization_address }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->organization_name }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->device->customer->name ?? '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->device->customer->phone_number ?? '-'  }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->device->customer->email ?? '-'  }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->device_uuid }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->type->name }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->message }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->created_at }}
        </td>
    </tr>

@endforeach

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td  height="20" >
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>