<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-center {
        border: 1px solid #000000;
        text-align: center;
    }

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
    }

</style>
<tr></tr>


<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>


<tr>
    <td align="left" valign="middle" height="22">
        <b>@lang('app.region_report_title_details')</b>
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.region') {{ $regionName }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.date_filter')  {{ $period }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.form_data') {{ get_current_time() }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.user')  {{ $userName }}
    </td>
</tr>
<tr>
    <td align="left" height="20px">
        @lang('app.region_note')
    </td>
</tr>
<tr>
    <td align="left" height="20px">
        @lang('app.region_note_1')
    </td>
</tr>
<tr>
    <td></td>
</tr>
<tr>
    <th height="20px" width="15" align="left" height="22px">@lang('app.cluster_id')</th>
    <th class="custom-border-text-left" width="25">@lang('app.cluster_name')</th>
    <th class="custom-border-text-left" width="25">@lang('app.retailer')</th>
    <th class="custom-border-text-left" width="50">@lang('app.address')</th>
    <th class="custom-border-text-left" width="25">@lang('app.latitude'), @lang('app.longitude')</th>
    <th class="custom-border-text-left" width="25">@lang('app.product_name')</th>
    <th class="custom-border-text-left" width="25">@lang('app.product_type')</th>
    <th class="custom-border-text-left" width="25">@lang('app.organization_name')</th>
    <th class="custom-border-text-left" width="25">@lang('app.product_excise_code')</th>
    <th class="custom-border-text-left" width="20">@lang('app.product_serial_number')</th>
    <th class="custom-border-text-left" width="25">@lang('app.ukm_status')</th>
    <th class="custom-border-text-left" width="60">@lang('app.scanned_at')</th>
</tr>

@foreach($checks as $key => $check)

    <tr>
        <td height="20px" class="custom-border-text-left">
            {{ $check['id'] ?? '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ isset($check['cluster_name']) && $check['cluster_name'] !='' ? $check['cluster_name']:  '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ isset($check['retailer']) && $check['retailer'] !='' ? $check['retailer']:  '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ trans('app.address_computed', ['city_name'=> $check['city_name'], 'street'=>$check['street'], 'house'=>$check['house'] ])   }}
        </td>

        <td class="custom-border-text-left">
            {{ format_coordinates( $check['latitude'], $check['longitude']) }}
        </td>

        <td class="custom-border-text-left" height="20px">
            {{ $check['product_name'] ?? '-'  }}
        </td>

        <td class="custom-border-text-left">
            {{ $check['product_type_name'] ?? '-' }}
        </td>

        <td class="custom-border-text-left" style="width: 45px;">
            {{ $check['organization_name'] ?? '-'  }}
        </td>

        <td class="custom-border-text-left" style="width: 50px;">
            {{ json_decode($check['item_sticker'])->excise_code  ?? '-' }}
        </td>

        <td class="custom-border-text-left" style="width: 50px;">
            {{ json_decode($check['item_sticker'])->serial_number ?? '-' }}
        </td>

        <td class="custom-border-text-left">
            {{  isset($check['item_status']) && $check['item_status'] =='duplicate' ? trans('app.duplicate' )
                      : trans('app.'. $check['item_status']) ?? ''  ?? '-'  }}
        </td>

        <td class="custom-border-text-left" style="width: 25px;">
            {{ $check['scanned_at'] ?? '-'  }}
        </td>
    </tr>
@endforeach

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td height="20">
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>