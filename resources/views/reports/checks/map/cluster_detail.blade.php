<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-center {
        border: 1px solid #000000;
        text-align: center;
    }

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
    }

    .info-line {
        height: 20px;
        text-align: left;
    }

</style>
<tr></tr>

<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>

<tr>
    <td align="left" valign="middle" height="22">
        <b>@lang('app.region_report_title_details')</b>
    </td>
</tr>
<tr>
    <td class="info-line" colspan="3">
        @lang('app.form_data') {{ get_current_time() }}
    </td>
</tr>
<tr>
    <td class="info-line" colspan="3">
        @lang('app.user')  {{ $userName }}
    </td>
</tr>
<tr>
    <td class="info-line">
        @lang('app.region_note')
    </td>
</tr>
<tr>
    <td class="info-line">
        @lang('app.region_note_1')
    </td>
</tr>
<tr>
    <td class="info-line">
        @lang('app.region_note_2')
    </td>
</tr>

<tr>
    <td class="info-line"><strong>@lang('app.cluster_info')</strong></td>
</tr>

<tr>
    <td class="info-line"> @lang('app.id'): {{$cluster->id}}</td>
</tr>
<tr>
    <td class="info-line"> @lang('app.name'): {{$cluster->name ?? '-'}}</td>
</tr>
<tr>
    <td class="info-line"> @lang('app.address') {{$cluster->address ?? '-'}}</td>
</tr>
<tr>
    <td class="info-line"> @lang('app.unique_devices') {{$cluster->unique_devices ?? '-'}}</td>
</tr>

<tr>
    <td></td>
</tr>

<tr>
    <th class="custom-border-text-left" width="25" height="20">@lang('app.retailer')</th>
    <th class="custom-border-text-left" width="25">@lang('app.latitude'), @lang('app.longitude')</th>
    <th class="custom-border-text-left" width="35">@lang('app.product_name')</th>
    <th class="custom-border-text-left" width="25">@lang('app.organization_name')</th>

    <th class="custom-border-text-left">@lang('app.sticker_uri')</th>
    <th class="custom-border-text-left">@lang('app.label_uri')</th>

    <th class="custom-border-text-left" width="25">@lang('app.product_excise_code')</th>
    <th class="custom-border-text-left" width="25">@lang('app.ukm_status')</th>
    <th class="custom-border-text-left" width="60">@lang('app.scanned_at')</th>
</tr>

@foreach($checks as $key => $check)

    <tr>
        <td class="custom-border-text-left">
            {{ isset($check->retailer) && $check->retailer !='' ? $check->retailer :  '-' }}
        </td>

        <td class="custom-border-text-left">
            {{  format_coordinates($check['geo_latitude'] , $check['geo_longitude']) }}
        </td>

        <td class="custom-border-text-left" height="20px">
            {{ ($check->item && $check->item->product && $check->item->product->type && $check->item->product->type->name) ? $check->item->product->type->name.', ' : '' }}{{ $check->item->product->name ?? '-'  }}
        </td>

        <td class="custom-border-text-left" style="width: 45px;">
            {{ $check->item->product->organization->name ?? '-'  }}
        </td>

        <td class="custom-border-text-left" width="20"  @if( hasCheckItems($check, $personal_items)) height="100"  @endif margin="0">
            @if( hasCheckItems($check, $personal_items))
                <img src="{{ storage_path('app/temp/' . $check->id) . '/label_uri.jpg' }}" alt="label_uri" width="50" height="100">
            @endif
        </td>

        <td class="custom-border-text-left" width="20"  @if( hasCheckItems($check, $personal_items)) height="100"  @endif margin="0">
        @if( hasCheckItems($check, $personal_items))
                <img src="{{ storage_path('app/temp/' . $check->id) . '/sticker_uri.jpg' }}" alt="sticker_uri"  width="150">
            @endif
        </td>


        <td class="custom-border-text-left" style="width: 50px;">
            {{ $check->item->sticker['excise_code']  ?? '-' }}
        </td>

        <td class="custom-border-text-left">
            {{  $check->item->status =='duplicate'
                ? trans('app.duplicate' )
                : trans('app.'. $check->item->status)  }}
        </td>

        <td class="custom-border-text-left" style="width: 25px;">
            {{ $check->created_at ?? '-'  }}
        </td>
    </tr>

@endforeach

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td height="20">
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>