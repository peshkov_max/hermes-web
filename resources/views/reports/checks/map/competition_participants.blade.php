<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-center {
        border: 1px solid #000000;
        text-align: center;
    }

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
        height: 22px;
    }

    .custom-border-text-center{
        border: 1px solid #000000;
        text-align: center;
    }

</style>

<tr></tr>

<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>
<tr>
    <td  align="left" valign="middle" height="22">
        <b>@lang('app.competition_title')</b>
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.region') {{ $regionName }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.date_filter') {{ get_month_ru(\Carbon\Carbon::createFromFormat(config('wipon.date_time_format'),$period['start'])->format('F') ) }} {{ \Carbon\Carbon::createFromFormat(config('wipon.date_time_format'),$period['end'])->format('Y')  }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.form_data') {{ get_current_time()  }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.user')  {{ $userName }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" >
    <b>    @lang('app.competition_note')</b>
    </td>
</tr>
<tr>
    <td align="left" height="20px" >
        @lang('app.competition_note_1')
    </td>
</tr>
<tr>
    <td align="left" height="20px" >
        @lang('app.competition_note_2')
    </td>
</tr>
<tr>
    <td align="left" height="20px" >
        @lang('app.competition_note_3')
    </td>
</tr>
<tr>
</tr>

<tr>
    <th class="custom-border-text-left" width="8px">№</th>
    <th class="custom-border-text-left" width="40">@lang('app.user_device_uuid')</th>
    <th class="custom-border-text-left" width="15">@lang('app.cluster_id')</th>
    <th class="custom-border-text-left" width="25">@lang('app.cluster_name')</th>
    <th class="custom-border-text-left" width="40">@lang('app.address')</th>
    <th class="custom-border-text-left" width="35">@lang('app.latitude'), @lang('app.longitude')</th>

    <th class="custom-border-text-left" width="30">@lang('app.product_name')</th>
    <th class="custom-border-text-left" width="30">@lang('app.product_type')</th>
    <th class="custom-border-text-left" width="30">@lang('app.organization_name')</th>
    <th class="custom-border-text-left" width="60">@lang('app.product_excise_code')</th>
    <th class="custom-border-text-left" width="25">@lang('app.scanned_at')</th>
    <th class="custom-border-text-left" width="25">@lang('app.ukm_status')</th>
</tr>

@foreach($items as $key => $item)
    <tr>
        <td class="custom-border-text-left">{{$key+1}}</td>

        <td class="custom-border-text-left">
            {{ $item->device_uuid }}
        </td>

        <td height="20px" class="custom-border-text-left">
            {{ $item->cluster_id }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->cluster_name ?? '-' }}
        </td>

        <td class="custom-border-text-left">
           г. {{ $item->city_name  ?? trans('app.no_city_name') }}, {{ $item->street ?? trans('app.no_street')}}, г. {{ $item->house ?? trans('app.no_house')}}
        </td>

        <td class="custom-border-text-left">
            {{  format_coordinates($item->latitude, $item->longitude) }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->product_name }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->product_type }}
        </td>

        <td class="custom-border-text-left">
            {{ $item->organization_name}}
        </td>

        <td class="custom-border-text-left">
            {{ $item->excise_code}}
        </td>

        <td class="custom-border-text-center">
            {{ $item->created_at }}
        </td>

        <td class="custom-border-text-center">
            {{ $item->is_duplicated ? trans('app.duplicate' ) : trans('app.'. $item->status) ?? ''}}
        </td>

    </tr>

@endforeach

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td  height="20" >
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>