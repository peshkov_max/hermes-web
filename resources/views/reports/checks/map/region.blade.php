<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style type="text/css">

    .custom-border-center {
        border: 1px solid #000000;
        text-align: center;
    }

    .custom-border-text-left {
        border: 1px solid #000000;
        text-align: left;
    }

</style>
<tr></tr>

<tr>
    <td align="center" valign="middle" height="57"></td>
</tr>

<tr>
    <td  align="left" valign="middle" height="22">
        <b>@lang('app.region_report_title')</b>
    </td>
</tr>

<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.region') {{ $regionName }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.date_filter') {{ $period }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.form_data') {{ get_current_time() }}
    </td>
</tr>
<tr>
    <td align="left" height="20px" colspan="3">
        @lang('app.user')  {{ $userName }}
    </td>
</tr>

<tr>
    <td align="left" height="20px" >
        @lang('app.region_note')
    </td>
</tr>
<tr>
    <td align="left" height="20px" >
        @lang('app.region_note_1')
    </td>
</tr>

<tr>
    <td></td>
    <td width="2px"></td>
</tr>

<tr style="border: 1px solid #000000;">
    <th rowspan="3" height="20px" width="11" valign="middle">@lang('app.cluster_id')</th>

    <th rowspan="3" width="35" valign="middle">@lang('app.cluster_name')</th>

    <th rowspan="3" width="45%" valign="middle">@lang('app.address')</th>

    <th rowspan="3" width="22" valign="middle">@lang('app.latitude'), @lang('app.longitude')</th>

    <th colspan="9" style="text-align: center;">@lang('app.status_ukm')</th>

    <th rowspan="3" width="23" style="vertical-align: middle;">@lang('app.unique_devices')</th>
</tr>

<tr>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
    <th colspan="2" height="20" style="text-align: center; border: 1px solid #000000;">@lang('app.product_valid')</th>
    <th colspan="2" style="text-align: center; border: 1px solid #000000;">@lang('app.product_fake')</th>
    <th colspan="2" style="text-align: center; border: 1px solid #000000;">@lang('app.product_atlas')</th>
    <th colspan="2" style="text-align: center; border: 1px solid #000000;">@lang('app.product_duplicate')</th>
    <th rowspan="2" style="vertical-align: middle; border: 1px solid #000000;">@lang('app.total_')</th>
</tr>

<tr style="border: 1px solid #000000;">
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>
    <th align="center"></th>

    <th align="center" width="9" height="20px">@lang('app.by_scan_short')</th>
    <th align="center" width="9">@lang('app.by_hand_short')</th>

    <th align="center" width="9">@lang('app.by_scan_short')</th>
    <th align="center" width="9">@lang('app.by_hand_short')</th>

    <th align="center" width="11">@lang('app.by_scan_short')</th>
    <th align="center" width="11">@lang('app.by_hand_short')</th>

    <th align="center" width="9">@lang('app.by_scan_short')</th>
    <th align="center" width="9">@lang('app.by_hand_short')</th>
</tr>

<?php

// count sums for groups
$check_valid_scan_sum = 0;

$check_valid_hand_sum = 0;
$check_fake_scan_sum = 0;
$check_fake_hand_sum = 0;
$check_atlas_scan_sum = 0;
$check_atlas_hand_sum = 0;
$check_duplicate_scan_sum = 0;
$check_duplicate_hand_sum = 0;

?>

@foreach($clusters as $key=> $cluster)

    <tr>
        <td height="22px" class="custom-border-text-left">
            {{ $cluster->id }}
        </td>

        <td height="20px" class="custom-border-text-left">
            {{ $cluster->name ??  '-' }}
        </td>

        <td class="custom-border-text-left">
            {{ $cluster->address }}
        </td>

        <td class="custom-border-text-left" width="24" >
            {{ format_coordinates($cluster->latitude,  $cluster->longitude) }}
        </td>

        <td class="custom-border-center">
            <?php $check_valid_scan_sum += $cluster->check_valid_scan; ?>
            {{ $cluster->check_valid_scan }}
        </td>

        <td class="custom-border-center">
            <?php $check_valid_hand_sum += $cluster->check_valid_hand;?>
            {{ $cluster->check_valid_hand }}
        </td>

        <td class="custom-border-center">
        <?php $check_fake_scan_sum += $cluster->check_fake_scan;?>
            {{ $cluster->check_fake_scan}}
        </td>

        <td class="custom-border-center">
        <?php $check_fake_hand_sum += $cluster->check_fake_hand;?>
            {{ $cluster->check_fake_hand}}
        </td>

        <td class="custom-border-center">
        <?php $check_atlas_scan_sum += $cluster->check_atlas_scan;?>
            {{ $cluster->check_atlas_scan}}
        </td>

        <td class="custom-border-center">
        <?php $check_atlas_hand_sum += $cluster->check_atlas_hand;?>
            {{ $cluster->check_atlas_hand}}
        </td>

        <td class="custom-border-center">
            <?php $check_duplicate_scan_sum += $cluster->check_duplicate_scan; ?>
            {{ $cluster->check_duplicate_scan }}
        </td>

        <td class="custom-border-center">
            <?php $check_duplicate_hand_sum += $cluster->check_duplicate_hand;?>
            {{ $cluster->check_duplicate_hand }}
        </td>

        <td class="custom-border-center">
            {{ $cluster->check_total }}
        </td>

        <td class="custom-border-center">
            {{ $cluster->unique_devices }}
        </td>
    </tr>
@endforeach

<tr>
   <td></td>
    <td></td>
    <td></td>

    <td align="right" height="20px" class="custom-border-center"><b>@lang('app.total_scans')</b></td>

    <td class="custom-border-center">
        {{ $check_valid_scan_sum }}
    </td>
    <td class="custom-border-center">
        {{ $check_valid_hand_sum }}
    </td>
    <td class="custom-border-center">
        {{ $check_fake_scan_sum }}
    </td>
    <td class="custom-border-center">
        {{ $check_fake_hand_sum }}
    </td>
    <td class="custom-border-center">
        {{ $check_atlas_scan_sum }}
    </td>
    <td class="custom-border-center">
        {{ $check_atlas_hand_sum }}
    </td>
    <td class="custom-border-center">
        {{ $check_duplicate_scan_sum }}
    </td>
    <td class="custom-border-center">
        {{ $check_duplicate_hand_sum }}
    </td>
    <td class="custom-border-center">
        {{ $clusters->sum('check_total') }}
    </td>
    <td class="custom-border-center">
        {{ $clusters->sum('unique_devices') }}
    </td>

</tr>

<tr></tr>

<tr>
    <td height="20">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td  height="20" >
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>