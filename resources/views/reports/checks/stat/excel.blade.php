<html>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">

    .bordered-center {
        border: 1px solid #000000;
        text-align: center;
    }

    .bordered-left{
        border: 1px solid #000000;
        text-align: left;
    }

    .bordered-right{
        border: 1px solid #000000;
        text-align: right;
    }
    
</style>
<tr></tr>

<tr>
    <td colspan="15" align="center" valign="middle" height="40">
        <b>@lang('app.cluster_stat')</b>
    </td>
</tr>

<tr></tr>

<tr>
    <th class="bordered-center">№</th>
    <th class="bordered-center" height="18">{{trans('app.region_name')}}</th>
    <th class="bordered-center"><span>{{trans('app.january')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.february')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.march')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.april')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.may')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.june')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.july')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.august')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.september')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.october')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.november')}}</span></th>
    <th class="bordered-center"><span>{{trans('app.december')}}</span></th>
    <th class="bordered-center">{{trans('app.total')}}</th>
</tr>

<?php
$i = 0;
$len = count($checks);
?>
@foreach($checks as $row => $check)

    <tr>
        <td class="bordered-left">{{$row+1}}</td>
        <td class="bordered-left">{{ $check['region_name'] }}</td>
        <td class="bordered-right" height="18">{{ $check['january']}}</td>
        <td class="bordered-right">{{ $check['february']}}</td>
        <td class="bordered-right">{{ $check['march']}}</td>
        <td class="bordered-right">{{ $check['april']}}</td>
        <td class="bordered-right">{{ $check['may'] }}</td>
        <td class="bordered-right">{{ $check['june'] }}</td>
        <td class="bordered-right">{{ $check['july']}}</td>
        <td class="bordered-right">{{ $check['august'] }}</td>
        <td class="bordered-right">{{ $check['september'] }}</td>
        <td class="bordered-right">{{ $check['october'] }}</td>
        <td class="bordered-right">{{ $check['november']}}</td>
        <td class="bordered-right">{{ $check['december'] }}</td>
        <td class="bordered-right">{{ $check['total'] }}</td>
    </tr>

    <?php  $i++; ?>
@endforeach

<tr></tr>

<tr>
    <td height="20" colspan="3">
        © 2016 Wipon
    </td>
</tr>

<tr>
    <td height="20" colspan="5">
        E-mail: info@wiponapp.com
    </td>
</tr>

</html>