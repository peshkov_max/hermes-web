@extends("layouts.app")

@section('css')
    @parent

    <?php $wipon_config = config('wipon'); ?>
    <?php $wipon_user_roles = auth()->user()->roles->pluck('name')->toArray(); ?>

    <script type="text/javascript">

        window.Laravel = {
            env: '{{ config( 'app.env' ) }}',
            roles: <?= json_encode($wipon_user_roles) ?>,
            wipon_config: <?= json_encode($wipon_config) ?>,
            current_locale: '{{ current_locale() }}',
            static_server: '{{ config('wipon.static_server').'/images' }}',
            path_to_i18n: '{{ elixir( 'js/i18n/localization.json' ) }}',
            csrf_token: '{{ csrf_token() }}',
            currentView: '{{$viewModel}}'
        };

    </script>
@stop


@section('content-header')
    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-12">
            {!! Breadcrumbs::render('vue_js', $viewModel) !!}
        </div>
    </div>
@stop

@section('content')
    <div class="main-box clearfix relative">
        <div id="app">
            <component is="{{ $viewModel }}"
                       :params="{{ isset($params) ? json_encode($params) : '{}' }}"
                       default-image-path="{{ asset(config('wipon.default_image_path')) }}"
                       current-locale="{{ current_locale() }}"
                       static-server="{{ config('wipon.static_server').'/images' }}"
                       user-time-zone="{{ config('wipon.user_time_zone', 'UTC') }}"
                       v-ref:vm
            ></component>
        </div>
    </div>
@stop

