<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

    <title>Wipon</title>

    <link type="image/x-icon" rel="shortcut icon" href="{{ asset('favicon.png') }}"/>
    <link rel="stylesheet" href="{{ elixir('css/vendor.css') }}"/>
    <link rel="stylesheet" href="{{ elixir('css/app.css')}}"/>

    <!--[if lt IE 9]>
    <script src="{{ elixir( 'js/forIE9.js') }}"></script>
    <![endif]-->
    @yield('css')
</head>
<body>
<div class="remodal-bg">
    <div id='ajax-wrapper' class="ajax-wrapper-style">

        {{-- Used in _header and _left_menu --}}
        <?php $organization = auth()->user()->organization;?>

        @include('partials._header')


        <div id='page-wrapper' class='container container-width'>
            <div class='row'>

                <div id='nav-col'>
                    @include('partials._left_menu')
                </div>
                <div id="content-wrapper">

                    @yield('content-header')

                    @yield('content')

                    @include('partials._footer')
                </div>
            </div>

        </div>
    </div>

</div>

<!-- app scripts -->
<script src='{{ elixir('js/vendor.js') }}'></script>

<script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?language=ru&libraries=drawing&key={{ env('GOOGLE_MAPS_KEY', '') }}"></script>

<script src='{{ elixir('js/app.js') }}'></script>
<script src="{{ elixir('js/vue_app.js') }}"></script>

<script type="text/javascript">

    var dict = {
        attention: '@lang('app.attention')',
        continue: '@lang('app.continue')',
        cancel: '@lang('app.cancel')',
        cancel_changes: '@lang('app.cancel_changes')',
        selected_items: '@lang('filter.selected_items')',
        unselect_all: '@lang('filter.unselect_all')'
    };

    App.init( dict );

    var imageServiceMessages = {
        error: '@lang('app.error')',
        warning: '@lang('app.warning')'
    };

    ImageService.init( imageServiceMessages );

</script>

@yield('js')

</body>
</html>