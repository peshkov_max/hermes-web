<html>
<head>
    <link href='{{asset('css/error.css')}}' rel='stylesheet' type='text/css'>
    <link type='image/x-icon' href='{{ asset('favicon.png') }}' rel='shortcut icon' />

    <title>Wipon</title>

</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">@yield("content","Unknown error")</div>
    </div>
</div>
</body>
</html>
