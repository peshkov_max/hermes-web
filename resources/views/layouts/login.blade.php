<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Wipon</title>

    <link type="image/x-icon" rel="shortcut icon" href="{{ asset('favicon.png') }}"/>
    <link rel="stylesheet" href="{{ elixir('css/vendor.css') }}" />
    <link rel="stylesheet" href="{{ elixir('css/app.css')}}" />

   <!--suppress CommaExpressionJS -->
    @if ( ! in_array(config('app.env'), ['local', 'testing', 'staging']))
        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                            (i[r].q = i[r].q || []).push( arguments )
                        }, i[r].l = 1 * new Date();
                a = s.createElement( o ),
                        m = s.getElementsByTagName( o )[0];
                a.async = 1;
                a.src   = g;
                m.parentNode.insertBefore( a, m )
            })( window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga' );

            ga( 'create', 'UA-74065417-1', 'auto' );
            ga( 'send', 'pageview' );

        </script>
    @endif

</head>
<body id="login-page">

@yield('content')

</body>
</html>