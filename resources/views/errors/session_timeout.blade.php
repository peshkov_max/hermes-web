@extends("layouts.error")

@section('content')
    Время сессии истекло. Вернитесь на предыдущую страницу, обновите её (F5). Если проблема не исчезла - обратитесь к администраторам сайта.
    <p style="font-size: 40px;">
        info@wiponapp.com
    </p>
@stop
