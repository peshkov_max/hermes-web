@extends("layouts.error")


@section('content')
    <div class='row'>

        <div class='col-lg-12'>

            <div class='row'>
                <div class='col-lg-12'>
                    <h1>@lang('app.error')</h1>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="main-box">
                    <header class="main-box-header clearfix">
                            <div class="alert alert-danger">

                            </div>
                    </header>

                </div>
            </div>
        </div>
    </div>

@stop


