@extends("layouts.app")

@section('content-header')
    <div class="row" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-12">
            {!! Breadcrumbs::render('home.dashboard') !!}
            <h1>@lang('app.general_information')</h1>
        </div>
    </div>
@stop


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="main-box clearfix">
                Wipon. Личный кабинет пользователя.
            </div>
        </div>
    </div>
@stop
