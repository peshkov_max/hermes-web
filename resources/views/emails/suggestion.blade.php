@lang('app.email_title') <br>
@lang('app.email_text', ['text' => $text]) <br>
@lang('app.notification_sent_at') @lang('app.email_time', ['time'=>$time]) @lang('app.email_date', ['date'=>$date])<br>