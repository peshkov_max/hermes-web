<p>Вас приветствует компания Wipon.</p>
<p>Статистика была обновлена. За последние 24 часа:</p>
<p><strong>Проверок УКМ: {{array_get($messageFields, "checks_total", 0)}}. Из них:</strong></p>

<ul>
    <li>Поддельных: {{ array_get($messageFields, "checks_fake.total", 0) }} (сканированием - {{ array_get($messageFields, "checks_fake.scan", 0)}}, вводом вручную - {{ array_get($messageFields, "checks_fake.hand", 0) }}) </li>
    <li>Подлинных: {{ array_get($messageFields, "checks_valid.total", 0) }} (сканированием - {{ array_get($messageFields, "checks_valid.scan", 0) }}, вводом вручную - {{ array_get($messageFields, "checks_valid.hand", 0) }}) </li>
    <li>Атлас: {{ array_get($messageFields, "checks_atlas.total", 0)}} (сканированием - {{ array_get($messageFields, "checks_atlas.scan", 0) }}, вводом вручную - {{ array_get($messageFields, "checks_atlas.hand", 0) }})</li>
    <li>Дубликатов: {{ array_get($messageFields, "checks_duplicate.total", 0) }} (сканированием - {{ array_get($messageFields, "checks_duplicate.scan", 0)}}, вводом вручную - {{ array_get($messageFields, "checks_duplicate.hand", 0) }}</li>
</ul>

@include('partials._email_footer')
