@lang('app.email_title') <br>
@lang('app.email_text', ['text' => $text]) <br>
@lang('app.notification_sent_at') @lang('app.email_time', ['time'=>$time]) @lang('app.email_date', ['date'=>$date])<br>
@lang('app.user_name',['user_name' => $userName ]) <br>
@lang('app.user_email', ['email'=>$email])<br>
@lang('app.phone', ['phone'=>$phone])<br>