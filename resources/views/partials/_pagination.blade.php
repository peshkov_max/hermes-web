<?php
// config
$link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
?>

@if ($paginator->lastPage() > 1)
    <ul class="pagination">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled hide-element' : '' }}" title="@lang('partials.first')">
            <a id="{{$prefix ?? ""}}-pagination-first-page-link" href="{{ $paginator->url(1) }}"><<</a>
        </li>
        <li class="{{ ($paginator->currentPage() == 1) || ($paginator->currentPage() - 10) < 0? ' disabled hide-element' : '' }}" title="@lang('partials.previous_ten')">
            <a id="{{$prefix ?? ""}}-pagination-prev-ten-page-link" href="{{ $paginator->url(($paginator->currentPage() - 10) > 0 ? $paginator->currentPage()-10 : 1 ) }}"><</a>
        </li>

        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
            $half_total_links = floor($link_limit / 2);
            $from = $paginator->currentPage() - $half_total_links;
            $to = $paginator->currentPage() + $half_total_links;
            if ($paginator->currentPage() < $half_total_links) {
                $to += $half_total_links - $paginator->currentPage();
            }
            if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
            }
            ?>
            @if ($from < $i && $i < $to)
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a id="{{$prefix ?? ""}}-pagination-{{$i}}-page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endif
        @endfor
        <li class="{{ ($paginator->currentPage() == $paginator->lastPage()) || ($paginator->lastPage()-$paginator->currentPage())<10 ? ' disabled hide-element' : '' }}" title="@lang('partials.next_ten')">
            <a id="{{$prefix ?? ""}}-pagination-next-ten-page-link" href="{{ $paginator->url(($paginator->lastPage()-$paginator->currentPage())>10 ? $paginator->currentPage()+10 : $paginator->lastPage()) }}" >></a>
        </li>
        <li class="{{($paginator->currentPage() == $paginator->lastPage()) ? ' disabled hide-element' : '' }}"  title="@lang('partials.last')">
            <a id="{{$prefix ?? ""}}-pagination-last-page-link" href="{{ $paginator->url($paginator->lastPage()) }}">>></a>
        </li>
    </ul>
@endif
