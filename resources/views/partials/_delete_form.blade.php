<div id="modal-delete-{!! $data->id !!}" class="modal text-left fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['method' => 'DELETE', 'url' => $routeName])!!}
            {!! Form::hidden('redirectAfterDeleteUrl', isset($redirectUrl) ? $redirectUrl : null )!!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title">@lang('app.Delete')</h1>
            </div>
            <div class="modal-body">
                <p>
                    @lang('app.delete_question', ['item'=> $item_name])
                </p>
            </div>
            <div class="modal-footer">
                <button type="submit" id="success-delete-modal-{!!  $data->id !!}" class="btn btn-primary">@lang('app.yes')</button>
                <button type="button" id="cancel-delete-modal-{!!  $data->id !!}" class="btn btn-default" data-dismiss="modal">@lang('app.no')</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
