<div id='col-left-inner' class='col-left-nano-content'>

    <div id='user-left-box' class='clearfix hidden-sm hidden-xs'>
        <div style="text-align: center;">
            <a href="{{ url('/') }}" style="display: inline-block;" id="organization-logo-id">
                @if( (null !== auth()->user()->organization) && null !== auth()->user()->organization->logo)
                    <img alt="@lang('app.logo')"
                         src="{{ img_static(auth()->user()->organization->logo->thumb_uri  ?? config('wipon.default_image_path')) }}"
                         onerror="this.src='{{ asset(config('wipon.default_image_path')) }}'">
                @else
                    <img alt="@lang('app.logo')"
                         src="{{ asset( config('wipon.default_image_path') ) }}">
                @endif
            </a>
        </div>
        <div class='user-box'>
            <h4>{{ auth()->user()->organization !== null ? auth()->user()->organization->name : 'Wipon' }}</h4>
        </div>
    </div>

    <div class='collapse navbar-collapse navbar-ex1-collapse' id='sidebar-nav'>
        @include('vendor.laravel-menu.bootstrap-menu-left')
    </div>
</div>


@section('js')
@parent

        <!-- this page specific inline scripts -->
<script type="text/javascript">

    $(document).ready(function() {
        $('#sidebar-nav  a').on('click', function(e){
            window.localStorage.removeItem( 'cabinet_navigation_history' );
        });
    });

</script>

@stop