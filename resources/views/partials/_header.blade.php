<header class='navbar' id='header-navbar'>

    <div class='container container-width'>

        <div>
            <a href='http://wiponapp.com' id="logo" class='navbar-brand col-xs-1'>
                <img alt="Wipon"
                     src="{{ asset(config('wipon.image_path')) }}" style="display: inline-block;"
                     onerror="this.src='{{ asset(config('wipon.default_image_path')) }}'"
                />
                <div class="style-logo-head">Wipon</div>
            </a>
        </div>

        <div class='clearfix'>

            <button class='navbar-toggle'
                    data-target='.navbar-ex1-collapse'
                    data-toggle='collapse'
                    type='button'
                    id="toggle-navigation"
            >
                <span class='sr-only'>Toggle navigation</span>
                <span class='fa fa-bars'></span>
            </button>

            <div class='nav-no-collapse navbar-left pull-left hidden-sm hidden-xs'>
                <ul class='nav navbar-nav pull-left'>
                    <li>
                        <a class='btn' id='make-small-nav'>
                            <i class='fa fa-bars'></i>
                        </a>
                    </li>
                </ul>
            </div>
            @if(config('app.env')!='production')
                <div style="float: left; padding-top: 10px; color: red;font-size: 24px; font-weight: 900;"> Wipon dev</div>
            @endif

            <div class='nav-no-collapse pull-right' id='header-nav'>
                <ul class='nav navbar-nav'>

                    <li class='dropdown hidden-sm hidden-xs visible-lg visible-md  profile-dropdown'>
                        <a class='dropdown-toggle' data-toggle='dropdown' id="menu-logo-link">

                            <img style="margin-top:5px; width: 25px; height: 25px;"
                                 src="{{ img_static($organization->logo->thumb_uri ?? config('wipon.default_image_path')) }}"
                                 onerror="this.src='{{ asset(config('wipon.default_image_path')) }}'"
                                 alt="@lang('app.logo')">
                            <b class='caret'></b>

                        </a>

                        <ul class='dropdown-menu' style="right: 0;left: initial;">
                            <li><a id="edit-profile" href='{{ route('users.edit', ['users'=> auth()->user()->id]) }}'><i
                                            class='fa fa-pencil'></i> @lang('app.profile_edit')</a></li>
                            <li><a id="edit-profile-password"
                                   href='{{ route('users.get_password', ['users'=> auth()->user()->id]) }}'><i
                                            class='fa fa-gears'></i> @lang('app.set_password')</a></li>
                            <li><a id="log-out" href='{{ url('logout') }}'><i
                                            class='fa fa-sign-out'></i> @lang('app._exit')</a></li>

                        </ul>
                    </li>


                    <li>
                        <a class='btn' id="log-out-sm" href='{{ url('logout') }}'>
                            <i class='fa fa-power-off'></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>