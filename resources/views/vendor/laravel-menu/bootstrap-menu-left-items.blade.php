@foreach($items as $item)

  <li @lm-attrs($item) @lm-endattrs>
    @if($item->link)
          <a @lm-attrs($item->link) @if($item->hasChildren()) class="dropdown-toggle" href="#" @endif  @lm-endattrs href="{!! $item->url() !!}">
              {!! $item->title !!}
              @if($item->hasChildren()) <div class="menu-item-icon-wipon-right"><i class="fa fa-chevron-circle-right drop-icon"></i> </div>@endif
          </a>
    @else
      {!! $item->title  !!}
    @endif
    @if($item->hasChildren())
      <ul class="submenu"  >
        @include(config('laravel-menu.views.bootstrap-items'), array('items' => $item->children()))
      </ul>
    @endif
  </li>
  @if($item->divider)
  	<li{!! Lavary\Menu\Builder::attributes($item->divider) !!}></li>
  @endif
@endforeach


