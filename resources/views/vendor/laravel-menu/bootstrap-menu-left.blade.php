<ul class='nav nav-pills nav-stacked'>
    @include('vendor.laravel-menu.bootstrap-menu-left-items', array('items' => \Wipon\Services\MenuGeneratorService::generate()->roots()))
    <li class='visible-xs hidden-lg hidden-md hidden-sm'>
        <a class="dropdown-toggle" href="#">
            <i class="fa fa-shield fa-1"></i>
            @if($organization) <span>{{$organization->name}}</span>
            @else <span>Внимание, пользователь не прикреплен к организации. Обратитесь к администраторам сайта. </span>
            @endif
            <i class="fa fa-chevron-circle-right drop-icon"></i> </a>
        <ul class="submenu">
            <li class="active">
                <a id="edit-profile" href='{{ route('users.edit', ['users'=> auth()->user()->id]) }}'>
                    <span>@lang('app.profile_edit')</span>

                </a>
            </li>
            <li>
                <a id="edit-profile-password" href='{{ route('users.get_password', ['users'=> auth()->user()->id]) }}'>
                    <span>@lang('app.set_password')</span>
                </a>
            </li>
            <li>
                <a id="log-out" href='{{ url('logout') }}'>
                    <span>@lang('app._exit')</span>
                </a>
            </li>
        </ul>
    </li>
</ul>


