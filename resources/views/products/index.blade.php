@extends("layouts.app")

@section('content-header')
    <div class="row" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
        <div class="col-sm-12">
            {!! Breadcrumbs::render('products.index') !!}
            <h1> @lang('app.product') </h1>
        </div>
    </div>
@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="main-box clearfix">

                <header class="main-box-header clearfix">

                    <div class="filter-block">
                        <div class='row'>
                            <div class='col-lg-12'>

                                @can('moderate_product')
                                <div class="form-group pull-right">
                                    <a href="{{route('products.merge')}}" id="merge-product-link"
                                       class="btn btn-primary pull-right"><i
                                                class="fa fa-object-group fa-lg"></i> @lang('app.merge_link')
                                    </a>
                                </div>
                                @endcan

                                <div class="form-group pull-right">
                                    <a id="create-product-link"
                                       href="{{ route('products.create').'?wizard=true' }}"
                                       class="btn btn-primary pull-right"><i
                                                class="fa fa-plus-circle fa-lg"></i> @lang('app.product_add')</a>
                                </div>

                                <div class="form-group pull-right">
                                    <button class="btn btn-primary pull-right" id="filter-trigger-button">
                                        <i class="fa fa-filter"></i> @lang('app.filter')</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="filter-block" id="product-filter-block" style="display: none;">

                        {!! Form::open([
                          'url' => route('products.index'),
                          'method'=>'GET',
                          'id' => 'filter-form',
                          "class"=>"form-horizontal" ]) !!}


                        <div class="form-group">
                            <label class="control-label col-sm-2" for="search">@lang('filter.search_name')</label>
                            <div class="controls text-left col-sm-3">
                                <div class="input-group">
                                    {!! Form::text('search',  Session::has('productFilter') && Session::get('productFilter')['query'] ? Session::get('productFilter')['query'] : null, ["id"=>"search" , 'class' =>'form-control', 'min'=>"3" ]) !!}
                                    <span class="input-group-btn">
                                       <button class="btn btn-default" type="button" id="product-search">
                                           <i class="fa fa-search"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>


                        @if(auth()->user()->isSuperAdmin())

                            <div class="form-group">
                                <label class="control-label col-sm-2"
                                       for="product-company">@lang('filter.company')</label>
                                <div class="controls text-left col-sm-3">

                                    {!! Form::select('organization_id', [], Session::get('productFilter')['organization_id'] ?? null, ["class"=> "form-control", "id"=>"product-organization",  "placeholder"=>trans('app.select_organization')])  !!}

                                </div>
                            </div>

                            {{--  FROZEN FUNCTIONALITY    --}}
                            {{--<div class="form-group">--}}
                            {{--<label class="control-label col-sm-2">@lang('filter.availability')</label>--}}
                            {{--<div class="text-left col-sm-10" id="enabled-block">--}}

                            {{--<div class="checkbox checkbox-success">--}}
                            {{--{!! Form::checkbox('use_availability',   Session::has('productFilter') && in_array(Session::get('productFilter')['is_enabled'], ['is_enabled', 'is_not_enabled']) ? 'true' : 'false', Session::has('productFilter') && in_array(Session::get('productFilter')['is_enabled'], ['is_enabled', 'is_not_enabled']) ? true : false  , ['id'=>'use-enabled', 'class' =>'stiled']) !!}--}}
                            {{--{!! Form::label('use-enabled', trans('app.use'))!!}--}}
                            {{--</div>--}}

                            {{--<div class="col-md-8">--}}
                            {{--<fieldset class="col-sm-12">--}}
                            {{--<div class="radio radio-info radio-inline">--}}
                            {{--{{ Form::radio('is_enabled', 'is_enabled',--}}
                            {{--Session::has('productFilter') && Session::get('productFilter')['is_enabled'] =='is_enabled' ? true : false, ['id'=>'is-enabled'] ) }}--}}
                            {{--<label class="self-written-style"--}}
                            {{--for="is-enabled">  @lang('filter.shown')</label>--}}
                            {{--</div>--}}
                            {{--<div class="radio radio-inline">--}}
                            {{--{{ Form::radio('is_enabled', 'is_not_enabled',--}}
                            {{--Session::has('productFilter') && Session::get('productFilter')['is_enabled'] =='is_not_enabled' ? true : false, ['id'=>'is-not-enabled']) }}--}}
                            {{--<label class="self-written-style"--}}
                            {{--for="is-not-enabled"> @lang('filter.hidden') </label>--}}
                            {{--</div>--}}
                            {{--</fieldset>--}}
                            {{--</div>--}}

                            {{--</div>--}}
                            {{--</div>--}}
                        @endif

                        @can('moderate_product')
                        <div class="form-group">
                            <label class="control-label col-sm-2">@lang('filter.moderation')</label>
                            <div class="text-left col-sm-10" id="moderated-block">


                                <div class="checkbox checkbox-success">
                                    {!! Form::checkbox('use_moderation',  Session::has('productFilter') && in_array(Session::get('productFilter')['is_moderated'], ['is_moderated', 'is_not_moderated']) ? 'true' : 'false' ,  Session::has('productFilter') && in_array(Session::get('productFilter')['is_moderated'], ['is_moderated', 'is_not_moderated']) ? true : false , ['id'=>'use-moderation', 'class' =>'stiled']) !!}
                                    {!! Form::label('use-moderation', trans('app.use'))!!}
                                </div>

                                <div class="col-md-8">
                                    <fieldset class="col-sm-12">
                                        <div class="radio radio-info radio-inline">
                                            {{ Form::radio('is_moderated', 'is_moderated',
                                        Session::has('productFilter') && Session::get('productFilter')['is_moderated'] =='is_moderated' ? true : false, ['id'=>'is-moderated'] ) }}
                                            <label class="self-written-style"
                                                   for="is-moderated">  @lang('filter.is_moderated')</label>
                                        </div>
                                        <div class="radio radio-inline">
                                            {{ Form::radio('is_moderated', 'is_not_moderated',
                                     Session::has('productFilter') && Session::get('productFilter')['is_moderated'] =='is_not_moderated' ? true : false, ['id'=>'is-not-moderated']) }}
                                            <label class="self-written-style"
                                                   for="is-not-moderated"> @lang('filter.is_noy_moderated') </label>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        @endcan

                        <div class="form-group">
                            <label for="sort" class="control-label col-sm-2">@lang('filter.sorting')</label>
                            <div class=" col-sm-10" id="sort-block">

                                <div class="checkbox checkbox-success">
                                    {!! Form::checkbox('use_sorting',  Session::has('productFilter') && in_array(Session::get('productFilter')['sort'], ['desc', 'asc']) ? 'true' : 'false' , Session::has('productFilter') && in_array(Session::get('productFilter')['sort'], ['desc', 'asc']) ? true : false  , ['id'=>'use-sort', 'class' =>'stiled']) !!}
                                    {!! Form::label('use-sort', trans('app.use')) !!}
                                </div>


                                <div class="col-md-8">
                                    <fieldset class="col-sm-12">
                                        <div class="radio radio-inline">
                                            {{ Form::radio('sort', 'asc',
                                        Session::has('productFilter') && Session::get('productFilter')['sort'] =='asc' ? true : false, ['id'=>'use-asc']) }}
                                            <label class="self-written-style"
                                                   for="use-asc"> @lang('filter.descending') </label>
                                        </div>
                                        <div class="radio radio-info radio-inline">
                                            {{ Form::radio('sort', 'desc',
                                        Session::has('productFilter') && Session::get('productFilter')['sort'] =='desc' ? true : false, ['id'=>'use-desc']) }}
                                            <label class="self-written-style"
                                                   for="use-desc">  @lang('filter.ascending')</label>
                                        </div>

                                    </fieldset>
                                </div>


                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="do-filter"></label>
                            <div class="text-left col-sm-10">
                                <div class="btn-group" data-toggle="buttons">
                                    {{Form::button('<i class="fa fa-filter"></i> '.trans('filter.do_filter'), ['id' => 'do-filter','type'=>'submit', 'class'=> ' btn btn-success'])}}
                                    {{Form::button(trans('filter.reset'), ['id' => 'do-reset','type'=>'submit', 'class'=> ' btn btn-default'])}}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>

                    <div class="filter-block">
                        <div class='row'>
                            <div class='col-lg-12'>

                                @can( 'moderate_product')
                                <div class="form-group pull-right" id="table-action-buttons" style="display: none;">

                                    {{--  FROZEN FUNCTIONALITY    --}}
                                    {{--<a id="assign-hide" href="{{route('app.set_status')}}"--}}
                                    {{--class="btn btn-success pull-right"> @lang('app.hide')</a>--}}
                                    {{--<a id="assign-show" href="{{route('app.set_status')}}"--}}
                                    {{--class="btn btn-success pull-right"> @lang('app.show')</a>--}}
                                    <a id="assign-set-is-moderated" href="{{route('products.set_status')}}"
                                       class="btn btn-success pull-right"> @lang('app.do_moderate')</a>
                                    <a id="assign-set-not-moderated" href="{{route('products.set_status')}}"
                                       class="btn btn-success pull-right"> @lang('app.do_not_moderate')</a>
                                </div>
                                @endcan

                            </div>
                        </div>
                    </div>
                </header>


                <div class="main-box-body clearfix">
                    <div class="table-responsive">
                        <div class="container container-width">


                            <div class="row pull-right">
                                <div class="col-lg-12">
                                    @include('partials._pagination', ['paginator' => $products, 'prefix'=>'top'])
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    @if(count($products)!=0)
                                        <table class="table" id="product-table">
                                            <thead>
                                            <tr>
                                                <th>№</th>
                                                <th width="10%">
                                                    <span>@lang('app.barcode')</span>
                                                </th>

                                                <th width="20%">
                                                    <span>@lang('app.product_title')</span>
                                                </th>

                                                <th width="20%">
                                                    <span>@lang('app.organization')</span>
                                                </th>

                                                {{--  FROZEN FUNCTIONALITY    --}}
                                                {{--<th>--}}
                                                {{--<span>@lang('filter.availability')</span>--}}
                                                {{--</th>--}}

                                                <th>
                                                    <span>@lang('filter.moderation')</span>
                                                </th>


                                                @can('moderate_product')

                                                <th class="text-center" width="8%"><span>@lang('app.actions')</span>
                                                </th>
                                                <th class="text-center">
                                                    <div class="checkbox">
                                                        <input type="checkbox" class="stiled"
                                                               id="select-all-checkboxes">
                                                        <label for="select-all-checkboxes"></label>
                                                    </div>
                                                </th>

                                                @endcan
                                            </tr>

                                            </thead>

                                            <tbody>

                                            <?php  /** @var  \Illuminate\Contracts\Pagination\Paginator $products */
                                            $i = get_table_index($products);?>

                                            @foreach ($products as $product)
                                                <tr>


                                                    <td class="text-center table-font-size">
                                                        {{$i++}}
                                                    </td>
                                                    <td class="table-font-size">
                                                        {{ $product->barcode ?? trans('app.no_info')}}
                                                    </td>

                                                    <td class="table-font-size">
                                                        <a href="{{route('products.show', [$product->id])}}"
                                                           id="show-product-{{ $product->id }}-link"
                                                           title="@lang('app.view')">  {{ $product->name }}</a>
                                                    </td>

                                                    <td class="table-font-size">
                                                        {{ $product->organization->name ?? trans('app.no_info')}}
                                                    </td>

                                                    {{--  FROZEN FUNCTIONALITY    --}}
                                                    {{--<td>--}}
                                                    {{--{!! $product->isEnabled() !!}--}}
                                                    {{--</td>--}}

                                                    <td>
                                                        {!! $product->isModerated() !!}
                                                    </td>


                                                    @can('manage_product')
                                                    <td class="text-center">

                                                        <a
                                                                id='edit-general-info-{{ $product->id }}-link'
                                                                href="{{route('products.edit',  [$product->id, 'previous_route' => \Request::route()->getName()] )}}"
                                                                class="table-link"
                                                                title="@lang('app.edit_general')">
                                                            <span class="fa-stack">
                                                                <i class="fa fa-square fa-stack-2x"></i>
                                                                <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>

                                                        <a
                                                                id='edit-visual-features-{{ $product->id }}-link'
                                                                href="{{route('products.show' , [$product->id, 'visual_features'=>true ])}}"
                                                                class="table-link"
                                                                title="@lang('app.edit_visual_features')">
                                                            <span class="fa-stack">
                                                                <i class="fa fa-square fa-stack-2x"></i>
                                                                <i class="fa fa-pencil-square fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>


                                                        <a id='show-product-{{ $product->id }}-table-link'
                                                           href="{{ route('products.show' , [$product->id ])}}"
                                                           class="table-link"
                                                           title="@lang('app.view')">
                                                            <span class="fa-stack">
                                                                <i class="fa fa-square fa-stack-2x"></i>
                                                                <i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                                                            </span>
                                                        </a>


                                                        @can('moderate_product')

                                                        <a
                                                                id='edit-aliases-{{ $product->id }}-link'
                                                                href="{{ route('aliases.index', ['aliasable_id'=> $product->id, 'aliasable_name' => $product->getTableName(), 'redirect_url' => \Request::fullUrl(), 'parent_breadcrumbs'=> 'products.index'])}}"
                                                                class="table-link"
                                                                style="margin:0px;"
                                                                title="@lang('app.edit_aliases')">
                                                     <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-pencil-square-o fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                        </a>


                                                        {{--  FROZEN FUNCTIONALITY    --}}

                                                        {{--                                                @if($product->is_enabled===true)--}}


                                                        {{--<a class="table-link"--}}
                                                        {{--href="#modal-set-is_enabled-{!! $product->id !!}"--}}
                                                        {{--data-toggle="modal" class="table-link"--}}
                                                        {{--title="@lang('app.hide')">--}}
                                                        {{--<span class="fa-stack">--}}
                                                        {{--<i class="fa fa-square fa-stack-2x"></i>--}}
                                                        {{--<i class="fa fa-unlock fa-stack-1x fa-inverse"></i>--}}
                                                        {{--</span>--}}
                                                        {{--</a>--}}

                                                        {{--@include('app.partials._set_status_form', ['product'=> $product, 'field' =>  'is_enabled', 'status' => 'false', 'message' => trans('app.hide'). '?'] )--}}
                                                        {{--@else--}}
                                                        {{--<a class="table-link"--}}
                                                        {{--href="#modal-set-is_enabled-{!! $product->id !!}"--}}
                                                        {{--data-toggle="modal" title="@lang('app.show')">--}}
                                                        {{--<span class="fa-stack">--}}
                                                        {{--<i class="fa fa-square fa-stack-2x"></i>--}}
                                                        {{--<i class="fa fa-lock fa-stack-1x fa-inverse"></i>--}}
                                                        {{--</span>--}}
                                                        {{--</a>--}}
                                                        {{--@include('app.partials._set_status_form', ['product'=> $product, 'field' =>  'is_enabled', 'status' => 'true', 'message' => trans('app.show'). '?'])--}}
                                                        {{--@endif--}}

                                                        @if($product->is_moderated===true)
                                                            <a id='set-not-moderated-{{ $product->id }}-link'
                                                               class="table-link"
                                                               href="#modal-set-is_moderated-{!! $product->id !!}"
                                                               data-toggle="modal"
                                                               title="@lang('app.set_not_moderated')">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-unlink fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                            </a>
                                                            @include('products.partials._set_status_form', ['product'=> $product, 'field' =>  'is_moderated', 'status' => 'false', 'message' => trans('app.set_not_moderated'). '?'])
                                                        @else
                                                            <a
                                                                    id='set-is-moderated-{{ $product->id }}-link'
                                                                    class="table-link"
                                                                    href="#modal-set-is_moderated-{!! $product->id !!}"
                                                                    data-toggle="modal"
                                                                    title="@lang('app.set_is_moderated')">
                                                                    <span class="fa-stack">
                                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                                        <i class="fa fa-circle fa-stack-1x fa-inverse"></i>
                                                                    </span>
                                                            </a>
                                                            @include('products.partials._set_status_form', ['product'=> $product, 'field' =>  'is_moderated', 'status' => 'true', 'message' => trans('app.set_is_moderated'). '?'])
                                                        @endif


                                                        <a
                                                                id='open-delete-modal-{{ $product->id }}-link'
                                                                class="table-link " data-toggle="modal"
                                                                href="#modal-delete-{!! $product->id !!}"
                                                                title="@lang('app.delete')">
                                                                <span class="fa-stack">
                                                                    <i class="fa fa-square fa-stack-2x"></i>
                                                                    <i class="fa fa-trash fa-stack-1x fa-inverse"></i>
                                                                </span>
                                                        </a>


                                                        @include('partials._delete_form', ['routeName'=> route('products.destroy', ['id'=>$product->id]), 'redirectUrl' => Request::fullUrl(), 'data' => $product, 'item_name'=>$product->name])

                                                        @endcan
                                                    </td>

                                                    @can('moderate_product')

                                                    <td class="text-center table-font-size">
                                                        <div class="checkbox">
                                                            <input type="checkbox" class="stiled"
                                                                   id="selected-{!! $product->id !!}">
                                                            <label for="selected-{!! $product->id !!}"></label>
                                                        </div>
                                                    </td>

                                                    @endcan
                                                    @endcan


                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    @else
                                        <div class="alert alert-danger">
                                            @lang('products.no_products')
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="row pull-right">
                                <div class="col-lg-12">
                                    @include('partials._pagination', ['paginator' => $products, 'prefix'=>'bottom'])
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('js')
    @parent


    <script>

        var dict = {
            chose_organization: '@lang('organizations.chose_organization')',
        };

        var config = {
            organization_search_url: '{{route('organizations.search')}}',
        };

        let productIndex = ProductIndex.init( dict, config );

        $( document ).ready( function() {

            $( '.disabled' ).click( function( e ) {
                e.preventDefault();
            } );

            /// set filter visibility

            @if(Session::has('productFilter'))
            <?php $productFilter = Session::get('productFilter'); ?>

            @if(isset($productFilter['use_availability']))


                @if( in_array($productFilter['is_enabled'], ['is_enabled']))
                    ProductIndex.setFilterParam( 'is-enabled', 'is_enabled', true, 'is-not-enabled', 'is_not_enabled', false, false );
            @elseif(in_array($productFilter['is_enabled'], ['is_not_enabled']))
                ProductIndex.setFilterParam( 'is-enabled', 'is_enabled', false, 'is-not-enabled', 'is_not_enabled', true, false );
            @else
                ProductIndex.setFilterParam( 'is-enabled', null, false, 'is-not-enabled', null, false, true );
            @endif

        @else
            ProductIndex.setFilterParam( 'is-enabled', null, false, 'is-not-enabled', null, false, true );
            @endif

            @if(isset($productFilter['use_moderation']))

                @if(in_array($productFilter['is_moderated'], ['is_moderated']))
                     ProductIndex.setFilterParam( 'is-moderated', 'is_moderated', true, 'is-not-moderated', 'is_not_moderated', false, false );
            @elseif(in_array($productFilter['is_moderated'], ['is_not_moderated']))
                 ProductIndex.setFilterParam( 'is-moderated', 'is_moderated', false, 'is-not-moderated', 'is_not_moderated', true, false );
            @else
                 ProductIndex.setFilterParam( 'is-moderated', null, false, 'is-not-moderated', null, false, true );
            @endif

        @else
            ProductIndex.setFilterParam( 'is-moderated', null, false, 'is-not-moderated', null, false, true );
            @endif


             @if(isset($productFilter['use_sorting']))
                @if(in_array($productFilter['sort'], ['desc']))
                    ProductIndex.setFilterParam( 'use-desc', 'desc', true, 'use-asc', 'asc', false, false );
            @elseif(in_array($productFilter['sort'], ['asc']))
                ProductIndex.setFilterParam( 'use-desc', 'desc', false, 'use-asc', 'asc', true, false );
            @else
                ProductIndex.setFilterParam( 'use-desc', null, false, 'use-asc', null, false, true );
            @endif
         @else
              ProductIndex.setFilterParam( 'use-desc', null, false, 'use-asc', null, false, true );
            @endif

            @endif


            <?php Session::forget('productFilter');?>

        } );

    </script>
@stop

