@extends("layouts.app")

@section('content-header')
    <div class="row">
        <div class="col-lg-12">
            {!! Breadcrumbs::render('products.create') !!}
            <h1>@lang('app.product_add')</h1>
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div>


                        {!! Form::open([ 'url' => route('products.store'), 'method'=>'POST',  'files' => true,  'id'=>'product-form',  "class"=>"form-horizontal" ]) !!}

                        <div class="row">
                            <div class="col-sm-6">

                                @include('products.partials._form', ['wizard' =>false])

                                <div class="col-lg-offset-4 col-lg-12">
                                    {!! button_save() !!}
                                    {!! button_cancel() !!}
                                </div>
                            </div>
                            <div class="col-sm-6">

                                @include('products.partials._image', ['wizard' =>false, 'product' =>$product ?? null])

                            </div>
                        </div>

                        {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>

    @stop

    @section('js')
    @parent

            <!-- this page specific inline scripts -->
    <script type="text/javascript">
        var dict = {
            type: '@lang('validation.required', ['attribute' => trans_bold('app.product_type')])',
            company: '@lang('validation.required', ['attribute' => trans_bold('app.organization')])',
            nameRu: '@lang('validation.required', ['attribute' => trans_bold('app.name_ru')])',
            descriptionRu: '@lang('validation.required', ['attribute' => trans_bold('app.description_ru')])',
            barcode: '@lang('validation.required', ['attribute' => trans_bold('app.barcode')])',
            nameEn: '@lang('validation.required', ['attribute' => trans_bold('app.name_en')])',
            descriptionEn: '@lang('validation.required', ['attribute' => trans_bold('app.description_en')])',
            selectImage: '@lang('app.select_image')',
            product_save_failed: '@lang('app.product_save_failed')',
            toBigImage: '@lang('app.to_big_image')',
            error: '@lang('app.error')',
            warning: '@lang('app.warning')',
            product_not_found: '@lang('app.product_not_found')',
            productSaved: '@lang('app.product_saved')',
            imageStored: '@lang('app.image_stored')',
            attention: '@lang('app.attention')',
            productWillBeSaved: '@lang('app.product_will_be_saved')',
            continue: '@lang('app.continue')',
            cancel: '@lang('app.cancel')',
            saved: '@lang('app.saved')',
            chose_organization:'@lang('organizations.chose_organization')',
        };

        var config = {
            organization_search_url:'{{ route('organizations.search') }}',
            base_url: '{{ route('products.store') }}'
        };

        let productInstance = Product.init( dict, config );

    </script>
@stop
