@extends("layouts.app")


@section('content-header')
    <div class="row">
        <div class="col-lg-12">
            {!! Breadcrumbs::render('products.create') !!}
            <h1>@lang('app.create_product')</h1>
        </div>
    </div>
@stop


@section('content')
    <div class="row">
        <div class="col-lg-12">


            <div class="panel panel-default">


                <div class="panel-body">
                    <div class="row">

                        <div class="fuelux">
                            <div class="wizard" data-initialize="wizard" id="product-wizard">


                                <div class="steps-container">
                                    <ul class="steps">
                                        <li id='wizard-first-step-li' data-step="1" data-name="campaign" class="active">
                                            <span class="badge">1</span>@lang('app.general_info')
                                            <span class="chevron"></span>
                                        </li>
                                        <li id='wizard-second-step-li'  data-step="2">
                                            <span class="badge">2</span>@lang('app.image')
                                            <span class="chevron"></span>
                                        </li>
                                        <li id='wizard-third-step-li' data-step="3" data-name="template">
                                            <span class="badge">3</span>@lang('app.visual_features')
                                            <span class="chevron"></span>
                                        </li>
                                    </ul>
                                </div>

                                <div class="actions">
                                    <button id="prev-wizard-step-button" type="button"
                                            class="btn btn-default btn-prev ">
                                        <span class="glyphicon glyphicon-arrow-left"></span>@lang('app.prev')
                                    </button>
                                    <button id="next-wizard-step-button" type="button"
                                            class="btn btn-primary btn-next">@lang('app.next')
                                        <span class="glyphicon glyphicon-arrow-right"></span>
                                    </button>
                                    <button id="next-product-button"
                                            type="button" class="btn btn-success btn-next-product"
                                            style="display: none;"
                                            title="@lang('app.next_product_description')">@lang('app.next_product')
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                    <button id="wizard-exit-button"
                                            type="button" class="btn btn-primary btn-wizard-exit"
                                            title="@lang('app.wizard_exit')">
                                        <span class="glyphicon glyphicon-log-out"></span>
                                    </button>
                                </div>

                                <div class="step-content">

                                    <div class="step-pane active " data-step="1">
                                        <h4 class="text-center">@lang('app.general_info_header')</h4>
                                        <hr>

                                        {!! Form::open([
                                        'url' => route('products.store'),
                                        'method'=>'POST',
                                        'files' => true,
                                        'id'=>'product-form',
                                        "class"=>"form-horizontal"
                                        ]) !!}
                                        {!! Form::hidden('wizard', true) !!}

                                        <div class="col-lg-6">

                                            @include('products.partials._form', ['wizard' =>true])

                                            <div class="form-group">
                                                <div class="col-lg-offset-4 col-lg-8">
                                                    {{ Form::button(
                                                    trans('app.next').'  <span class="glyphicon glyphicon-arrow-right"></span> ',
                                                    ['id' => 'first-step-next-button','class'=> ' btn btn-primary'])}}
                                                    {!! button_cancel() !!}
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-lg-3 visible-lg hidden-md hidden-sm hidden-xs">

                                            <div class="form-group">
                                                <div class="col-sm-12 col-lg-12">
                                                    <h5><i class="fa fa-info-circle"
                                                           aria-hidden="true"></i> @lang('app.tooltip')</h5>
                                                    <div class="alert alert-info">@lang('app.instruction')</div>
                                                </div>
                                            </div>

                                        </div>


                                        {!! Form::close() !!}

                                    </div>

                                    <div class="step-pane sample-pane" data-step="2">
                                        <h4 class="text-center"> @lang('app.fulfill_image')</h4>
                                        <hr>

                                        <div class="col-lg-5 hidden-lg visible-md visible-sm visible-xs">
                                            <h5><i class="fa fa-info-circle" aria-hidden="true"></i> @lang('app.tooltip')</h5>
                                            <div class="alert alert-info">@lang('app.instruction')</div>
                                        </div>

                                        <div class="col-lg-7">
                                            <form id="product-image-form" enctype="multipart/form-data">
                                                @include('products.partials._image', ['wizard' =>true])
                                            </form>
                                        </div>

                                        <div class="col-lg-3 visible-lg hidden-md hidden-sm hidden-xs">
                                            <h5><i class="fa fa-info-circle"
                                                   aria-hidden="true"></i> @lang('app.tooltip')</h5>
                                            <div class="alert alert-info">@lang('app.wizard_second_step')</div>
                                        </div>
                                    </div>

                                    <div class="step-pane sample-pane" data-step="3">
                                        @include('products.partials._visual_feature')
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @stop

    @section('js')
    @parent

            <!-- this page specific inline scripts -->
    <script type="text/javascript">

        var dict = {
            type: '@lang('validation.required', ['attribute' => trans_bold('app.product_type')])',
            company: '@lang('validation.required', ['attribute' => trans_bold('app.organization')])',
            nameRu: '@lang('validation.required', ['attribute' => trans_bold('app.name_ru')])',
            legalName: '@lang('validation.required', ['attribute' => trans_bold('app.legal_name')])',
            descriptionRu: '@lang('validation.required', ['attribute' => trans_bold('app.description_ru')])',
            barcode: '@lang('validation.required', ['attribute' => trans_bold('app.barcode')])',
            nameEn: '@lang('validation.required', ['attribute' => trans_bold('app.name_en')])',
            descriptionEn: '@lang('validation.required', ['attribute' => trans_bold('app.description_en')])',
            selectImage: '@lang('app.select_image')',
            selectProductType: '@lang('app.select_type')',
            product_save_failed: '@lang('app.product_save_failed')',
            toBigImage: '@lang('app.to_big_image')',
            error: '@lang('app.error')',
            warning: '@lang('app.warning')',
            product_not_found: '@lang('app.product_not_found')',
            productSaved: '@lang('app.product_saved')',
            imageStored: '@lang('app.image_stored')',
            attention: '@lang('app.attention')',
            productWillBeSaved: '@lang('app.product_will_be_saved')',
            continue: '@lang('app.continue')',
            cancel: '@lang('app.cancel')',
            saved: '@lang('app.saved')',
            chose_organization: '@lang('organizations.chose_organization')',
            fill_background_image: '@lang('app.fill_background_image')',
            barcode_max_length:'@lang('app.barcode_max_length')',

        };

        var config = {
            organization_search_url: '{{route('organizations.search')}}',
            base_url: '{{route('products.store')}}',
            product_types: '{!! $product_types->toJson() !!}'
        };

        let productInstance = Product.init( dict, config );

        ProductWizard.init( productInstance );

    </script>
@stop
