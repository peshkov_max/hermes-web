<div class="col-sm-12">
    <a
            id="product-list-link"
            class="btn btn-primary"
            href="#"
            title="@lang('app.to_list')"><i class="fa fa-chevron-left"></i> @lang('app.to_list')</a>

    @can('manage_product')

    <a
            id="edit-product-link"
            class="btn btn-primary"
            href="{{ route('vue-products.edit', [ "product"=>$product->id]) }}"
            title="@lang('app.edit')"
    ><i class="fa fa-pencil"></i> @lang('app.edit')</a>

    @endcan


    @if(isset($product))

        @if(!isset($forAliases))

            @if(auth()->user()->hasRole('super-admin'))
                <a id="edit-product-aliases-link"
                   target="_blank"
                   href="{{ route('aliases.index', ['aliasable_id'=> $product->id, 'aliasable_name' => $product->getTableName()])}}"
                   class="btn btn-primary"
                   title="@lang('app.edit_aliases')">
                    @lang('app.edit_aliases')

                </a>
            @endif

        @endif

    @endif

    @if(isset($forAliases))
        <a id="create-product-alias"
           data-toggle="modal"
           data-target="#add-alias-form"
           class="btn btn-primary"
        ><i class="fa fa-plus-circle fa-lg"></i>
            @lang('app.aliases_create')
        </a>
    @endif

</div>

@section('js')
    @parent
    <script type="text/javascript">
        $( '#product-list-link' ).on( 'click', function( e ) {
            e.preventDefault( e );
            var url = document.referrer;

            if (~url.indexOf('edit') || ~url.indexOf('create')) {
                location.replace('{{ route('products.index') }}');
                return;
            }

            location.replace(url);
        } )
    </script>
@endsection