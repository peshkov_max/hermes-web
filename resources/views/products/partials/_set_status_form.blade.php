
<div id="modal-set-{!! $field !!}-{!! $product->id !!}" class="modal text-left fade">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::model($product, [ 'route' =>  ['products.update', $product->id],'method' => 'PATCH']) !!}
            {!! Form::hidden('previous_route',\Request::route()->getName()) !!}
            {!! Form::hidden('product_id', $product->id) !!}
            {!! Form::hidden($field, $status) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h1 class="modal-title">@lang('app.set_status')</h1>
            </div>
            <div class="modal-body">
                <p>
                   {{ $message }}
                </p>
            </div>
            <div class="modal-footer">
                <button type="submit" id="confirm-modal-button-{!! $product->id !!}" class="btn btn-primary">@lang('app.yes')</button>
                <button type="button" id="cancel-modal-button-{!! $product->id !!}" class="btn btn-default" data-dismiss="modal">@lang('app.no')</button>
            </div>
            {!! Form::close() !!}
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

