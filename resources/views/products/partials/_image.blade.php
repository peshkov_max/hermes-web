<div class="form-group" style="margin-bottom: 0px; ">
    @if($wizard===true)
        <input type="hidden" name="image_id" id="image-id">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="_method" value="PATCH">
    @endif


    <div class=" @if($wizard===true) col-lg-10 col-md-10 col-sm-10 col-xs-9 @else  col-lg-12 col-md-12 col-sm-12 col-xs-12 @endif ">

        <div class="row">
            <h5><i class="fa fa-cog" aria-hidden="true"></i> @lang('app.image_selected')</h5>

            <div class="img-container" id="image-workSpace"
                 style="width: auto;  height: 500px;">
                <img src="{{ img_static($product->image->original_uri ?? config('wipon.default_image_path'))}}"
                     onerror="this.src='{{ asset(config('wipon.default_image_path')) }}'"
                     alt="@lang('app.image_selected')" style="max-width: 100%; padding-left: 10px; margin-right: 10px"
                     class="cropper-hidden" id="image-rendered">
            </div>

            <span class="help-block" id="product-img-help-block"></span>

        </div>

        <div class="row">
            <div class="col-lg-6 col-sm-12" style="padding-left: 0;">
                @if($wizard === false && isset($product) )
                    <div class="btn-group">
                        <div>
                            <div class="checkbox">
                                {!! Form::checkbox('change_image',  'value', 'true', ['id'=>'change-image', 'class' =>'stiled']) !!}
                                {!! Form::label('change-image', trans('app.change_image'))!!}
                                {!! Form::hidden('change_background_image',  'false', ['id'=>'change-background-image']) !!}
                            </div>
                        </div>
                    </div>
                    <br>
                @endif

                <div class="btn-group" style="margin-top: 5px;">
                    <label class="btn btn-primary btn-upload main-image" for="input-image"
                           title="Upload image file">
                        <input type="file" name="image" id="input-image"
                               class="sr-only" accept="image/*">
                            <span class="docs-tooltip"
                                  title="@lang('app.select_image_button')">
                                <span class="fa fa-upload"></span> @lang('app.select_image_button')</span>
                    </label>
                </div>


                <div class="btn-group" style="margin-top: 5px;">
                    <label class="btn btn-primary btn-upload background-image" for="input-image-background"
                           title="Upload image file">
                        <input type="file" name="image_background" id="input-image-background"
                               class="sr-only" accept="image/*">
                            <span class="docs-tooltip"
                                  title="@lang('app.image_background')">
                                <span class="fa fa-upload"></span> @lang('app.image_background')</span>
                    </label>
                </div>
            </div>
        </div>

        @if($wizard===true)
            <div class="row">
                <div class="col-sm-6"  style="padding-left: 0;">
                    <div class="btn-group" style="margin-top: 5px;">
                        {{ Form::button(trans('app.next').'  <span class="glyphicon glyphicon-arrow-right"></span> ', ['id' => 'second-step-next-button','class'=> ' btn btn-primary'])}}
                    </div>
                </div>
            </div>
        @endif

        <input type="hidden" name="data_x" id="data-x">
        <input type="hidden" name="data_y" id="data-y">
        <input type="hidden" name="data_width" id="data-width">
        <input type="hidden" name="data_height" id="data-height">
    </div>

</div>


