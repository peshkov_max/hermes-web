<div class="row margin-top-10" >
    <div class="col-lg-12">
        <div class="alert alert-info" >
            <p id="no-map">@lang('app.visual_feature_info')</p>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-sm-6 pull-left">

        <h4 class="text-left">@lang('app.authenticity_features')</h4>
        <button class="btn btn-primary pull-left"
                id="add-features"
                data-toggle="modal"
                data-target="#feature-form-modal" style="margin-bottom: 10px;">@lang('app.add')</button>
    </div>


    <div class="col-sm-6 ">
        <h4 class="text-right">@lang('app.authenticity_features_comparison')</h4>
        <button class="btn btn-primary  pull-right"
                id="add-comparison-features"
                data-toggle="modal"
                data-target="#feature-form-modal" style="margin-bottom: 10px;">@lang('app.add')</button>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <table class="table table-bordered table-hover" id="feature-list">
            <thead>
            <tr>
                <th width="3%" class="text-center">@lang('app.order_number')</th>
                <th width="15%">@lang('app.pictures')</th>
                <th width="50%">@lang('app.description')</th>
                <th width="15%" class="text-center">@lang('app.actions')</th>
            </tr>
            </thead>
            <tbody id="feature-table-tbody">

            @if(isset($product))

                <?php $count = $product->visual_features->count(); ?>

                @if($count>0)
                    <?php $visual_features = $product->visual_features()->orderBy('order_number')->get(); ?>
                    @foreach($visual_features as $visual_feature)

                        <tr id="{{$visual_feature->id}}">
                            <td class='priority text-center'>{{$visual_feature->order_number}}</td>
                            <td>
                                <img class="img-thumbnail"
                                     src="{{img_static($visual_feature->image->thumb_uri)}}"
                                     id="feature-list-image-{{$visual_feature->image_id}}"
                                     style=" height: 60px;"
                                     alt="Pic"
                                     onerror="this.src='{{ asset(config('wipon.default_sync_image_path') )}}'"
                                >

                                @if($visual_feature->image_fake!=null)
                                    &nbsp;<img
                                            alt="Feature"
                                            class="img-thumbnail"
                                            src="{{ img_static($visual_feature->image_fake->thumb_uri) }}"
                                            id="feature-list-image-{{ $visual_feature->fake_image_id }}"
                                            style=" height: 60px;"
                                            onerror="this.src='{{ asset(config('wipon.default_sync_image_path') )}}'">
                                @endif
                            </td>
                            <td>{{$visual_feature->description}}</td>
                            <td class='text-center'>
                                <button class='btn btn-edit btn-primary'
                                        onclick="visualFeature.edit(@if($visual_feature->image_fake!=null)'comparative'@else 'not-comparative' @endif, '{{$visual_feature->id}}', true)">@lang('app.edit_feature')</button>
                                <button class='btn btn-delete btn-danger'> @lang('app.delete')</button>
                            </td>
                        </tr>
                    @endforeach
                @endif

            @endif
            </tbody>
        </table>

        <form id="delete-feature-form">
            <input type="hidden" name="_method" value="DELETE">
        </form>

    </div>
</div>

<div class="modal fade" id="feature-form-modal" tabindex="-1" role="dialog" aria-labelledby="modal-label"
     aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button id="close-modal" type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="modal-label">@lang('app.visual_feature')</h4>
            </div>

            <div class="modal-body">
                <input type="hidden" name="update" id="update" value="false">

                <form class="form-horizontal" role="form" id="feature-form" enctype="multipart/form-data">

                    <input type="hidden" name="_method" id="feature-form-method" value="POST">
                    <input type="hidden" name="feature_product_id" id="feature-product-id"
                           value="{{ isset($product)?$product->id : ''}}">
                    <input type="hidden" name="feature_id" id="feature-id" value="">
                    <input type="hidden" name="update_feature" id="update-feature">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-info">
                                @lang('app.select_image_description')
                            </div>
                        </div>

                        <div class="col-sm-6" id="original-image-block">
                            <div class="">
                                <label for="image-original"
                                       class="control-label">@lang('app.visual_feature_original_pic')</label>

                                <div class="img-container" id="original-img-work-space"
                                     style=" height: 400px!important;">
                                    <img
                                            onerror="this.src='{{ asset(config('wipon.default_sync_image_path') )}}'"
                                            src="{{asset(config('wipon.default_image_path'))}}"
                                            alt="Picture"
                                            class="cropper-hidden"
                                            id="feature-original-img-rendered">
                                </div>

                                <span class="help-block" id="feature-original-img-help-block"></span>
                            </div>

                            <div class="btn-group">
                                <div>
                                    <div class="checkbox">
                                        {!! Form::checkbox('change_image_original',  'value', 'true', [
                                        'id'=>'change-image-original',
                                        'class' =>'stiled',

                                        ]) !!}
                                        {!! Form::label('change-image-original', trans('app.change_image'))!!}
                                    </div>
                                </div>
                            </div>
                            <br>

                            <div class="btn-group" style="margin-top: 5px;">
                                <label class="btn btn-primary btn-upload " for="feature-original-img-input"
                                       title="@lang('app.image_selector')">
                                        <span class="docs-tooltip" data-toggle="tooltip"
                                              title="@lang('app.image_selector')">
                                            <span class="fa fa-upload"></span> @lang('app.image_selector')</span>
                                    <input type="file" class="sr-only" id="feature-original-img-input"
                                           name="feature_image_original" accept="image/*">
                                </label>
                            </div>


                            <input type="hidden" id="feature-original-img-data-x" name="feature_original_data_x">
                            <input type="hidden" id="feature-original-img-data-y" name="feature_original_data_y">
                            <input type="hidden" id="feature-original-img-data-width"
                                   name="feature_original_data_width">
                            <input type="hidden" id="feature-original-img-data-height"
                                   name="feature_original_data_height">
                        </div>


                        <div class="col-sm-6" id="fake-image-block">

                            <div class="">
                                <label for="image-fake"
                                       class="control-label">@lang('app.visual_feature_fake_pic')</label>


                                <div class="img-container" id="fake-img-work-space" style="height: 400px!important;">
                                    <img
                                            src="{{asset(config('wipon.default_image_path'))}}"
                                            alt="Picture"
                                            onerror="this.src='{{ asset(config('wipon.default_sync_image_path') )}}'"
                                            class="cropper-hidden"
                                            id="feature-fake-img-rendered"
                                    >
                                </div>

                                <span class="help-block" id="feature-fake-img-help-block"></span>
                            </div>


                            <div class="btn-group">
                                <div>
                                    <div class="checkbox">
                                        {!! Form::checkbox('change_image_fake',  'value', 'true', ['id'=>'change-image-fake', 'class' =>'stiled']) !!}
                                        {!! Form::label('change-image-fake', trans('app.change_image'))!!}
                                    </div>
                                </div>
                            </div>
                            <br>


                            <div class="btn-group" style="margin-top: 5px;">
                                <label class="btn btn-primary btn-upload btn-upload-image-fake"
                                       for="feature-fake-img-input"
                                       title="@lang('app.image_selector')">
                                        <span class="docs-tooltip" data-toggle="tooltip"
                                              title="@lang('app.image_selector')">
                                            <span class="fa fa-upload"></span> @lang('app.image_selector')
                                        </span>
                                    <input type="file" class="sr-only" id="feature-fake-img-input"
                                           name="feature_image_fake"
                                           accept="image/*">
                                </label>
                            </div>


                            <input type="hidden" id="feature-fake-img-data-x" name="feature_fake_data_x">
                            <input type="hidden" id="feature-fake-img-data-y" name="feature_fake_data_y">
                            <input type="hidden" id="feature-fake-img-data-width" name="feature_fake_data_width">
                            <input type="hidden" id="feature-fake-img-data-height" name="feature_fake_data_height">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 ">
                            <div>
                                <label for="feature-description-ru"
                                       class="control-label">@lang('app.description_ru')</label>
                                <textarea rows="3" class="form-control" id="feature-description-ru" maxlength="200"
                                          name="description_ru" cols="50"></textarea>
                                <span class="help-block" id="feature-desc-ru-helper-block"></span>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div>
                                <label for="feature-description-en"
                                       class="control-label">@lang('app.description_en')</label>
                                <textarea rows="3" class="form-control" id="feature-description-en" maxlength="200"
                                          name="description_en" cols="50"></textarea>
                                <span class="help-block" id="feature-desc-en-helper-block"></span>
                            </div>
                        </div>
                    </div>

                </form>

                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-primary"
                            id="save-feature"
                    >@lang('app.save')</button>

                    <button type="button"
                            class="btn btn-success"
                            id="cancel-adding-feature"
                    >@lang('app.close_modal')</button>
                </div>

            </div>
        </div>
    </div>
</div>


@section('js')
    @parent

    <script type="text/javascript">

        var dict = {
            selectImage: '@lang('app.select_image')',
            toBigImage: '@lang('app.to_big_image')',
            descriptionRuRequired: '@lang('app.feature_description_ru_required')',
            descriptionEnRequired: '@lang('app.feature_description_en_required')',
            imageRequired: '@lang('app.image_required')',
            edit: '@lang('app.edit_feature')',
            deleteFeature: '@lang('app.delete')',
            attention: '@lang('app.attention')',
            itemWillBeRemoved: '@lang('app.item_will_be_removed')',
            continue: '@lang('app.continue')',
            cancel: '@lang('app.cancel')',
            success: '@lang('app.success')',
            error: '@lang('app.error')',
            featureRemoved: '@lang('app.feature_removed')',
            feature_limit_reached: '@lang('app.feature_limit_reached')'
        };

        var config = {
            base_url: '{{ route('visual-features.store') }}',
            app_path: '{{ URL::to('/').'/' }}',
            static_path: '{{ config('wipon.static_server') .'/images/'}}',
            default_image: '{{ asset(config('wipon.default_image_path')) }}',
            default_image_loading: '{{ asset(config('wipon.default_sync_image_path')) }}'
        };

        var visualFeature = VisualFeature.init( dict, config );
    </script>
@endsection
