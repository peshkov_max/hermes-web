{!! Form::hidden('product_id', $product->id ?? null, ['id'=> 'product-id'])!!}
{!! Form::hidden('_method', 'POST', ['id'=> 'product-form-method'])!!}

@if(isset($previous_route))
    {!! Form::hidden('previous_route', $previous_route) !!}
@endif

@if (get_i('previous_route') =='aliases.edit')
    {!! Form::hidden('go_to', URL::previous()) !!}
@endif

<div class="form-group ">
    <div class="col-lg-offset-4 col-lg-7" id="product-info-block">
        <h5>@lang('app.product_info')</h5>
    </div>
</div>

@can('moderate_product')

<div class="form-group required ">
    {!! Form::label('organization_id', trans('app.organization'), ["class"=> "col-lg-4 control-label"]) !!}
    <div class="col-lg-7">
        {!! Form::select('organization_id', $organizations ?? [], $product->organization_id ?? null, ["class"=> "form-control", "id"=>"product-organization",  ]) !!}
        <span class="help-block"
              id="product-company-helper-block"></span>
    </div>
</div>

@if($wizard!==true)
    <div class="form-group">
        <div class="col-lg-offset-4 col-lg-7">
            <div class="checkbox">
                {!! Form::checkbox('is_moderated',  'value', $product->is_moderated ?? 'true' , ['id'=>'is-moderated', 'class' =>'stiled']) !!}
                {!! Form::label('is-moderated', trans('app.set_is_moderated'))!!}
            </div>
        </div>
    </div>

    {{--  FROZEN FUNCTIONALITY    --}}
    {{--<div class="form-group">--}}
    {{--<div class="col-lg-offset-4 col-lg-7">--}}
    {{--<div class="checkbox">--}}
    {{--{!! Form::checkbox('is_enabled',  'value',  $product->is_enabled ?? 'true' , ['id'=>'is-enabled', 'class' =>'stiled']) !!}--}}
    {{--{!! Form::label('is-enabled', trans('app.shown'))!!}--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
@endif

@endif

@if(isset($product))
    <div class="fuelux">


        @endif
        <div class="form-group required ">
            {!!Form::label('product_type_id', trans('app.product_type'), ["class"=> "col-lg-4 control-label"])!!}
            <div class="col-lg-7">
                {!! Form::hidden('product_type_id', $product->product_type_id ?? null) !!}

                <ul class="tree tree-folder-select" role="tree" id="productTypeTree">
                    <li class="tree-branch hide" data-template="treebranch" role="treeitem" aria-expanded="false">
                        <div class="tree-branch-header">
                            <button type="button" class="glyphicon icon-caret glyphicon-play"><span
                                        class="sr-only">Open</span>
                            </button>
                            <button type="button" class="tree-branch-name">
                                <span class="glyphicon icon-folder glyphicon-folder-close"></span>
                                <span class="tree-label"></span>
                            </button>
                        </div>
                        <ul class="tree-branch-children" role="group"></ul>
                        <div class="tree-loader" role="alert">Loading...</div>
                    </li>
                    <li class="tree-item hide" data-template="treeitem" role="treeitem">
                        <button type="button" class="tree-item-name">
                            <span class="glyphicon icon-item"></span>
                            <span class="tree-label"></span>
                        </button>
                    </li>
                </ul>



        <span class="help-block"
              id="product-type-helper-block"></span>
            </div>
        </div>
        @if(isset($product))

    </div>
@endif
<div class="form-group required ">
    {!! Form::label('name_en',trans('app.product_en'), ["class"=> "col-lg-4
    control-label"]) !!}
    <div class="col-lg-7">
        {!!Form::text('name_en', $product->name_en ?? null, ["class"=> "form-control", "id"=>"name-en", "size"=>"255", 'placeholder' => trans('app.product_name_placeholder_en')]) !!}
        <span class="help-block"
              id="name-en-helper-block"></span>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('name',trans('app.product_ru'), ["class"=> "col-lg-4
    control-label"]) !!}
    <div class="col-lg-7">
        {!!Form::text('name_ru', $product->name_ru ?? null, ["class"=> "form-control", "id"=>"name-ru", "size"=>"255", 'placeholder' => trans('app.product_name_placeholder_ru')]) !!}
        <span class="help-block"
              id="name-ru-helper-block"></span>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('legal_name',trans('app.legal_name'), ["class"=> "col-lg-4
    control-label"]) !!}
    <div class="col-lg-7">
        {!!Form::text('legal_name', $product->legal_name ?? null, ["class"=> "form-control", "id"=>"legal-name", "size"=>"255",  "placeholder" => trans('app.legal_name_example')]) !!}
        <span class="help-block"
              id="name-ru-helper-block"></span>
    </div>
</div>


<div class="form-group ">
    {!! Form::label('description_en', trans('app.product_description_en'), ["class"=> "col-lg-4
    control-label"]) !!}
    <div class="col-lg-7">
        {!!Form::textarea('description_en', $product->common_info['description_en'] ?? null, ["rows" => "3" ,"class"=> "form-control", "id"=>"description-en" ,'maxlength' =>"200"]) !!}
        <span class="help-block"
              id="description-en-helper-block"></span>
    </div>
</div>

<div class="form-group ">
    {!! Form::label('description_ru',trans('app.product_description_ru'), ["class"=> "col-lg-4
    control-label"]) !!}
    <div class="col-lg-7">
        {!!Form::textarea('description_ru', $product->common_info['description_ru'] ?? null, ["rows" =>"3","class"=> "form-control", "id"=>"description-ru",'maxlength' =>"200"]) !!}
        <span class="help-block"
              id="legal-name-helper-block"></span>
    </div>
</div>

<div class="form-group required">
    {!! Form::label('barcode',trans('app.barcode'), ["class"=> "col-lg-4
    control-label"]) !!}
    <div class="col-lg-7">
        {!! Form::text('barcode',  isset($product) ? $product->barcode: null ,["class"=>
        "form-control",  "maxlength"=>"13", "id"=>"barcode",]) !!}
        <span class="help-block" id="barcode-helper-block"></span>

    </div>
</div>

@if(isset($product))

    @can('moderate_product')
    <div class="form-group">
        <div class="col-lg-offset-4 col-sm-offset-0 col-lg-7  col-sm-7">
            <a
                    id="store-product-button"
                    target="_blank"
                    href="{{ route('aliases.index', ['aliasable_id'=> $product->id, 'aliasable_name' => $product->getTableName(), 'redirect_url' => \Request::fullUrl(), 'parent_breadcrumbs'=> 'app.update'])}}"
                    class="btn btn-primary btn-sm"
                    title="@lang('app.edit_aliases')">
                @lang('app.edit_aliases')
            </a>

        </div>
    </div>
    @endcan
@endif




