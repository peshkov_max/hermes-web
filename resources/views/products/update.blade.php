@extends("layouts.app")


@section('content-header')
    <div class="row">
        <div class="col-lg-12">
            {!! Breadcrumbs::render('products.update', $product) !!}
            <h1>{{$product->name}}</h1>
        </div>
    </div>
@stop


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-body">


                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" @if(!isset($visual_features)) class="active" @endif>
                            <a id="product-general-info-tab" href="#general" aria-controls="general" role="tab"
                               data-toggle="tab">@lang('app.general_info')</a>
                        </li>
                        <li role="presentation" @if(isset($visual_features)) class="active" @endif>
                            <a id="product-visual-features-tab" href="#visual_features" aria-controls="visual_features" role="tab"
                               data-toggle="tab">@lang('app.visual_features_show_form')</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane  @if(!isset($visual_features)) active @endif "
                             id="general">

                            {!! Form::model($product, [ 'route' =>  ['products.update', $product->id],
                                     'class'=>'form-horizontal',
                                     'method' => 'PATCH',
                                     'files' => true,
                                     'id'=>'product-form',
                                 ]) !!}


                            <div class="row">
                                <div class="col-sm-6">

                                    @include('products.partials._form', ['wizard' =>false])
                                    <div class="form-group">
                                        <div class="col-lg-offset-4 col-lg-12">
                                            {!! button_cancel() !!}
                                            {!! button_save() !!}
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    @include('products.partials._image', ['wizard' =>false, 'product' =>$product ?? null])
                                </div>
                            </div>

                            {!! Form::close() !!}

                        </div>

                        <div role="tabpanel" class="tab-pane  @if(isset($visual_features)) active @endif"
                             id="visual_features">
                            @include("products.partials._visual_feature", ['product' =>$product ])
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    @stop


    @section('js')
    @parent
            <!-- this page specific inline scripts -->
    <script type="text/javascript">
        var dict = {

            type: '@lang('validation.required', ['attribute' => trans_bold('app.product_type')])',
            company: '@lang('validation.required', ['attribute' => trans_bold('app.organization')])',
            nameRu: '@lang('validation.required', ['attribute' => trans_bold('app.name_ru')])',
            legalName: '@lang('validation.required', ['attribute' => trans_bold('app.legal_name')])',
            descriptionRu: '@lang('validation.required', ['attribute' => trans_bold('app.description_ru')])',
            barcode: '@lang('validation.required', ['attribute' => trans_bold('app.barcode')])',
            nameEn: '@lang('validation.required', ['attribute' => trans_bold('app.name_en')])',
            descriptionEn: '@lang('validation.required', ['attribute' => trans_bold('app.description_en')])',
            selectImage: '@lang('app.select_image')',
            product_save_failed: '@lang('app.product_save_failed')',
            toBigImage: '@lang('app.to_big_image')',
            error: '@lang('app.error')',
            warning: '@lang('app.warning')',
            product_not_found: '@lang('app.product_not_found')',
            productSaved: '@lang('app.product_saved')',
            imageStored: '@lang('app.image_stored')',
            attention: '@lang('app.attention')',
            productWillBeSaved: '@lang('app.product_will_be_saved')',
            continue: '@lang('app.continue')',
            cancel: '@lang('app.cancel')',
            saved: '@lang('app.saved')',
            chose_organization:'@lang('organizations.chose_organization')',
            fill_background_image:'@lang('app.fill_background_image')',
            barcode_max_length:'@lang('app.barcode_max_length')',
        };

        var config = {
            organization_search_url:'{{route('organizations.search')}}',
            base_url: '{{route('products.store')}}',
            product_types: '{!! $product_types->toJson() !!}',
            product: '{!! $product->load('organization')->toJson() !!}',
        };

        let productInstance = Product.init( dict, config );

        let  $productImage = $("#loaded-image" );
        $productImage.attr('src', $productImage.attr('src') + '?' + new Date().getTime());

    </script>
@stop
