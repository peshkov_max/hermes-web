@extends("layouts.app")

@section('content-header')
    <div class="row">
        <div class="col-lg-12">
            {!! Breadcrumbs::render('products.show', $product) !!}
            <h1>@lang('app.view_product')</h1>

            @can('own_product')
            @include('partials._delete_form', ['routeName'=> route('products.destroy', ["product"=>$product->id]),'data' => $product, 'item_name'=>$product->name])
            @endcan
        </div>
    </div>
@stop

@section('content')
    <div class='row'>
        <div class='col-lg-12'>

            <div class="main-box clearfix">
                <header class="main-box-header clearfix">
                    @include('products.partials._show_header', ['breadcrumbs' =>'products.show'])
                </header>

                <div class="main-box-body clearfix">

                    <div>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" @if(!isset($visual_features)) class="active" @endif>
                                <a id="tab-product-general-info" href="#general" aria-controls="general" role="tab"
                                   data-toggle="tab">@lang('app.general_info')</a>
                            </li>
                            <li role="presentation" @if(isset($visual_features)) class="active" @endif>
                                <a id="tab-product-visual-features" href="#visual_features"
                                   aria-controls="visual_features" role="tab"
                                   data-toggle="tab">@lang('app.visual_features_show_form')</a>
                            </li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane  @if(!isset($visual_features)) active @endif " id="general">

                                <table class="table table-striped table-bordered">
                                    <tbody>
                                    <input type="hidden" id="product-id" value="{{$product->id}}">
                                    <tr>
                                        <td width="300" align="right"
                                            style="font-weight:bold">@lang('app.product_name')</td>
                                        <td id="product-name">{{$product->name}}</td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="right"
                                            style="font-weight:bold">@lang('app.legal_name')</td>
                                        <td id="product-legal-name">{{$product->legal_name}}</td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="right"
                                            style="font-weight:bold">@lang('app.barcode')</td>
                                        <td id="product-barcode">{{$product->barcode}}</td>
                                    </tr>


                                    @if($product->description)
                                        <tr>
                                            <td width="300" align="right"
                                                style="font-weight:bold">@lang('app.description')</td>
                                            <td id="product-description-{{$product->id}}">{{$product->description}}</td>
                                        </tr>
                                    @endif

                                    @if($product->image)
                                        <tr>
                                            <td width="300" align="right"
                                                style="font-weight:bold">@lang('app.image')</td>
                                            <td><img src="{{img_static($product->image->thumb_uri)}}"
                                                     onerror="this.src='{{ asset(config('wipon.default_image_path')) }}'"
                                                     style="height: 100px;"
                                                     alt="@lang('app.image')"
                                                     class="img-rounded img-responsive"
                                                     id="product-image"
                                                ></td>
                                        </tr>
                                    @endif

                                    @if($product->background_image)
                                        <tr>
                                            <td width="300" align="right"
                                                style="font-weight:bold">@lang('app.image_background')</td>
                                            <td><img src="{{img_static($product->background_image->thumb_uri )}}"
                                                     alt="@lang('app.image_background')"
                                                     onerror="this.src='{{ asset(config('wipon.default_image_path')) }}'"
                                                     style="height: 100px;"
                                                     class="img-rounded img-responsive"
                                                     id="product-image-background"

                                                ></td>
                                        </tr>
                                    @endif


                                    @can('moderate_product')
                                    {{--  FROZEN FUNCTIONALITY    --}}
                                    {{--<tr>--}}
                                    {{--<td width="300" align="right"--}}
                                    {{--style="font-weight:bold">@lang('app.is_enabled')</td>--}}
                                    {{--<td>{!!  $product->isEnabled() !!}--}}

                                    {{--@if($product->is_enabled===true)--}}
                                    {{--<a class="btn btn-primary  btn-sm" href="#modal-set-is_enabled-{!! $product->id !!}" data-toggle="modal" title="@lang('app.hide')">@lang('app.hide')?</a>--}}
                                    {{--@include('products.partials._set_status_form', ['product'=> $product, 'field' =>  'is_enabled', 'status' => 'false', 'message' => trans('app.hide'). '?'])--}}
                                    {{--@else--}}
                                    {{--<a class="btn btn-primary btn-sm" href="#modal-set-is_enabled-{!! $product->id !!}" data-toggle="modal" title="@lang('app.show')">@lang('app.show')?</a>--}}
                                    {{--@include('products.partials._set_status_form', ['product'=> $product, 'field' =>  'is_enabled', 'status' => 'true', 'message' => trans('app.show'). '?'])--}}
                                    {{--@endif--}}
                                    {{--</td>--}}
                                    {{--</tr>--}}
                                    <tr>
                                        <td width="300" align="right"
                                            style="font-weight:bold">@lang('app.is_moderated')</td>
                                        <td>


                                            {!! $product->isModerated() !!}
                                            @if($product->is_moderated===true)
                                                <a id="open-modal-set-moderation-{!! $product->id !!}"
                                                   class="btn btn-primary btn-sm"
                                                   href="#modal-set-is_moderated-{!! $product->id !!}"
                                                   data-toggle="modal"
                                                   title="@lang('app.set_not_moderated')"
                                                >@lang('app.set_not_moderated')</a>

                                                @include('products.partials._set_status_form', ['product'=> $product, 'field' =>  'is_moderated', 'status' => 'false', 'message' => trans('app.set_not_moderated'). '?'])
                                            @else
                                                <a id="open-modal-set-moderation-{!! $product->id !!}"
                                                   class="btn btn-primary btn-sm"
                                                   href="#modal-set-is_moderated-{!! $product->id !!}"
                                                   data-toggle="modal"
                                                   title="@lang('app.set_is_moderated')"
                                                >@lang('app.set_is_moderated')</a>

                                                @include('products.partials._set_status_form', ['product'=> $product, 'field' =>  'is_moderated', 'status' => 'true', 'message' => trans('app.set_is_moderated'). '?'])
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="300" align="right"
                                            style="font-weight:bold">@lang('app.organization')</td>
                                        <td>
                                            @if($product->organization)
                                                {{ link_to_route(
                                                'organizations.manage_users',
                                                $product->organization->name ,
                                                ['organizations'=>$product->organization->id],
                                                ['id'=>'open-product-organization-link']
                                                )}}
                                            @else
                                                {{trans('app.no_info')}}
                                            @endif
                                        </td>
                                    </tr>
                                    @endcan


                                    </tbody>
                                </table>


                            </div>

                            <div role="tabpanel" class="tab-pane  @if(isset($visual_features)) active @endif" id="visual_features">
                                @include("products.partials._visual_feature")
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    @parent

    <script type="javascript">

        let $productImage = $( "#product-image" );
        $productImage.attr( 'src', $productImage.attr( 'src' ) + '?' + new Date().getTime() );

        let $productBackgroundImage = $( "#product-image-background" );
        $productBackgroundImage.attr( 'src', $productBackgroundImage.attr( 'src' ) + '?' + new Date().getTime() );

    </script>

@stop
