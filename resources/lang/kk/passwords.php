<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Құпия сөз кемінде 6 символдан құрылуы тиіс.",
	"user" => "Мұндай e-mail адресі бар пайдаланушы жоқ.",
	"token" => "Токен сәйкес келмейді, құпия сөзді қалпына келтіру мүмкін емес.",
	"sent" => "Құпия сөзді қалпына келтіру үшін, сіздің поштаңызға сілтеме жіберілді!",
	"reset" => "Сіздің құпия сөзіңіз өзгертілді!",
    "email_template"=>"Құпия сөзді қалпына келтіру үшін, сілтеме бойынша өтіңіз: ",
];
