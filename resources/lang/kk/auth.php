<?php

return array(
    'remember_me' => 'Мені есте сақтау керек па?',
    'forget_password' => 'Құпия сөзіңізді ұмыттыңыз ба?',
    'login' => 'Кіру',
    'wrong_email'=>'Сіз қате Email-ді тердіңіз',
    'put_email'=>'Өзіңіздің Email-ді теріңіз',
    'remind_password'=>'Құпия сөзді қалпына келтіру',
    'wrong_password'=>'Сіз ескі құпиясөзді қате тердіңіз'
);