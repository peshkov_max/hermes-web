<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    
    
    'login_or_email' =>'Login or Email',
    'password' => 'Password',
    'password_repeat' => 'Password repeat',
    
    'failed' => 'These credentials do not match our records.',
    'failed_no_user' => 'Login or password is not correct.',
    'failed_creditionals'=> 'Login or password is not correct',
    
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'remember_me' => 'Remember me?',
    'forget_password' => 'Forgot password?',
    'login' => 'Log in',
    'wrong_email'=>'You have entered wrong email.',
    'put_email'=>'Enter your email',
    'remind_password'=>'Remind password',
    'wrong_password'=>'Wrong old password'
];
