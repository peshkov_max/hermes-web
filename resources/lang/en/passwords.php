<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'We have e-mailed your password reset link!',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",

    "input_failed" => "Вы неверно заполнили поля ввода.",
    "do_reset" => "Изменить пароль",
    "email" => "E-Mail",

    "password_new" =>"Новый Пароль",
    "password_repeat"=>"Повторите пароль",
    
    "click_to_reset" => "Click here to reset your password: ",

];
