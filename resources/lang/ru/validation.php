<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    "accepted"    => "The <b>:attribute</b> must be accepted.",
    "active_url"  => "The <b>:attribute</b> is not a valid URL.",
    "after"       => "The <b>:attribute</b> must be a date after :date.",
    "alpha"       => "The <b>:attribute</b> may only contain letters.",
    "alpha_dash"  => "The <b>:attribute</b> may only contain letters, numbers, and dashes.",
    "alpha_num"   => "The <b>:attribute</b> may only contain letters and numbers.",
    "array"       => "The <b>:attribute</b> must be an array.",
    "before"      => "The <b>:attribute</b> must be a date before :date.",
    "between"     => [
        "numeric" => "The <b>:attribute</b> must be between :min and :max.",
        "file"    => "The <b>:attribute</b> must be between :min and :max kilobytes.",
        "string"  => "The <b>:attribute</b> must be between :min and :max characters.",
        "array"   => "The <b>:attribute</b> must have between :min and :max items.",
    ],
    "boolean"     => "The <b>:attribute</b> field must be true or false.",
    "confirmed"   => "Введенные пароли на совпадают",
    "date"        => "Поле <b>:attribute</b> заполнено неверно. Формат даты должен быть: '0000-00-00 00:00:00'. Либо вы можете оставить поле пустым, в случае отсутствия даты",
    "date_format" => "The <b>:attribute</b> does not match the format :format.",
    "different"   => "Поле <b>:attribute</b> и :other должны отличаться.",
    "digits"      => "Поле <b>:attribute</b> должно быть :digits числом",

    "digits_between"   => "Поле <b>:attribute</b> должно быть от :min до :max",
    "email"            => "Поле <b>:attribute</b> должно быть содержать электронный адрес верного формата",
    "filled"           => "The <b>:attribute</b> field is required.",
    "exists"           => "Записи с таким <b>:attribute</b> не существует в базе данных.",
    "image"            => "Поле <b>:attribute</b> должно быть картинкой.",
    "in"               => "The selected <b>:attribute</b> is invalid.",
    "integer"          => "Поле <b>:attribute</b> должно быть числом",
    "ip"               => "<b>:attribute</b> .",
    "max"              => [
        "numeric" => "Поле <b>:attribute</b> не должно превышать :max символов",
        "file"    => "The <b>:attribute</b> may not be greater than :max kilobytes.",
        "string"  => "Поле <b>:attribute</b> не должно превышать :max символов",
        "array"   => "The <b>:attribute</b> may not have more than :max items.",
    ],
    "mimes"            => "Файл <b>:attribute</b> должно иметь один из следйющих типов: :values.",
    "min"              => [
        "numeric" => "The <b>:attribute</b> must be at least :min.",
        "file"    => "The <b>:attribute</b> must be at least :min kilobytes.",
        "string"  => "В поле <b>:attribute</b> должно быть как минимум :min символов.",
        "array"   => "The <b>:attribute</b> must have at least :min items.",
    ],
    "not_in"           => "The selected <b>:attribute</b> is invalid.",
    "numeric"          => "В поле <b>:attribute</b> должны быть числа.",
    "regex"            => "Поле <b>:attribute</b> должно быть в формате :format.",
    "regex_string"     => "Поле <b>:attribute</b> должно быть строкой.",
    "regex_latin"      => "Поле <b>:attribute</b> должно содержать символы латинского алфавита. Недопускается ввод цифр.",
    "regex_not_number" => "Поле <b>:attribute</b> не должно содержать цифры.",

    "required"                => "Поле <b>:attribute</b> обязательно для заполнения.",
    "required_if"             => "The <b>:attribute</b> field is required when :other is :value.",
    "required_with"           => "The <b>:attribute</b> field is required when :values is present.",
    "required_with_all"       => "The <b>:attribute</b> field is required when :values is present.",
    "required_without"        => "The <b>:attribute</b> field is required when :values is not present.",
    "required_without_all"    => "The <b>:attribute</b> field is required when none of :values are present.",
    "same"                    => "The <b>:attribute</b> and :other must match.",
    "size"                    => [
        "numeric" => "The <b>:attribute</b> must be :size.",
        "file"    => "The <b>:attribute</b> must be :size kilobytes.",
        "string"  => "The <b>:attribute</b> must be :size characters.",
        "array"   => "The <b>:attribute</b> must contain :size items.",
    ],
    "unique"                  => "Значение поля <b>:attribute</b> уже занято. Оно должно быть уникальным!",
    "url"                     => "The <b>:attribute</b> format is invalid.",
    "timezone"                => "The <b>:attribute</b> must be a valid zone.",

    /*
      |--------------------------------------------------------------------------
      | Wipon validation messages
      |--------------------------------------------------------------------------
    */
    "credentials"             => "Введенные вами Email и пароль не совпадают!",
    "user_does_not_exist"     => "Пользователя с логином :email не существует!",
    "point_must_not_be_mull"  => 'Вы забыли указать кластер на карте',
    "city_id_cluster_form"    => "Ошибка, город для выбранных кластеров не был определён!",
    "role_error"              => "Вы не можете назначить несуществующую роль пользователю. Вы ввели: :value",
    "invalid_uuid"            => "Устройства с таким UUID не существует",
    "invalid_uuid_with_value" => "Устройства с UUID: <strong>:uuid</strong> не существует",
    "push_token_absent"       => "У устройства отсутствует <b>push_token</b>",
    "regex_latin_user"        => 'Поле <b>:attribute</b> может содержать только латиницу, ".", "-". Не может содержать пробелов.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
//        'attribute-name' => [
//            'rule-name' => 'custom-message',
//        ],

        'city_id' => [
            'not_in' => "Невозможно объединить город с самим собой",
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'password'            => "Пароль",
        'password_old'        => 'Старый пароль',
        'email'               => "Email",
        'phone_number'        => "Телефон",
        'topic'               => "Тема",
        'description'         => "Описание",
        'cto'                 => "Главный технолог",
        'feature_description' => "Описание",
        'image'               => "Картинка",
        'answer'              => "Ваш ответ",
        'barcode'             => "Штрих код",
        'manufacturer'        => "Производитель",
        'name'                => "Имя",
        'role'                => "Роль",
        'query'               => ' поиск ',
        'title'               => "Описание",
        'latitude'            => "Широта",
        'longitude'           => "Долгота",
        'dev_name'            => "Внутреннее название",

        'accuracy'         => "Внутренний радиус",
        'virtual_accuracy' => "Внешний радиус",

        'name_ru'                 => 'Наименование (RU)',
        'name_kk'                 => 'Наименование (KK)',
        'name_en'                 => 'Наименование (EN)',
        'country_id'              => 'Страна',
        'region_id'               => 'Область',
        'city_id'                 => 'Город',
        'regional_center_city_id' => 'Областной центр',
        'display_name'            => 'Отображаемое название роли',
        'city_secondary'          => 'Город',
        'country_secondary'       => 'Страна',
        'region_secondary'        => 'Область',
        'username'                => "Имя пользователя",
        'secondary_city_id'       => 'Второстепенный город',
        'continent_id'            => 'Континент',
        'cluster_ids_array'       => 'Массив ID кластеров',
        'cluster_ids_json'        => 'JSON',
        'login'                   => "Логином или Email",
        "description_ru"          => "Описание по-русски",
        'product_ru'              => 'Наименование по-русски',
        "product_type_id"         => "Тип продукта",
        'contract_number'         => "Номер контракта",
        'password_confirmation'   => "Подтверждение пароля",
        "text"                    => "Текст сообщения",
        'bin'                     => 'БИН/ИИН',
        'sum'                     => 'Итоговая сумма',
        'legal_name'              => 'Юридическое название',
        'cluster_type_id'         => 'Тип кластера',
        'organization'            => 'Организация',
        'roles'                   => 'Роль',
        'uuid'                    => 'UUID',
        'work_phone_number'       => 'Рабочий телефон',
        'address'                 => 'Адрес',
        'licence'                 => 'Лицензия',
        'region'                  => 'Регион',
        'type'                    => 'Тип',
        'house'                   => 'Дом',
        'street'                  => 'Улица',
        'user_id'                 => 'ID пользователя',
    ],

    'feedback'             => [
        'password' => "Пароль",
        'email'    => "Email",
    ],
    'cluster_ids_json'     => 'При удалении кластеров, необходимо передавать ID кластеров в формате JSON',
    'cluster_ids_array'    => 'При удалении кластеров, необходимо передавать ID кластеров в формате JSON, в массиве',
    'cluster_ids_each'     => 'Один из выбранных кластеров не существует в базе данных',
    'ids_must_be_integers' => 'ID кластеров должны содержать только цифты',
    'password_old'         => 'Вы неверно указали старый пароль!',
    'role_must_be_unique'  => 'Значение поля <strong>Роль</strong> уже занято. Оно должно быть уникальным!!',

];
