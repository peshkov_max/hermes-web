<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'login_or_email' =>'Логин или Email',
    'password' => 'Пароль',
    'password_repeat' => 'Повторить ввод пароля',
    
    'failed' => 'Вы некорректно заполнили поля авторизации.',
    'failed_no_user' => 'Пользователя с таким Логином/Email не существует',
    'failed_creditionals'=> 'Некоректный логин или пароль',
    
    'throttle' => 'Слишком много попыток войти в систему. Пожалуйста попробуйте через :seconds секунд.',
    
    'remember_me' => 'Запомнить меня?',
    'forget_password' => 'Забыли пароль?',
    'login' => 'Войти',
    'wrong_email'=>'Вы ввели неправильный Email',
    'put_email'=>'Введите ваш Email',
    'remind_password'=>'Восстановить пароль',
    'wrong_password'=>'Вы неправильно ввели старый пароль',
];
