<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password"        => "Пароль должен состоять как миниму из 6 символов.",
    "user"            => "Пользователя с таким e-mail адресом не существует!",
    "token"           => "Токен для восстановления пароля неверен.",
    "sent"            => "Ссылка для восстановления пароля была отправлена вам на почту!",
    "reset"           => "Ваш пароль был изменен!",
    "email_template"  => "Перейдите по ссылке для восстановления пароля: ",
    "input_failed"    => "Вы неверно заполнили поля ввода.",
    "do_reset"        => "Изменить пароль",
    "email"           => "E-Mail",
    "password_new"    => "Новый Пароль",
    "password_repeat" => "Повторите пароль",
    "click_to_reset"  => "Для изменения пароля перейдите по ссылке:",

];
